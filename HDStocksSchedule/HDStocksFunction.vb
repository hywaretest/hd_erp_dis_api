﻿Imports System.Data.SqlClient

Public Class HDStocksFunction
    Private pf As New DBFuntion

    Public Sub New()

    End Sub
    '2019/08/26
    Public Function D_STOCKS_SETTLEMENT(COUNT_DATE As DateTime, COUNT_TYPE As Integer, COUNT_RESULT As Integer) As Boolean
        Dim sSql As String = ""
        Try
            '2019/08/26寫入庫存結算紀錄檔
            Dim Type_Str As String = ""
            If COUNT_TYPE = "2" Then
                Type_Str = "2.手動結算"
            Else
                Type_Str = "1.系統結算"
            End If
            Dim Result_Str As String = ""
            If COUNT_RESULT = "2" Then
                Result_Str = "2.結算失敗"
            Else
                Result_Str = "1.結算完成"
            End If
            Dim table3 As String = ""
            table3 = "D_STOCKS_SETTLEMENT"
            Dim pk3() As String = {"COUNT_DATE"}
            Dim col_value3(,) As String = {
                {"COUNT_DATE", COUNT_DATE.ToString("yyyy/MM/dd")},
                {"COUNT_TYPE", Type_Str},
                {"COUNT_RESULT", Result_Str},
                {"CREATE_USER", "系統"},
                {"CREATE_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
        }
            sSql = pf.GetInsertquery(table3, col_value3)
            Console.WriteLine(sSql)
            pf.ExecSQL(sSql)
        Catch ex As Exception
            pf.MyError(ex.ToString)
        End Try
        Return True
    End Function

    '檢查公司庫存每日檔
    '2019/04/16 新增SaleQty可銷售數量
    'Good_Qty依照生效日 SaleQty依照覆核日
    Public Function CHK_DEPOT_STOCK_DAYS() As Boolean
        Dim conn_str As String
        conn_str = My.Settings.HD_ERP_DBConnection
        Dim conn As New SqlConnection(conn_str)
        Dim iRowAffected As Integer = 0 '接收更新資料A後的影響筆數
        Dim SaveComplete As Boolean = True

        Dim sSql As String = ""
        Dim xRow As DataRow

        '開啟連接物件
        conn.Open()

        '啟用交易紀錄
        Dim transaction = conn.BeginTransaction
        Try
            Dim command = conn.CreateCommand
            command.Connection = conn
            command.Transaction = transaction
            '判斷公司庫存每日檔是否有資料，如果沒有每日庫存檔，自己做一個前一日每日庫存資料，資料來源D_DEPOT_STOCKS
            '若有資料者，取每日庫存檔的最新日期開始結算滾動到昨日的每日庫存資料
            Dim dt As New DataTable
            'dt = pf.GetDB("D_DEPOT_STOCKS_DAYS,NOLOCK", {{"CUSTOM_SQL", " CONVERT(varchar,[EFFECTIVE_DATE],111)='" & DateTime.Now.AddDays(-1).ToString("yyyy/MM/dd") & "'"}})
            'Console.WriteLine(sSql)
            Dim Str As String = ""
            Str = "  SELECT [DEPOT_ID],[PRODUCT_ID],[BATCH_NUM],MAX([EFFECTIVE_DATE]) AS EFFECTIVE_DATE"
            Str &= " From [D_DEPOT_STOCKS_DAYS]	  GROUP BY [DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
            Str &= " HAVING MAX([EFFECTIVE_DATE])<'" & DateTime.Now.AddDays(-1).ToString("yyyy/MM/dd") & "'"
            dt = pf.GetDataTable(Str)
            'If dt.Rows.Count = 0 Then
            If dt.Rows.Count > 0 Then
                '取每日庫存最新日期
                'sSql = "SELECT TOP 1 CONVERT(varchar,[EFFECTIVE_DATE],111) FROM [D_DEPOT_STOCKS_DAYS] WITH (NOLOCK) ORDER BY [EFFECTIVE_DATE] DESC"
                'Console.WriteLine(sSql)
                'Dim qdt As New DataTable
                'qdt = pf.GetDataTable(sSql)
                'If qdt.Rows.Count > 0 Then
                If dt.Rows.Count > 0 Then
                    Dim start_date As New DateTime
                    'start_date = Convert.ToDateTime(qdt.Rows(0).Item(0))
                    Dim a As Integer = 0
                    For a = 0 To dt.Rows.Count - 1
                        start_date = Convert.ToDateTime(dt.Rows(a).Item("EFFECTIVE_DATE"))

                        'start_date = Convert.ToDateTime(dt.Rows(0).Item(0))
                        Dim end_date As DateTime
                        end_date = DateTime.Now
                        Dim days As Integer = 1
                        Dim chk_date As New DateTime
                        chk_date = start_date.AddDays(days)
                        Do While chk_date.ToString("yyyy/MM/dd") < end_date.ToString("yyyy/MM/dd")                         '
                            Console.WriteLine(chk_date.ToString("yyyy/MM/dd"))
                            '建立生效日的每日庫存的資料並且計算
                            If UPDATE_D_DEPOT_STOCKS_DAYS_BY_DPB(chk_date, dt.Rows(a).Item("DEPOT_ID"), dt.Rows(a).Item("PRODUCT_ID"), dt.Rows(a).Item("BATCH_NUM"), command) = False Then
                                D_STOCKS_SETTLEMENT(chk_date, "1", "2")
                                transaction.Rollback()
                                SaveComplete = False
                                Return SaveComplete
                            End If
                            'If UPDATE_D_DEPOT_STOCKS_DAYS(chk_date, command) = False Then
                            '    D_STOCKS_SETTLEMENT(chk_date, "1", "2")
                            '    transaction.Rollback()
                            '    SaveComplete = False
                            '    Return SaveComplete
                            'End If
                            Dim PASS_CHECK_DATE As DateTime
                            If SaveComplete = True Then
                                Dim k As Integer = 0
                                '生效日小於計算日要從生效日重新計算庫存數直到計算日
                                sSql = "SELECT * FROM [D_DEPOT_STOCKS_DIFF] WITH (NOLOCK)"
                                sSql &= " WHERE CONVERT(VARCHAR,[EFFECTIVE_DATE],111)<'" & chk_date.ToString("yyyy/MM/dd") & "'"
                                sSql &= " AND CONVERT(VARCHAR,[CHANGE_DATE],111)='" & chk_date.ToString("yyyy/MM/dd") & "'"
                                sSql &= " AND [DEPOT_ID]='" & dt.Rows(a).Item("DEPOT_ID") & "'"
                                sSql &= " AND [PRODUCT_ID]='" & dt.Rows(a).Item("PRODUCT_ID") & "'"
                                sSql &= " AND [BATCH_NUM]='" & dt.Rows(a).Item("BATCH_NUM") & "'"
                                'sSql &= " GROUP BY [EFFECTIVE_DATE]"
                                sSql &= " ORDER BY [EFFECTIVE_DATE]"
                                Console.WriteLine(sSql)
                                Dim other_dt As New DataTable
                                other_dt = pf.GetDataTable(sSql)
                                If other_dt.Rows.Count > 0 Then
                                    For k = 0 To other_dt.Rows.Count - 1
                                        PASS_CHECK_DATE = CDate(other_dt.Rows(k).Item("EFFECTIVE_DATE")).ToString("yyyy/MM/dd")
                                        While PASS_CHECK_DATE.ToString("yyyy/MM/dd") < end_date.ToString("yyyy/MM/dd")
                                            '檢查D_DEPOT_STOCKS_DAYS是否有前一天的記錄，如果沒有自動新增一筆記錄
                                            sSql = "SELECT * ,(Select TOP 1 [VALID_PERIOD] from [P_SHIPPED_GOODS_DETAIL] WHERE [DEPOT_ID]=[D_DEPOT_STOCKS_DIFF].[DEPOT_ID] and [PRODUCT_ID]=[D_DEPOT_STOCKS_DIFF].[PRODUCT_ID] and [BATCH_NUM]=[D_DEPOT_STOCKS_DIFF].[BATCH_NUM]) AS [VALID_PERIOD]FROM [D_DEPOT_STOCKS_DIFF] WITH (NOLOCK)"
                                            'sSql &= " INNER JOIN [D_DEPOT_BASE] ON [D_DEPOT_BASE].[DEPOT_ID]=[D_DEPOT_STOCKS_DIFF].[DEPOT_ID] AND [D_DEPOT_BASE].[DEPOT_TYPE]='公司'"
                                            sSql &= " WHERE CONVERT(VARCHAR,[EFFECTIVE_DATE],111)='" & PASS_CHECK_DATE.ToString("yyyy/MM/dd") & "'"
                                            sSql &= " AND CONVERT(VARCHAR,[CHANGE_DATE],111)='" & chk_date.ToString("yyyy/MM/dd") & "'"
                                            Console.WriteLine(sSql)
                                            Dim check_dt1 As DataTable = pf.GetDataTable(sSql)
                                            For Each check_row In check_dt1.Rows
                                                Dim check_where(,) As String = {
                                                    {"DEPOT_ID", check_row.Item("DEPOT_ID")},
                                                    {"PRODUCT_ID", check_row.Item("PRODUCT_ID")},
                                                    {"BATCH_NUM", check_row.Item("BATCH_NUM")},
                                                    {"EFFECTIVE_DATE", PASS_CHECK_DATE.AddDays(-1).ToString("yyyy/MM/dd")}
                                                }
                                                Dim check_dt2 As DataTable = pf.GetDB("D_DEPOT_STOCKS_DAYS,NOLOCK", check_where)
                                                If check_dt2.Rows.Count = 0 Then
                                                    Dim Valid_Date As Date
                                                    If pf.CheckDateTimeNull(check_row.item("VALID_PERIOD")) = "" Then
                                                        check_where = {
                                                            {"DEPOT_ID", check_row.Item("DEPOT_ID")},
                                                            {"PRODUCT_ID", check_row.Item("PRODUCT_ID")},
                                                            {"BATCH_NUM", check_row.Item("BATCH_NUM")},
                                                            {"CUSTOM_SQL", " ISNULL(VALID_PERIOD,'')<>''"}
                                                        }
                                                        Dim check_dt3 As DataTable = pf.GetDB("D_DEPOT_STOCKS_DAYS,NOLOCK", check_where)
                                                        If check_dt3.Rows.Count > 0 Then
                                                            Valid_Date = CDate(check_dt3.Rows(0).Item("VALID_PERIOD")).ToString("yyyy/MM/dd")
                                                        End If
                                                    Else
                                                        Valid_Date = CDate(check_row.item("VALID_PERIOD")).ToString("yyyy/MM/dd")
                                                    End If
                                                    Dim Insert_Data(,) As String = {
                                                        {"DEPOT_ID", check_row.Item("DEPOT_ID")},
                                                        {"PRODUCT_ID", check_row.Item("PRODUCT_ID")},
                                                        {"BATCH_NUM", check_row.Item("BATCH_NUM")},
                                                        {"EFFECTIVE_DATE", PASS_CHECK_DATE.AddDays(-1).ToString("yyyy/MM/dd")},
                                                        {"VALID_PERIOD", Valid_Date},
                                                        {"GOOD_QTY", "0"},
                                                        {"BAD_QTY", "0"},
                                                        {"INV_QTY", "0"},
                                                        {"DEPOT_QTY", "0"},
                                                        {"SALE_QTY", "0"}
                                                    }
                                                    Dim sSql6 As String = pf.GetInsertquery("D_DEPOT_STOCKS_DAYS", Insert_Data)
                                                    Console.WriteLine(sSql6)
                                                    command.CommandText = sSql6
                                                    iRowAffected = command.ExecuteNonQuery()
                                                    If iRowAffected = 0 Then
                                                        transaction.Rollback()
                                                        'Exit Sub
                                                    End If
                                                End If
                                            Next
                                            '建立生效日的每日庫存的資料並且計算
                                            'If UPDATE_D_DEPOT_STOCKS_DAYS(other_dt.Rows(k).Item("EFFECTIVE_DATE"), command) = False Then
                                            If UPDATE_D_DEPOT_STOCKS_DAYS_BY_DPB(PASS_CHECK_DATE, dt.Rows(a).Item("DEPOT_ID"), dt.Rows(a).Item("PRODUCT_ID"), dt.Rows(a).Item("BATCH_NUM"), command) = False Then
                                                '2019/08/26寫入庫存結算紀錄檔
                                                D_STOCKS_SETTLEMENT(chk_date, "1", "2")
                                                transaction.Rollback()
                                                SaveComplete = False
                                                Return SaveComplete
                                            End If
                                            PASS_CHECK_DATE = PASS_CHECK_DATE.AddDays(1).ToString("yyyy/MM/dd")
                                            Console.WriteLine(PASS_CHECK_DATE.ToString("yyyy/MM/dd"))
                                        End While
                                    Next
                                End If
                            End If
                            '2019/08/26寫入庫存結算紀錄檔
                            D_STOCKS_SETTLEMENT(chk_date, "1", "1")
                            days = days + 1
                            chk_date = start_date.AddDays(days)
                            'Console.WriteLine(chk_date)
                        Loop
                    Next
                Else

                    '預設將D_DEPOT_STOCKS現有的庫存自動做一個昨日的每日庫存檔
                    Dim qdt2 As New DataTable
                    'sSql = "SELECT * FROM [D_DEPOT_STOCKS] WITH (NOLOCK) ORDER BY DEPOT_ID,PRODUCT_ID,BATCH_NUM "
                    'qdt2 = pf.GetDataTable(sSql)
                    Dim swhere(,) As String = {
                        {"ORDERBY_ASC", "DEPOT_ID"},
                        {"ORDERBY_ASC", "PRODUCT_ID"},
                        {"ORDERBY_ASC", "BATCH_NUM"}
                    }
                    qdt2 = pf.GetDB("D_DEPOT_STOCKS,NOLOCK", swhere)
                    Dim table As String = "D_DEPOT_STOCKS_DAYS"
                    Dim j As Integer = 0
                    For j = 0 To qdt2.Rows.Count - 1
                        xRow = qdt2.Rows(j)
                        Dim col_value(,) As String = {
                            {"DEPOT_ID", pf.CheckStringDBNull(xRow.Item("DEPOT_ID"))},
                            {"PRODUCT_ID", pf.CheckStringDBNull(xRow.Item("PRODUCT_ID"))},
                            {"BATCH_NUM", pf.CheckStringDBNull(xRow.Item("BATCH_NUM"))},
                            {"EFFECTIVE_DATE", DateTime.Now.AddDays(-1).ToString("yyyy/MM/dd")},
                            {"GOOD_QTY", pf.CheckNull(xRow.Item("GOOD_QTY"))},
                            {"BAD_QTY", pf.CheckNull(xRow.Item("BAD_QTY"))},
                            {"CUS_CON_QTY", pf.CheckNull(xRow.Item("CUS_CON_QTY"))},
                            {"COST_PRICE", pf.CheckNull(xRow.Item("COST_PRICE"))},
                            {"UNIT_PRICE", pf.CheckNull(xRow.Item("UNIT_PRICE"))},
                            {"VALID_PERIOD", xRow.Item("VALID_PERIOD")},
                            {"RED_SHOW", xRow.Item("RED_SHOW")},
                            {"SALE_QTY", pf.CheckNull(xRow.Item("DEPOT_QTY"))},
                            {"DEPOT_QTY", pf.CheckNull(xRow.Item("DEPOT_QTY"))},
                            {"INV_QTY", pf.CheckNull(xRow.Item("DEPOT_QTY"))},
                            {"CREATE_USER", "系統"},
                            {"CREATE_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                        }
                        sSql = pf.GetInsertquery(table, col_value)
                        Console.WriteLine(sSql)
                        command.CommandText = sSql
                        iRowAffected = command.ExecuteNonQuery()
                        If iRowAffected = 0 Then
                            SaveComplete = False
                            transaction.Rollback()
                        End If
                    Next
                End If
            End If

            command.Dispose()
            If SaveComplete = True Then
                transaction.Commit()
            End If
        Catch ex As Exception
            'pf.MyInformation(xRow.Item("DEPOT_ID") & "-" & xRow.Item("PRODUCT_ID") & "-" & xRow.Item("BATCH_NUM"))
            pf.MyError(ex.ToString)
            transaction.Rollback()
            Return False
        Finally
            conn.Close()
            conn.Dispose()
        End Try

        Return True

    End Function

    '計算D_DEPOT_STOCKS_DAYS的數量
    'CHECK_DATE：每日庫存的日期
    Public Function UPDATE_D_DEPOT_STOCKS_DAYS(CHECK_DATE As Date, sqlCommand As SqlCommand) As Boolean
        Dim i As Integer = 0
        Dim iRowAffected As Integer = 0
        Dim table As String = "D_DEPOT_STOCKS_DAYS"
        Dim xRow As DataRow
        Dim SaveOK As Boolean = True
        Dim sSql As String = ""
        Dim sSql2 As String = ""
        Dim sSql3 As String = ""
        Dim sSql4 As String = ""
        Try
            '產生庫存每日檔資料
            Dim qdt2 As New DataTable
            sSql = ""
            sSql = "SELECT [D_DEPOT_STOCKS_DAYS].*,[D_DEPOT_BASE].[DEPOT_TYPE] FROM [D_DEPOT_STOCKS_DAYS] WITH (NOLOCK)"
            sSql &= " INNER JOIN [D_DEPOT_BASE] ON [D_DEPOT_BASE].[DEPOT_ID]=[D_DEPOT_STOCKS_DAYS].[DEPOT_ID]"
            sSql &= " WHERE Convert(varchar, [EFFECTIVE_DATE], 111) ='" & CHECK_DATE.AddDays(-1).ToString("yyyy/MM/dd") & "'"
            'sSql &= " AND [D_DEPOT_STOCKS_DAYS].[DEPOT_ID]='W0200101'"
            'sSql &= " AND [D_DEPOT_STOCKS_DAYS].[PRODUCT_ID]='J12A11'"
            sSql &= " ORDER BY [DEPOT_ID],[PRODUCT_ID],[BATCH_NUM] Desc"
            Console.WriteLine(sSql)
            qdt2 = pf.GetDataTable(sSql)
            'Dim where1(,) As String = {
            '    {"CUSTOM_SQL", " Convert(varchar, [EFFECTIVE_DATE], 111) ='" & CHECK_DATE.AddDays(-1).ToString("yyyy/MM/dd") & "'"},
            '    {"ORDERBY_ASC", "DEPOT_ID"},
            '    {"ORDERBY_ASC", "PRODUCT_ID"},
            '    {"ORDERBY_ASC", "BATCH_NUM"}
            '}
            'qdt2 = pf.GetDB("D_DEPOT_STOCKS_DAYS,NOLOCK", where1)

            If qdt2.Rows.Count = 0 Then
                pf.MyError("每日庫存檔無[" & CHECK_DATE.AddDays(-1).ToString("yyyy/MM/dd") & "]的資料，請洽系統管理員!")
                Return False
            Else
                For i = 0 To qdt2.Rows.Count - 1
                    xRow = qdt2.Rows(i)
                    '判斷資料是否存在
                    Dim chk_data As New DataTable
                    'sSql2 = ""
                    'sSql2 = "SELECT * FROM [D_DEPOT_STOCKS_DAYS] WITH (NOLOCK)"
                    'sSql2 &= " WHERE CONVERT(varchar, [EFFECTIVE_DATE], 111) ='" & CHECK_DATE.ToString("yyyy/MM/dd") & "'"
                    'sSql2 &= " AND [DEPOT_ID]='" & xRow.Item("DEPOT_ID").ToString & "'"
                    'sSql2 &= " AND [PRODUCT_ID]='" & xRow.Item("PRODUCT_ID").ToString & "'"
                    'sSql2 &= " AND [BATCH_NUM]='" & xRow.Item("BATCH_NUM").ToString & "'"
                    'Console.WriteLine(sSql2)
                    'chk_data = pf.GetDataTable(sSql2)
                    Dim where2(,) As String = {
                        {"CUSTOM_SQL", " Convert(varchar, [EFFECTIVE_DATE], 111) ='" & CHECK_DATE.ToString("yyyy/MM/dd") & "'"},
                        {"DEPOT_ID", xRow.Item("DEPOT_ID").ToString},
                        {"PRODUCT_ID", xRow.Item("PRODUCT_ID").ToString},
                        {"BATCH_NUM", xRow.Item("BATCH_NUM").ToString}
                    }
                    chk_data = pf.GetDB("D_DEPOT_STOCKS_DAYS,NOLOCK", where2)
                    If chk_data.Rows.Count = 0 Then
                        Dim col_value(,) As String = {
                            {"DEPOT_ID", pf.CheckStringDBNull(xRow.Item("DEPOT_ID"))},
                            {"PRODUCT_ID", pf.CheckStringDBNull(xRow.Item("PRODUCT_ID"))},
                            {"BATCH_NUM", pf.CheckStringDBNull(xRow.Item("BATCH_NUM"))},
                            {"EFFECTIVE_DATE", CHECK_DATE.ToString("yyyy/MM/dd")},
                            {"GOOD_QTY", pf.CheckNull(xRow.Item("GOOD_QTY"))},
                            {"BAD_QTY", pf.CheckNull(xRow.Item("BAD_QTY"))},
                            {"CUS_CON_QTY", pf.CheckNull(xRow.Item("CUS_CON_QTY"))},
                            {"COST_PRICE", pf.CheckNull(xRow.Item("COST_PRICE"))},
                            {"UNIT_PRICE", pf.CheckNull(xRow.Item("UNIT_PRICE"))},
                            {"VALID_PERIOD", pf.CheckDateTimeNull(xRow.Item("VALID_PERIOD"))},
                            {"RED_SHOW", pf.CheckNull(xRow.Item("RED_SHOW"))},
                            {"SALE_QTY", pf.CheckNull(xRow.Item("SALE_QTY"))},
                            {"DEPOT_QTY", pf.CheckNull(xRow.Item("DEPOT_QTY"))},
                            {"INV_QTY", pf.CheckNull(xRow.Item("INV_QTY"))},
                            {"CREATE_USER", "系統"},
                            {"CREATE_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                        }
                        sSql2 = pf.GetInsertquery(table, col_value)
                        Console.WriteLine(sSql2)
                        sqlCommand.CommandText = sSql2
                        iRowAffected = sqlCommand.ExecuteNonQuery()
                        If iRowAffected = 0 Then
                            pf.MyError("新增每日庫存檔日期[" & CHECK_DATE.ToString("yyyy/MM/dd") & "]的資料發生錯誤!")
                            Return False
                        End If
                    End If

                    '開始計算庫存數
                    '計算異動每日結算庫存數INV_QTY-依照生效日EFFECTIVE_DATE
                    sSql3 = ""
                    Dim New_Inv_Qty As Double = 0
                    Dim New_Inv_Bad_Qty As Double = 0
                    sSql3 = "SELECT [DEPOT_ID],[PRODUCT_ID],[BATCH_NUM],SUM(ISNULL([G_DIFF_QTY],0)) AS G_DIFF_QTY "
                    sSql3 &= ",SUM(ISNULL([B_DIFF_QTY],0)) AS B_DIFF_QTY,SUM(ISNULL([SUPPLIER_DIFF_QTY],0)) AS SUPPLIER_DIFF_QTY "
                    'sSql3 &= ",(SELECT [DEPOT_TYPE] FROM [D_DEPOT_BASE] WHERE [D_DEPOT_BASE].[DEPOT_ID]=[D_DEPOT_STOCKS_DIFF].[DEPOT_ID])  AS [DEPOT_TYPE] "
                    sSql3 &= " FROM [D_DEPOT_STOCKS_DIFF] With (NOLOCK)"
                    sSql3 &= " WHERE Convert(varchar, [EFFECTIVE_DATE], 111) ='" & CHECK_DATE.ToString("yyyy/MM/dd") & "'"
                    sSql3 &= " AND [DEPOT_ID]='" & xRow.Item("DEPOT_ID").ToString & "'"
                    sSql3 &= " AND [PRODUCT_ID]='" & xRow.Item("PRODUCT_ID").ToString & "'"
                    sSql3 &= " AND [BATCH_NUM]='" & xRow.Item("BATCH_NUM").ToString & "'"
                    sSql3 &= " GROUP BY [DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                    Console.WriteLine(sSql3)
                    Dim qdt3 As New DataTable
                    qdt3 = pf.GetDataTable(sSql3)
                    If qdt3.Rows.Count > 0 Then
                        If xRow.Item("DEPOT_TYPE") = "公司" Then
                            New_Inv_Qty = Convert.ToDouble(pf.CheckNull(xRow.Item("INV_QTY"))) + Convert.ToDouble(pf.CheckNull(qdt3.Rows(0).Item("G_DIFF_QTY")))
                            New_Inv_Bad_Qty = Convert.ToDouble(pf.CheckNull(xRow.Item("INV_QTY"))) + Convert.ToDouble(pf.CheckNull(qdt3.Rows(0).Item("B_DIFF_QTY")))
                        ElseIf xRow.Item("DEPOT_TYPE") = "經銷商" Then
                            New_Inv_Qty = Convert.ToDouble(pf.CheckNull(xRow.Item("INV_QTY"))) + Convert.ToDouble(pf.CheckNull(qdt3.Rows(0).Item("SUPPLIER_DIFF_QTY")))
                            New_Inv_Bad_Qty = Convert.ToDouble(pf.CheckNull(xRow.Item("INV_BAD_QTY")))
                        End If
                        Dim PK2() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM", "EFFECTIVE_DATE"}
                        Dim col_value2(,) As String = {
                            {"DEPOT_ID", xRow.Item("DEPOT_ID")},
                            {"PRODUCT_ID", xRow.Item("PRODUCT_ID")},
                            {"BATCH_NUM", xRow.Item("BATCH_NUM")},
                            {"EFFECTIVE_DATE", CHECK_DATE},
                            {"INV_QTY", New_Inv_Qty},
                            {"INV_BAD_QTY", New_Inv_Bad_Qty},
                            {"ALTER_USER", "系統"},
                            {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                        }
                        sSql3 = pf.GetUpdatequery(table, PK2, col_value2)
                        Console.WriteLine(sSql3)
                        sqlCommand.CommandText = sSql3
                        iRowAffected = sqlCommand.ExecuteNonQuery()
                        If iRowAffected = 0 Then
                            pf.MyError("1-異動每日庫存檔日期[" & CHECK_DATE.ToString("yyyy/MM/dd") & "]的資料發生錯誤!")
                            Return False
                        End If
                    Else
                        If xRow.Item("DEPOT_TYPE") = "公司" Then
                            New_Inv_Qty = Convert.ToDouble(pf.CheckNull(xRow.Item("INV_QTY")))
                            New_Inv_Bad_Qty = Convert.ToDouble(pf.CheckNull(xRow.Item("INV_BAD_QTY")))
                        ElseIf xRow.Item("DEPOT_TYPE") = "經銷商" Then
                            New_Inv_Qty = Convert.ToDouble(pf.CheckNull(xRow.Item("INV_QTY")))
                            New_Inv_Bad_Qty = Convert.ToDouble(pf.CheckNull(xRow.Item("INV_BAD_QTY")))
                        End If
                        'New_Inv_Qty = Convert.ToDouble(pf.CheckNull(xRow.Item("INV_QTY")))
                        Dim PK2() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM", "EFFECTIVE_DATE"}
                        Dim col_value2(,) As String = {
                            {"DEPOT_ID", xRow.Item("DEPOT_ID")},
                            {"PRODUCT_ID", xRow.Item("PRODUCT_ID")},
                            {"BATCH_NUM", xRow.Item("BATCH_NUM")},
                            {"EFFECTIVE_DATE", CHECK_DATE},
                            {"INV_QTY", New_Inv_Qty},
                            {"INV_BAD_QTY", New_Inv_Bad_Qty},
                            {"ALTER_USER", "系統"},
                            {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                        }
                        sSql3 = pf.GetUpdatequery(table, PK2, col_value2)
                        Console.WriteLine(sSql3)
                        sqlCommand.CommandText = sSql3
                        iRowAffected = sqlCommand.ExecuteNonQuery()
                        If iRowAffected = 0 Then
                            pf.MyError("1-異動每日庫存檔-每日結算庫存數量-日期[" & CHECK_DATE.ToString("yyyy/MM/dd") & "]的資料發生錯誤!")
                            Return False
                        End If
                    End If

                    '計算2-依照覆核日
                    ''計算異動倉庫現有庫存數Good_QTY,DEPOT_QTY-依照覆核日CHANGE_DATE
                    sSql4 = ""
                    Dim New_Good_Qty As Double = 0
                    Dim New_Bad_Qty As Double = 0
                    Dim New_Depot_Qty As Double = 0
                    sSql4 = "SELECT [DEPOT_ID],[PRODUCT_ID],[BATCH_NUM],SUM(ISNULL([G_DIFF_QTY],0)) AS G_DIFF_QTY "
                    sSql4 &= " ,SUM(ISNULL([B_DIFF_QTY],0)) AS B_DIFF_QTY "
                    sSql4 &= ",SUM(ISNULL([B_DIFF_QTY],0)) AS B_DIFF_QTY,SUM(ISNULL([SUPPLIER_DIFF_QTY],0)) AS SUPPLIER_DIFF_QTY "
                    'sSql4 &= ",(SELECT [DEPOT_TYPE] FROM [D_DEPOT_BASE] WHERE [D_DEPOT_BASE].[DEPOT_ID]=[D_DEPOT_STOCKS_DIFF].[DEPOT_ID])"
                    sSql4 &= " FROM [D_DEPOT_STOCKS_DIFF] WITH (NOLOCK)"
                    sSql4 &= " WHERE Convert(varchar, [CHANGE_DATE], 111) ='" & CHECK_DATE.ToString("yyyy/MM/dd") & "'"
                    sSql4 &= " AND [DEPOT_ID]='" & xRow.Item("DEPOT_ID").ToString & "'"
                    sSql4 &= " AND [PRODUCT_ID]='" & xRow.Item("PRODUCT_ID").ToString & "'"
                    sSql4 &= " AND [BATCH_NUM]='" & xRow.Item("BATCH_NUM").ToString & "'"
                    sSql4 &= " GROUP BY [DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                    Console.WriteLine(sSql4)
                    Dim qdt4 As New DataTable
                    qdt4 = pf.GetDataTable(sSql4)
                    If qdt4.Rows.Count > 0 Then
                        If xRow.Item("DEPOT_TYPE") = "公司" Then
                            New_Good_Qty = Convert.ToDouble(pf.CheckNull(xRow.Item("GOOD_QTY"))) + Convert.ToDouble(pf.CheckNull(qdt4.Rows(0).Item("G_DIFF_QTY")))
                            New_Bad_Qty = Convert.ToDouble(pf.CheckNull(xRow.Item("BAD_QTY"))) + Convert.ToDouble(pf.CheckNull(qdt4.Rows(0).Item("B_DIFF_QTY")))
                            New_Depot_Qty = Convert.ToDouble(pf.CheckNull(xRow.Item("DEPOT_QTY"))) + Convert.ToDouble(pf.CheckNull(qdt4.Rows(0).Item("G_DIFF_QTY")))
                        ElseIf xRow.Item("DEPOT_TYPE") = "經銷商" Then
                            New_Good_Qty = Convert.ToDouble(pf.CheckNull(xRow.Item("GOOD_QTY"))) + Convert.ToDouble(pf.CheckNull(qdt4.Rows(0).Item("SUPPLIER_DIFF_QTY")))
                            New_Bad_Qty = Convert.ToDouble(pf.CheckNull(xRow.Item("BAD_QTY"))) + Convert.ToDouble(pf.CheckNull(qdt4.Rows(0).Item("B_DIFF_QTY")))
                            New_Depot_Qty = Convert.ToDouble(pf.CheckNull(xRow.Item("DEPOT_QTY"))) + Convert.ToDouble(pf.CheckNull(qdt4.Rows(0).Item("SUPPLIER_DIFF_QTY")))
                        End If
                        Dim PK2() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM", "EFFECTIVE_DATE"}
                        Dim col_value2(,) As String = {
                            {"DEPOT_ID", xRow.Item("DEPOT_ID")},
                            {"PRODUCT_ID", xRow.Item("PRODUCT_ID")},
                            {"BATCH_NUM", xRow.Item("BATCH_NUM")},
                            {"EFFECTIVE_DATE", CHECK_DATE},
                            {"GOOD_QTY", New_Good_Qty},
                            {"BAD_QTY", New_Bad_Qty},
                            {"DEPOT_QTY", New_Depot_Qty},
                            {"ALTER_USER", "系統"},
                            {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                        }
                        sSql4 = pf.GetUpdatequery(table, PK2, col_value2)
                        Console.WriteLine(sSql4)
                        sqlCommand.CommandText = sSql4
                        iRowAffected = sqlCommand.ExecuteNonQuery()
                        If iRowAffected = 0 Then
                            pf.MyError("2-異動每日庫存檔-每日結算庫存數量-日期[" & CHECK_DATE.ToString("yyyy/MM/dd") & "]的資料發生錯誤!")
                            Return False
                        End If
                    Else
                        If xRow.Item("DEPOT_TYPE") = "公司" Then
                            New_Good_Qty = Convert.ToDouble(pf.CheckNull(xRow.Item("GOOD_QTY")))
                            New_Bad_Qty = Convert.ToDouble(pf.CheckNull(xRow.Item("BAD_QTY")))
                            New_Depot_Qty = Convert.ToDouble(pf.CheckNull(xRow.Item("DEPOT_QTY")))
                        ElseIf xRow.Item("DEPOT_TYPE") = "經銷商" Then
                            New_Good_Qty = Convert.ToDouble(pf.CheckNull(xRow.Item("GOOD_QTY")))
                            New_Bad_Qty = Convert.ToDouble(pf.CheckNull(xRow.Item("BAD_QTY")))
                            New_Depot_Qty = Convert.ToDouble(pf.CheckNull(xRow.Item("DEPOT_QTY")))
                        End If
                        'New_Good_Qty = Convert.ToDouble(pf.CheckNull(xRow.Item("GOOD_QTY")))
                        'New_Bad_Qty = Convert.ToDouble(pf.CheckNull(xRow.Item("BAD_QTY")))
                        'New_Depot_Qty = Convert.ToDouble(pf.CheckNull(xRow.Item("DEPOT_QTY")))
                        Dim PK2() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM", "EFFECTIVE_DATE"}
                        Dim col_value2(,) As String = {
                            {"DEPOT_ID", xRow.Item("DEPOT_ID")},
                            {"PRODUCT_ID", xRow.Item("PRODUCT_ID")},
                            {"BATCH_NUM", xRow.Item("BATCH_NUM")},
                            {"EFFECTIVE_DATE", CHECK_DATE},
                            {"GOOD_QTY", New_Good_Qty},
                            {"BAD_QTY", New_Bad_Qty},
                            {"DEPOT_QTY", New_Depot_Qty},
                            {"ALTER_USER", "系統"},
                            {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                        }
                        sSql4 = pf.GetUpdatequery(table, PK2, col_value2)
                        Console.WriteLine(sSql4)
                        sqlCommand.CommandText = sSql4
                        iRowAffected = sqlCommand.ExecuteNonQuery()
                        If iRowAffected = 0 Then
                            pf.MyError("2-異動每日庫存檔-倉庫現有庫存數量-日期[" & CHECK_DATE.ToString("yyyy/MM/dd") & "]的資料發生錯誤!")
                            Return False
                        End If
                    End If

                    '--------------------------------------------------------
                    '計算3-依照覆核日
                    ''計算即時可動支庫存數,覆核日CHANGE_DATE
                    Dim sSql5 As String = ""
                    Dim New_Sale_Qty As Double = 0
                    sSql5 = "SELECT [DEPOT_ID],[PRODUCT_ID],[BATCH_NUM],SUM(ISNULL([KEYIN_DIFF_QTY],0)) AS KEYIN_DIFF_QTY "
                    sSql5 &= " FROM [D_DEPOT_STOCKS_KEYIN_DIFF] WITH (NOLOCK)"
                    sSql5 &= " WHERE Convert(varchar, [CHANGE_DATE], 111) ='" & CHECK_DATE.ToString("yyyy/MM/dd") & "'"
                    sSql5 &= " AND [DEPOT_ID]='" & xRow.Item("DEPOT_ID").ToString & "'"
                    sSql5 &= " AND [PRODUCT_ID]='" & xRow.Item("PRODUCT_ID").ToString & "'"
                    sSql5 &= " AND [BATCH_NUM]='" & xRow.Item("BATCH_NUM").ToString & "'"
                    sSql5 &= " GROUP BY [DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                    Console.WriteLine(sSql5)
                    Dim qdt5 As New DataTable
                    qdt5 = pf.GetDataTable(sSql5)
                    If qdt5.Rows.Count > 0 Then
                        'New_Good_Qty = Convert.ToDouble(pf.CheckNull(xRow.Item("DEPOT_QTY"))) + Convert.ToDouble(pf.CheckNull(qdt4.Rows(0).Item("G_DIFF_QTY")))
                        'New_Bad_Qty = Convert.ToDouble(pf.CheckNull(xRow.Item("BAD_QTY"))) + Convert.ToDouble(pf.CheckNull(qdt4.Rows(0).Item("B_DIFF_QTY")))
                        New_Sale_Qty = Convert.ToDouble(pf.CheckNull(xRow.Item("SALE_QTY"))) + Convert.ToDouble(pf.CheckNull(qdt5.Rows(0).Item("KEYIN_DIFF_QTY")))
                        Dim PK2() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM", "EFFECTIVE_DATE"}
                        Dim col_value2(,) As String = {
                        {"DEPOT_ID", xRow.Item("DEPOT_ID")},
                        {"PRODUCT_ID", xRow.Item("PRODUCT_ID")},
                        {"BATCH_NUM", xRow.Item("BATCH_NUM")},
                        {"EFFECTIVE_DATE", CHECK_DATE},
                        {"SALE_QTY", New_Sale_Qty},
                        {"ALTER_USER", "系統"},
                        {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                    }
                        sSql5 = pf.GetUpdatequery(table, PK2, col_value2)
                        Console.WriteLine(sSql5)
                        sqlCommand.CommandText = sSql5
                        iRowAffected = sqlCommand.ExecuteNonQuery()
                        If iRowAffected = 0 Then
                            pf.MyError("2-異動每日庫存檔-每日結算庫存數量-日期[" & CHECK_DATE.ToString("yyyy/MM/dd") & "]的資料發生錯誤!")
                            Return False
                        End If
                    Else

                        New_Sale_Qty = Convert.ToDouble(pf.CheckNull(xRow.Item("SALE_QTY")))
                        Dim PK2() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM", "EFFECTIVE_DATE"}
                        Dim col_value2(,) As String = {
                        {"DEPOT_ID", xRow.Item("DEPOT_ID")},
                        {"PRODUCT_ID", xRow.Item("PRODUCT_ID")},
                        {"BATCH_NUM", xRow.Item("BATCH_NUM")},
                        {"EFFECTIVE_DATE", CHECK_DATE},
                        {"SALE_QTY", New_Sale_Qty},
                        {"ALTER_USER", "系統"},
                        {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                    }
                        sSql5 = pf.GetUpdatequery(table, PK2, col_value2)
                        Console.WriteLine(sSql5)
                        sqlCommand.CommandText = sSql5
                        iRowAffected = sqlCommand.ExecuteNonQuery()
                        If iRowAffected = 0 Then
                            pf.MyError("2-異動每日庫存檔-倉庫現有庫存數量-日期[" & CHECK_DATE.ToString("yyyy/MM/dd") & "]的資料發生錯誤!")
                            Return False
                        End If
                    End If
                    '--------------------------------------------------------

                Next


            End If

        Catch ex As Exception
            pf.MyError(ex.ToString)
            Return False
        End Try

        Return True

    End Function

    '2019/11/28 edit by yuling
    '計算D_DEPOT_STOCKS_DAYS的數量，限盤整單使用
    'CHECK_DATE：每日庫存的日期
    'STOCK_OUT_ID：出庫單編號
    'STOCK_IN_ID：入庫單編號
    Public Function UPDATE_D_DEPOT_STOCKS_DAYS2(STOCK_OUT_ID As String, STOCK_IN_ID As String, CHECK_DATE As Date, sqlCommand As SqlCommand) As Boolean
        Dim i As Integer = 0
        Dim iRowAffected As Integer = 0
        Dim table As String = "D_DEPOT_STOCKS_DAYS"
        Dim xRow As DataRow
        Dim yRow As DataRow
        Dim SaveOK As Boolean = True
        Dim sSql As String = ""
        Dim sSql2 As String = ""
        Dim sSql3 As String = ""
        Dim sSql4 As String = ""
        Try
            '產生庫存每日檔資料
            Dim qdt2 As New DataTable
            'sSql = ""
            'sSql = "SELECT * FROM [D_DEPOT_STOCKS_DAYS] WITH (NOLOCK)"
            'sSql &= " WHERE Convert(varchar, [EFFECTIVE_DATE], 111) ='" & CHECK_DATE.AddDays(-1).ToString("yyyy/MM/dd") & "'"
            'sSql &= " ORDER BY [DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
            'Console.WriteLine(sSql)
            'qdt2 = pf.GetDataTable(sSql)
            Dim where1(,) As String = {
                {"CUSTOM_SQL", " Convert(varchar, [EFFECTIVE_DATE], 111) ='" & CHECK_DATE.AddDays(-1).ToString("yyyy/MM/dd") & "'"},
                {"ORDERBY_ASC", "DEPOT_ID"},
                {"ORDERBY_ASC", "PRODUCT_ID"},
                {"ORDERBY_ASC", "BATCH_NUM"}
            }
            qdt2 = pf.GetDB("D_DEPOT_STOCKS_DAYS,NOLOCK", where1)
            If qdt2.Rows.Count = 0 Then
                pf.MyError("每日庫存檔無[" & CHECK_DATE.AddDays(-1).ToString("yyyy/MM/dd") & "]的資料，請洽系統管理員!")
                Return False
            Else
                '僅計算出庫單與入庫單
                sSql3 = "SELECT [DEPOT_ID],[PRODUCT_ID],[BATCH_NUM] FROM [D_DEPOT_STOCKS_DIFF] WITH (NOLOCK)"
                If STOCK_OUT_ID <> "" AndAlso STOCK_IN_ID <> "" Then
                    sSql3 &= " WHERE [PAPER_ID] IN ('" & STOCK_OUT_ID & "','" & STOCK_IN_ID & "')"
                ElseIf STOCK_OUT_ID <> "" AndAlso STOCK_IN_ID = "" Then
                    sSql3 &= " WHERE [PAPER_ID] IN ('" & STOCK_OUT_ID & "')"
                ElseIf STOCK_OUT_ID = "" AndAlso STOCK_IN_ID <> "" Then
                    sSql3 &= " WHERE [PAPER_ID] IN ('" & STOCK_IN_ID & "')"
                End If
                Dim checkDB As DataTable
                sSql3 &= " GROUP BY [DEPOT_ID],[PRODUCT_ID],[BATCH_NUM] "
                sSql3 &= " ORDER BY [DEPOT_ID],[PRODUCT_ID],[BATCH_NUM] "
                Console.WriteLine(sSql3)
                checkDB = pf.GetDataTable(sSql3)
                Dim DAY_INV_QTY As Double = 0
                Dim DAY_INV_BAD_QTY As Double = 0
                Dim DAY_DEPOT_QTY As Double = 0
                Dim DAY_SALE_QTY As Double = 0
                Dim DAY_GOOD_QTY As Double = 0
                Dim DAY_BAD_QTY As Double = 0
                For i = 0 To checkDB.Rows.Count - 1
                    xRow = checkDB.Rows(i)
                    DAY_INV_QTY = 0
                    DAY_INV_BAD_QTY = 0
                    DAY_DEPOT_QTY = 0
                    DAY_SALE_QTY = 0
                    DAY_GOOD_QTY = 0
                    DAY_BAD_QTY = 0

                    '前一日庫存
                    Dim pre_data As DataTable
                    Dim where2(,) As String = {
                        {"CUSTOM_SQL", " Convert(varchar, [EFFECTIVE_DATE], 111) ='" & CHECK_DATE.AddDays(-1).ToString("yyyy/MM/dd") & "'"},
                        {"DEPOT_ID", xRow.Item("DEPOT_ID").ToString},
                        {"PRODUCT_ID", xRow.Item("PRODUCT_ID").ToString},
                        {"PRODUCT_ID", xRow.Item("PRODUCT_ID").ToString}
                    }
                    pre_data = pf.GetDB("D_DEPOT_STOCKS_DAYS,NOLOCK", where2)
                    yRow = pre_data.Rows(0)
                    DAY_INV_QTY = pf.CheckNull(yRow.Item("INV_QTY"))
                    DAY_INV_BAD_QTY = pf.CheckNull(yRow.Item("INV_BAD_QTY"))
                    DAY_DEPOT_QTY = pf.CheckNull(yRow.Item("DEPOT_QTY"))
                    DAY_SALE_QTY = pf.CheckNull(yRow.Item("SALE_QTY"))
                    DAY_GOOD_QTY = pf.CheckNull(yRow.Item("GOOD_QTY"))
                    DAY_BAD_QTY = pf.CheckNull(yRow.Item("BAD_QTY"))

                    ''判斷當日的庫存資料是否存在，不存在取前一日庫存新增
                    Dim chk_data As New DataTable
                    Dim where3(,) As String = {
                        {"CUSTOM_SQL", " Convert(varchar, [EFFECTIVE_DATE], 111) ='" & CHECK_DATE.ToString("yyyy/MM/dd") & "'"},
                        {"DEPOT_ID", xRow.Item("DEPOT_ID").ToString},
                        {"PRODUCT_ID", xRow.Item("PRODUCT_ID").ToString},
                        {"PRODUCT_ID", xRow.Item("PRODUCT_ID").ToString}
                    }
                    chk_data = pf.GetDB("D_DEPOT_STOCKS_DAYS,NOLOCK", where3)
                    If chk_data.Rows.Count = 0 Then
                        Dim col_value(,) As String = {
                            {"DEPOT_ID", yRow.Item("DEPOT_ID")},
                            {"PRODUCT_ID", yRow.Item("PRODUCT_ID")},
                            {"BATCH_NUM", yRow.Item("BATCH_NUM")},
                            {"EFFECTIVE_DATE", CHECK_DATE.ToString("yyyy/MM/dd")},
                            {"GOOD_QTY", pf.CheckNull(yRow.Item("GOOD_QTY"))},         '倉庫現有庫存(良品數量)
                            {"BAD_QTY", pf.CheckNull(yRow.Item("BAD_QTY"))},           '破損品數量)
                            {"CUS_CON_QTY", pf.CheckNull(yRow.Item("CUS_CON_QTY"))},   '客戶寄售數量
                            {"COST_PRICE", pf.CheckNull(yRow.Item("COST_PRICE"))},
                            {"UNIT_PRICE", pf.CheckNull(yRow.Item("UNIT_PRICE"))},
                            {"VALID_PERIOD", yRow.Item("VALID_PERIOD")},
                            {"RED_SHOW", yRow.Item("RED_SHOW")},
                            {"SALE_QTY", pf.CheckNull(yRow.Item("SALE_QTY"))},         '即時可動支庫存
                            {"DEPOT_QTY", pf.CheckNull(yRow.Item("DEPOT_QTY"))},       '倉庫現有庫存，依照覆核日/異動日
                            {"INV_QTY", pf.CheckNull(yRow.Item("INV_QTY"))},           '每日結算庫存，依照生效日
                            {"INV_BAD_QTY", pf.CheckNull(yRow.Item("INV_BAD_QTY"))},
                            {"CREATE_USER", "系統"},
                            {"CREATE_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                        }
                        sSql2 = pf.GetInsertquery(table, col_value)
                        Console.WriteLine(sSql2)
                        sqlCommand.CommandText = sSql2
                        iRowAffected = sqlCommand.ExecuteNonQuery()
                        If iRowAffected = 0 Then
                            pf.MyError("新增每日庫存檔日期[" & CHECK_DATE.ToString("yyyy/MM/dd") & "]的資料發生錯誤!")
                            Return False
                        End If
                    End If

                    '開始計算庫存數
                    '計算異動每日結算庫存數Good_QTY,INV_QTY-依照生效日EFFECTIVE_DATE
                    sSql3 = ""
                    Dim New_Inv_Qty As Double = 0
                    Dim New_Inv_Bad_Qty As Double = 0
                    sSql3 = "SELECT [DEPOT_ID],[PRODUCT_ID],[BATCH_NUM],SUM(ISNULL([G_DIFF_QTY],0)) AS G_DIFF_QTY "
                    sSql3 &= ",SUM(ISNULL([B_DIFF_QTY],0)) AS B_DIFF_QTY"
                    sSql3 &= " FROM [D_DEPOT_STOCKS_DIFF] With (NOLOCK)"
                    sSql3 &= " WHERE Convert(varchar, [EFFECTIVE_DATE], 111) ='" & CHECK_DATE.ToString("yyyy/MM/dd") & "'"
                    sSql3 &= " AND [DEPOT_ID]='" & xRow.Item("DEPOT_ID").ToString & "'"
                    sSql3 &= " AND [PRODUCT_ID]='" & xRow.Item("PRODUCT_ID").ToString & "'"
                    sSql3 &= " AND [BATCH_NUM]='" & xRow.Item("BATCH_NUM").ToString & "'"
                    sSql3 &= " GROUP BY [DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                    Console.WriteLine(sSql3)
                    Dim qdt3 As New DataTable
                    qdt3 = pf.GetDataTable(sSql3)
                    If qdt3.Rows.Count > 0 Then
                        New_Inv_Qty = DAY_INV_QTY + Convert.ToDouble(pf.CheckNull(qdt3.Rows(0).Item("G_DIFF_QTY")))
                        New_Inv_Bad_Qty = DAY_INV_BAD_QTY + Convert.ToDouble(pf.CheckNull(qdt3.Rows(0).Item("B_DIFF_QTY")))
                        Dim PK2() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM", "EFFECTIVE_DATE"}
                        Dim col_value2(,) As String = {
                            {"DEPOT_ID", xRow.Item("DEPOT_ID")},
                            {"PRODUCT_ID", xRow.Item("PRODUCT_ID")},
                            {"BATCH_NUM", xRow.Item("BATCH_NUM")},
                            {"EFFECTIVE_DATE", CHECK_DATE},
                            {"INV_QTY", New_Inv_Qty},
                            {"INV_BAD_QTY", New_Inv_Bad_Qty},
                            {"ALTER_USER", "系統"},
                            {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                        }
                        sSql3 = pf.GetUpdatequery(table, PK2, col_value2)
                        Console.WriteLine(sSql3)
                        sqlCommand.CommandText = sSql3
                        iRowAffected = sqlCommand.ExecuteNonQuery()
                        If iRowAffected = 0 Then
                            pf.MyError("1-異動每日庫存檔日期[" & CHECK_DATE.ToString("yyyy/MM/dd") & "]的資料發生錯誤!")
                            Return False
                        End If
                    Else
                        New_Inv_Qty = DAY_INV_QTY
                        New_Inv_Bad_Qty = DAY_INV_BAD_QTY
                        Dim PK2() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM", "EFFECTIVE_DATE"}
                        Dim col_value2(,) As String = {
                            {"DEPOT_ID", xRow.Item("DEPOT_ID")},
                            {"PRODUCT_ID", xRow.Item("PRODUCT_ID")},
                            {"BATCH_NUM", xRow.Item("BATCH_NUM")},
                            {"EFFECTIVE_DATE", CHECK_DATE},
                            {"INV_QTY", New_Inv_Qty},
                            {"INV_BAD_QTY", New_Inv_Bad_Qty},
                            {"ALTER_USER", "系統"},
                            {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                        }
                        sSql3 = pf.GetUpdatequery(table, PK2, col_value2)
                        Console.WriteLine(sSql3)
                        sqlCommand.CommandText = sSql3
                        iRowAffected = sqlCommand.ExecuteNonQuery()
                        If iRowAffected = 0 Then
                            pf.MyError("1-異動每日庫存檔-每日結算庫存數量-日期[" & CHECK_DATE.ToString("yyyy/MM/dd") & "]的資料發生錯誤!")
                            Return False
                        End If
                    End If
                    '計算2-依照覆核日
                    ''計算異動倉庫現有庫存數DEPOT_QTY-依照覆核日CHANGE_DATE
                    sSql4 = ""
                    Dim New_Good_Qty As Double = 0
                    Dim New_Bad_Qty As Double = 0
                    Dim New_Depot_Qty As Double = 0
                    sSql4 = "SELECT [DEPOT_ID],[PRODUCT_ID],[BATCH_NUM],SUM(ISNULL([G_DIFF_QTY],0)) AS G_DIFF_QTY "
                    sSql4 &= " ,SUM(ISNULL([B_DIFF_QTY],0)) AS B_DIFF_QTY "
                    sSql4 &= " FROM [D_DEPOT_STOCKS_DIFF] WITH (NOLOCK)"
                    sSql4 &= " WHERE Convert(varchar, [CHANGE_DATE], 111) ='" & CHECK_DATE.ToString("yyyy/MM/dd") & "'"
                    sSql4 &= " AND [DEPOT_ID]='" & xRow.Item("DEPOT_ID").ToString & "'"
                    sSql4 &= " AND [PRODUCT_ID]='" & xRow.Item("PRODUCT_ID").ToString & "'"
                    sSql4 &= " AND [BATCH_NUM]='" & xRow.Item("BATCH_NUM").ToString & "'"
                    sSql4 &= " GROUP BY [DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                    Console.WriteLine(sSql4)
                    Dim qdt4 As New DataTable
                    qdt4 = pf.GetDataTable(sSql4)
                    If qdt4.Rows.Count > 0 Then
                        'New_Depot_Qty = Convert.ToDouble(xRow.Item("DEPOT_QTY")) + Convert.ToDouble(qdt4.Rows(0).Item("G_DIFF_QTY"))
                        New_Good_Qty = DAY_GOOD_QTY + Convert.ToDouble(pf.CheckNull(qdt4.Rows(0).Item("G_DIFF_QTY")))
                        New_Bad_Qty = DAY_BAD_QTY + Convert.ToDouble(pf.CheckNull(qdt4.Rows(0).Item("B_DIFF_QTY")))
                        New_Depot_Qty = DAY_DEPOT_QTY + Convert.ToDouble(pf.CheckNull(qdt4.Rows(0).Item("G_DIFF_QTY")))
                        Dim PK2() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM", "EFFECTIVE_DATE"}
                        Dim col_value2(,) As String = {
                            {"DEPOT_ID", xRow.Item("DEPOT_ID")},
                            {"PRODUCT_ID", xRow.Item("PRODUCT_ID")},
                            {"BATCH_NUM", xRow.Item("BATCH_NUM")},
                            {"EFFECTIVE_DATE", CHECK_DATE},
                            {"GOOD_QTY", New_Good_Qty},
                            {"BAD_QTY", New_Bad_Qty},
                            {"DEPOT_QTY", New_Depot_Qty},
                            {"ALTER_USER", "系統"},
                            {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                        }
                        sSql4 = pf.GetUpdatequery(table, PK2, col_value2)
                        Console.WriteLine(sSql4)
                        sqlCommand.CommandText = sSql4
                        iRowAffected = sqlCommand.ExecuteNonQuery()
                        If iRowAffected = 0 Then
                            pf.MyError("2-異動每日庫存檔-每日結算庫存數量-日期[" & CHECK_DATE.ToString("yyyy/MM/dd") & "]的資料發生錯誤!")
                            Return False
                        End If
                    Else
                        'New_Depot_Qty = Convert.ToDouble(xRow.Item("DEPOT_QTY"))
                        New_Good_Qty = DAY_GOOD_QTY
                        New_Bad_Qty = DAY_BAD_QTY
                        New_Depot_Qty = DAY_DEPOT_QTY
                        Dim PK2() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM", "EFFECTIVE_DATE"}
                        Dim col_value2(,) As String = {
                            {"DEPOT_ID", xRow.Item("DEPOT_ID")},
                            {"PRODUCT_ID", xRow.Item("PRODUCT_ID")},
                            {"BATCH_NUM", xRow.Item("BATCH_NUM")},
                            {"EFFECTIVE_DATE", CHECK_DATE},
                            {"GOOD_QTY", New_Good_Qty},
                            {"BAD_QTY", New_Bad_Qty},
                            {"DEPOT_QTY", New_Depot_Qty},
                            {"ALTER_USER", "系統"},
                            {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                        }
                        sSql4 = pf.GetUpdatequery(table, PK2, col_value2)
                        Console.WriteLine(sSql4)
                        sqlCommand.CommandText = sSql4
                        iRowAffected = sqlCommand.ExecuteNonQuery()
                        If iRowAffected = 0 Then
                            pf.MyError("2-異動每日庫存檔-倉庫現有庫存數量-日期[" & CHECK_DATE.ToString("yyyy/MM/dd") & "]的資料發生錯誤!")
                            Return False
                        End If
                    End If

                    '--------------------------------------------------------
                    '計算3-依照覆核日
                    ''計算即時可動支庫存數,覆核日CHANGE_DATE
                    Dim sSql5 As String = ""
                    Dim New_Sale_Qty As Double = 0
                    sSql5 = "SELECT [DEPOT_ID],[PRODUCT_ID],[BATCH_NUM],SUM(ISNULL([KEYIN_DIFF_QTY],0)) AS KEYIN_DIFF_QTY "
                    sSql5 &= " FROM [D_DEPOT_STOCKS_KEYIN_DIFF] WITH (NOLOCK)"
                    sSql5 &= " WHERE Convert(varchar, [CHANGE_DATE], 111) >='" & CHECK_DATE.ToString("yyyy/MM/dd") & "'"
                    sSql5 &= " AND [DEPOT_ID]='" & xRow.Item("DEPOT_ID").ToString & "'"
                    sSql5 &= " AND [PRODUCT_ID]='" & xRow.Item("PRODUCT_ID").ToString & "'"
                    sSql5 &= " AND [BATCH_NUM]='" & xRow.Item("BATCH_NUM").ToString & "'"
                    sSql5 &= " GROUP BY [DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                    Console.WriteLine(sSql5)
                    Dim qdt5 As New DataTable
                    qdt5 = pf.GetDataTable(sSql5)
                    If qdt5.Rows.Count > 0 Then
                        'New_Good_Qty = Convert.ToDouble(pf.CheckNull(xRow.Item("DEPOT_QTY"))) + Convert.ToDouble(pf.CheckNull(qdt4.Rows(0).Item("G_DIFF_QTY")))
                        'New_Bad_Qty = Convert.ToDouble(pf.CheckNull(xRow.Item("BAD_QTY"))) + Convert.ToDouble(pf.CheckNull(qdt4.Rows(0).Item("B_DIFF_QTY")))
                        New_Sale_Qty = DAY_SALE_QTY + Convert.ToDouble(pf.CheckNull(qdt5.Rows(0).Item("KEYIN_DIFF_QTY")))
                        Dim PK2() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM", "EFFECTIVE_DATE"}
                        Dim col_value2(,) As String = {
                            {"DEPOT_ID", xRow.Item("DEPOT_ID")},
                            {"PRODUCT_ID", xRow.Item("PRODUCT_ID")},
                            {"BATCH_NUM", xRow.Item("BATCH_NUM")},
                            {"EFFECTIVE_DATE", CHECK_DATE},
                            {"SALE_QTY", New_Sale_Qty},
                            {"ALTER_USER", "系統"},
                            {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                        }
                        sSql5 = pf.GetUpdatequery(table, PK2, col_value2)
                        Console.WriteLine(sSql5)
                        sqlCommand.CommandText = sSql5
                        iRowAffected = sqlCommand.ExecuteNonQuery()
                        If iRowAffected = 0 Then
                            pf.MyError("2-異動每日庫存檔-每日結算庫存數量-日期[" & CHECK_DATE.ToString("yyyy/MM/dd") & "]的資料發生錯誤!")
                            Return False
                        End If
                    Else

                        New_Sale_Qty = DAY_SALE_QTY
                        Dim PK2() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM", "EFFECTIVE_DATE"}
                        Dim col_value2(,) As String = {
                            {"DEPOT_ID", xRow.Item("DEPOT_ID")},
                            {"PRODUCT_ID", xRow.Item("PRODUCT_ID")},
                            {"BATCH_NUM", xRow.Item("BATCH_NUM")},
                            {"EFFECTIVE_DATE", CHECK_DATE},
                            {"SALE_QTY", New_Sale_Qty},
                            {"ALTER_USER", "系統"},
                            {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                        }
                        sSql5 = pf.GetUpdatequery(table, PK2, col_value2)
                        Console.WriteLine(sSql5)
                        sqlCommand.CommandText = sSql5
                        iRowAffected = sqlCommand.ExecuteNonQuery()
                        If iRowAffected = 0 Then
                            pf.MyError("2-異動每日庫存檔-倉庫現有庫存數量-日期[" & CHECK_DATE.ToString("yyyy/MM/dd") & "]的資料發生錯誤!")
                            Return False
                        End If
                    End If
                    '--------------------------------------------------------
                Next
            End If

        Catch ex As Exception
            pf.MyError(ex.ToString)
            Return False
        End Try

        Return True

    End Function

    '檢查客戶寄售庫存每日檔
    Public Function CHK_D_CUS_CON_QTY_DAYS() As Boolean
        Dim conn_str As String
        conn_str = My.Settings.HD_ERP_DBConnection
        Dim conn As New SqlConnection(conn_str)
        Dim iRowAffectedA As Integer = 0 '接收更新資料A後的影響筆數
        Dim iRowAffectedB As Integer = 0 '接收更新資料B後的影響筆數
        Dim iRowAffectedC As Integer = 0 '接收更新資料C後的影響筆數
        Dim SaveComplete As Boolean = True
        Dim i As Integer = 0
        Dim j As Integer = 0
        Dim sSql As String = ""

        '開啟連接物件
        conn.Open()

        '啟用交易紀錄
        Dim transaction_cus = conn.BeginTransaction

        Try
            Dim command = conn.CreateCommand
            command.Connection = conn
            command.Transaction = transaction_cus
            '檢查庫存每日檔前
            Dim chk_dt As New DataTable
            'chk_dt = pf.GetDB("D_CUS_CON_QTY_DAYS,NOLOCK", {})
            chk_dt = pf.GetDataTable("SELECT COUNT(*) FROM D_CUS_CON_QTY_DAYS WITH (NOLOCK)")
            Dim stocks_num As Integer = 0
            If chk_dt.Rows.Count > 0 Then
                stocks_num = 1
            End If
            If stocks_num = 0 Then
                Dim cus_qdt As New DataTable
                cus_qdt = pf.GetDB("D_CUS_CON_QTY,NOLOCK", {})
                If cus_qdt.Rows.Count > 0 Then
                    Dim sDate As DateTime = DateTime.Now.AddDays(-10)
                    Dim eDate As DateTime = DateTime.Now.AddDays(-1)
                    Dim checkDate As DateTime = sDate
                    Dim chkDay As Integer = 0
                    Dim yRow As DataRow
                    Do While checkDate.ToString("yyyy/MM/dd") <= eDate.ToString("yyyy/MM/dd")
                        For j = 0 To cus_qdt.Rows.Count - 1
                            yRow = cus_qdt.Rows(j)
                            Dim cus_table As String = "D_CUS_CON_QTY_DAYS"
                            Dim cus_col_value(,) As String = {
                                {"DEPOT_ID", yRow.Item("DEPOT_ID")},
                                {"PRODUCT_ID", yRow.Item("PRODUCT_ID")},
                                {"BATCH_NUM", yRow.Item("BATCH_NUM")},
                                {"EFFECTIVE_DATE", checkDate.ToString("yyyy/MM/dd")},
                                {"CUSTOMER_ID", yRow.Item("CUSTOMER_ID")},
                                {"CUS_INV_QTY", pf.CheckNull(yRow.Item("CUS_INV_QTY"))},
                                {"CUS_DEPOT_QTY", pf.CheckNull(yRow.Item("CUS_DEPOT_QTY"))},
                                {"CUS_SALE_QTY", pf.CheckNull(yRow.Item("CUS_SALE_QTY"))},
                                {"CREATE_USER", "系統"},
                                {"CREATE_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                            }
                            sSql = pf.GetInsertquery(cus_table, cus_col_value)
                            Console.WriteLine(sSql)
                            command.CommandText = sSql
                            iRowAffectedB = command.ExecuteNonQuery()
                            If iRowAffectedB = 0 Then
                                SaveComplete = False
                                transaction_cus.Rollback()
                            End If
                        Next
                        chkDay = chkDay + 1
                        checkDate = sDate.AddDays(chkDay)
                    Loop
                End If
            Else
                '[D_CUS_CON_QTY_DAYS]有資料者
                'sSql = "SELECT TOP 1 CONVERT(varchar,[EFFECTIVE_DATE],111) FROM [D_CUS_CON_QTY_DAYS] WITH (NOLOCK)"
                'sSql &= " ORDER BY  CONVERT(varchar, [EFFECTIVE_DATE], 111) DESC"
                'Dim qdt As New DataTable
                'qdt = pf.GetDataTable(sSql)

                Dim Str As String = ""
                Str = "SELECT [CUSTOMER_ID],[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM],MAX([EFFECTIVE_DATE]) AS EFFECTIVE_DATE"
                Str &= " From [D_CUS_CON_QTY_DAYS]  WITH (NOLOCK)"
                Str &= " GROUP BY [CUSTOMER_ID],[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                Str &= " HAVING MAX([EFFECTIVE_DATE])<'" & DateTime.Now.ToString("yyyy/MM/dd") & "'"
                Console.WriteLine(Str)
                Dim qdt As New DataTable
                qdt = pf.GetDataTable(Str)
                If qdt.Rows.Count > 0 Then
                    Dim a As Integer = 0
                    Dim start_date As New DateTime
                    For a = 0 To qdt.Rows.Count - 1
                        start_date = Convert.ToDateTime(qdt.Rows(a).Item("EFFECTIVE_DATE"))
                        Dim end_date As New DateTime
                        end_date = DateTime.Now
                        Dim adays As Integer = 1
                        Dim chk_date As New DateTime
                        chk_date = start_date.AddDays(adays)
                        Do While chk_date.ToString("yyyy/MM/dd") < end_date.ToString("yyyy/MM/dd")
                            If UPDATE_D_CUS_CON_QTY_DAYS_BY_CDPB(chk_date, qdt.Rows(a).Item("CUSTOMER_ID"), qdt.Rows(a).Item("DEPOT_ID"), qdt.Rows(a).Item("PRODUCT_ID"), qdt.Rows(a).Item("BATCH_NUM"), command) = False Then
                                transaction_cus.Rollback()
                                Exit Do
                            End If
                            'If UPDATE_D_CUS_CON_QTY_DAYS(chk_date, command) = False Then
                            '    transaction_cus.Rollback()
                            '    Exit Do
                            'End If
                            If SaveComplete = True Then
                                Dim k As Integer = 0
                                '生效日小於計算日要從生效日重新計算庫存數直到計算日
                                sSql = "SELECT [EFFECTIVE_DATE] FROM [D_DEPOT_STOCKS_DIFF] WITH (NOLOCK)"
                                sSql &= " WHERE CONVERT(VARCHAR,[EFFECTIVE_DATE],111)<'" & chk_date.ToString("yyyy/MM/dd") & "'"
                                sSql &= " AND CONVERT(VARCHAR,[CHANGE_DATE],111)='" & chk_date.ToString("yyyy/MM/dd") & "'"
                                sSql &= " AND ISNULL([CUS_DIFF_QTY],0)<>'0'"
                                sSql &= " AND [CUSTOMER_ID]='" & qdt.Rows(a).Item("CUSTOMER_ID") & "'"
                                sSql &= " AND [DEPOT_ID]='" & qdt.Rows(a).Item("DEPOT_ID") & "'"
                                sSql &= " AND [PRODUCT_ID]='" & qdt.Rows(a).Item("PRODUCT_ID") & "'"
                                sSql &= " AND [BATCH_NUM]='" & qdt.Rows(a).Item("BATCH_NUM") & "'"
                                sSql &= " GROUP BY [EFFECTIVE_DATE]"
                                sSql &= " ORDER BY [EFFECTIVE_DATE]"
                                Dim other_dt As New DataTable
                                other_dt = pf.GetDataTable(sSql)
                                If other_dt.Rows.Count > 0 Then
                                    For k = 0 To other_dt.Rows.Count - 1
                                        '建立生效日的每日庫存的資料並且計算
                                        If UPDATE_D_CUS_CON_QTY_DAYS_BY_CDPB(other_dt.Rows(k).Item("EFFECTIVE_DATE"), qdt.Rows(a).Item("CUSTOMER_ID"), qdt.Rows(a).Item("DEPOT_ID"), qdt.Rows(a).Item("PRODUCT_ID"), qdt.Rows(a).Item("BATCH_NUM"), command) = False Then
                                            transaction_cus.Rollback()
                                            SaveComplete = False
                                            Return SaveComplete
                                            Exit Do
                                        End If
                                        'If UPDATE_D_CUS_CON_QTY_DAYS(other_dt.Rows(k).Item("EFFECTIVE_DATE"), command) = False Then
                                        '    transaction_cus.Rollback()
                                        '    SaveComplete = False
                                        '    Return SaveComplete
                                        '    Exit Do
                                        'End If
                                    Next
                                End If
                            End If
                            adays = adays + 1
                            chk_date = start_date.AddDays(adays)
                        Loop
                    Next
                End If


                'Dim start_date As New DateTime
                'start_date = Convert.ToDateTime(qdt.Rows(0).Item(0))
                'Dim end_date As New DateTime
                'end_date = DateTime.Now
                'Dim adays As Integer = 1
                'Dim chk_date As New DateTime
                'chk_date = start_date.AddDays(adays)
                'Do While chk_date.ToString("yyyy/MM/dd") < end_date.ToString("yyyy/MM/dd")
                '    If UPDATE_D_CUS_CON_QTY_DAYS(chk_date, command) = False Then
                '        transaction_cus.Rollback()
                '        Exit Do
                '    End If

                '    If SaveComplete = True Then
                '        Dim k As Integer = 0
                '        '生效日小於計算日要從生效日重新計算庫存數直到計算日
                '        sSql = "SELECT [EFFECTIVE_DATE] FROM [D_DEPOT_STOCKS_DIFF] WITH (NOLOCK)"
                '        sSql &= " WHERE CONVERT(VARCHAR,[EFFECTIVE_DATE],111)<'" & chk_date.ToString("yyyy/MM/dd") & "'"
                '        sSql &= " AND CONVERT(VARCHAR,[CHANGE_DATE],111)='" & chk_date.ToString("yyyy/MM/dd") & "'"
                '        sSql &= " AND ISNULL([CUS_DIFF_QTY],0)<>'0'"
                '        sSql &= " GROUP BY [EFFECTIVE_DATE]"
                '        sSql &= " ORDER BY [EFFECTIVE_DATE]"
                '        Dim other_dt As New DataTable
                '        other_dt = pf.GetDataTable(sSql)
                '        If other_dt.Rows.Count > 0 Then
                '            For k = 0 To other_dt.Rows.Count - 1
                '                '建立生效日的每日庫存的資料並且計算
                '                If UPDATE_D_CUS_CON_QTY_DAYS(other_dt.Rows(k).Item("EFFECTIVE_DATE"), command) = False Then
                '                    transaction_cus.Rollback()
                '                    SaveComplete = False
                '                    Return SaveComplete
                '                    Exit Do
                '                End If
                '            Next
                '        End If
                '    End If
                '    adays = adays + 1
                '    chk_date = start_date.AddDays(adays)
                'Loop
            End If
            'transaction_cus.Commit()
            command.Dispose()
            If SaveComplete = True Then
                transaction_cus.Commit()
            End If
        Catch ex As Exception
            transaction_cus.Rollback()
            pf.MyError(ex.ToString)
            Return False
        Finally
            conn.Close()
            conn.Dispose()
        End Try
        Return True
    End Function

    '計算客戶寄售每日庫存檔的庫存數量
    Public Function UPDATE_D_CUS_CON_QTY_DAYS(CHECK_DATE As Date, sqlCommand As SqlCommand) As Boolean
        Dim sSql As String = ""
        Dim table As String = "D_CUS_CON_QTY_DAYS"
        Dim yRow As DataRow
        Dim iRowAffected As Integer = 0
        Try
            Dim qdt2 As New DataTable
            'sSql = "SELECT * FROM [D_CUS_CON_QTY_DAYS]  WITH (NOLOCK)"
            'sSql &= " WHERE CONVERT(varchar,[EFFECTIVE_DATE],111)='" & CHECK_DATE.AddDays(-1).ToString("yyyy/MM/dd") & "'"
            'sSql &= " ORDER BY [CUSTOMER_ID],[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
            'qdt2 = pf.GetDataTable(sSql)
            Dim where1(,) As String = {
            {"CUSTOM_SQL", " CONVERT(varchar,[EFFECTIVE_DATE],111)='" & CHECK_DATE.AddDays(-1).ToString("yyyy/MM/dd") & "'"},
            {"ORDERBY_ASC", "CUSTOMER_ID"},
            {"ORDERBY_ASC", "DEPOT_ID"},
            {"ORDERBY_ASC", "PRODUCT_ID"},
            {"ORDERBY_ASC", "BATCH_NUM"}
        }
            qdt2 = pf.GetDB("D_CUS_CON_QTY_DAYS,NOLOCK", where1)
            For j = 0 To qdt2.Rows.Count - 1
                yRow = qdt2.Rows(j)
                '判斷資料是否存在
                Dim chk_data As New DataTable
                'sSql = "SELECT * FROM [D_CUS_CON_QTY_DAYS] WITH (NOLOCK)"
                'sSql &= " WHERE Convert(varchar, [EFFECTIVE_DATE], 111) ='" & CHECK_DATE.ToString("yyyy/MM/dd") & "'"
                'sSql &= " AND [DEPOT_ID]='" & yRow.Item("DEPOT_ID").ToString & "'"
                'sSql &= " AND [PRODUCT_ID]='" & yRow.Item("PRODUCT_ID").ToString & "'"
                'sSql &= " AND [BATCH_NUM]='" & yRow.Item("BATCH_NUM").ToString & "'"
                'sSql &= " AND [CUSTOMER_ID]='" & yRow.Item("CUSTOMER_ID").ToString & "'"
                'chk_data = pf.GetDataTable(sSql)
                Dim where2(,) As String = {
                {"CUSTOM_SQL", " CONVERT(varchar,[EFFECTIVE_DATE],111)='" & CHECK_DATE.ToString("yyyy/MM/dd") & "'"},
                {"CUSTOMER_ID", yRow.Item("CUSTOMER_ID").ToString},
                {"DEPOT_ID", yRow.Item("DEPOT_ID").ToString},
                {"PRODUCT_ID", yRow.Item("PRODUCT_ID").ToString},
                {"BATCH_NUM", yRow.Item("BATCH_NUM").ToString}
            }
                chk_data = pf.GetDB("D_CUS_CON_QTY_DAYS,NOLOCK", where2)

                If chk_data.Rows.Count = 0 Then
                    Dim col_value(,) As String = {
                    {"DEPOT_ID", yRow.Item("DEPOT_ID")},
                    {"PRODUCT_ID", yRow.Item("PRODUCT_ID")},
                    {"BATCH_NUM", yRow.Item("BATCH_NUM")},
                    {"EFFECTIVE_DATE", CHECK_DATE.ToString("yyyy/MM/dd")},
                    {"CUSTOMER_ID", yRow.Item("CUSTOMER_ID")},
                    {"CUS_INV_QTY", pf.CheckNull(yRow.Item("CUS_INV_QTY"))},
                    {"CUS_DEPOT_QTY", pf.CheckNull(yRow.Item("CUS_DEPOT_QTY"))},
                    {"CUS_SALE_QTY", pf.CheckNull(yRow.Item("CUS_SALE_QTY"))},
                    {"CREATE_USER", "系統"},
                    {"CREATE_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                }
                    sSql = pf.GetInsertquery(table, col_value)
                    Console.WriteLine(sSql)
                    sqlCommand.CommandText = sSql
                    iRowAffected = sqlCommand.ExecuteNonQuery()
                    If iRowAffected = 0 Then
                        Return False
                    End If
                End If

                '計算每日結算庫存數
                '依照生效日-CUS_INV_QTY
                sSql = ""
                Dim New_Qty As Double = 0
                sSql = "SELECT [CUSTOMER_ID],[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM],SUM(ISNULL([CUS_DIFF_QTY],0)) as CUS_DIFF_QTY "
                sSql &= " FROM [D_DEPOT_STOCKS_DIFF] WITH (NOLOCK)  WHERE [EFFECTIVE_DATE]='" & CHECK_DATE.ToString("yyyy/MM/dd") & "'"
                sSql &= " AND [CUSTOMER_ID]='" & yRow.Item("CUSTOMER_ID").ToString & "'"
                sSql &= " AND [DEPOT_ID]='" & yRow.Item("DEPOT_ID").ToString & "'"
                sSql &= " AND [PRODUCT_ID]='" & yRow.Item("PRODUCT_ID").ToString & "'"
                sSql &= " AND [BATCH_NUM]='" & yRow.Item("BATCH_NUM").ToString & "'"
                sSql &= " AND ISNULL([CUS_DIFF_QTY],0)<>'0'"
                sSql &= " GROUP BY [CUSTOMER_ID],[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                Console.WriteLine(sSql)
                Dim qdt3 As New DataTable
                qdt3 = pf.GetDataTable(sSql)
                If qdt3.Rows.Count > 0 Then
                    New_Qty = Convert.ToDouble(pf.CheckNull(yRow.Item("CUS_INV_QTY"))) + Convert.ToDouble(pf.CheckNull(qdt3.Rows(0).Item("CUS_DIFF_QTY")))
                    Dim cus_table As String = "D_CUS_CON_QTY_DAYS"
                    Dim cus_pk2() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM", "EFFECTIVE_DATE", "CUSTOMER_ID"}
                    Dim cus_col_value2(,) As String = {
                    {"DEPOT_ID", yRow.Item("DEPOT_ID")},
                    {"PRODUCT_ID", yRow.Item("PRODUCT_ID")},
                    {"BATCH_NUM", yRow.Item("BATCH_NUM")},
                    {"EFFECTIVE_DATE", CHECK_DATE.ToString("yyyy/MM/dd")},
                    {"CUSTOMER_ID", yRow.Item("CUSTOMER_ID")},
                    {"CUS_INV_QTY", New_Qty},
                    {"ALTER_USER", "系統"},
                    {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                }
                    sSql = pf.GetUpdatequery(cus_table, cus_pk2, cus_col_value2)
                    Console.WriteLine(sSql)
                    sqlCommand.CommandText = sSql
                    iRowAffected = sqlCommand.ExecuteNonQuery()
                    If iRowAffected = 0 Then
                        Return False
                    End If
                Else
                    New_Qty = Convert.ToDouble(pf.CheckNull(yRow.Item("CUS_INV_QTY")))
                    Dim cus_table As String = "D_CUS_CON_QTY_DAYS"
                    Dim cus_pk2() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM", "EFFECTIVE_DATE", "CUSTOMER_ID"}
                    Dim cus_col_value2(,) As String = {
                    {"DEPOT_ID", yRow.Item("DEPOT_ID")},
                    {"PRODUCT_ID", yRow.Item("PRODUCT_ID")},
                    {"BATCH_NUM", yRow.Item("BATCH_NUM")},
                    {"EFFECTIVE_DATE", CHECK_DATE.ToString("yyyy/MM/dd")},
                    {"CUSTOMER_ID", yRow.Item("CUSTOMER_ID")},
                    {"CUS_INV_QTY", New_Qty},
                    {"ALTER_USER", "系統"},
                    {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                }
                    sSql = pf.GetUpdatequery(cus_table, cus_pk2, cus_col_value2)
                    Console.WriteLine(sSql)
                    sqlCommand.CommandText = sSql
                    iRowAffected = sqlCommand.ExecuteNonQuery()
                    If iRowAffected = 0 Then
                        Return False
                    End If
                End If

                '計算倉庫現有庫存數
                '依照覆核日CUS_DEPOT_QTY
                sSql = ""
                Dim New_Depot_Qty As Double = 0
                sSql = "SELECT [CUSTOMER_ID],[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM],SUM(ISNULL([CUS_DIFF_QTY],0)) as CUS_DIFF_QTY "
                sSql &= " FROM [D_DEPOT_STOCKS_DIFF] WITH (NOLOCK)  WHERE [CHANGE_DATE]='" & CHECK_DATE.ToString("yyyy/MM/dd") & "'"
                sSql &= " AND [CUSTOMER_ID]='" & yRow.Item("CUSTOMER_ID").ToString & "'"
                sSql &= " AND [DEPOT_ID]='" & yRow.Item("DEPOT_ID").ToString & "'"
                sSql &= " AND [PRODUCT_ID]='" & yRow.Item("PRODUCT_ID").ToString & "'"
                sSql &= " AND [BATCH_NUM]='" & yRow.Item("BATCH_NUM").ToString & "'"
                sSql &= " AND ISNULL([CUS_DIFF_QTY],0)<>'0'"
                sSql &= " GROUP BY [CUSTOMER_ID],[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                Console.WriteLine(sSql)
                Dim qdt4 As New DataTable
                qdt4 = pf.GetDataTable(sSql)
                If qdt4.Rows.Count > 0 Then
                    New_Depot_Qty = Convert.ToDouble(pf.CheckNull(yRow.Item("CUS_DEPOT_QTY"))) + Convert.ToDouble(pf.CheckNull(qdt4.Rows(0).Item("CUS_DIFF_QTY")))
                    Dim cus_table As String = "D_CUS_CON_QTY_DAYS"
                    Dim cus_pk2() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM", "EFFECTIVE_DATE", "CUSTOMER_ID"}
                    Dim cus_col_value2(,) As String = {
                    {"DEPOT_ID", yRow.Item("DEPOT_ID")},
                    {"PRODUCT_ID", yRow.Item("PRODUCT_ID")},
                    {"BATCH_NUM", yRow.Item("BATCH_NUM")},
                    {"EFFECTIVE_DATE", CHECK_DATE.ToString("yyyy/MM/dd")},
                    {"CUSTOMER_ID", yRow.Item("CUSTOMER_ID")},
                    {"CUS_DEPOT_QTY", New_Depot_Qty},
                    {"ALTER_USER", "系統"},
                    {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                }
                    sSql = pf.GetUpdatequery(cus_table, cus_pk2, cus_col_value2)
                    Console.WriteLine(sSql)
                    sqlCommand.CommandText = sSql
                    iRowAffected = sqlCommand.ExecuteNonQuery()
                    If iRowAffected = 0 Then
                        Return False
                    End If
                Else
                    New_Depot_Qty = Convert.ToDouble(pf.CheckNull(yRow.Item("CUS_DEPOT_QTY")))
                    Dim cus_table As String = "D_CUS_CON_QTY_DAYS"
                    Dim cus_pk2() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM", "EFFECTIVE_DATE", "CUSTOMER_ID"}
                    Dim cus_col_value2(,) As String = {
                    {"DEPOT_ID", yRow.Item("DEPOT_ID")},
                    {"PRODUCT_ID", yRow.Item("PRODUCT_ID")},
                    {"BATCH_NUM", yRow.Item("BATCH_NUM")},
                    {"EFFECTIVE_DATE", CHECK_DATE.ToString("yyyy/MM/dd")},
                    {"CUSTOMER_ID", yRow.Item("CUSTOMER_ID")},
                    {"CUS_DEPOT_QTY", New_Depot_Qty},
                    {"ALTER_USER", "系統"},
                    {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                }
                    sSql = pf.GetUpdatequery(cus_table, cus_pk2, cus_col_value2)
                    Console.WriteLine(sSql)
                    sqlCommand.CommandText = sSql
                    iRowAffected = sqlCommand.ExecuteNonQuery()
                    If iRowAffected = 0 Then
                        Return False
                    End If
                End If

                '------------------------------------------------------
                '結算即時可動支每日庫存
                '資料表：D_DEPOT_STOCKS_KEYIN_DIFF
                '寄售倉：CUS_DIFF_QTY
                '計算倉庫現有庫存數
                '依照覆核日CUS_DEPOT_QTY
                sSql = ""
                Dim New_Sale_Qty As Double = 0
                sSql = "SELECT [CUSTOMER_ID],[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM],SUM(ISNULL([CUS_DIFF_QTY],0)) as CUS_DIFF_QTY "
                sSql &= " FROM [D_DEPOT_STOCKS_KEYIN_DIFF] WITH (NOLOCK)  WHERE [CHANGE_DATE]='" & CHECK_DATE.ToString("yyyy/MM/dd") & "'"
                sSql &= " AND [CUSTOMER_ID]='" & yRow.Item("CUSTOMER_ID").ToString & "'"
                sSql &= " AND [DEPOT_ID]='" & yRow.Item("DEPOT_ID").ToString & "'"
                sSql &= " AND [PRODUCT_ID]='" & yRow.Item("PRODUCT_ID").ToString & "'"
                sSql &= " AND [BATCH_NUM]='" & yRow.Item("BATCH_NUM").ToString & "'"
                'sSql &= " AND ISNULL([CUS_DIFF_QTY],0)<>'0'"
                sSql &= " GROUP BY [CUSTOMER_ID],[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                Console.WriteLine(sSql)
                Dim qdt5 As New DataTable
                qdt5 = pf.GetDataTable(sSql)
                If qdt5.Rows.Count > 0 Then
                    New_Sale_Qty = Convert.ToDouble(pf.CheckNull(yRow.Item("CUS_SALE_QTY"))) + Convert.ToDouble(pf.CheckNull(qdt5.Rows(0).Item("CUS_DIFF_QTY")))
                    Dim cus_table As String = "D_CUS_CON_QTY_DAYS"
                    Dim cus_pk2() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM", "EFFECTIVE_DATE", "CUSTOMER_ID"}
                    Dim cus_col_value2(,) As String = {
                    {"DEPOT_ID", yRow.Item("DEPOT_ID")},
                    {"PRODUCT_ID", yRow.Item("PRODUCT_ID")},
                    {"BATCH_NUM", yRow.Item("BATCH_NUM")},
                    {"EFFECTIVE_DATE", CHECK_DATE.ToString("yyyy/MM/dd")},
                    {"CUSTOMER_ID", yRow.Item("CUSTOMER_ID")},
                    {"CUS_SALE_QTY", New_Sale_Qty},
                    {"ALTER_USER", "系統"},
                    {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                }
                    sSql = pf.GetUpdatequery(cus_table, cus_pk2, cus_col_value2)
                    Console.WriteLine(sSql)
                    sqlCommand.CommandText = sSql
                    iRowAffected = sqlCommand.ExecuteNonQuery()
                    If iRowAffected = 0 Then
                        Return False
                    End If
                Else
                    New_Sale_Qty = Convert.ToDouble(pf.CheckNull(yRow.Item("CUS_SALE_QTY")))
                    Dim cus_table As String = "D_CUS_CON_QTY_DAYS"
                    Dim cus_pk2() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM", "EFFECTIVE_DATE", "CUSTOMER_ID"}
                    Dim cus_col_value2(,) As String = {
                    {"DEPOT_ID", yRow.Item("DEPOT_ID")},
                    {"PRODUCT_ID", yRow.Item("PRODUCT_ID")},
                    {"BATCH_NUM", yRow.Item("BATCH_NUM")},
                    {"EFFECTIVE_DATE", CHECK_DATE.ToString("yyyy/MM/dd")},
                    {"CUSTOMER_ID", yRow.Item("CUSTOMER_ID")},
                    {"CUS_SALE_QTY", New_Sale_Qty},
                    {"ALTER_USER", "系統"},
                    {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                }
                    sSql = pf.GetUpdatequery(cus_table, cus_pk2, cus_col_value2)
                    Console.WriteLine(sSql)
                    sqlCommand.CommandText = sSql
                    iRowAffected = sqlCommand.ExecuteNonQuery()
                    If iRowAffected = 0 Then
                        Return False
                    End If
                End If
                '------------------------------------------------------
            Next
        Catch ex As Exception
            pf.MyError(ex.ToString)
            Return False
        End Try
        Return True
    End Function

    '檢查經銷商庫存每日檔
    Public Function CHK_D_DEALER_CON_QTY_DAYS() As Boolean
        Dim conn_str As String
        conn_str = My.Settings.HD_ERP_DBConnection
        Dim conn As New SqlConnection(conn_str)
        Dim iRowAffectedA As Integer = 0 '接收更新資料A後的影響筆數
        Dim iRowAffectedB As Integer = 0 '接收更新資料B後的影響筆數
        Dim iRowAffectedC As Integer = 0 '接收更新資料C後的影響筆數
        Dim SaveComplete As Boolean = True
        Dim sSql As String = ""
        '開啟連接物件
        conn.Open()
        '啟用交易紀錄
        Dim transaction = conn.BeginTransaction
        Try
            Dim Command = conn.CreateCommand
            Command.Connection = conn
            Command.Transaction = transaction

            Dim chk_dt As New DataTable
            chk_dt = pf.GetDB("D_DEALER_CON_QTY_DAYS,NOLOCK", {})
            If chk_dt.Rows.Count = 0 Then
                Dim cus_qdt As New DataTable
                cus_qdt = pf.GetDB("D_DEALER_CON_QTY,NOLOCK", {})
                If cus_qdt.Rows.Count > 0 Then
                    Dim yRow As DataRow
                    For j As Integer = 0 To cus_qdt.Rows.Count - 1
                        yRow = cus_qdt.Rows(j)
                        Dim cus_table As String = "D_DEALER_CON_QTY_DAYS"
                        Dim cus_col_value(,) As String = {
                            {"DEPOT_ID", yRow.Item("DEPOT_ID")},
                            {"PRODUCT_ID", yRow.Item("PRODUCT_ID")},
                            {"BATCH_NUM", yRow.Item("BATCH_NUM")},
                            {"EFFECTIVE_DATE", DateTime.Now.AddDays(-1).ToString("yyyy/MM/dd")},
                            {"SUPPLIER_ID", yRow.Item("SUPPLIER_ID")},
                            {"CON_INV_QTY", pf.CheckNull(yRow.Item("CON_INV_QTY"))},
                            {"CON_DEPOT_QTY", pf.CheckNull(yRow.Item("CON_DEPOT_QTY"))},
                            {"CON_SALE_QTY", pf.CheckNull(yRow.Item("CON_SALE_QTY"))},
                            {"CREATE_USER", "系統"},
                            {"CREATE_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                        }
                        sSql = pf.GetInsertquery(cus_table, cus_col_value)
                        Console.WriteLine(sSql)
                        Command.CommandText = sSql
                        iRowAffectedB = Command.ExecuteNonQuery()
                        If iRowAffectedB = 0 Then
                            SaveComplete = False
                            transaction.Rollback()
                        End If
                    Next
                End If
                If SaveComplete = True Then
                    transaction.Commit()
                End If
                Return SaveComplete
            Else
                '有資料
                'sSql = "SELECT TOP 1 CONVERT(varchar,[EFFECTIVE_DATE],111) FROM [D_DEALER_CON_QTY_DAYS] WITH (NOLOCK) ORDER BY CONVERT(varchar,[EFFECTIVE_DATE],111) DESC "
                'Dim qdt As New DataTable
                'qdt = pf.GetDataTable(sSql)
                Dim Str As String = ""
                Str = "SELECT [SUPPLIER_ID],[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM],MAX([EFFECTIVE_DATE]) AS EFFECTIVE_DATE"
                Str &= " From [D_DEALER_CON_QTY_DAYS]  WITH (NOLOCK)"
                Str &= " GROUP BY [SUPPLIER_ID],[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                Str &= " HAVING MAX([EFFECTIVE_DATE])<'" & DateTime.Now.ToString("yyyy/MM/dd") & "'"
                Dim qdt As New DataTable
                qdt = pf.GetDataTable(Str)
                If qdt.Rows.Count > 0 Then
                    Dim start_date As New DateTime
                    Dim end_date As New DateTime
                    end_date = DateTime.Now
                    Dim adays As Integer = 1
                    Dim chk_date As New DateTime
                    Dim a As Integer = 0
                    For a = 0 To qdt.Rows.Count - 1
                        start_date = Convert.ToDateTime(qdt.Rows(a).Item("EFFECTIVE_DATE"))
                        adays = 1
                        chk_date = start_date.AddDays(adays)
                        Dim table As String = "D_DEALER_CON_QTY_DAYS"
                        Do While chk_date.ToString("yyyy/MM/dd") < end_date.ToString("yyyy/MM/dd")
                            'If UPDATE_D_DEALER_CON_QTY_DAYS(chk_date, Command) = False Then
                            '    transaction.Rollback()
                            '    Exit Do
                            'End If
                            If UPDATE_D_DEALER_CON_QTY_DAYS_BY_SDPB(chk_date, qdt.Rows(a).Item("SUPPLIER_ID"), qdt.Rows(a).Item("DEPOT_ID"), qdt.Rows(a).Item("PRODUCT_ID"), qdt.Rows(a).Item("BATCH_NUM"), Command) = False Then
                                transaction.Rollback()
                                Exit Do
                            End If
                            If SaveComplete = True Then
                                Dim k As Integer = 0
                                '生效日小於計算日要從生效日重新計算庫存數直到計算日
                                sSql = "SELECT [SUPPLIER_ID],[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM],[EFFECTIVE_DATE] FROM [D_DEPOT_STOCKS_DIFF] WITH (NOLOCK)"
                                sSql &= " WHERE CONVERT(VARCHAR,[EFFECTIVE_DATE],111)<'" & chk_date.ToString("yyyy/MM/dd") & "'"
                                sSql &= " AND CONVERT(VARCHAR,[CHANGE_DATE],111)='" & chk_date.ToString("yyyy/MM/dd") & "'"
                                sSql &= " AND SUPPLIER_ID='" & qdt.Rows(a).Item("SUPPLIER_ID") & "'"
                                sSql &= " AND DEPOT_ID='" & qdt.Rows(a).Item("DEPOT_ID") & "'"
                                sSql &= " AND PRODUCT_ID='" & qdt.Rows(a).Item("PRODUCT_ID") & "'"
                                sSql &= " AND BATCH_NUM='" & qdt.Rows(a).Item("BATCH_NUM") & "'"
                                'sSql &= " AND ISNULL([SUPPLIER_DIFF_QTY],0)<>'0'"
                                sSql &= " GROUP BY [SUPPLIER_ID],[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM],[EFFECTIVE_DATE]"
                                sSql &= " ORDER BY [EFFECTIVE_DATE]"
                                Dim other_dt As New DataTable
                                other_dt = pf.GetDataTable(sSql)
                                If other_dt.Rows.Count > 0 Then
                                    For k = 0 To other_dt.Rows.Count - 1
                                        '建立生效日的每日庫存的資料並且計算
                                        If UPDATE_D_DEALER_CON_QTY_DAYS_BY_SDPB(other_dt.Rows(k).Item("EFFECTIVE_DATE"), other_dt.Rows(k).Item("SUPPLIER_ID"), other_dt.Rows(k).Item("DEPOT_ID"), other_dt.Rows(k).Item("PRODUCT_ID"), other_dt.Rows(k).Item("BATCH_NUM"), Command) = False Then
                                            transaction.Rollback()
                                            SaveComplete = False
                                            Return SaveComplete
                                            Exit Do
                                        End If
                                        'If UPDATE_D_DEALER_CON_QTY_DAYS(other_dt.Rows(k).Item("EFFECTIVE_DATE"), Command) = False Then
                                        '    transaction.Rollback()
                                        '    SaveComplete = False
                                        '    Return SaveComplete
                                        '    Exit Do
                                        'End If
                                    Next
                                End If
                            End If

                            adays = adays + 1
                            chk_date = start_date.AddDays(adays)
                        Loop
                    Next
                End If

            End If
            Command.Dispose()
            If SaveComplete = True Then
                transaction.Commit()
            End If

        Catch ex As Exception
            pf.MyError(ex.ToString)
            transaction.Rollback()
            Return False
        Finally
            conn.Close()
            conn.Dispose()
        End Try

        Return True

    End Function

    '計算經銷商每日庫存檔的庫存數量
    Public Function UPDATE_D_DEALER_CON_QTY_DAYS(CHECK_DATE As Date, sqlCommand As SqlCommand) As Boolean
        Dim sSql As String = ""
        Dim table As String = "D_DEALER_CON_QTY_DAYS"
        Dim yRow As DataRow
        Dim iRowAffected As Integer = 0
        Try
            sSql = "SELECT * FROM [D_DEALER_CON_QTY_DAYS] WITH (NOLOCK) WHERE CONVERT(varchar,[EFFECTIVE_DATE],111)='" & CHECK_DATE.AddDays(-1).ToString("yyyy/MM/dd") & "'"
            sSql &= " ORDER BY [SUPPLIER_ID],[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
            Dim qdt2 As New DataTable
            qdt2 = pf.GetDataTable(sSql)
            For j = 0 To qdt2.Rows.Count - 1
                yRow = qdt2.Rows(j)
                '判斷資料是否存在
                sSql = "SELECT * FROM [D_DEALER_CON_QTY_DAYS] WITH (NOLOCK)"
                sSql &= " WHERE Convert(varchar, [EFFECTIVE_DATE], 111) ='" & CHECK_DATE.ToString("yyyy/MM/dd") & "'"
                sSql &= " AND [DEPOT_ID]='" & yRow.Item("DEPOT_ID").ToString & "'"
                sSql &= " AND [PRODUCT_ID]='" & yRow.Item("PRODUCT_ID").ToString & "'"
                sSql &= " AND [BATCH_NUM]='" & yRow.Item("BATCH_NUM").ToString & "'"
                sSql &= " AND [SUPPLIER_ID]='" & yRow.Item("SUPPLIER_ID").ToString & "'"
                Dim chk_data As New DataTable
                chk_data = pf.GetDataTable(sSql)
                If chk_data.Rows.Count = 0 Then
                    Dim col_value(,) As String = {
                        {"DEPOT_ID", yRow.Item("DEPOT_ID")},
                        {"PRODUCT_ID", yRow.Item("PRODUCT_ID")},
                        {"BATCH_NUM", yRow.Item("BATCH_NUM")},
                        {"EFFECTIVE_DATE", CHECK_DATE.ToString("yyyy/MM/dd")},
                        {"SUPPLIER_ID", yRow.Item("SUPPLIER_ID")},
                        {"CON_INV_QTY", pf.CheckNull(yRow.Item("CON_INV_QTY"))},
                        {"CON_DEPOT_QTY", pf.CheckNull(yRow.Item("CON_DEPOT_QTY"))},
                        {"CON_SALE_QTY", pf.CheckNull(yRow.Item("CON_SALE_QTY"))},
                        {"CREATE_USER", "系統"},
                        {"CREATE_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                    }
                    sSql = pf.GetInsertquery(table, col_value)
                    Console.WriteLine(sSql)
                    sqlCommand.CommandText = sSql
                    iRowAffected = sqlCommand.ExecuteNonQuery()
                    If iRowAffected = 0 Then
                        Return False
                    End If
                End If

                '計算異動每日結算庫存數
                '依照生效日-CON_INV_QTY
                sSql = ""
                Dim New_Inv_Qty As Double = 0
                sSql = "SELECT [SUPPLIER_ID],[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM],SUM(ISNULL([SUPPLIER_DIFF_QTY],0)) as SUPPLIER_DIFF_QTY "
                sSql &= " FROM [D_DEPOT_STOCKS_DIFF] WITH (NOLOCK)  WHERE [EFFECTIVE_DATE]='" & CHECK_DATE.ToString("yyyy/MM/dd") & "'"
                sSql &= " AND [SUPPLIER_ID]='" & yRow.Item("SUPPLIER_ID").ToString & "'"
                sSql &= " AND [DEPOT_ID]='" & yRow.Item("DEPOT_ID").ToString & "'"
                sSql &= " AND [PRODUCT_ID]='" & yRow.Item("PRODUCT_ID").ToString & "'"
                sSql &= " AND [BATCH_NUM]='" & yRow.Item("BATCH_NUM").ToString & "'"
                'sSql &= " AND ISNULL([SUPPLIER_DIFF_QTY],0)<>'0'"
                sSql &= " GROUP BY [SUPPLIER_ID],[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                Console.WriteLine(sSql)
                Dim qdt3 As New DataTable
                qdt3 = pf.GetDataTable(sSql)
                If qdt3.Rows.Count > 0 Then
                    New_Inv_Qty = Convert.ToDouble(pf.CheckNull(yRow.Item("CON_INV_QTY"))) + Convert.ToDouble(pf.CheckNull(qdt3.Rows(0).Item("SUPPLIER_DIFF_QTY")))
                    Dim dealer_pk2() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM", "EFFECTIVE_DATE", "SUPPLIER_ID"}
                    Dim dealer_col_value2(,) As String = {
                        {"DEPOT_ID", yRow.Item("DEPOT_ID")},
                        {"PRODUCT_ID", yRow.Item("PRODUCT_ID")},
                        {"BATCH_NUM", yRow.Item("BATCH_NUM")},
                        {"EFFECTIVE_DATE", CHECK_DATE.ToString("yyyy/MM/dd")},
                        {"SUPPLIER_ID", yRow.Item("SUPPLIER_ID")},
                        {"CON_INV_QTY", New_Inv_Qty},
                        {"ALTER_USER", "系統"},
                        {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                    }
                    sSql = pf.GetUpdatequery(table, dealer_pk2, dealer_col_value2)
                    Console.WriteLine(sSql)
                    sqlCommand.CommandText = sSql
                    iRowAffected = sqlCommand.ExecuteNonQuery()
                    If iRowAffected = 0 Then
                        Return False
                    End If
                Else
                    New_Inv_Qty = Convert.ToDouble(pf.CheckNull(yRow.Item("CON_INV_QTY")))
                    Dim dealer_pk2() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM", "EFFECTIVE_DATE", "SUPPLIER_ID"}
                    Dim dealer_col_value2(,) As String = {
                        {"DEPOT_ID", yRow.Item("DEPOT_ID")},
                        {"PRODUCT_ID", yRow.Item("PRODUCT_ID")},
                        {"BATCH_NUM", yRow.Item("BATCH_NUM")},
                        {"EFFECTIVE_DATE", CHECK_DATE.ToString("yyyy/MM/dd")},
                        {"SUPPLIER_ID", yRow.Item("SUPPLIER_ID")},
                        {"CON_INV_QTY", New_Inv_Qty},
                        {"ALTER_USER", "系統"},
                        {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                    }
                    sSql = pf.GetUpdatequery(table, dealer_pk2, dealer_col_value2)
                    Console.WriteLine(sSql)
                    sqlCommand.CommandText = sSql
                    iRowAffected = sqlCommand.ExecuteNonQuery()
                    If iRowAffected = 0 Then
                        Return False
                    End If
                End If
                '更新經銷商倉
                Dim dt As DataTable = pf.GetDB("D_DEPOT_BASE,NOLOCK", {{"DEPOT_ID", yRow.Item("DEPOT_ID")}})
                If dt.Rows.Count > 0 Then
                    If dt.Rows(0).Item("DEPOT_TYPE") = "經銷商" Then
                        Dim table2 As String = "D_DEPOT_STOCKS_DAYS"
                        Dim pk2() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM", "EFFECTIVE_DATE"}
                        Dim col_value2(,) As String = {
                            {"DEPOT_ID", yRow.Item("DEPOT_ID")},
                            {"PRODUCT_ID", yRow.Item("PRODUCT_ID")},
                            {"BATCH_NUM", yRow.Item("BATCH_NUM")},
                            {"EFFECTIVE_DATE", CHECK_DATE.ToString("yyyy/MM/dd")},
                            {"SUPPLIER_ID", yRow.Item("SUPPLIER_ID")},
                            {"INV_QTY", New_Inv_Qty},
                            {"ALTER_USER", "系統"},
                            {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                        }
                        sSql = pf.GetUpdatequery(table2, pk2, col_value2)
                        Console.WriteLine(sSql)
                        sqlCommand.CommandText = sSql
                        iRowAffected = sqlCommand.ExecuteNonQuery()
                        If iRowAffected = 0 Then
                            Return False
                        End If
                    End If
                End If
                '計算異動倉庫現有庫存數
                '依照覆核日
                sSql = ""
                Dim New_Depot_Qty As Double = 0
                sSql = "SELECT [SUPPLIER_ID],[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM],SUM(ISNULL([SUPPLIER_DIFF_QTY],0)) as SUPPLIER_DIFF_QTY "
                sSql &= " FROM [D_DEPOT_STOCKS_DIFF] WITH (NOLOCK)  WHERE [CHANGE_DATE]='" & CHECK_DATE.ToString("yyyy/MM/dd") & "'"
                sSql &= " AND [SUPPLIER_ID]='" & yRow.Item("SUPPLIER_ID").ToString & "'"
                sSql &= " AND [DEPOT_ID]='" & yRow.Item("DEPOT_ID").ToString & "'"
                sSql &= " AND [PRODUCT_ID]='" & yRow.Item("PRODUCT_ID").ToString & "'"
                sSql &= " AND [BATCH_NUM]='" & yRow.Item("BATCH_NUM").ToString & "'"
                sSql &= " AND ISNULL([SUPPLIER_DIFF_QTY],0)<>'0'"
                sSql &= " GROUP BY [SUPPLIER_ID],[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                Console.WriteLine(sSql)
                Dim qdt4 As New DataTable
                qdt4 = pf.GetDataTable(sSql)
                If qdt4.Rows.Count > 0 Then
                    New_Depot_Qty = Convert.ToDouble(pf.CheckNull(yRow.Item("CON_DEPOT_QTY"))) + Convert.ToDouble(pf.CheckNull(qdt4.Rows(0).Item("SUPPLIER_DIFF_QTY")))
                    Dim dealer_pk2() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM", "EFFECTIVE_DATE", "SUPPLIER_ID"}
                    Dim dealer_col_value2(,) As String = {
                        {"DEPOT_ID", yRow.Item("DEPOT_ID")},
                        {"PRODUCT_ID", yRow.Item("PRODUCT_ID")},
                        {"BATCH_NUM", yRow.Item("BATCH_NUM")},
                        {"EFFECTIVE_DATE", CHECK_DATE.ToString("yyyy/MM/dd")},
                        {"SUPPLIER_ID", yRow.Item("SUPPLIER_ID")},
                        {"CON_DEPOT_QTY", New_Depot_Qty},
                        {"ALTER_USER", "系統"},
                        {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                    }
                    sSql = pf.GetUpdatequery(table, dealer_pk2, dealer_col_value2)
                    Console.WriteLine(sSql)
                    sqlCommand.CommandText = sSql
                    iRowAffected = sqlCommand.ExecuteNonQuery()
                    If iRowAffected = 0 Then
                        Return False
                    End If
                Else
                    New_Depot_Qty = Convert.ToDouble(pf.CheckNull(yRow.Item("CON_DEPOT_QTY")))
                    Dim dealer_pk2() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM", "EFFECTIVE_DATE", "SUPPLIER_ID"}
                    Dim dealer_col_value2(,) As String = {
                        {"DEPOT_ID", yRow.Item("DEPOT_ID")},
                        {"PRODUCT_ID", yRow.Item("PRODUCT_ID")},
                        {"BATCH_NUM", yRow.Item("BATCH_NUM")},
                        {"EFFECTIVE_DATE", CHECK_DATE.ToString("yyyy/MM/dd")},
                        {"SUPPLIER_ID", yRow.Item("SUPPLIER_ID")},
                        {"CON_DEPOT_QTY", New_Depot_Qty},
                        {"ALTER_USER", "系統"},
                        {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                    }
                    sSql = pf.GetUpdatequery(table, dealer_pk2, dealer_col_value2)
                    Console.WriteLine(sSql)
                    sqlCommand.CommandText = sSql
                    iRowAffected = sqlCommand.ExecuteNonQuery()
                    If iRowAffected = 0 Then
                        Return False
                    End If
                End If
                '更新經銷商倉
                Dim dt2 As DataTable = pf.GetDB("D_DEPOT_BASE,NOLOCK", {{"DEPOT_ID", yRow.Item("DEPOT_ID")}})
                If dt2.Rows.Count > 0 Then
                    If dt2.Rows(0).Item("DEPOT_TYPE") = "經銷商" Then
                        Dim table2 As String = "D_DEPOT_STOCKS_DAYS"
                        Dim pk2() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM", "EFFECTIVE_DATE"}
                        Dim col_value2(,) As String = {
                            {"DEPOT_ID", yRow.Item("DEPOT_ID")},
                            {"PRODUCT_ID", yRow.Item("PRODUCT_ID")},
                            {"BATCH_NUM", yRow.Item("BATCH_NUM")},
                            {"EFFECTIVE_DATE", CHECK_DATE.ToString("yyyy/MM/dd")},
                            {"SUPPLIER_ID", yRow.Item("SUPPLIER_ID")},
                            {"GOOD_QTY", New_Depot_Qty},
                            {"DEPOT_QTY", New_Depot_Qty},
                            {"ALTER_USER", "系統"},
                            {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                        }
                        sSql = pf.GetUpdatequery(table2, pk2, col_value2)
                        Console.WriteLine(sSql)
                        sqlCommand.CommandText = sSql
                        iRowAffected = sqlCommand.ExecuteNonQuery()
                        If iRowAffected = 0 Then
                            Return False
                        End If
                    End If
                End If

                '即時可動支庫存
                '計算異動倉庫現有庫存數
                '依照覆核日
                sSql = ""
                Dim New_Sale_Qty As Double = 0
                sSql = "SELECT [SUPPLIER_ID],[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM],SUM(ISNULL([SUPPLIER_DIFF_QTY],0)) as SUPPLIER_DIFF_QTY "
                sSql &= " FROM [D_DEPOT_STOCKS_KEYIN_DIFF] WITH (NOLOCK)  WHERE [CHANGE_DATE]='" & CHECK_DATE.ToString("yyyy/MM/dd") & "'"
                sSql &= " AND [SUPPLIER_ID]='" & yRow.Item("SUPPLIER_ID").ToString & "'"
                sSql &= " AND [DEPOT_ID]='" & yRow.Item("DEPOT_ID").ToString & "'"
                sSql &= " AND [PRODUCT_ID]='" & yRow.Item("PRODUCT_ID").ToString & "'"
                sSql &= " AND [BATCH_NUM]='" & yRow.Item("BATCH_NUM").ToString & "'"
                sSql &= " AND ISNULL([SUPPLIER_DIFF_QTY],0)<>'0'"
                sSql &= " GROUP BY [SUPPLIER_ID],[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                Console.WriteLine(sSql)
                Dim qdt5 As New DataTable
                qdt5 = pf.GetDataTable(sSql)
                If qdt5.Rows.Count > 0 Then
                    New_Sale_Qty = Convert.ToDouble(pf.CheckNull(yRow.Item("CON_SALE_QTY"))) + Convert.ToDouble(pf.CheckNull(qdt5.Rows(0).Item("SUPPLIER_DIFF_QTY")))
                    Dim dealer_pk2() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM", "EFFECTIVE_DATE", "SUPPLIER_ID"}
                    Dim dealer_col_value2(,) As String = {
                        {"DEPOT_ID", yRow.Item("DEPOT_ID")},
                        {"PRODUCT_ID", yRow.Item("PRODUCT_ID")},
                        {"BATCH_NUM", yRow.Item("BATCH_NUM")},
                        {"EFFECTIVE_DATE", CHECK_DATE.ToString("yyyy/MM/dd")},
                        {"SUPPLIER_ID", yRow.Item("SUPPLIER_ID")},
                        {"CON_SALE_QTY", New_Sale_Qty},
                        {"ALTER_USER", "系統"},
                        {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                    }
                    sSql = pf.GetUpdatequery(table, dealer_pk2, dealer_col_value2)
                    Console.WriteLine(sSql)
                    sqlCommand.CommandText = sSql
                    iRowAffected = sqlCommand.ExecuteNonQuery()
                    If iRowAffected = 0 Then
                        Return False
                    End If
                Else
                    New_Sale_Qty = Convert.ToDouble(pf.CheckNull(yRow.Item("CON_SALE_QTY")))
                    Dim dealer_pk2() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM", "EFFECTIVE_DATE", "SUPPLIER_ID"}
                    Dim dealer_col_value2(,) As String = {
                        {"DEPOT_ID", yRow.Item("DEPOT_ID")},
                        {"PRODUCT_ID", yRow.Item("PRODUCT_ID")},
                        {"BATCH_NUM", yRow.Item("BATCH_NUM")},
                        {"EFFECTIVE_DATE", CHECK_DATE.ToString("yyyy/MM/dd")},
                        {"SUPPLIER_ID", yRow.Item("SUPPLIER_ID")},
                        {"CON_SALE_QTY", New_Sale_Qty},
                        {"ALTER_USER", "系統"},
                        {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                    }
                    sSql = pf.GetUpdatequery(table, dealer_pk2, dealer_col_value2)
                    Console.WriteLine(sSql)
                    sqlCommand.CommandText = sSql
                    iRowAffected = sqlCommand.ExecuteNonQuery()
                    If iRowAffected = 0 Then
                        Return False
                    End If
                End If
                '更新經銷商倉
                Dim dt3 As DataTable = pf.GetDB("D_DEPOT_BASE,NOLOCK", {{"DEPOT_ID", yRow.Item("DEPOT_ID")}})
                If dt3.Rows.Count > 0 Then
                    If dt3.Rows(0).Item("DEPOT_TYPE") = "經銷商" Then
                        Dim table2 As String = "D_DEPOT_STOCKS_DAYS"
                        Dim pk2() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM", "EFFECTIVE_DATE"}
                        Dim col_value2(,) As String = {
                            {"DEPOT_ID", yRow.Item("DEPOT_ID")},
                            {"PRODUCT_ID", yRow.Item("PRODUCT_ID")},
                            {"BATCH_NUM", yRow.Item("BATCH_NUM")},
                            {"EFFECTIVE_DATE", CHECK_DATE.ToString("yyyy/MM/dd")},
                            {"SUPPLIER_ID", yRow.Item("SUPPLIER_ID")},
                            {"SALE_QTY", New_Sale_Qty},
                            {"ALTER_USER", "系統"},
                            {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                        }
                        sSql = pf.GetUpdatequery(table2, pk2, col_value2)
                        Console.WriteLine(sSql)
                        sqlCommand.CommandText = sSql
                        iRowAffected = sqlCommand.ExecuteNonQuery()
                        If iRowAffected = 0 Then
                            Return False
                        End If
                    End If
                End If
            Next
        Catch ex As Exception
            pf.MyError(ex.ToString)
            Return False
        End Try
        Return True
    End Function

    '異動出庫單的倉庫實際庫存數量DEPOT_QTY[當日庫存][看覆核日]
    '計算公式：庫存數量=前一日的期末庫存數量+覆核日=當日的庫存異動數量
    '更新到[D_DEPOT_STOCKS]
    'STOCK_OUT_ID:出庫單編號
    Function UPDATE_D_DEPOT_STOCKS_OUT(STOCK_OUT_ID As String, command As SqlCommand) As Boolean
        Dim sSql As String = ""
        Dim iAffectRows As Integer = 0
        Dim PAPER_FROM As String = ""
        Try
            Dim mdt As New DataTable
            Dim ddt As New DataTable
            '出庫單主檔資料
            mdt = pf.GetDB("D_STOCK_OUT_MASTER,NOLOCK", {{"STOCK_OUT_ID", STOCK_OUT_ID}})
            Dim xRow As DataRow
            xRow = mdt.Rows(0)
            PAPER_FROM = xRow.Item("PAPER_FROM")
            '生效日
            Dim EFFECTIVE_DATE As Date = Convert.ToDateTime(xRow.Item("EFFECTIVE_DATE")).ToString("yyyy/MM/dd")
            '覆核日
            Dim CHANGE_DATE As Date = Nothing
            If xRow.Item("VOUCHER_STATUS").ToString = "確認" Then
                '覆核日
                CHANGE_DATE = Convert.ToDateTime(xRow.Item("VOUCHER_TIME")).ToString("yyyy/MM/dd")
            ElseIf xRow.Item("VOUCHER_STATUS").ToString = "生效" Or xRow.Item("VOUCHER_STATUS").ToString = "作廢" Then
                '異動日期
                CHANGE_DATE = Convert.ToDateTime(xRow.Item("ALTER_TIME")).ToString("yyyy/MM/dd")
            End If

            '單據狀態:銷貨單、銷貨換出單、進退單、供應商產品換出單，異動倉庫現有庫存的日期以出庫確認的出庫日期為準 
            '出庫確認=1 出庫日期
            If PAPER_FROM = "銷貨單" Or PAPER_FROM = "銷貨換出單" Or PAPER_FROM = "進退單" Or PAPER_FROM = "供應商產品換出單" Then
                If xRow.Item("VOUCHER_STATUS").ToString = "生效" Then
                    If DBNull.Value.Equals(xRow.Item("ALTER_TIME")) Then
                        CHANGE_DATE = Convert.ToDateTime(xRow.Item("CREATE_TIME")).ToString("yyyy/MM/dd")
                    Else
                        CHANGE_DATE = Convert.ToDateTime(xRow.Item("ALTER_TIME")).ToString("yyyy/MM/dd")
                    End If
                ElseIf xRow.Item("VOUCHER_STATUS").ToString = "確認" Then
                    'CHANGE_DATE = Convert.ToDateTime(xRow.Item("ALTER_TIME")).ToString("yyyy/MM/dd")
                    If pf.CheckNull(xRow.Item("OUTBOUND_CHECK")) = 1 Then
                        CHANGE_DATE = Convert.ToDateTime(xRow.Item("OUTBOUND_CHECK_DATE")).ToString("yyyy/MM/dd")
                    ElseIf pf.CheckNull(xRow.Item("OUTBOUND_CHECK")) = 0 Then
                        CHANGE_DATE = Convert.ToDateTime(xRow.Item("ALTER_TIME")).ToString("yyyy/MM/dd")
                    End If
                ElseIf xRow.Item("VOUCHER_STATUS").ToString = "作廢" Then
                    CHANGE_DATE = Convert.ToDateTime(xRow.Item("ALTER_TIME")).ToString("yyyy/MM/dd")
                End If
            End If

            '單據狀態:盤整單、寄售盤整單，異動倉庫現有庫存的日期以盤整日期為準 
            If PAPER_FROM = "盤整單" Or PAPER_FROM = "寄售盤整單" Then
                If xRow.Item("VOUCHER_STATUS").ToString = "確認" Then
                    '覆核日
                    CHANGE_DATE = Convert.ToDateTime(xRow.Item("EFFECTIVE_DATE")).ToString("yyyy/MM/dd")
                ElseIf xRow.Item("VOUCHER_STATUS").ToString = "生效" Or xRow.Item("VOUCHER_STATUS").ToString = "作廢" Then
                    '異動日期
                    CHANGE_DATE = Convert.ToDateTime(xRow.Item("ALTER_TIME")).ToString("yyyy/MM/dd")
                End If
            End If
            '將明細寫入[D_DEPOT_STOCKS_DIFF]，然後再統一計算庫存異動數量
            ddt = pf.GetDB("D_STOCK_OUT_DETAIL,NOLOCK", {{"STOCK_OUT_ID", STOCK_OUT_ID}})
            Dim table As String = "D_DEPOT_STOCKS_DIFF"
            For j As Integer = 0 To ddt.Rows.Count - 1
                Dim yRow As DataRow
                yRow = ddt.Rows(j)
                Dim total_qty As Double = 0
                Dim good_qty As Double = 0
                Dim bad_qty As Double = 0
                If pf.CheckIfDBNull(yRow.Item("STOCK_CHECK")) = 1 Then
                    '出庫確認
                    total_qty = (Convert.ToDouble(pf.CheckNull(yRow.Item("QTY"))) + Convert.ToDouble(pf.CheckNull(yRow.Item("PRESENT_QTY")))) * (-1)
                    good_qty = (Convert.ToDouble(pf.CheckNull(yRow.Item("QTY"))) + Convert.ToDouble(pf.CheckNull(yRow.Item("PRESENT_QTY")))) * (-1)
                    bad_qty = Convert.ToDouble(pf.CheckNull(pf.CheckNull(yRow.Item("BAD_QTY"))) * (-1))

                ElseIf pf.CheckIfDBNull(yRow.Item("STOCK_CHECK")) = 0 Then
                    '取消出庫確認
                    total_qty = (Convert.ToDouble(pf.CheckNull(yRow.Item("QTY"))) + Convert.ToDouble(pf.CheckNull(yRow.Item("PRESENT_QTY"))))
                    good_qty = (Convert.ToDouble(pf.CheckNull(yRow.Item("QTY"))) + Convert.ToDouble(pf.CheckNull(yRow.Item("PRESENT_QTY"))))
                    bad_qty = Convert.ToDouble(pf.CheckNull(yRow.Item("BAD_QTY")))
                End If
                If xRow.Item("STOCK_TYPE").ToString = "公司" Then
                    Dim col_value(,) As String = {
                        {"PAPER_ID", pf.CheckStringDBNull(yRow.Item("STOCK_OUT_ID"))},
                        {"EFFECTIVE_DATE", xRow.Item("EFFECTIVE_DATE")},
                        {"CHANGE_DATE", CHANGE_DATE},
                        {"DEPOT_ID", pf.CheckStringDBNull(yRow.Item("DEPOT_ID"))},
                        {"PRODUCT_ID", pf.CheckStringDBNull(yRow.Item("PRODUCT_ID"))},
                        {"BATCH_NUM", pf.CheckStringDBNull(yRow.Item("BATCH_NUM"))},
                        {"G_DIFF_QTY", good_qty},
                        {"B_DIFF_QTY", bad_qty},
                        {"C_DIFF_QTY", 0},
                        {"CUS_DIFF_QTY", 0},
                        {"SUPPLIER_DIFF_QTY", 0},
                        {"VOUCHER_STATUS", pf.CheckStringDBNull(xRow.Item("VOUCHER_STATUS"))},
                        {"CUSTOMER_ID", pf.CheckStringDBNull(xRow.Item("CUSTOMER_ID"))},
                        {"SUPPLIER_ID", pf.CheckStringDBNull(xRow.Item("SUPPLIER_ID"))},
                        {"CREATE_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                    }
                    Console.WriteLine(pf.GetInsertquery(table, col_value))
                    sSql = pf.GetInsertquery(table, col_value)

                ElseIf xRow.Item("STOCK_TYPE").ToString = "寄售" Then

                    Dim col_value(,) As String = {
                        {"PAPER_ID", pf.CheckStringDBNull(yRow.Item("STOCK_OUT_ID"))},
                        {"EFFECTIVE_DATE", xRow.Item("EFFECTIVE_DATE")},
                        {"CHANGE_DATE", CHANGE_DATE},
                        {"DEPOT_ID", pf.CheckStringDBNull(yRow.Item("DEPOT_ID"))},
                        {"PRODUCT_ID", pf.CheckStringDBNull(yRow.Item("PRODUCT_ID"))},
                        {"BATCH_NUM", pf.CheckStringDBNull(yRow.Item("BATCH_NUM"))},
                        {"G_DIFF_QTY", 0},
                        {"B_DIFF_QTY", 0},
                        {"C_DIFF_QTY", 0},
                        {"CUS_DIFF_QTY", total_qty},
                        {"SUPPLIER_DIFF_QTY", 0},
                        {"VOUCHER_STATUS", pf.CheckStringDBNull(xRow.Item("VOUCHER_STATUS"))},
                        {"CUSTOMER_ID", pf.CheckStringDBNull(xRow.Item("CUSTOMER_ID"))},
                        {"SUPPLIER_ID", pf.CheckStringDBNull(xRow.Item("SUPPLIER_ID"))},
                        {"CREATE_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                    }
                    sSql = pf.GetInsertquery(table, col_value)

                ElseIf xRow.Item("STOCK_TYPE").ToString = "經銷" Then

                    Dim col_value(,) As String = {
                        {"PAPER_ID", pf.CheckStringDBNull(yRow.Item("STOCK_OUT_ID"))},
                        {"EFFECTIVE_DATE", xRow.Item("EFFECTIVE_DATE")},
                        {"CHANGE_DATE", CHANGE_DATE},
                        {"DEPOT_ID", pf.CheckStringDBNull(yRow.Item("DEPOT_ID"))},
                        {"PRODUCT_ID", pf.CheckStringDBNull(yRow.Item("PRODUCT_ID"))},
                        {"BATCH_NUM", pf.CheckStringDBNull(yRow.Item("BATCH_NUM"))},
                        {"G_DIFF_QTY", 0},
                        {"B_DIFF_QTY", 0},
                        {"C_DIFF_QTY", 0},
                        {"CUS_DIFF_QTY", 0},
                        {"SUPPLIER_DIFF_QTY", total_qty},
                        {"VOUCHER_STATUS", pf.CheckStringDBNull(xRow.Item("VOUCHER_STATUS"))},
                        {"CUSTOMER_ID", pf.CheckStringDBNull(xRow.Item("CUSTOMER_ID"))},
                        {"SUPPLIER_ID", pf.CheckStringDBNull(xRow.Item("SUPPLIER_ID"))},
                        {"CREATE_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                    }
                    sSql = pf.GetInsertquery(table, col_value)
                End If
                Console.WriteLine(sSql)
                command.CommandText = sSql
                iAffectRows = command.ExecuteNonQuery()
                If iAffectRows = 0 Then
                    Return False
                End If
            Next

            Dim CHECK_DATE As New DateTime
            CHECK_DATE = DateTime.Now

            '異動當日庫存的數量-以覆核日
            If xRow.Item("STOCK_TYPE").ToString = "公司" Then
                sSql = ""
                Dim New_Qty As Double = 0
                Dim Good_New_Qty As Double = 0
                Dim Bad_New_Qty As Double = 0
                '當日庫存異動數量
                sSql = "SELECT CONVERT(VARCHAR,[CHANGE_DATE],111) AS CDATE,[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                sSql &= ",SUM(ISNULL([G_DIFF_QTY],0)) as G_DIFF_QTY ,SUM(ISNULL([B_DIFF_QTY],0)) as B_DIFF_QTY "
                sSql &= " FROM [D_DEPOT_STOCKS_DIFF] WITH (NOLOCK) "
                sSql &= " WHERE [CHANGE_DATE] ='" & CHECK_DATE.ToString("yyyy/MM/dd") & "'"
                sSql &= " GROUP BY CONVERT(VARCHAR,[CHANGE_DATE],111),[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                Console.WriteLine(sSql)
                Dim qdt As New DataTable
                qdt = pf.GetDataTable(sSql)
                If qdt.Rows.Count > 0 Then
                    For j = 0 To qdt.Rows.Count - 1
                        Dim zRow As DataRow = qdt.Rows(j)
                        '取最近一次的庫存期末數量
                        'sSql = "SELECT TOP 1 [EFFECTIVE_DATE],[GOOD_QTY] FROM [D_DEPOT_STOCKS_DAYS] WITH (NOLOCK) "
                        sSql = "SELECT TOP 1 [EFFECTIVE_DATE],[GOOD_QTY],[BAD_QTY],[DEPOT_QTY]"
                        sSql &= " From [D_DEPOT_STOCKS_DAYS] WITH (NOLOCK) "
                        sSql &= " Where [DEPOT_ID] ='" & zRow.Item("DEPOT_ID").ToString & "'"
                        sSql &= " AND [PRODUCT_ID]='" & zRow.Item("PRODUCT_ID").ToString & "'"
                        sSql &= " AND [BATCH_NUM]='" & zRow.Item("BATCH_NUM").ToString & "'"
                        sSql &= " AND [EFFECTIVE_DATE]='" & CHECK_DATE.AddDays(-1).ToString("yyyy/MM/dd") & "'"
                        sSql &= " ORDER BY [EFFECTIVE_DATE] DESC "
                        Console.WriteLine(sSql)
                        Dim qdt2 As New DataTable
                        qdt2 = pf.GetDataTable(sSql)
                        If qdt2.Rows.Count > 0 Then
                            '有查詢到前一日的期末庫存，庫存數量直接運算後更新
                            New_Qty = Convert.ToDouble(pf.CheckNull(qdt2.Rows(0).Item("DEPOT_QTY"))) + Convert.ToDouble(pf.CheckNull(zRow.Item("G_DIFF_QTY")))
                            Good_New_Qty = Convert.ToDouble(pf.CheckNull(qdt2.Rows(0).Item("DEPOT_QTY"))) + Convert.ToDouble(pf.CheckNull(zRow.Item("G_DIFF_QTY")))
                            Bad_New_Qty = Convert.ToDouble(pf.CheckNull(qdt2.Rows(0).Item("BAD_QTY"))) + Convert.ToDouble(pf.CheckNull(zRow.Item("B_DIFF_QTY")))
                        Else
                            '若每日庫存沒有資料者，直接新增更新
                            '2019-04-01討論後需要再驗證
                            New_Qty = Convert.ToDouble(pf.CheckNull(zRow.Item("G_DIFF_QTY")))
                            Good_New_Qty = Convert.ToDouble(pf.CheckNull(zRow.Item("G_DIFF_QTY")))
                            Bad_New_Qty = Convert.ToDouble(pf.CheckNull(zRow.Item("B_DIFF_QTY")))
                        End If
                        Dim tableName As String = "D_DEPOT_STOCKS"
                        Dim pk() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM"}
                        Dim col_value(,) As String = {
                            {"DEPOT_ID", zRow.Item("DEPOT_ID").ToString},
                            {"PRODUCT_ID", zRow.Item("PRODUCT_ID").ToString},
                            {"BATCH_NUM", zRow.Item("BATCH_NUM").ToString},
                            {"GOOD_QTY", Good_New_Qty},
                            {"BAD_QTY", Bad_New_Qty},
                            {"DEPOT_QTY", Good_New_Qty},
                            {"ALTER_USER", "系統"},
                            {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                        }
                        sSql = pf.GetUpdatequery(tableName, pk, col_value)
                        Console.WriteLine(sSql)
                        command.CommandText = sSql
                        iAffectRows = command.ExecuteNonQuery()
                        If iAffectRows = 0 Then
                            Return False
                        End If
                    Next
                End If

            ElseIf xRow.Item("STOCK_TYPE").ToString = "寄售" Then

                '異動客戶寄售庫存[D_CUS_CON_QTY_DAYS]&[D_CUS_CON_QTY]
                sSql = "SELECT [CUSTOMER_ID],CONVERT(VARCHAR,[CHANGE_DATE],111) AS CDATE,[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                sSql &= ",SUM(ISNULL([CUS_DIFF_QTY],0)) as CUS_DIFF_QTY "
                sSql &= " FROM [D_DEPOT_STOCKS_DIFF] WITH (NOLOCK) "
                sSql &= " WHERE [CHANGE_DATE] ='" & CHECK_DATE.ToString("yyyy/MM/dd") & "'"
                sSql &= " AND [CUSTOMER_ID] ='" & xRow.Item("CUSTOMER_ID").ToString & "'"
                sSql &= " GROUP BY [CUSTOMER_ID],CONVERT(VARCHAR,[CHANGE_DATE],111),[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                Console.WriteLine(sSql)
                Dim qdt As New DataTable
                qdt = pf.GetDataTable(sSql)
                If qdt.Rows.Count > 0 Then
                    Dim CUS_New_Qty As Double = 0
                    For j = 0 To qdt.Rows.Count - 1
                        Dim zRow As DataRow = qdt.Rows(j)
                        '取D_CUS_CON_QTY_DAYS最近一次的庫存期末數量
                        sSql = "SELECT TOP 1 [CUS_DEPOT_QTY] FROM [D_CUS_CON_QTY_DAYS] WITH (NOLOCK)"
                        sSql &= " WHERE [DEPOT_ID]='" & zRow.Item("DEPOT_ID").ToString & "'"
                        sSql &= " AND [PRODUCT_ID]='" & zRow.Item("PRODUCT_ID").ToString & "'"
                        sSql &= " AND [BATCH_NUM]='" & zRow.Item("BATCH_NUM").ToString & "'"
                        sSql &= " AND [CUSTOMER_ID]='" & zRow.Item("CUSTOMER_ID").ToString & "'"
                        sSql &= " ORDER BY [EFFECTIVE_DATE] DESC "
                        Console.WriteLine(sSql)
                        Dim qdt2 As New DataTable
                        qdt2 = pf.GetDataTable(sSql)
                        If qdt2.Rows.Count > 0 Then
                            CUS_New_Qty = Convert.ToDouble(pf.CheckNull(qdt2.Rows(0).Item("CUS_DEPOT_QTY"))) + Convert.ToDouble(pf.CheckNull(zRow.Item("CUS_DIFF_QTY")))
                        Else
                            '直接更新
                            CUS_New_Qty = Convert.ToDouble(pf.CheckNull(zRow.Item("CUS_DIFF_QTY")))
                        End If

                        '判斷更新方式
                        Dim tableName As String = "D_CUS_CON_QTY"
                        Dim chk_dt As New DataTable
                        'sSql = "SELECT * FROM [" & tableName & "] WITH (NOLOCK) "
                        'sSql &= " WHERE [DEPOT_ID]='" & zRow.Item("DEPOT_ID").ToString & "'"
                        'sSql &= " AND [PRODUCT_ID]='" & zRow.Item("PRODUCT_ID").ToString & "'"
                        'sSql &= " AND [BATCH_NUM]='" & zRow.Item("BATCH_NUM").ToString & "'"
                        'sSql &= " AND [CUSTOMER_ID]='" & zRow.Item("CUSTOMER_ID").ToString & "'"
                        'chk_dt = pf.GetDataTable(sSql)
                        Dim chkWhere(,) As String = {
                            {"DEPOT_ID", zRow.Item("DEPOT_ID").ToString},
                            {"PRODUCT_ID", zRow.Item("PRODUCT_ID").ToString},
                            {"BATCH_NUM", zRow.Item("BATCH_NUM").ToString},
                            {"CUSTOMER_ID", zRow.Item("CUSTOMER_ID").ToString}
                        }
                        chk_dt = pf.GetDB(tableName & ",NOLOCK", chkWhere)
                        If chk_dt.Rows.Count > 0 Then
                            Dim pk() As String = {"CUSTOMER_ID", "DEPOT_ID", "PRODUCT_ID", "BATCH_NUM"}
                            Dim col_value(,) As String = {
                                {"CUSTOMER_ID", zRow.Item("CUSTOMER_ID").ToString},
                                {"DEPOT_ID", zRow.Item("DEPOT_ID").ToString},
                                {"PRODUCT_ID", zRow.Item("PRODUCT_ID").ToString},
                                {"BATCH_NUM", zRow.Item("BATCH_NUM").ToString},
                                {"CUS_DEPOT_QTY", CUS_New_Qty},
                                {"ALTER_USER", "系統"},
                                {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                            }
                            sSql = pf.GetUpdatequery(tableName, pk, col_value)
                        Else

                            Dim col_value(,) As String = {
                                {"CUSTOMER_ID", zRow.Item("CUSTOMER_ID").ToString},
                                {"DEPOT_ID", zRow.Item("DEPOT_ID").ToString},
                                {"PRODUCT_ID", zRow.Item("PRODUCT_ID").ToString},
                                {"BATCH_NUM", zRow.Item("BATCH_NUM").ToString},
                                {"CUS_DEPOT_QTY", CUS_New_Qty},
                                {"CREATE_USER", "系統"},
                                {"CREATE_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                            }
                            sSql = pf.GetInsertquery(tableName, col_value)
                        End If
                        Console.WriteLine(sSql)
                        command.CommandText = sSql
                        iAffectRows = command.ExecuteNonQuery()
                        If iAffectRows = 0 Then
                            Return False
                        End If
                    Next
                End If

            ElseIf xRow.Item("STOCK_TYPE").ToString = "經銷" Then

                '異動經銷商庫存[D_DEALER_CON_QTY]&[D_DEALER_CON_QTY_DAYS]
                sSql = "SELECT [SUPPLIER_ID],CONVERT(VARCHAR,[CHANGE_DATE],111) AS CDATE,[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                sSql &= ",SUM(ISNULL([SUPPLIER_DIFF_QTY],0)) as SUPPLIER_DIFF_QTY "
                sSql &= " FROM [D_DEPOT_STOCKS_DIFF] WITH (NOLOCK) "
                sSql &= " WHERE [CHANGE_DATE] ='" & CHECK_DATE.ToString("yyyy/MM/dd") & "'"
                sSql &= " AND [SUPPLIER_ID] ='" & xRow.Item("SUPPLIER_ID").ToString & "'"
                sSql &= " GROUP BY [SUPPLIER_ID],CONVERT(VARCHAR,[CHANGE_DATE],111),[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                Console.WriteLine(sSql)
                Dim qdt As New DataTable
                qdt = pf.GetDataTable(sSql)
                If qdt.Rows.Count > 0 Then

                    Dim Supplier_New_Qty As Double = 0
                    For j = 0 To qdt.Rows.Count - 1
                        Dim zRow As DataRow = qdt.Rows(j)
                        '取D_DEALER_CON_QTY_DAYS最近一次的庫存期末數量
                        sSql = "SELECT TOP 1 [CON_DEPOT_QTY] FROM [D_DEALER_CON_QTY_DAYS] WITH (NOLOCK)"
                        sSql &= " WHERE [DEPOT_ID]='" & zRow.Item("DEPOT_ID").ToString & "'"
                        sSql &= " AND [PRODUCT_ID]='" & zRow.Item("PRODUCT_ID").ToString & "'"
                        sSql &= " AND [BATCH_NUM]='" & zRow.Item("BATCH_NUM").ToString & "'"
                        sSql &= " AND [SUPPLIER_ID]='" & zRow.Item("SUPPLIER_ID").ToString & "'"
                        sSql &= " ORDER BY [EFFECTIVE_DATE] DESC "
                        Console.WriteLine(sSql)
                        Dim qdt2 As New DataTable
                        qdt2 = pf.GetDataTable(sSql)
                        If qdt2.Rows.Count > 0 Then
                            Supplier_New_Qty = Convert.ToDouble(pf.CheckNull(qdt2.Rows(0).Item("CON_DEPOT_QTY"))) + Convert.ToDouble(pf.CheckNull(zRow.Item("SUPPLIER_DIFF_QTY")))
                        Else
                            '直接更新
                            Supplier_New_Qty = Convert.ToDouble(pf.CheckNull(zRow.Item("SUPPLIER_DIFF_QTY")))
                        End If

                        '判斷更新方式
                        Dim tableName As String = "D_DEALER_CON_QTY"
                        sSql = "SELECT * FROM [" & tableName & "] WITH (NOLOCK) "
                        sSql &= " WHERE [DEPOT_ID]='" & zRow.Item("DEPOT_ID").ToString & "'"
                        sSql &= " AND [PRODUCT_ID]='" & zRow.Item("PRODUCT_ID").ToString & "'"
                        sSql &= " AND [BATCH_NUM]='" & zRow.Item("BATCH_NUM").ToString & "'"
                        sSql &= " AND [SUPPLIER_ID]='" & zRow.Item("SUPPLIER_ID").ToString & "'"
                        Dim chk_dt As New DataTable
                        chk_dt = pf.GetDataTable(sSql)
                        If chk_dt.Rows.Count > 0 Then
                            Dim pk() As String = {"SUPPLIER_ID", "DEPOT_ID", "PRODUCT_ID", "BATCH_NUM"}
                            Dim col_value(,) As String = {
                                {"SUPPLIER_ID", zRow.Item("SUPPLIER_ID").ToString},
                                {"DEPOT_ID", zRow.Item("DEPOT_ID").ToString},
                                {"PRODUCT_ID", zRow.Item("PRODUCT_ID").ToString},
                                {"BATCH_NUM", zRow.Item("BATCH_NUM").ToString},
                                {"CON_DEPOT_QTY", Supplier_New_Qty},
                                {"ALTER_USER", "系統"},
                                {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                            }
                            sSql = pf.GetUpdatequery(tableName, pk, col_value)
                        Else

                            Dim col_value(,) As String = {
                                {"SUPPLIER_ID", zRow.Item("SUPPLIER_ID").ToString},
                                {"DEPOT_ID", zRow.Item("DEPOT_ID").ToString},
                                {"PRODUCT_ID", zRow.Item("PRODUCT_ID").ToString},
                                {"BATCH_NUM", zRow.Item("BATCH_NUM").ToString},
                                {"CON_DEPOT_QTY", Supplier_New_Qty},
                                {"ALTER_USER", "系統"},
                                {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                            }
                            sSql = pf.GetInsertquery(tableName, col_value)
                        End If
                        Console.WriteLine(sSql)
                        command.CommandText = sSql
                        iAffectRows = command.ExecuteNonQuery()
                        If iAffectRows = 0 Then
                            Return False
                        End If

                        '更新經銷商倉庫
                        Dim dt As DataTable = pf.GetDB("D_DEPOT_BASE,NOLOCK", {{"DEPOT_ID", zRow.Item("DEPOT_ID").ToString}})
                        If dt.Rows.Count > 0 Then
                            If dt.Rows(0).Item("DEPOT_TYPE").ToString = "經銷商" AndAlso dt.Rows(0).Item("SUPPLIER_ID").ToString = zRow.Item("SUPPLIER_ID").ToString Then
                                '取產品的VALID_PERIOD
                                Dim pWhere(,) As String = {
                                    {"PRODUCT_ID", zRow.Item("PRODUCT_ID")},
                                    {"BATCH_NUM", zRow.Item("BATCH_NUM")},
                                    {"ORDERBY_ASC", "DEPOT_ID"}
                                }
                                Dim pdt As DataTable = pf.GetDB("D_DEPOT_STOCKS,NOLOCK", pWhere)

                                Dim tableName2 As String = "D_DEPOT_STOCKS"
                                Dim pk2() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM"}
                                Dim col_value2(,) As String = {
                                    {"DEPOT_ID", zRow.Item("DEPOT_ID").ToString},
                                    {"PRODUCT_ID", zRow.Item("PRODUCT_ID").ToString},
                                    {"BATCH_NUM", zRow.Item("BATCH_NUM").ToString},
                                    {"GOOD_QTY", Supplier_New_Qty},
                                    {"DEPOT_QTY", Supplier_New_Qty},
                                    {"VALID_PERIOD", CType(pdt.Rows(0).Item("VALID_PERIOD"), DateTime).ToString("yyyy/MM/dd")},
                                    {"ALTER_USER", "系統"},
                                    {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff")}
                                }
                                sSql = pf.GetUpdatequery(tableName2, pk2, col_value2)
                                Console.WriteLine(sSql)
                                command.CommandText = sSql
                                iAffectRows = command.ExecuteNonQuery()
                                If iAffectRows = 0 Then
                                    Return False
                                End If
                            End If
                        End If
                    Next
                End If
            End If
        Catch ex As Exception
            pf.MyError(ex.ToString)
            Return False
        End Try
        Return True
    End Function

    '2019.11.19 edit by 玉玲
    '異動出庫單的即時可動支庫存數量[可動支庫存庫存][看覆核日] ，資料現當日有效
    '計算公式：庫存數量=前一日的期末庫存數量[DEPOT_QTY]+覆核日=當日的庫存異動數量
    '更新到[D_DEPOT_STOCKS]的[SALE_QTY]
    'STOCK_OUT_ID:出庫單編號
    Function UPDATE_D_DEPOT_STOCKS_OUT_SALEQTY(STOCK_OUT_ID As String, Old_VOUCHER_STATUS As String, New_VOUCHER_STATUS As String, command As SqlCommand) As Boolean
        Dim sSql As String = ""
        Dim iAffectRows As Integer = 0
        Try
            Dim mdt As New DataTable
            Dim ddt As New DataTable
            '出庫單主檔資料
            mdt = pf.GetDB("D_STOCK_OUT_MASTER,NOLOCK", {{"STOCK_OUT_ID", STOCK_OUT_ID}})
            Dim xRow As DataRow
            xRow = mdt.Rows(0)
            '生效日
            Dim EFFECTIVE_DATE As Date = Convert.ToDateTime(xRow.Item("EFFECTIVE_DATE")).ToString("yyyy/MM/dd")
            '覆核日
            Dim CHANGE_DATE As Date = Nothing

            If DBNull.Value.Equals(xRow.Item("ALTER_TIME")) Then
                CHANGE_DATE = Convert.ToDateTime(xRow.Item("CREATE_TIME")).ToString("yyyy/MM/dd")
            Else
                CHANGE_DATE = Convert.ToDateTime(xRow.Item("ALTER_TIME")).ToString("yyyy/MM/dd")
            End If

            '將明細寫入[D_DEPOT_STOCKS_KEYIN_DIFF]，然後再統一計算庫存異動數量
            ddt = pf.GetDB("D_STOCK_OUT_DETAIL,NOLOCK", {{"STOCK_OUT_ID", STOCK_OUT_ID}})
            Dim table As String = "D_DEPOT_STOCKS_KEYIN_DIFF"
            For j As Integer = 0 To ddt.Rows.Count - 1
                Dim yRow As DataRow
                yRow = ddt.Rows(j)
                Dim total_qty As Double = 0
                Dim good_qty As Double = 0
                Dim bad_qty As Double = 0
                If Old_VOUCHER_STATUS = "" AndAlso New_VOUCHER_STATUS = "生效" Then
                    total_qty = (Convert.ToDouble(yRow.Item("QTY")) + Convert.ToDouble(yRow.Item("PRESENT_QTY"))) * (-1)
                ElseIf Old_VOUCHER_STATUS = "確認" AndAlso New_VOUCHER_STATUS = "作廢" Then
                    total_qty = (Convert.ToDouble(yRow.Item("QTY")) + Convert.ToDouble(yRow.Item("PRESENT_QTY")))
                ElseIf Old_VOUCHER_STATUS = "確認" AndAlso New_VOUCHER_STATUS = "生效" Then
                    'total_qty = (Convert.ToDouble(yRow.Item("QTY")) + Convert.ToDouble(yRow.Item("PRESENT_QTY")))
                    total_qty = 0
                ElseIf Old_VOUCHER_STATUS = "生效" AndAlso New_VOUCHER_STATUS = "作廢" Then
                    total_qty = (Convert.ToDouble(pf.CheckNull(yRow.Item("QTY"))) + Convert.ToDouble(yRow.Item("PRESENT_QTY")))
                End If

                '出庫單使用
                If Old_VOUCHER_STATUS = "否" AndAlso New_VOUCHER_STATUS = "是" Then
                    total_qty = (Convert.ToDouble(pf.CheckNull(yRow.Item("QTY"))) + Convert.ToDouble(pf.CheckNull(yRow.Item("PRESENT_QTY")))) * (-1)
                ElseIf Old_VOUCHER_STATUS = "是" AndAlso New_VOUCHER_STATUS = "否" Then
                    total_qty = (Convert.ToDouble(pf.CheckNull(yRow.Item("QTY"))) + Convert.ToDouble(pf.CheckNull(yRow.Item("PRESENT_QTY"))))
                End If

                If xRow.Item("STOCK_TYPE").ToString = "公司" Then
                    Dim col_value(,) As String = {
                        {"PAPER_ID", pf.CheckStringDBNull(yRow.Item("STOCK_OUT_ID"))},
                        {"EFFECTIVE_DATE", xRow.Item("EFFECTIVE_DATE")},
                        {"CHANGE_DATE", CHANGE_DATE},
                        {"DEPOT_ID", pf.CheckStringDBNull(yRow.Item("DEPOT_ID"))},
                        {"PRODUCT_ID", pf.CheckStringDBNull(yRow.Item("PRODUCT_ID"))},
                        {"BATCH_NUM", pf.CheckStringDBNull(yRow.Item("BATCH_NUM"))},
                        {"CUS_DIFF_QTY", 0},
                        {"SUPPLIER_DIFF_QTY", 0},
                        {"KEYIN_DIFF_QTY", total_qty},
                        {"VOUCHER_STATUS", pf.CheckStringDBNull(xRow.Item("VOUCHER_STATUS"))},
                        {"CUSTOMER_ID", pf.CheckStringDBNull(xRow.Item("CUSTOMER_ID"))},
                        {"SUPPLIER_ID", pf.CheckStringDBNull(xRow.Item("SUPPLIER_ID"))},
                        {"CREATE_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff")}
                    }
                    sSql = pf.GetInsertquery(table, col_value)

                ElseIf xRow.Item("STOCK_TYPE").ToString = "寄售" Then

                    Dim col_value(,) As String = {
                        {"PAPER_ID", pf.CheckStringDBNull(yRow.Item("STOCK_OUT_ID"))},
                        {"EFFECTIVE_DATE", xRow.Item("EFFECTIVE_DATE")},
                        {"CHANGE_DATE", CHANGE_DATE},
                        {"DEPOT_ID", pf.CheckStringDBNull(yRow.Item("DEPOT_ID"))},
                        {"PRODUCT_ID", pf.CheckStringDBNull(yRow.Item("PRODUCT_ID"))},
                        {"BATCH_NUM", pf.CheckStringDBNull(yRow.Item("BATCH_NUM"))},
                        {"CUS_DIFF_QTY", total_qty},
                        {"SUPPLIER_DIFF_QTY", "0"},
                        {"KEYIN_DIFF_QTY", "0"},
                        {"VOUCHER_STATUS", pf.CheckStringDBNull(xRow.Item("VOUCHER_STATUS"))},
                        {"CUSTOMER_ID", pf.CheckStringDBNull(xRow.Item("CUSTOMER_ID"))},
                        {"SUPPLIER_ID", pf.CheckStringDBNull(xRow.Item("SUPPLIER_ID"))},
                        {"CREATE_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff")}
                    }
                    sSql = pf.GetInsertquery(table, col_value)

                ElseIf xRow.Item("STOCK_TYPE").ToString = "經銷" Then

                    Dim col_value(,) As String = {
                        {"PAPER_ID", pf.CheckStringDBNull(yRow.Item("STOCK_OUT_ID"))},
                        {"EFFECTIVE_DATE", xRow.Item("EFFECTIVE_DATE")},
                        {"CHANGE_DATE", CHANGE_DATE},
                        {"DEPOT_ID", pf.CheckStringDBNull(yRow.Item("DEPOT_ID"))},
                        {"PRODUCT_ID", pf.CheckStringDBNull(yRow.Item("PRODUCT_ID"))},
                        {"BATCH_NUM", pf.CheckStringDBNull(yRow.Item("BATCH_NUM"))},
                        {"CUS_DIFF_QTY", "0"},
                        {"SUPPLIER_DIFF_QTY", total_qty},
                        {"KEYIN_DIFF_QTY", "0"},
                        {"VOUCHER_STATUS", pf.CheckStringDBNull(xRow.Item("VOUCHER_STATUS"))},
                        {"CUSTOMER_ID", pf.CheckStringDBNull(xRow.Item("CUSTOMER_ID"))},
                        {"SUPPLIER_ID", pf.CheckStringDBNull(xRow.Item("SUPPLIER_ID"))},
                        {"CREATE_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff")}
                    }
                    sSql = pf.GetInsertquery(table, col_value)
                End If
                Console.WriteLine(sSql)
                command.CommandText = sSql
                iAffectRows = command.ExecuteNonQuery()
                If iAffectRows = 0 Then
                    Return False
                End If
            Next

            Dim CHECK_DATE As New DateTime
            CHECK_DATE = DateTime.Now
            '異動當日即時可動支庫存的數量-以覆核日
            If xRow.Item("STOCK_TYPE").ToString = "公司" Then
                sSql = ""
                Dim New_Qty As Double = 0
                Dim Good_New_Qty As Double = 0
                Dim Bad_New_Qty As Double = 0
                '當日庫存異動數量
                sSql = "SELECT CONVERT(VARCHAR,[CHANGE_DATE],111) AS CDATE,[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                sSql &= ",SUM(ISNULL([KEYIN_DIFF_QTY],0)) as KEYIN_DIFF_QTY "
                sSql &= " FROM [D_DEPOT_STOCKS_KEYIN_DIFF] WITH (NOLOCK) "
                sSql &= " WHERE [CHANGE_DATE] ='" & CHECK_DATE.ToString("yyyy/MM/dd") & "'"
                sSql &= " GROUP BY CONVERT(VARCHAR,[CHANGE_DATE],111),[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                Console.WriteLine(sSql)
                Dim qdt As New DataTable
                qdt = pf.GetDataTable(sSql)
                If qdt.Rows.Count > 0 Then
                    For j = 0 To qdt.Rows.Count - 1
                        Dim zRow As DataRow = qdt.Rows(j)
                        '取最近一次的即時可動支庫存期末數量[SALE_QTY]
                        sSql = "SELECT TOP 1 [EFFECTIVE_DATE],[GOOD_QTY],[BAD_QTY],[DEPOT_QTY],[SALE_QTY] FROM [D_DEPOT_STOCKS_DAYS] WITH (NOLOCK) "
                        sSql &= " WHERE [DEPOT_ID]='" & zRow.Item("DEPOT_ID").ToString & "'"
                        sSql &= " AND [PRODUCT_ID]='" & zRow.Item("PRODUCT_ID").ToString & "'"
                        sSql &= " AND [BATCH_NUM]='" & zRow.Item("BATCH_NUM").ToString & "'"
                        sSql &= " AND [EFFECTIVE_DATE]='" & CHECK_DATE.AddDays(-1).ToString("yyyy/MM/dd") & "'"
                        sSql &= " ORDER BY [EFFECTIVE_DATE] DESC "
                        Console.WriteLine(sSql)
                        Dim qdt2 As New DataTable
                        qdt2 = pf.GetDataTable(sSql)
                        If qdt2.Rows.Count > 0 Then
                            '有查詢到前一日的期末庫存，庫存數量直接運算後更新
                            'New_Qty = Convert.ToDouble(qdt2.Rows(0).Item("SALE_QTY")) + Convert.ToDouble(zRow.Item("KEYIN_DIFF_QTY"))
                            New_Qty = Convert.ToDouble(pf.CheckNull(qdt2.Rows(0).Item("SALE_QTY"))) + Convert.ToDouble(pf.CheckNull(zRow.Item("KEYIN_DIFF_QTY")))
                        Else
                            '若每日庫存沒有資料者，直接更新
                            '2019-04-01討論後需要再驗證
                            New_Qty = Convert.ToDouble(pf.CheckNull(zRow.Item("KEYIN_DIFF_QTY")))
                        End If

                        Dim tableName As String = "D_DEPOT_STOCKS"
                        Dim pk() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM"}
                        Dim col_value(,) As String = {
                            {"DEPOT_ID", zRow.Item("DEPOT_ID").ToString},
                            {"PRODUCT_ID", zRow.Item("PRODUCT_ID").ToString},
                            {"BATCH_NUM", zRow.Item("BATCH_NUM").ToString},
                            {"SALE_QTY", New_Qty},
                            {"ALTER_USER", "系統"},
                            {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff")}
                        }
                        sSql = pf.GetUpdatequery(tableName, pk, col_value)
                        Console.WriteLine(sSql)
                        command.CommandText = sSql
                        iAffectRows = command.ExecuteNonQuery()
                        If iAffectRows = 0 Then
                            Return False
                        End If
                    Next
                End If
            ElseIf xRow.Item("STOCK_TYPE").ToString = "寄售" Then

                '異動客戶寄售庫存[D_CUS_CON_QTY_DAYS]&[D_CUS_CON_QTY]
                sSql = "SELECT [CUSTOMER_ID],CONVERT(VARCHAR,[CHANGE_DATE],111) AS CDATE,[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                sSql &= ",SUM(ISNULL([CUS_DIFF_QTY],0)) as CUS_DIFF_QTY "
                sSql &= " FROM [D_DEPOT_STOCKS_KEYIN_DIFF] WITH (NOLOCK) "
                sSql &= " WHERE [CHANGE_DATE] ='" & CHECK_DATE.ToString("yyyy/MM/dd") & "'"
                sSql &= " AND [CUSTOMER_ID] ='" & xRow.Item("CUSTOMER_ID").ToString & "'"
                sSql &= " GROUP BY [CUSTOMER_ID],CONVERT(VARCHAR,[CHANGE_DATE],111),[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                Console.WriteLine(sSql)
                Dim qdt As New DataTable
                qdt = pf.GetDataTable(sSql)
                If qdt.Rows.Count > 0 Then
                    Dim CUS_New_Qty As Double = 0
                    For j = 0 To qdt.Rows.Count - 1
                        Dim zRow As DataRow = qdt.Rows(j)
                        '取D_CUS_CON_QTY_DAYS最近一次的庫存期末數量
                        sSql = "SELECT TOP 1 [CUS_DEPOT_QTY],[CUS_SALE_QTY] FROM [D_CUS_CON_QTY_DAYS] WITH (NOLOCK)"
                        sSql &= " WHERE [DEPOT_ID]='" & zRow.Item("DEPOT_ID").ToString & "'"
                        sSql &= " AND [PRODUCT_ID]='" & zRow.Item("PRODUCT_ID").ToString & "'"
                        sSql &= " AND [BATCH_NUM]='" & zRow.Item("BATCH_NUM").ToString & "'"
                        sSql &= " AND [CUSTOMER_ID]='" & zRow.Item("CUSTOMER_ID").ToString & "'"
                        sSql &= " ORDER BY [EFFECTIVE_DATE] DESC "
                        Console.WriteLine(sSql)
                        Dim qdt2 As New DataTable
                        qdt2 = pf.GetDataTable(sSql)
                        If qdt2.Rows.Count > 0 Then
                            CUS_New_Qty = Convert.ToDouble(pf.CheckNull(qdt2.Rows(0).Item("CUS_SALE_QTY"))) + Convert.ToDouble(pf.CheckNull(zRow.Item("CUS_DIFF_QTY")))
                        Else
                            '直接更新
                            CUS_New_Qty = Convert.ToDouble(pf.CheckNull(zRow.Item("CUS_DIFF_QTY")))
                        End If
                        '判斷更新方式
                        Dim tableName As String = "D_CUS_CON_QTY"
                        'sSql = "SELECT * FROM [" & tableName & "] WITH (NOLOCK) "
                        'sSql &= " WHERE [DEPOT_ID]='" & zRow.Item("DEPOT_ID").ToString & "'"
                        'sSql &= " AND [PRODUCT_ID]='" & zRow.Item("PRODUCT_ID").ToString & "'"
                        'sSql &= " AND [BATCH_NUM]='" & zRow.Item("BATCH_NUM").ToString & "'"
                        'sSql &= " AND [CUSTOMER_ID]='" & zRow.Item("CUSTOMER_ID").ToString & "'"
                        Dim chk_dt As New DataTable
                        'chk_dt = pf.GetDataTable(sSql)
                        Dim check_where(,) As String = {
                            {"DEPOT_ID", zRow.Item("DEPOT_ID").ToString},
                            {"PRODUCT_ID", zRow.Item("PRODUCT_ID").ToString},
                            {"BATCH_NUM", zRow.Item("BATCH_NUM").ToString},
                            {"CUSTOMER_ID", zRow.Item("CUSTOMER_ID").ToString}
                        }
                        chk_dt = pf.GetDB(tableName & ",NOLOCK", check_where)
                        If chk_dt.Rows.Count > 0 Then
                            Dim pk() As String = {"CUSTOMER_ID", "DEPOT_ID", "PRODUCT_ID", "BATCH_NUM"}
                            Dim col_value(,) As String = {
                                {"CUSTOMER_ID", zRow.Item("CUSTOMER_ID").ToString},
                                {"DEPOT_ID", zRow.Item("DEPOT_ID").ToString},
                                {"PRODUCT_ID", zRow.Item("PRODUCT_ID").ToString},
                                {"BATCH_NUM", zRow.Item("BATCH_NUM").ToString},
                                {"CUS_SALE_QTY", CUS_New_Qty},
                                {"ALTER_USER", "系統"},
                                {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff")}
                            }
                            sSql = pf.GetUpdatequery(tableName, pk, col_value)
                        Else
                            Dim col_value(,) As String = {
                                {"CUSTOMER_ID", zRow.Item("CUSTOMER_ID").ToString},
                                {"DEPOT_ID", zRow.Item("DEPOT_ID").ToString},
                                {"PRODUCT_ID", zRow.Item("PRODUCT_ID").ToString},
                                {"BATCH_NUM", zRow.Item("BATCH_NUM").ToString},
                                {"CUS_SALE_QTY", CUS_New_Qty},
                                {"CREATE_USER", "系統"},
                                {"CREATE_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff")}
                            }
                            sSql = pf.GetInsertquery(tableName, col_value)
                        End If

                        Console.WriteLine(sSql)
                        command.CommandText = sSql
                        iAffectRows = command.ExecuteNonQuery()
                        If iAffectRows = 0 Then
                            Return False
                        End If
                    Next
                End If

            ElseIf xRow.Item("STOCK_TYPE").ToString = "經銷" Then
                '異動經銷商庫存[D_DEALER_CON_QTY]&[D_DEALER_CON_QTY_DAYS]
                sSql = "SELECT [SUPPLIER_ID],CONVERT(VARCHAR,[CHANGE_DATE],111) AS CDATE,[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                sSql &= ",SUM(ISNULL([SUPPLIER_DIFF_QTY],0)) as SUPPLIER_DIFF_QTY "
                sSql &= " FROM [D_DEPOT_STOCKS_KEYIN_DIFF] WITH (NOLOCK) "
                sSql &= " WHERE [CHANGE_DATE] ='" & CHECK_DATE.ToString("yyyy/MM/dd") & "'"
                sSql &= " AND [SUPPLIER_ID] ='" & xRow.Item("SUPPLIER_ID").ToString & "'"
                sSql &= " GROUP BY [SUPPLIER_ID],CONVERT(VARCHAR,[CHANGE_DATE],111),[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                Console.WriteLine(sSql)
                Dim qdt As New DataTable
                qdt = pf.GetDataTable(sSql)
                If qdt.Rows.Count > 0 Then
                    Dim Supplier_New_Qty As Double = 0
                    For j = 0 To qdt.Rows.Count - 1
                        Dim zRow As DataRow = qdt.Rows(j)
                        '取D_DEALER_CON_QTY_DAYS最近一次的庫存期末數量
                        sSql = "SELECT TOP 1 [CON_DEPOT_QTY],[CON_SALE_QTY] FROM [D_DEALER_CON_QTY_DAYS] WITH (NOLOCK)"
                        sSql &= " WHERE [DEPOT_ID]='" & zRow.Item("DEPOT_ID").ToString & "'"
                        sSql &= " AND [PRODUCT_ID]='" & zRow.Item("PRODUCT_ID").ToString & "'"
                        sSql &= " AND [BATCH_NUM]='" & zRow.Item("BATCH_NUM").ToString & "'"
                        sSql &= " AND [SUPPLIER_ID]='" & zRow.Item("SUPPLIER_ID").ToString & "'"
                        sSql &= " ORDER BY [EFFECTIVE_DATE] DESC "
                        Console.WriteLine(sSql)
                        Dim qdt2 As New DataTable
                        qdt2 = pf.GetDataTable(sSql)
                        If qdt2.Rows.Count > 0 Then
                            Supplier_New_Qty = Convert.ToDouble(pf.CheckNull(qdt2.Rows(0).Item("CON_SALE_QTY"))) + Convert.ToDouble(pf.CheckNull(zRow.Item("SUPPLIER_DIFF_QTY")))
                        Else
                            '直接更新
                            Supplier_New_Qty = Convert.ToDouble(pf.CheckNull(zRow.Item("SUPPLIER_DIFF_QTY")))
                        End If
                        '判斷更新方式
                        Dim tableName As String = "D_DEALER_CON_QTY"
                        Dim chk_dt As New DataTable
                        'sSql = "SELECT * FROM [" & tableName & "] WITH (NOLOCK) "
                        'sSql &= " WHERE [DEPOT_ID]='" & zRow.Item("DEPOT_ID").ToString & "'"
                        'sSql &= " AND [PRODUCT_ID]='" & zRow.Item("PRODUCT_ID").ToString & "'"
                        'sSql &= " AND [BATCH_NUM]='" & zRow.Item("BATCH_NUM").ToString & "'"
                        'sSql &= " AND [SUPPLIER_ID]='" & zRow.Item("SUPPLIER_ID").ToString & "'"
                        'chk_dt = pf.GetDataTable(sSql)
                        Dim check_where(,) As String = {
                            {"DEPOT_ID", zRow.Item("DEPOT_ID").ToString},
                            {"PRODUCT_ID", zRow.Item("PRODUCT_ID").ToString},
                            {"BATCH_NUM", zRow.Item("BATCH_NUM").ToString},
                            {"SUPPLIER_ID", zRow.Item("SUPPLIER_ID").ToString}
                        }
                        chk_dt = pf.GetDB(tableName & ",NOLOCK", check_where)
                        If chk_dt.Rows.Count > 0 Then
                            Dim pk() As String = {"SUPPLIER_ID", "DEPOT_ID", "PRODUCT_ID", "BATCH_NUM"}
                            Dim col_value(,) As String = {
                                {"SUPPLIER_ID", zRow.Item("SUPPLIER_ID").ToString},
                                {"DEPOT_ID", zRow.Item("DEPOT_ID").ToString},
                                {"PRODUCT_ID", zRow.Item("PRODUCT_ID").ToString},
                                {"BATCH_NUM", zRow.Item("BATCH_NUM").ToString},
                                {"CON_SALE_QTY", Supplier_New_Qty},
                                {"ALTER_USER", "系統"},
                                {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff")}
                            }
                            sSql = pf.GetUpdatequery(tableName, pk, col_value)
                        Else
                            Dim col_value(,) As String = {
                                {"SUPPLIER_ID", zRow.Item("SUPPLIER_ID").ToString},
                                {"DEPOT_ID", zRow.Item("DEPOT_ID").ToString},
                                {"PRODUCT_ID", zRow.Item("PRODUCT_ID").ToString},
                                {"BATCH_NUM", zRow.Item("BATCH_NUM").ToString},
                                {"CON_SALE_QTY", Supplier_New_Qty},
                                {"CREATE_USER", "系統"},
                                {"CREATE_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff")}
                            }
                            sSql = pf.GetInsertquery(tableName, col_value)
                        End If
                        Console.WriteLine(sSql)
                        command.CommandText = sSql
                        iAffectRows = command.ExecuteNonQuery()
                        If iAffectRows = 0 Then
                            Return False
                        End If

                        '更新經銷商倉
                        Dim dt As DataTable = pf.GetDB("D_DEPOT_BASE,NOLOCK", {{"DEPOT_ID", zRow.Item("DEPOT_ID").ToString}})
                        If dt.Rows.Count > 0 Then
                            If dt.Rows(0).Item("DEPOT_TYPE").ToString = "經銷商" AndAlso dt.Rows(0).Item("SUPPLIER_ID").ToString = zRow.Item("SUPPLIER_ID").ToString Then
                                '取產品的VALID_PERIOD
                                Dim pWhere(,) As String = {
                                    {"PRODUCT_ID", zRow.Item("PRODUCT_ID")},
                                    {"BATCH_NUM", zRow.Item("BATCH_NUM")},
                                    {"ORDERBY_ASC", "DEPOT_ID"}
                                }
                                Dim pdt As DataTable = pf.GetDB("D_DEPOT_STOCKS,NOLOCK", pWhere)
                                Dim tableName2 As String = "D_DEPOT_STOCKS"
                                Dim pk2() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM"}
                                Dim col_value2(,) As String = {
                                    {"DEPOT_ID", zRow.Item("DEPOT_ID").ToString},
                                    {"PRODUCT_ID", zRow.Item("PRODUCT_ID").ToString},
                                    {"BATCH_NUM", zRow.Item("BATCH_NUM").ToString},
                                    {"SALE_QTY", Supplier_New_Qty},
                                    {"VALID_PERIOD", CType(pdt.Rows(0).Item("VALID_PERIOD"), DateTime).ToString("yyyy/MM/dd")},
                                    {"ALTER_USER", "系統"},
                                    {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff")}
                                }
                                '判斷更新方式(insert/update)
                                Dim zWhere(,) As String = {
                                    {"DEPOT_ID", zRow.Item("DEPOT_ID").ToString},
                                    {"PRODUCT_ID", zRow.Item("PRODUCT_ID").ToString},
                                    {"BATCH_NUM", zRow.Item("BATCH_NUM").ToString}
                                }
                                Dim zdt As DataTable = pf.GetDB("D_DEPOT_STOCKS,NOLOCK", zWhere)
                                If zdt.Rows.Count > 0 Then
                                    sSql = pf.GetUpdatequery(tableName2, pk2, col_value2)
                                Else
                                    col_value2 = {
                                        {"DEPOT_ID", zRow.Item("DEPOT_ID").ToString},
                                        {"PRODUCT_ID", zRow.Item("PRODUCT_ID").ToString},
                                        {"BATCH_NUM", zRow.Item("BATCH_NUM").ToString},
                                        {"SALE_QTY", Supplier_New_Qty},
                                        {"VALID_PERIOD", CType(pdt.Rows(0).Item("VALID_PERIOD"), DateTime).ToString("yyyy/MM/dd")},
                                        {"CREATE_USER", "系統"},
                                        {"CREATE_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff")}
                                    }
                                    sSql = pf.GetInsertquery(tableName2, col_value2)
                                End If
                                Console.WriteLine(sSql)
                                command.CommandText = sSql
                                iAffectRows = command.ExecuteNonQuery()
                                If iAffectRows = 0 Then
                                    Return False
                                End If
                            End If
                        End If
                    Next
                End If
            End If
        Catch ex As Exception
            pf.MyError(ex.ToString)
            Return False
        End Try
        Return True
    End Function

    '2019.11.19 edit by 玉玲
    '異動出庫單的即時可動支庫存數量[可動支庫存庫存][看覆核日]
    '計算公式：庫存數量=前一日的期末庫存數量[DEPOT_QTY]+覆核日=當日的庫存異動數量
    '更新到[D_DEPOT_STOCKS]
    'STOCK_OUT_ID:出庫單編號
    Function ROLLBACK_D_DEPOT_STOCKS_OUT_SALEQTY(STOCK_OUT_ID As String, command As SqlCommand) As Boolean
        Dim sSql As String = ""
        Dim iAffectRows As Integer = 0
        Try
            Dim mdt As New DataTable
            Dim ddt As New DataTable
            '出庫單主檔資料
            mdt = pf.GetDB("D_STOCK_OUT_MASTER,NOLOCK", {{"STOCK_OUT_ID", STOCK_OUT_ID}})
            Dim xRow As DataRow
            xRow = mdt.Rows(0)
            '生效日
            Dim EFFECTIVE_DATE As Date = Convert.ToDateTime(xRow.Item("EFFECTIVE_DATE")).ToString("yyyy/MM/dd")
            '覆核日
            Dim CHANGE_DATE As Date = Nothing
            '異動日期
            CHANGE_DATE = DateTime.Now.ToString("yyyy/MM/dd")
            '將明細寫入[D_DEPOT_STOCKS_KEYIN_DIFF]，然後再統一計算庫存異動數量
            ddt = pf.GetDB("D_STOCK_OUT_DETAIL,NOLOCK", {{"STOCK_OUT_ID", STOCK_OUT_ID}})
            Dim table As String = "D_DEPOT_STOCKS_KEYIN_DIFF"
            For j As Integer = 0 To ddt.Rows.Count - 1
                Dim yRow As DataRow
                yRow = ddt.Rows(j)
                Dim total_qty As Double = 0
                If xRow.Item("VOUCHER_STATUS").ToString = "生效" Then
                    total_qty = (Convert.ToDouble(pf.CheckNull(yRow.Item("QTY"))) + Convert.ToDouble(pf.CheckNull(yRow.Item("PRESENT_QTY"))))
                ElseIf xRow.Item("VOUCHER_STATUS").ToString = "作廢" Then
                    total_qty = (Convert.ToDouble(pf.CheckNull(yRow.Item("QTY"))) + Convert.ToDouble(pf.CheckNull(yRow.Item("PRESENT_QTY")))) * (-1)
                ElseIf xRow.Item("VOUCHER_STATUS").ToString = "確認" Then
                    total_qty = 0
                End If
                If xRow.Item("STOCK_TYPE").ToString = "公司" Then
                    Dim col_value(,) As String = {
                        {"PAPER_ID", pf.CheckStringDBNull(yRow.Item("STOCK_OUT_ID"))},
                        {"EFFECTIVE_DATE", xRow.Item("EFFECTIVE_DATE")},
                        {"CHANGE_DATE", CHANGE_DATE},
                        {"DEPOT_ID", pf.CheckStringDBNull(yRow.Item("DEPOT_ID"))},
                        {"PRODUCT_ID", pf.CheckStringDBNull(yRow.Item("PRODUCT_ID"))},
                        {"BATCH_NUM", pf.CheckStringDBNull(yRow.Item("BATCH_NUM"))},
                        {"CUS_DIFF_QTY", 0},
                        {"SUPPLIER_DIFF_QTY", 0},
                        {"KEYIN_DIFF_QTY", total_qty},
                        {"VOUCHER_STATUS", pf.CheckStringDBNull(xRow.Item("VOUCHER_STATUS"))},
                        {"CUSTOMER_ID", pf.CheckStringDBNull(xRow.Item("CUSTOMER_ID"))},
                        {"SUPPLIER_ID", pf.CheckStringDBNull(xRow.Item("SUPPLIER_ID"))},
                        {"CREATE_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff")}
                    }
                    sSql = pf.GetInsertquery(table, col_value)

                ElseIf xRow.Item("STOCK_TYPE").ToString = "寄售" Then

                    Dim col_value(,) As String = {
                        {"PAPER_ID", pf.CheckStringDBNull(yRow.Item("STOCK_OUT_ID"))},
                        {"EFFECTIVE_DATE", xRow.Item("EFFECTIVE_DATE")},
                        {"CHANGE_DATE", CHANGE_DATE},
                        {"DEPOT_ID", pf.CheckStringDBNull(yRow.Item("DEPOT_ID"))},
                        {"PRODUCT_ID", pf.CheckStringDBNull(yRow.Item("PRODUCT_ID"))},
                        {"BATCH_NUM", pf.CheckStringDBNull(yRow.Item("BATCH_NUM"))},
                        {"CUS_DIFF_QTY", total_qty},
                        {"SUPPLIER_DIFF_QTY", 0},
                        {"KEYIN_DIFF_QTY", 0},
                        {"VOUCHER_STATUS", pf.CheckStringDBNull(xRow.Item("VOUCHER_STATUS"))},
                        {"CUSTOMER_ID", pf.CheckStringDBNull(xRow.Item("CUSTOMER_ID"))},
                        {"SUPPLIER_ID", pf.CheckStringDBNull(xRow.Item("SUPPLIER_ID"))},
                        {"CREATE_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff")}
                    }
                    sSql = pf.GetInsertquery(table, col_value)

                ElseIf xRow.Item("STOCK_TYPE").ToString = "經銷" Then

                    Dim col_value(,) As String = {
                        {"PAPER_ID", pf.CheckStringDBNull(yRow.Item("STOCK_OUT_ID"))},
                        {"EFFECTIVE_DATE", xRow.Item("EFFECTIVE_DATE")},
                        {"CHANGE_DATE", CHANGE_DATE},
                        {"DEPOT_ID", pf.CheckStringDBNull(yRow.Item("DEPOT_ID"))},
                        {"PRODUCT_ID", pf.CheckStringDBNull(yRow.Item("PRODUCT_ID"))},
                        {"BATCH_NUM", pf.CheckStringDBNull(yRow.Item("BATCH_NUM"))},
                        {"CUS_DIFF_QTY", 0},
                        {"SUPPLIER_DIFF_QTY", total_qty},
                        {"KEYIN_DIFF_QTY", 0},
                        {"VOUCHER_STATUS", pf.CheckStringDBNull(xRow.Item("VOUCHER_STATUS"))},
                        {"CUSTOMER_ID", pf.CheckStringDBNull(xRow.Item("CUSTOMER_ID"))},
                        {"SUPPLIER_ID", pf.CheckStringDBNull(xRow.Item("SUPPLIER_ID"))},
                        {"CREATE_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff")}
                    }
                    sSql = pf.GetInsertquery(table, col_value)

                End If
                Console.WriteLine(sSql)
                command.CommandText = sSql
                iAffectRows = command.ExecuteNonQuery()
                If iAffectRows = 0 Then
                    Return False
                End If
            Next

            Dim CHECK_DATE As New DateTime
            CHECK_DATE = DateTime.Now
            '異動當日庫存的數量-以覆核日
            If xRow.Item("STOCK_TYPE").ToString = "公司" Then
                sSql = ""
                Dim New_Qty As Double = 0
                Dim Good_New_Qty As Double = 0
                Dim Bad_New_Qty As Double = 0
                '當日庫存異動數量
                sSql = "SELECT CONVERT(VARCHAR,[CHANGE_DATE],111) AS CDATE,[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                sSql &= ",SUM(ISNULL([KEYIN_DIFF_QTY],0)) as KEYIN_DIFF_QTY "
                sSql &= " FROM [D_DEPOT_STOCKS_KEYIN_DIFF] WITH (NOLOCK) "
                sSql &= " WHERE [CHANGE_DATE] ='" & CHECK_DATE.ToString("yyyy/MM/dd") & "'"
                sSql &= " GROUP BY CONVERT(VARCHAR,[CHANGE_DATE],111),[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                Console.WriteLine(sSql)
                Dim qdt As New DataTable
                qdt = pf.GetDataTable(sSql)
                If qdt.Rows.Count > 0 Then
                    For j = 0 To qdt.Rows.Count - 1
                        Dim zRow As DataRow = qdt.Rows(j)
                        '取最近一次的即時可動支庫存期末數量[SALE_QTY]
                        sSql = "SELECT TOP 1 [EFFECTIVE_DATE],[GOOD_QTY],[BAD_QTY],[DEPOT_QTY],[SALE_QTY] FROM [D_DEPOT_STOCKS_DAYS] WITH (NOLOCK) "
                        sSql &= " WHERE [DEPOT_ID]='" & zRow.Item("DEPOT_ID").ToString & "'"
                        sSql &= " AND [PRODUCT_ID]='" & zRow.Item("PRODUCT_ID").ToString & "'"
                        sSql &= " AND [BATCH_NUM]='" & zRow.Item("BATCH_NUM").ToString & "'"
                        sSql &= " ORDER BY [EFFECTIVE_DATE] DESC "
                        Console.WriteLine(sSql)
                        Dim qdt2 As New DataTable
                        qdt2 = pf.GetDataTable(sSql)
                        If qdt2.Rows.Count > 0 Then
                            '有查詢到前一日的期末庫存，庫存數量直接運算後更新
                            New_Qty = Convert.ToDouble(pf.CheckNull(qdt2.Rows(0).Item("SALE_QTY"))) + Convert.ToDouble(pf.CheckNull(zRow.Item("KEYIN_DIFF_QTY")))
                        Else
                            '若每日庫存沒有資料者，直接更新
                            '2019-04-01討論後需要再驗證
                            New_Qty = Convert.ToDouble(pf.CheckNull(zRow.Item("KEYIN_DIFF_QTY")))
                        End If
                        Dim tableName As String = "D_DEPOT_STOCKS"
                        Dim pk() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM"}
                        Dim col_value(,) As String = {
                            {"DEPOT_ID", zRow.Item("DEPOT_ID").ToString},
                            {"PRODUCT_ID", zRow.Item("PRODUCT_ID").ToString},
                            {"BATCH_NUM", zRow.Item("BATCH_NUM").ToString},
                            {"SALE_QTY", New_Qty},
                            {"ALTER_USER", "系統"},
                            {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff")}
                        }
                        sSql = pf.GetUpdatequery(tableName, pk, col_value)
                        Console.WriteLine(sSql)
                        command.CommandText = sSql
                        iAffectRows = command.ExecuteNonQuery()
                        If iAffectRows = 0 Then
                            Return False
                        End If
                    Next

                End If

            ElseIf xRow.Item("STOCK_TYPE").ToString = "寄售" Then

                '異動客戶寄售庫存[D_CUS_CON_QTY_DAYS]&[D_CUS_CON_QTY]
                sSql = "SELECT [CUSTOMER_ID],CONVERT(VARCHAR,[CHANGE_DATE],111) AS CDATE,[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                sSql &= ",SUM(ISNULL([CUS_DIFF_QTY],0)) as CUS_DIFF_QTY "
                sSql &= " FROM [D_DEPOT_STOCKS_KEYIN_DIFF] WITH (NOLOCK) "
                sSql &= " WHERE [CHANGE_DATE] ='" & CHECK_DATE.ToString("yyyy/MM/dd") & "'"
                sSql &= " AND [CUSTOMER_ID] ='" & xRow.Item("CUSTOMER_ID").ToString & "'"
                sSql &= " GROUP BY [CUSTOMER_ID],CONVERT(VARCHAR,[CHANGE_DATE],111),[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                Console.WriteLine(sSql)
                Dim qdt As New DataTable
                qdt = pf.GetDataTable(sSql)
                If qdt.Rows.Count > 0 Then
                    Dim CUS_New_Qty As Double = 0
                    For j = 0 To qdt.Rows.Count - 1
                        Dim zRow As DataRow = qdt.Rows(j)
                        '取D_CUS_CON_QTY_DAYS最近一次的庫存期末數量
                        sSql = "SELECT TOP 1 [CUS_DEPOT_QTY],[CUS_SALE_QTY] FROM [D_CUS_CON_QTY_DAYS] WITH (NOLOCK)"
                        sSql &= " WHERE [DEPOT_ID]='" & zRow.Item("DEPOT_ID").ToString & "'"
                        sSql &= " AND [PRODUCT_ID]='" & zRow.Item("PRODUCT_ID").ToString & "'"
                        sSql &= " AND [BATCH_NUM]='" & zRow.Item("BATCH_NUM").ToString & "'"
                        sSql &= " AND [CUSTOMER_ID]='" & zRow.Item("CUSTOMER_ID").ToString & "'"
                        sSql &= " ORDER BY [EFFECTIVE_DATE] DESC "
                        Console.WriteLine(sSql)
                        Dim qdt2 As New DataTable
                        qdt2 = pf.GetDataTable(sSql)
                        If qdt2.Rows.Count > 0 Then
                            CUS_New_Qty = Convert.ToDouble(pf.CheckNull(qdt2.Rows(0).Item("CUS_SALE_QTY"))) + Convert.ToDouble(pf.CheckNull(zRow.Item("CUS_DIFF_QTY")))
                        Else
                            '直接更新
                            CUS_New_Qty = Convert.ToDouble(pf.CheckNull(zRow.Item("CUS_SALE_QTY")))
                        End If
                        '判斷更新方式
                        Dim tableName As String = "D_CUS_CON_QTY"
                        sSql = "SELECT * FROM [" & tableName & "] WITH (NOLOCK) "
                        sSql &= " WHERE [DEPOT_ID]='" & zRow.Item("DEPOT_ID").ToString & "'"
                        sSql &= " AND [PRODUCT_ID]='" & zRow.Item("PRODUCT_ID").ToString & "'"
                        sSql &= " AND [BATCH_NUM]='" & zRow.Item("BATCH_NUM").ToString & "'"
                        sSql &= " AND [CUSTOMER_ID]='" & zRow.Item("CUSTOMER_ID").ToString & "'"
                        Dim chk_dt As New DataTable
                        chk_dt = pf.GetDataTable(sSql)
                        If chk_dt.Rows.Count > 0 Then
                            Dim pk() As String = {"CUSTOMER_ID", "DEPOT_ID", "PRODUCT_ID", "BATCH_NUM"}
                            Dim col_value(,) As String = {
                                {"CUSTOMER_ID", zRow.Item("CUSTOMER_ID").ToString},
                                {"DEPOT_ID", zRow.Item("DEPOT_ID").ToString},
                                {"PRODUCT_ID", zRow.Item("PRODUCT_ID").ToString},
                                {"BATCH_NUM", zRow.Item("BATCH_NUM").ToString},
                                {"CUS_SALE_QTY", CUS_New_Qty},
                                {"ALTER_USER", "系統"},
                                {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff")}
                            }
                            sSql = pf.GetUpdatequery(tableName, pk, col_value)
                        Else

                            Dim col_value(,) As String = {
                                {"CUSTOMER_ID", zRow.Item("CUSTOMER_ID").ToString},
                                {"DEPOT_ID", zRow.Item("DEPOT_ID").ToString},
                                {"PRODUCT_ID", zRow.Item("PRODUCT_ID").ToString},
                                {"BATCH_NUM", zRow.Item("BATCH_NUM").ToString},
                                {"CUS_SALE_QTY", CUS_New_Qty},
                                {"CREATE_USER", "系統"},
                                {"CREATE_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff")}
                            }
                            sSql = pf.GetInsertquery(tableName, col_value)
                        End If
                        Console.WriteLine(sSql)
                        command.CommandText = sSql
                        iAffectRows = command.ExecuteNonQuery()
                        If iAffectRows = 0 Then
                            Return False
                        End If
                    Next
                End If

            ElseIf xRow.Item("STOCK_TYPE").ToString = "經銷" Then
                '異動經銷商庫存[D_DEALER_CON_QTY]&[D_DEALER_CON_QTY_DAYS]
                sSql = "SELECT [SUPPLIER_ID],CONVERT(VARCHAR,[CHANGE_DATE],111) AS CDATE,[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                sSql &= ",SUM(ISNULL([SUPPLIER_DIFF_QTY],0)) as SUPPLIER_DIFF_QTY "
                sSql &= " FROM [D_DEPOT_STOCKS_KEYIN_DIFF] WITH (NOLOCK) "
                sSql &= " WHERE [CHANGE_DATE] ='" & CHECK_DATE.ToString("yyyy/MM/dd") & "'"
                sSql &= " AND [SUPPLIER_ID] ='" & xRow.Item("SUPPLIER_ID").ToString & "'"
                sSql &= " GROUP BY [SUPPLIER_ID],CONVERT(VARCHAR,[CHANGE_DATE],111),[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                Console.WriteLine(sSql)
                Dim qdt As New DataTable
                qdt = pf.GetDataTable(sSql)
                If qdt.Rows.Count > 0 Then

                    Dim Supplier_New_Qty As Double = 0
                    For j = 0 To qdt.Rows.Count - 1
                        Dim zRow As DataRow = qdt.Rows(j)
                        '取D_DEALER_CON_QTY_DAYS最近一次的庫存期末數量
                        sSql = "SELECT TOP 1 [CON_DEPOT_QTY],[CON_SALE_QTY] FROM [D_DEALER_CON_QTY_DAYS] WITH (NOLOCK)"
                        sSql &= " WHERE [DEPOT_ID]='" & zRow.Item("DEPOT_ID").ToString & "'"
                        sSql &= " AND [PRODUCT_ID]='" & zRow.Item("PRODUCT_ID").ToString & "'"
                        sSql &= " AND [BATCH_NUM]='" & zRow.Item("BATCH_NUM").ToString & "'"
                        sSql &= " AND [SUPPLIER_ID]='" & zRow.Item("SUPPLIER_ID").ToString & "'"
                        sSql &= " ORDER BY [EFFECTIVE_DATE] DESC "
                        Console.WriteLine(sSql)
                        Dim qdt2 As New DataTable
                        qdt2 = pf.GetDataTable(sSql)
                        If qdt2.Rows.Count > 0 Then
                            Supplier_New_Qty = Convert.ToDouble(pf.CheckNull(qdt2.Rows(0).Item("CON_SALE_QTY"))) + Convert.ToDouble(pf.CheckNull(zRow.Item("SUPPLIER_DIFF_QTY")))
                        Else
                            '直接更新
                            Supplier_New_Qty = Convert.ToDouble(pf.CheckNull(zRow.Item("SUPPLIER_DIFF_QTY")))
                        End If

                        '判斷更新方式
                        Dim tableName As String = "D_DEALER_CON_QTY"
                        sSql = "SELECT * FROM [" & tableName & "] WITH (NOLOCK) "
                        sSql &= " WHERE [DEPOT_ID]='" & zRow.Item("DEPOT_ID").ToString & "'"
                        sSql &= " AND [PRODUCT_ID]='" & zRow.Item("PRODUCT_ID").ToString & "'"
                        sSql &= " AND [BATCH_NUM]='" & zRow.Item("BATCH_NUM").ToString & "'"
                        sSql &= " AND [SUPPLIER_ID]='" & zRow.Item("SUPPLIER_ID").ToString & "'"
                        Dim chk_dt As New DataTable
                        chk_dt = pf.GetDataTable(sSql)
                        If chk_dt.Rows.Count > 0 Then
                            Dim pk() As String = {"SUPPLIER_ID", "DEPOT_ID", "PRODUCT_ID", "BATCH_NUM"}
                            Dim col_value(,) As String = {
                                {"SUPPLIER_ID", zRow.Item("SUPPLIER_ID").ToString},
                                {"DEPOT_ID", zRow.Item("DEPOT_ID").ToString},
                                {"PRODUCT_ID", zRow.Item("PRODUCT_ID").ToString},
                                {"BATCH_NUM", zRow.Item("BATCH_NUM").ToString},
                                {"CON_SALE_QTY", Supplier_New_Qty},
                                {"ALTER_USER", "系統"},
                                {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff")}
                            }
                            sSql = pf.GetUpdatequery(tableName, pk, col_value)
                        Else

                            Dim col_value(,) As String = {
                                {"SUPPLIER_ID", zRow.Item("SUPPLIER_ID").ToString},
                                {"DEPOT_ID", zRow.Item("DEPOT_ID").ToString},
                                {"PRODUCT_ID", zRow.Item("PRODUCT_ID").ToString},
                                {"BATCH_NUM", zRow.Item("BATCH_NUM").ToString},
                                {"CON_SALE_QTY", Supplier_New_Qty},
                                {"CREATE_USER", "系統"},
                                {"CREATE_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff")}
                            }
                            sSql = pf.GetInsertquery(tableName, col_value)
                        End If
                        Console.WriteLine(sSql)
                        command.CommandText = sSql
                        iAffectRows = command.ExecuteNonQuery()
                        If iAffectRows = 0 Then
                            Return False
                        End If
                        '更新經銷商庫存
                        Dim dt As DataTable = pf.GetDB("D_DEPOT_BASE,NOLOCK", {{"DEPOT_ID", zRow.Item("DEPOT_ID").ToString}})
                        If dt.Rows.Count > 0 Then
                            If dt.Rows(0).Item("DEPOT_TYPE").ToString = "經銷商" AndAlso dt.Rows(0).Item("SUPPLIER_ID").ToString = zRow.Item("SUPPLIER_ID").ToString Then
                                Dim tableName2 As String = "D_DEPOT_STOCKS"
                                Dim pk2() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM"}
                                Dim col_value2(,) As String = {
                                    {"DEPOT_ID", zRow.Item("DEPOT_ID").ToString},
                                    {"PRODUCT_ID", zRow.Item("PRODUCT_ID").ToString},
                                    {"BATCH_NUM", zRow.Item("BATCH_NUM").ToString},
                                    {"SALE_QTY", Supplier_New_Qty},
                                    {"ALTER_USER", "系統"},
                                    {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff")}
                                }
                                '判斷更新方式(insert/update)
                                Dim zWhere(,) = {
                                    {"DEPOT_ID", zRow.Item("DEPOT_ID").ToString},
                                    {"PRODUCT_ID", zRow.Item("PRODUCT_ID").ToString},
                                    {"BATCH_NUM", zRow.Item("BATCH_NUM").ToString}
                                }
                                Dim zqdt As DataTable = pf.GetDB("D_DEPOT_STOCKS,NOLOCK", zWhere)
                                If zqdt.Rows.Count > 0 Then
                                    col_value2 = {
                                        {"DEPOT_ID", zRow.Item("DEPOT_ID").ToString},
                                        {"PRODUCT_ID", zRow.Item("PRODUCT_ID").ToString},
                                        {"BATCH_NUM", zRow.Item("BATCH_NUM").ToString},
                                        {"SALE_QTY", Supplier_New_Qty},
                                        {"COST_PRICE", zqdt.Rows(0).Item("COST_PRICE")},
                                        {"UNIT_PRICE", zqdt.Rows(0).Item("UNIT_PRICE")},
                                        {"VALID_PERIOD", CType(zqdt.Rows(0).Item("VALID_PERIOD"), DateTime).ToString("yyyy/MM/dd")},
                                        {"RED_SHOW", zqdt.Rows(0).Item("RED_SHOW")},
                                        {"ALTER_USER", "系統"},
                                        {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff")}
                                    }
                                    sSql = pf.GetUpdatequery(tableName2, pk2, col_value2)
                                Else
                                    col_value2 = {
                                        {"DEPOT_ID", zRow.Item("DEPOT_ID").ToString},
                                        {"PRODUCT_ID", zRow.Item("PRODUCT_ID").ToString},
                                        {"BATCH_NUM", zRow.Item("BATCH_NUM").ToString},
                                        {"SALE_QTY", Supplier_New_Qty},
                                        {"COST_PRICE", zqdt.Rows(0).Item("COST_PRICE")},
                                        {"UNIT_PRICE", zqdt.Rows(0).Item("UNIT_PRICE")},
                                        {"VALID_PERIOD", CType(zqdt.Rows(0).Item("VALID_PERIOD"), DateTime).ToString("yyyy/MM/dd")},
                                        {"RED_SHOW", zqdt.Rows(0).Item("RED_SHOW")},
                                        {"CREATE_USER", "系統"},
                                        {"CREATE_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff")}
                                    }
                                    sSql = pf.GetInsertquery(tableName2, col_value2)
                                End If
                                Console.WriteLine(sSql)
                                command.CommandText = sSql
                                iAffectRows = command.ExecuteNonQuery()
                                If iAffectRows = 0 Then
                                    Return False
                                End If
                            End If
                        End If
                    Next
                End If
            End If
        Catch ex As Exception
            pf.MyError(ex.ToString)
            Return False
        End Try
        Return True
    End Function

    '異動入庫單的當日庫存庫存數量
    '計算公式：庫存數量=前一日的期末庫存數量+[覆核日]=當日庫存異動數量
    '更新到[D_DEPOT_STOCKS]
    'STOCK_IN_ID:出庫單編號
    Function UPDATE_D_DEPOT_STOCKS_IN(STOCK_IN_ID As String, command As SqlCommand) As Boolean
        Dim sSql As String = ""
        Dim iAffectRows As Integer = 0
        Try
            Dim mdt As New DataTable
            Dim ddt As New DataTable
            '出庫單主檔資料
            mdt = pf.GetDB("D_STOCK_IN_MASTER,NOLOCK", {{"STOCK_IN_ID", STOCK_IN_ID}})
            Dim xRow As DataRow
            xRow = mdt.Rows(0)
            '生效日
            Dim EFFECTIVE_DATE As Date = Convert.ToDateTime(xRow.Item("EFFECTIVE_DATE")).ToString("yyyy/MM/dd")
            '覆核日
            Dim CHANGE_DATE As Date = Nothing
            If xRow.Item("VOUCHER_STATUS").ToString = "確認" Then
                '覆核日
                CHANGE_DATE = Convert.ToDateTime(xRow.Item("VOUCHER_TIME")).ToString("yyyy/MM/dd")
            ElseIf xRow.Item("VOUCHER_STATUS").ToString = "生效" Or xRow.Item("VOUCHER_STATUS").ToString = "作廢" Then
                '異動日期
                CHANGE_DATE = Convert.ToDateTime(xRow.Item("ALTER_TIME")).ToString("yyyy/MM/dd")
            End If

            If xRow.Item("PAPER_FROM") = "盤整單" Or xRow.Item("PAPER_FROM") = "寄售盤整單" Then
                If xRow.Item("VOUCHER_STATUS").ToString = "確認" Then
                    '覆核日
                    CHANGE_DATE = Convert.ToDateTime(xRow.Item("EFFECTIVE_DATE")).ToString("yyyy/MM/dd")
                ElseIf xRow.Item("VOUCHER_STATUS").ToString = "生效" Or xRow.Item("VOUCHER_STATUS").ToString = "作廢" Then
                    '異動日期
                    CHANGE_DATE = Convert.ToDateTime(xRow.Item("ALTER_TIME")).ToString("yyyy/MM/dd")
                End If
            End If

            '單據狀態:銷貨單、銷貨換出單、進退單、供應商產品換出單，異動倉庫現有庫存的日期以出庫確認的出庫日期為準 
            '出庫確認=1 出庫日期
            'If xRow.Item("PAPER_FROM") = "銷貨單" Or xRow.Item("PAPER_FROM") = "銷貨換出單" Or xRow.Item("PAPER_FROM") = "進退單" Or xRow.Item("PAPER_FROM") = "供應商產品換出單" Then
            If xRow.Item("PAPER_FROM") = "銷貨單" Or xRow.Item("PAPER_FROM") = "銷貨換出單" Or xRow.Item("PAPER_FROM") = "進退單" Or xRow.Item("PAPER_FROM") = "供應商產品換出單" Then
                If xRow.Item("VOUCHER_STATUS").ToString = "生效" Then
                    If DBNull.Value.Equals(xRow.Item("ALTER_TIME")) Then
                        CHANGE_DATE = Convert.ToDateTime(xRow.Item("CREATE_TIME")).ToString("yyyy/MM/dd")
                    Else
                        CHANGE_DATE = Convert.ToDateTime(xRow.Item("ALTER_TIME")).ToString("yyyy/MM/dd")
                    End If
                ElseIf xRow.Item("VOUCHER_STATUS").ToString = "確認" Then
                    'CHANGE_DATE = Convert.ToDateTime(xRow.Item("ALTER_TIME")).ToString("yyyy/MM/dd")
                    If pf.CheckNull(xRow.Item("INBOUND_CHECK")) = 1 Then
                        CHANGE_DATE = Convert.ToDateTime(xRow.Item("INBOUND_CHECK_DATE")).ToString("yyyy/MM/dd")
                    ElseIf pf.CheckNull(xRow.Item("INBOUND_CHECK")) = 0 Then
                        CHANGE_DATE = Convert.ToDateTime(xRow.Item("ALTER_TIME")).ToString("yyyy/MM/dd")
                    End If
                ElseIf xRow.Item("VOUCHER_STATUS").ToString = "作廢" Then
                    CHANGE_DATE = Convert.ToDateTime(xRow.Item("ALTER_TIME")).ToString("yyyy/MM/dd")
                End If
            End If

            '將明細寫入[D_DEPOT_STOCKS_DIFF]，然後再統一計算庫存異動數量
            ddt = pf.GetDB("D_STOCK_IN_DETAIL,NOLOCK", {{"STOCK_IN_ID", STOCK_IN_ID}})
            Dim table As String = "D_DEPOT_STOCKS_DIFF"
            For j As Integer = 0 To ddt.Rows.Count - 1
                Dim yRow As DataRow
                yRow = ddt.Rows(j)
                Dim total_qty As Double = 0
                '良品
                Dim good_qty As Double = 0
                '破損品
                Dim bad_qty As Double = 0
                If pf.CheckIfDBNull(yRow.Item("STOCK_CHECK")) = 1 Then
                    '入庫確認
                    total_qty = (Convert.ToDouble(pf.CheckNull(yRow.Item("QTY"))) + Convert.ToDouble(pf.CheckNull(yRow.Item("PRESENT_QTY"))) + Convert.ToDouble(pf.CheckNull(yRow.Item("NG_QTY"))))
                    good_qty = (Convert.ToDouble(pf.CheckNull(yRow.Item("QTY"))) + Convert.ToDouble(pf.CheckNull(yRow.Item("PRESENT_QTY"))) + Convert.ToDouble(pf.CheckNull(yRow.Item("NG_QTY"))))
                    bad_qty = Convert.ToDouble(pf.CheckNull(yRow.Item("BAD_QTY")))
                ElseIf pf.CheckIfDBNull(yRow.Item("STOCK_CHECK")) = 0 Then
                    '取消入庫確認
                    total_qty = (Convert.ToDouble(pf.CheckNull(yRow.Item("QTY"))) + Convert.ToDouble(pf.CheckNull(yRow.Item("PRESENT_QTY"))) + Convert.ToDouble(pf.CheckNull(yRow.Item("NG_QTY")))) * (-1)
                    good_qty = (Convert.ToDouble(pf.CheckNull(yRow.Item("QTY"))) + Convert.ToDouble(pf.CheckNull(yRow.Item("PRESENT_QTY"))) + Convert.ToDouble(pf.CheckNull(yRow.Item("NG_QTY")))) * (-1)
                    bad_qty = Convert.ToDouble(pf.CheckNull(yRow.Item("BAD_QTY"))) * (-1)
                End If
                If xRow.Item("STOCK_TYPE").ToString = "公司" Then
                    Dim col_value(,) As String = {
                        {"PAPER_ID", pf.CheckStringDBNull(yRow.Item("STOCK_IN_ID"))},
                        {"EFFECTIVE_DATE", xRow.Item("EFFECTIVE_DATE")},
                        {"CHANGE_DATE", CHANGE_DATE},
                        {"DEPOT_ID", pf.CheckStringDBNull(yRow.Item("DEPOT_ID"))},
                        {"PRODUCT_ID", pf.CheckStringDBNull(yRow.Item("PRODUCT_ID"))},
                        {"BATCH_NUM", pf.CheckStringDBNull(yRow.Item("BATCH_NUM"))},
                        {"G_DIFF_QTY", good_qty},
                        {"B_DIFF_QTY", bad_qty},
                        {"C_DIFF_QTY", 0},
                        {"CUS_DIFF_QTY", 0},
                        {"SUPPLIER_DIFF_QTY", 0},
                        {"VOUCHER_STATUS", pf.CheckStringDBNull(xRow.Item("VOUCHER_STATUS"))},
                        {"CUSTOMER_ID", pf.CheckStringDBNull(xRow.Item("CUSTOMER_ID"))},
                        {"SUPPLIER_ID", pf.CheckStringDBNull(xRow.Item("SUPPLIER_ID"))},
                        {"CREATE_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                    }
                    sSql = pf.GetInsertquery(table, col_value)

                ElseIf xRow.Item("STOCK_TYPE").ToString = "寄售" Then

                    Dim col_value(,) As String = {
                        {"PAPER_ID", pf.CheckStringDBNull(yRow.Item("STOCK_IN_ID"))},
                        {"EFFECTIVE_DATE", xRow.Item("EFFECTIVE_DATE")},
                        {"CHANGE_DATE", CHANGE_DATE},
                        {"DEPOT_ID", pf.CheckStringDBNull(yRow.Item("DEPOT_ID"))},
                        {"PRODUCT_ID", pf.CheckStringDBNull(yRow.Item("PRODUCT_ID"))},
                        {"BATCH_NUM", pf.CheckStringDBNull(yRow.Item("BATCH_NUM"))},
                        {"G_DIFF_QTY", 0},
                        {"B_DIFF_QTY", 0},
                        {"C_DIFF_QTY", 0},
                        {"CUS_DIFF_QTY", total_qty},
                        {"SUPPLIER_DIFF_QTY", 0},
                        {"VOUCHER_STATUS", pf.CheckStringDBNull(xRow.Item("VOUCHER_STATUS"))},
                        {"CUSTOMER_ID", pf.CheckStringDBNull(xRow.Item("CUSTOMER_ID"))},
                        {"SUPPLIER_ID", pf.CheckStringDBNull(xRow.Item("SUPPLIER_ID"))},
                        {"CREATE_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                    }
                    sSql = pf.GetInsertquery(table, col_value)

                ElseIf xRow.Item("STOCK_TYPE").ToString = "經銷" Then
                    Dim col_value(,) As String = {
                        {"PAPER_ID", pf.CheckStringDBNull(yRow.Item("STOCK_IN_ID"))},
                        {"EFFECTIVE_DATE", xRow.Item("EFFECTIVE_DATE")},
                        {"CHANGE_DATE", CHANGE_DATE},
                        {"DEPOT_ID", pf.CheckStringDBNull(yRow.Item("DEPOT_ID"))},
                        {"PRODUCT_ID", pf.CheckStringDBNull(yRow.Item("PRODUCT_ID"))},
                        {"BATCH_NUM", pf.CheckStringDBNull(yRow.Item("BATCH_NUM"))},
                        {"G_DIFF_QTY", 0},
                        {"B_DIFF_QTY", 0},
                        {"C_DIFF_QTY", 0},
                        {"CUS_DIFF_QTY", 0},
                        {"SUPPLIER_DIFF_QTY", total_qty},
                        {"VOUCHER_STATUS", pf.CheckStringDBNull(xRow.Item("VOUCHER_STATUS"))},
                        {"CUSTOMER_ID", pf.CheckStringDBNull(xRow.Item("CUSTOMER_ID"))},
                        {"SUPPLIER_ID", pf.CheckStringDBNull(xRow.Item("SUPPLIER_ID"))},
                        {"CREATE_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                    }
                    sSql = pf.GetInsertquery(table, col_value)

                End If
                Console.WriteLine(sSql)
                command.CommandText = sSql
                iAffectRows = command.ExecuteNonQuery()
                If iAffectRows = 0 Then
                    Return False
                End If
            Next

            Dim CHECK_DATE As New DateTime
            CHECK_DATE = DateTime.Now

            '即時異動當日庫存數量[覆核日]
            If xRow.Item("STOCK_TYPE").ToString = "公司" Then
                sSql = ""
                Dim New_Qty As Double = 0
                Dim Good_New_Qty As Double = 0
                Dim Bad_New_Qty As Double = 0
                '當日庫存異動數量
                sSql = "SELECT CONVERT(VARCHAR,[CHANGE_DATE],111) AS CDATE,[D_DEPOT_STOCKS_DIFF].[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                sSql &= ",SUM(ISNULL([G_DIFF_QTY],0)) as G_DIFF_QTY,SUM(ISNULL([B_DIFF_QTY],0)) as B_DIFF_QTY "
                sSql &= " FROM [D_DEPOT_STOCKS_DIFF] WITH (NOLOCK) "
                sSql &= " INNER JOIN [D_DEPOT_BASE] ON [D_DEPOT_BASE].[DEPOT_ID]=[D_DEPOT_STOCKS_DIFF].[DEPOT_ID] "
                sSql &= " AND [D_DEPOT_BASE].[DEPOT_TYPE]='公司'"
                sSql &= " WHERE [CHANGE_DATE] ='" & CHECK_DATE.ToString("yyyy/MM/dd") & "'"
                sSql &= " GROUP BY CONVERT(VARCHAR,[CHANGE_DATE],111),[D_DEPOT_STOCKS_DIFF].[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                Console.WriteLine(sSql)
                Dim qdt As New DataTable
                qdt = pf.GetDataTable(sSql)
                If qdt.Rows.Count > 0 Then
                    For j = 0 To qdt.Rows.Count - 1
                        Dim zRow As DataRow = qdt.Rows(j)
                        '取最近一次的庫存期末數量DEPOT_QTY
                        sSql = "SELECT TOP 1 [EFFECTIVE_DATE],[GOOD_QTY],[BAD_QTY],[DEPOT_QTY] FROM [D_DEPOT_STOCKS_DAYS] WITH (NOLOCK) "
                        sSql &= " WHERE [DEPOT_ID]='" & zRow.Item("DEPOT_ID").ToString & "'"
                        sSql &= " AND [PRODUCT_ID]='" & zRow.Item("PRODUCT_ID").ToString & "'"
                        sSql &= " AND [BATCH_NUM]='" & zRow.Item("BATCH_NUM").ToString & "'"
                        sSql &= " ORDER BY [EFFECTIVE_DATE] DESC "
                        Console.WriteLine(sSql)
                        Dim qdt2 As New DataTable
                        qdt2 = pf.GetDataTable(sSql)
                        If qdt2.Rows.Count > 0 Then
                            '有查詢到前一日的期末庫存，庫存數量直接運算後更新
                            New_Qty = Convert.ToDouble(pf.CheckNull(qdt2.Rows(0).Item("DEPOT_QTY"))) + Convert.ToDouble(pf.CheckNull(zRow.Item("G_DIFF_QTY")))
                            Good_New_Qty = Convert.ToDouble(pf.CheckNull(qdt2.Rows(0).Item("DEPOT_QTY"))) + Convert.ToDouble(pf.CheckNull(zRow.Item("G_DIFF_QTY")))
                            Bad_New_Qty = Convert.ToDouble(pf.CheckNull(qdt2.Rows(0).Item("BAD_QTY"))) + Convert.ToDouble(pf.CheckNull(zRow.Item("B_DIFF_QTY")))
                        Else
                            '若每日庫存沒有資料者，直接更新
                            New_Qty = Convert.ToDouble(pf.CheckNull(zRow.Item("G_DIFF_QTY")))
                            Good_New_Qty = Convert.ToDouble(pf.CheckNull(zRow.Item("G_DIFF_QTY")))
                            Bad_New_Qty = Convert.ToDouble(pf.CheckNull(zRow.Item("B_DIFF_QTY")))
                        End If

                        Dim tableName As String = "D_DEPOT_STOCKS"
                        Dim pk() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM"}
                        Dim col_value(,) As String = {
                            {"DEPOT_ID", zRow.Item("DEPOT_ID").ToString},
                            {"PRODUCT_ID", zRow.Item("PRODUCT_ID").ToString},
                            {"BATCH_NUM", zRow.Item("BATCH_NUM").ToString},
                            {"GOOD_QTY", Good_New_Qty},
                            {"BAD_QTY", Bad_New_Qty},
                            {"DEPOT_QTY", Good_New_Qty},
                            {"ALTER_USER", "系統"},
                            {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                        }

                        '判斷D_DEPOT_STOCKS資料是否存在
                        Dim where_exist(,) As String = {
                            {"DEPOT_ID", zRow.Item("DEPOT_ID").ToString},
                            {"PRODUCT_ID", zRow.Item("PRODUCT_ID").ToString},
                            {"BATCH_NUM", zRow.Item("BATCH_NUM").ToString}
                        }
                        Dim check_exist As DataTable = pf.GetDB("D_DEPOT_STOCKS,NOLOCK", where_exist)
                        If check_exist.Rows.Count > 0 Then
                            sSql = pf.GetUpdatequery(tableName, pk, col_value)
                        Else
                            sSql = pf.GetInsertquery(tableName, col_value)
                        End If
                        Console.WriteLine(sSql)
                        command.CommandText = sSql
                        iAffectRows = command.ExecuteNonQuery()
                        If iAffectRows = 0 Then
                            Return False
                        End If
                    Next

                End If

            ElseIf xRow.Item("STOCK_TYPE").ToString = "寄售" Then

                '異動客戶寄售庫存[D_CUS_CON_QTY_DAYS]&[D_CUS_CON_QTY]
                sSql = "SELECT [CUSTOMER_ID],CONVERT(VARCHAR,[CHANGE_DATE],111) AS CDATE,[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                sSql &= ",SUM(ISNULL([CUS_DIFF_QTY],0)) as CUS_DIFF_QTY "
                sSql &= " FROM [D_DEPOT_STOCKS_DIFF] WITH (NOLOCK) "
                sSql &= " WHERE [CHANGE_DATE] ='" & CHECK_DATE.ToString("yyyy/MM/dd") & "'"
                sSql &= " AND [CUSTOMER_ID] ='" & xRow.Item("CUSTOMER_ID").ToString & "'"
                sSql &= " GROUP BY [CUSTOMER_ID],CONVERT(VARCHAR,[CHANGE_DATE],111),[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                Console.WriteLine(sSql)
                Dim qdt As New DataTable
                qdt = pf.GetDataTable(sSql)
                If qdt.Rows.Count > 0 Then
                    Dim CUS_New_Qty As Double = 0
                    For j = 0 To qdt.Rows.Count - 1
                        Dim zRow As DataRow = qdt.Rows(j)
                        '取D_CUS_CON_QTY_DAYS最近一次的庫存期末數量
                        sSql = "SELECT TOP 1 [CUS_DEPOT_QTY] FROM [D_CUS_CON_QTY_DAYS] WITH (NOLOCK)"
                        sSql &= " WHERE [DEPOT_ID]='" & zRow.Item("DEPOT_ID").ToString & "'"
                        sSql &= " AND [PRODUCT_ID]='" & zRow.Item("PRODUCT_ID").ToString & "'"
                        sSql &= " AND [BATCH_NUM]='" & zRow.Item("BATCH_NUM").ToString & "'"
                        sSql &= " AND [CUSTOMER_ID]='" & zRow.Item("CUSTOMER_ID").ToString & "'"
                        sSql &= " ORDER BY [EFFECTIVE_DATE] DESC "
                        Console.WriteLine(sSql)
                        Dim qdt2 As New DataTable
                        qdt2 = pf.GetDataTable(sSql)
                        If qdt2.Rows.Count > 0 Then
                            CUS_New_Qty = Convert.ToDouble(pf.CheckNull(qdt2.Rows(0).Item("CUS_DEPOT_QTY"))) + Convert.ToDouble(pf.CheckNull(zRow.Item("CUS_DIFF_QTY")))
                        Else
                            '直接更新
                            CUS_New_Qty = Convert.ToDouble(pf.CheckNull(zRow.Item("CUS_DIFF_QTY")))
                        End If

                        '判斷更新方式
                        Dim tableName As String = "D_CUS_CON_QTY"
                        sSql = "SELECT * FROM [" & tableName & "] WITH (NOLOCK) "
                        sSql &= " WHERE [DEPOT_ID]='" & zRow.Item("DEPOT_ID").ToString & "'"
                        sSql &= " AND [PRODUCT_ID]='" & zRow.Item("PRODUCT_ID").ToString & "'"
                        sSql &= " AND [BATCH_NUM]='" & zRow.Item("BATCH_NUM").ToString & "'"
                        sSql &= " AND [CUSTOMER_ID]='" & zRow.Item("CUSTOMER_ID").ToString & "'"
                        Dim chk_dt As New DataTable
                        chk_dt = pf.GetDataTable(sSql)
                        If chk_dt.Rows.Count > 0 Then
                            'UPDATE
                            Dim pk() As String = {"CUSTOMER_ID", "DEPOT_ID", "PRODUCT_ID", "BATCH_NUM"}
                            Dim col_value(,) As String = {
                                {"CUSTOMER_ID", zRow.Item("CUSTOMER_ID").ToString},
                                {"DEPOT_ID", zRow.Item("DEPOT_ID").ToString},
                                {"PRODUCT_ID", zRow.Item("PRODUCT_ID").ToString},
                                {"BATCH_NUM", zRow.Item("BATCH_NUM").ToString},
                                {"CUS_DEPOT_QTY", CUS_New_Qty},
                                {"ALTER_USER", "系統"},
                                {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                            }
                            sSql = pf.GetUpdatequery(tableName, pk, col_value)
                        Else
                            'INSERT
                            Dim col_value(,) As String = {
                                {"CUSTOMER_ID", zRow.Item("CUSTOMER_ID").ToString},
                                {"DEPOT_ID", zRow.Item("DEPOT_ID").ToString},
                                {"PRODUCT_ID", zRow.Item("PRODUCT_ID").ToString},
                                {"BATCH_NUM", zRow.Item("BATCH_NUM").ToString},
                                {"CUS_DEPOT_QTY", CUS_New_Qty},
                                {"CREATE_USER", "系統"},
                                {"CREATE_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                            }
                            sSql = pf.GetInsertquery(tableName, col_value)
                        End If
                        Console.WriteLine(sSql)
                        command.CommandText = sSql
                        iAffectRows = command.ExecuteNonQuery()
                        If iAffectRows = 0 Then
                            Return False
                        End If
                    Next

                End If

            ElseIf xRow.Item("STOCK_TYPE").ToString = "經銷" Then
                '異動經銷商庫存[D_DEALER_CON_QTY]&[D_DEALER_CON_QTY_DAYS]
                sSql = "SELECT [SUPPLIER_ID],CONVERT(VARCHAR,[CHANGE_DATE],111) AS CDATE,[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                sSql &= ",SUM(ISNULL([SUPPLIER_DIFF_QTY],0)) as SUPPLIER_DIFF_QTY "
                sSql &= " FROM [D_DEPOT_STOCKS_DIFF] WITH (NOLOCK) "
                sSql &= " WHERE [CHANGE_DATE] ='" & CHECK_DATE.ToString("yyyy/MM/dd") & "'"
                sSql &= " AND [SUPPLIER_ID] ='" & xRow.Item("SUPPLIER_ID").ToString & "'"
                sSql &= " GROUP BY [SUPPLIER_ID],CONVERT(VARCHAR,[CHANGE_DATE],111),[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                Console.WriteLine(sSql)
                Dim qdt As New DataTable
                qdt = pf.GetDataTable(sSql)
                If qdt.Rows.Count > 0 Then

                    Dim Supplier_New_Qty As Double = 0
                    For j = 0 To qdt.Rows.Count - 1
                        Dim zRow As DataRow = qdt.Rows(j)
                        '取D_DEALER_CON_QTY_DAYS最近一次的庫存期末數量
                        sSql = "SELECT TOP 1 [CON_DEPOT_QTY] FROM [D_DEALER_CON_QTY_DAYS] WITH (NOLOCK)"
                        sSql &= " WHERE [DEPOT_ID]='" & zRow.Item("DEPOT_ID").ToString & "'"
                        sSql &= " AND [PRODUCT_ID]='" & zRow.Item("PRODUCT_ID").ToString & "'"
                        sSql &= " AND [BATCH_NUM]='" & zRow.Item("BATCH_NUM").ToString & "'"
                        sSql &= " AND [SUPPLIER_ID]='" & zRow.Item("SUPPLIER_ID").ToString & "'"
                        sSql &= " ORDER BY [EFFECTIVE_DATE] DESC "
                        Console.WriteLine(sSql)
                        Dim qdt2 As New DataTable
                        qdt2 = pf.GetDataTable(sSql)
                        Dim CON_DEPOT_QTY As Double = 0
                        If qdt2.Rows.Count > 0 Then
                            CON_DEPOT_QTY = Convert.ToDouble(pf.CheckNull(qdt2.Rows(0).Item("CON_DEPOT_QTY")))
                            Supplier_New_Qty = Convert.ToDouble(pf.CheckNull(qdt2.Rows(0).Item("CON_DEPOT_QTY"))) + Convert.ToDouble(pf.CheckNull(zRow.Item("SUPPLIER_DIFF_QTY")))
                        Else
                            '直接更新
                            Supplier_New_Qty = Convert.ToDouble(pf.CheckNull(zRow.Item("SUPPLIER_DIFF_QTY")))
                        End If
                        '判斷更新方式
                        Dim tableName As String = "D_DEALER_CON_QTY"
                        sSql = "SELECT * FROM [" & tableName & "] WITH (NOLOCK) "
                        sSql &= " WHERE [DEPOT_ID]='" & zRow.Item("DEPOT_ID").ToString & "'"
                        sSql &= " AND [PRODUCT_ID]='" & zRow.Item("PRODUCT_ID").ToString & "'"
                        sSql &= " AND [BATCH_NUM]='" & zRow.Item("BATCH_NUM").ToString & "'"
                        sSql &= " AND [SUPPLIER_ID]='" & zRow.Item("SUPPLIER_ID").ToString & "'"
                        Dim zqdt As New DataTable
                        zqdt = pf.GetDataTable(sSql)
                        If zqdt.Rows.Count > 0 Then
                            'UPDATE
                            Dim pk() As String = {"SUPPLIER_ID", "DEPOT_ID", "PRODUCT_ID", "BATCH_NUM"}
                            Dim col_value(,) As String = {
                                {"SUPPLIER_ID", zRow.Item("SUPPLIER_ID").ToString},
                                {"DEPOT_ID", zRow.Item("DEPOT_ID").ToString},
                                {"PRODUCT_ID", zRow.Item("PRODUCT_ID").ToString},
                                {"BATCH_NUM", zRow.Item("BATCH_NUM").ToString},
                                {"CON_DEPOT_QTY", Supplier_New_Qty},
                                {"ALTER_USER", "系統"},
                                {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                            }
                            sSql = pf.GetUpdatequery(tableName, pk, col_value)
                        Else
                            'INSERT
                            Dim col_value(,) As String = {
                                {"SUPPLIER_ID", zRow.Item("SUPPLIER_ID").ToString},
                                {"DEPOT_ID", zRow.Item("DEPOT_ID").ToString},
                                {"PRODUCT_ID", zRow.Item("PRODUCT_ID").ToString},
                                {"BATCH_NUM", zRow.Item("BATCH_NUM").ToString},
                                {"CON_DEPOT_QTY", Supplier_New_Qty},
                                {"CREATE_USER", "系統"},
                                {"CREATE_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                            }
                            sSql = pf.GetInsertquery(tableName, col_value)
                        End If
                        Console.WriteLine(sSql)
                        command.CommandText = sSql
                        iAffectRows = command.ExecuteNonQuery()
                        If iAffectRows = 0 Then
                            Return False
                        End If

                        '2020-12-31
                        '更新D_DEPOT_STOCKS的經銷倉
                        '取經銷商倉庫代號
                        Dim qdt3 As DataTable = pf.GetDB("D_DEPOT_BASE,NOLOCK", {{"DEPOT_ID", zRow.Item("DEPOT_ID")}})
                        If qdt3.Rows.Count > 0 Then
                            If qdt3.Rows(0).Item("DEPOT_TYPE").ToString = "經銷商" AndAlso qdt3.Rows(0).Item("DEPOT_ID").ToString = zRow.Item("DEPOT_ID").ToString Then
                                '取產品的VALID_PERIOD
                                Dim pWhere(,) As String = {
                                        {"PRODUCT_ID", zRow.Item("PRODUCT_ID")},
                                        {"BATCH_NUM", zRow.Item("BATCH_NUM")},
                                        {"ORDERBY_ASC", "DEPOT_ID"}
                                    }
                                Dim pdt As DataTable = pf.GetDB("D_DEPOT_STOCKS,NOLOCK", pWhere)
                                '取最近一次的庫存期末數量DEPOT_QTY
                                sSql = "SELECT TOP 1 [EFFECTIVE_DATE],[GOOD_QTY],[BAD_QTY],[DEPOT_QTY] FROM [D_DEPOT_STOCKS_DAYS] WITH (NOLOCK) "
                                sSql &= " WHERE [DEPOT_ID]='" & zRow.Item("DEPOT_ID").ToString & "'"
                                sSql &= " AND [PRODUCT_ID]='" & zRow.Item("PRODUCT_ID").ToString & "'"
                                sSql &= " AND [BATCH_NUM]='" & zRow.Item("BATCH_NUM").ToString & "'"
                                sSql &= " ORDER BY [EFFECTIVE_DATE] DESC "
                                Console.WriteLine(sSql)
                                Dim New_Qty As Double = 0
                                Dim Good_New_Qty As Double = 0
                                Dim Bad_New_Qty As Double = 0
                                qdt2 = pf.GetDataTable(sSql)
                                If qdt2.Rows.Count > 0 Then
                                    '有查詢到前一日的期末庫存，庫存數量直接運算後更新
                                    New_Qty = Convert.ToDouble(pf.CheckNull(qdt2.Rows(0).Item("DEPOT_QTY"))) + Convert.ToDouble(pf.CheckNull(zRow.Item("SUPPLIER_DIFF_QTY")))
                                    Good_New_Qty = Convert.ToDouble(pf.CheckNull(qdt2.Rows(0).Item("DEPOT_QTY"))) + Convert.ToDouble(pf.CheckNull(zRow.Item("SUPPLIER_DIFF_QTY")))
                                    New_Qty = Supplier_New_Qty
                                    Good_New_Qty = Supplier_New_Qty
                                Else

                                    '若每日庫存沒有資料者，直接更新
                                    New_Qty = Convert.ToDouble(pf.CheckNull(zRow.Item("SUPPLIER_DIFF_QTY")))
                                    Good_New_Qty = Convert.ToDouble(pf.CheckNull(zRow.Item("SUPPLIER_DIFF_QTY")))
                                End If

                                '異動倉庫現有庫存
                                Dim tableName2 As String = "D_DEPOT_STOCKS"
                                Dim pk2() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM"}
                                Dim col_value2(,) As String = {
                                    {"DEPOT_ID", zRow.Item("DEPOT_ID").ToString},
                                    {"PRODUCT_ID", zRow.Item("PRODUCT_ID").ToString},
                                    {"BATCH_NUM", zRow.Item("BATCH_NUM").ToString},
                                    {"GOOD_QTY", Supplier_New_Qty},
                                    {"DEPOT_QTY", Supplier_New_Qty},
                                    {"ALTER_USER", "系統"},
                                    {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                                }
                                '判斷D_DEPOT_STOCKS資料是否存在
                                Dim where_exist2(,) As String = {
                                        {"DEPOT_ID", zRow.Item("DEPOT_ID").ToString},
                                        {"PRODUCT_ID", zRow.Item("PRODUCT_ID").ToString},
                                        {"BATCH_NUM", zRow.Item("BATCH_NUM").ToString}
                                    }
                                Dim check_exist As DataTable = pf.GetDB("D_DEPOT_STOCKS,NOLOCK", where_exist2)
                                If check_exist.Rows.Count > 0 Then
                                    col_value2 = {
                                        {"DEPOT_ID", zRow.Item("DEPOT_ID").ToString},
                                        {"PRODUCT_ID", zRow.Item("PRODUCT_ID").ToString},
                                        {"BATCH_NUM", zRow.Item("BATCH_NUM").ToString},
                                        {"GOOD_QTY", Supplier_New_Qty},
                                        {"DEPOT_QTY", Supplier_New_Qty},
                                        {"COST_PRICE", pf.CheckNull(pdt.Rows(0).Item("COST_PRICE"))},
                                        {"UNIT_PRICE", pf.CheckNull(pdt.Rows(0).Item("UNIT_PRICE"))},
                                        {"VALID_PERIOD", pf.CheckDateTimeNull(pdt.Rows(0).Item("VALID_PERIOD"))},
                                        {"RED_SHOW", pf.CheckNull(pdt.Rows(0).Item("RED_SHOW"))},
                                        {"ALTER_USER", "系統"},
                                        {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                                    }
                                    sSql = pf.GetUpdatequery(tableName2, pk2, col_value2)
                                Else
                                    col_value2 = {
                                        {"DEPOT_ID", zRow.Item("DEPOT_ID").ToString},
                                        {"PRODUCT_ID", zRow.Item("PRODUCT_ID").ToString},
                                        {"BATCH_NUM", zRow.Item("BATCH_NUM").ToString},
                                        {"GOOD_QTY", Supplier_New_Qty},
                                        {"DEPOT_QTY", Supplier_New_Qty},
                                        {"COST_PRICE", pf.CheckNull(pdt.Rows(0).Item("COST_PRICE"))},
                                        {"UNIT_PRICE", pf.CheckNull(pdt.Rows(0).Item("UNIT_PRICE"))},
                                        {"VALID_PERIOD", pf.CheckDateTimeNull(pdt.Rows(0).Item("VALID_PERIOD"))},
                                        {"RED_SHOW", pf.CheckNull(pdt.Rows(0).Item("RED_SHOW"))},
                                        {"CREATE_USER", "系統"},
                                        {"CREATE_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                                    }
                                    sSql = pf.GetInsertquery(tableName2, col_value2)
                                End If
                                Console.WriteLine(sSql)
                                command.CommandText = sSql
                                iAffectRows = command.ExecuteNonQuery()
                                If iAffectRows = 0 Then
                                    Return False
                                End If
                            End If
                        End If
                    Next
                End If
            End If
        Catch ex As Exception
            pf.MyError(ex.ToString)
            Return False
        End Try
        Return True
    End Function

    '2019-11-19 edit by yuling
    '異動入庫單的當日可動支即時庫存庫存數量
    '計算公式：庫存數量=前一日的期末庫存數量[DEPOT_QTY]+[覆核日]=當日庫存異動數量
    '更新到[D_DEPOT_STOCKS]的[SALE_QTY]
    'STOCK_IN_ID:出庫單編號
    Function UPDATE_D_DEPOT_STOCKS_IN_SALEQTY(STOCK_IN_ID As String, OLD_VOUCHER_STATUS As String, NEW_VOUCHER_STATUS As String, command As SqlCommand) As Boolean
        Dim sSql As String = ""
        Dim iAffectRows As Integer = 0
        Try
            Dim mdt As New DataTable
            Dim ddt As New DataTable
            '入庫單主檔資料
            mdt = pf.GetDB("D_STOCK_IN_MASTER,NOLOCK", {{"STOCK_IN_ID", STOCK_IN_ID}})
            Dim xRow As DataRow
            xRow = mdt.Rows(0)
            '生效日
            Dim EFFECTIVE_DATE As Date = Convert.ToDateTime(xRow.Item("EFFECTIVE_DATE")).ToString("yyyy/MM/dd")
            '覆核日
            Dim CHANGE_DATE As Date = Nothing
            If xRow.Item("VOUCHER_STATUS").ToString = "確認" Then
                '覆核日
                CHANGE_DATE = Convert.ToDateTime(xRow.Item("VOUCHER_TIME")).ToString("yyyy/MM/dd")
            ElseIf xRow.Item("VOUCHER_STATUS").ToString = "生效" Or xRow.Item("VOUCHER_STATUS").ToString = "作廢" Then
                '異動日期
                CHANGE_DATE = Convert.ToDateTime(xRow.Item("ALTER_TIME")).ToString("yyyy/MM/dd")
            End If

            '出庫單確認時使用
            If OLD_VOUCHER_STATUS = "否" AndAlso NEW_VOUCHER_STATUS = "是" Then
                CHANGE_DATE = Convert.ToDateTime(xRow.Item("ALTER_TIME")).ToString("yyyy/MM/dd")
            ElseIf OLD_VOUCHER_STATUS = "是" AndAlso NEW_VOUCHER_STATUS = "否" Then
                CHANGE_DATE = Convert.ToDateTime(xRow.Item("ALTER_TIME")).ToString("yyyy/MM/dd")
            End If

            '將明細寫入[D_DEPOT_STOCKS_KEYIN_DIFF]，然後再統一計算庫存異動數量
            ddt = pf.GetDB("D_STOCK_IN_DETAIL,NOLOCK", {{"STOCK_IN_ID", STOCK_IN_ID}})
            Dim table As String = "D_DEPOT_STOCKS_KEYIN_DIFF"
            For j As Integer = 0 To ddt.Rows.Count - 1
                Dim yRow As DataRow
                yRow = ddt.Rows(j)
                Dim total_qty As Double = 0
                '良品
                Dim good_qty As Double = 0
                '破損品
                Dim bad_qty As Double = 0
                'If pf.CheckIfDBNull(yRow.Item("STOCK_CHECK")) = 1 Then
                '    '入庫確認
                '    total_qty = (Convert.ToDouble(yRow.Item("QTY")) + Convert.ToDouble(yRow.Item("PRESENT_QTY")))
                'ElseIf pf.CheckIfDBNull(yRow.Item("STOCK_CHECK")) = 0 Then
                '    '取消入庫確認
                '    total_qty = (Convert.ToDouble(yRow.Item("QTY")) + Convert.ToDouble(yRow.Item("PRESENT_QTY"))) * (-1)
                'End If
                If OLD_VOUCHER_STATUS = "生效" AndAlso NEW_VOUCHER_STATUS = "確認" Then
                    total_qty = (Convert.ToDouble(pf.CheckNull(yRow.Item("QTY"))) + Convert.ToDouble(pf.CheckNull(yRow.Item("PRESENT_QTY"))) + Convert.ToDouble(pf.CheckNull(yRow.Item("NG_QTY"))))
                ElseIf OLD_VOUCHER_STATUS = "確認" AndAlso NEW_VOUCHER_STATUS = "作廢" Then
                    total_qty = (Convert.ToDouble(pf.CheckNull(yRow.Item("QTY"))) + Convert.ToDouble(pf.CheckNull(yRow.Item("PRESENT_QTY"))) + Convert.ToDouble(pf.CheckNull(yRow.Item("NG_QTY")))) * (-1)
                ElseIf OLD_VOUCHER_STATUS = "確認" AndAlso NEW_VOUCHER_STATUS = "生效" Then
                    total_qty = (Convert.ToDouble(pf.CheckNull(yRow.Item("QTY"))) + Convert.ToDouble(pf.CheckNull(yRow.Item("PRESENT_QTY"))) + Convert.ToDouble(pf.CheckNull(yRow.Item("NG_QTY")))) * (-1)
                End If

                '出庫單確認時使用
                If OLD_VOUCHER_STATUS = "否" AndAlso NEW_VOUCHER_STATUS = "是" Then
                    total_qty = (Convert.ToDouble(pf.CheckNull(yRow.Item("QTY"))) + Convert.ToDouble(pf.CheckNull(yRow.Item("PRESENT_QTY"))) + Convert.ToDouble(pf.CheckNull(yRow.Item("NG_QTY"))))
                ElseIf OLD_VOUCHER_STATUS = "是" AndAlso NEW_VOUCHER_STATUS = "否" Then
                    total_qty = (Convert.ToDouble(pf.CheckNull(yRow.Item("QTY"))) + Convert.ToDouble(pf.CheckNull(yRow.Item("PRESENT_QTY"))) + Convert.ToDouble(pf.CheckNull(yRow.Item("NG_QTY")))) * (-1)
                End If

                If xRow.Item("STOCK_TYPE").ToString = "公司" Then
                    Dim col_value(,) As String = {
                        {"PAPER_ID", pf.CheckStringDBNull(yRow.Item("STOCK_IN_ID"))},
                        {"EFFECTIVE_DATE", xRow.Item("EFFECTIVE_DATE")},
                        {"CHANGE_DATE", CHANGE_DATE},
                        {"DEPOT_ID", pf.CheckStringDBNull(yRow.Item("DEPOT_ID"))},
                        {"PRODUCT_ID", pf.CheckStringDBNull(yRow.Item("PRODUCT_ID"))},
                        {"BATCH_NUM", pf.CheckStringDBNull(yRow.Item("BATCH_NUM"))},
                        {"CUS_DIFF_QTY", 0},
                        {"SUPPLIER_DIFF_QTY", 0},
                        {"KEYIN_DIFF_QTY", total_qty},
                        {"VOUCHER_STATUS", pf.CheckStringDBNull(xRow.Item("VOUCHER_STATUS"))},
                        {"CUSTOMER_ID", pf.CheckStringDBNull(xRow.Item("CUSTOMER_ID"))},
                        {"SUPPLIER_ID", pf.CheckStringDBNull(xRow.Item("SUPPLIER_ID"))},
                        {"CREATE_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                    }
                    sSql = pf.GetInsertquery(table, col_value)

                ElseIf xRow.Item("STOCK_TYPE").ToString = "寄售" Then

                    Dim col_value(,) As String = {
                        {"PAPER_ID", pf.CheckStringDBNull(yRow.Item("STOCK_IN_ID"))},
                        {"EFFECTIVE_DATE", xRow.Item("EFFECTIVE_DATE")},
                        {"CHANGE_DATE", CHANGE_DATE},
                        {"DEPOT_ID", pf.CheckStringDBNull(yRow.Item("DEPOT_ID"))},
                        {"PRODUCT_ID", pf.CheckStringDBNull(yRow.Item("PRODUCT_ID"))},
                        {"BATCH_NUM", pf.CheckStringDBNull(yRow.Item("BATCH_NUM"))},
                        {"CUS_DIFF_QTY", total_qty},
                        {"SUPPLIER_DIFF_QTY", 0},
                        {"KEYIN_DIFF_QTY", 0},
                        {"VOUCHER_STATUS", pf.CheckStringDBNull(xRow.Item("VOUCHER_STATUS"))},
                        {"CUSTOMER_ID", pf.CheckStringDBNull(xRow.Item("CUSTOMER_ID"))},
                        {"SUPPLIER_ID", pf.CheckStringDBNull(xRow.Item("SUPPLIER_ID"))},
                        {"CREATE_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                    }
                    sSql = pf.GetInsertquery(table, col_value)

                ElseIf xRow.Item("STOCK_TYPE").ToString = "經銷" Then
                    Dim col_value(,) As String = {
                        {"PAPER_ID", pf.CheckStringDBNull(yRow.Item("STOCK_IN_ID"))},
                        {"EFFECTIVE_DATE", xRow.Item("EFFECTIVE_DATE")},
                        {"CHANGE_DATE", CHANGE_DATE},
                        {"DEPOT_ID", pf.CheckStringDBNull(yRow.Item("DEPOT_ID"))},
                        {"PRODUCT_ID", pf.CheckStringDBNull(yRow.Item("PRODUCT_ID"))},
                        {"BATCH_NUM", pf.CheckStringDBNull(yRow.Item("BATCH_NUM"))},
                        {"CUS_DIFF_QTY", 0},
                        {"SUPPLIER_DIFF_QTY", total_qty},
                        {"KEYIN_DIFF_QTY", 0},
                        {"VOUCHER_STATUS", pf.CheckStringDBNull(xRow.Item("VOUCHER_STATUS"))},
                        {"CUSTOMER_ID", pf.CheckStringDBNull(xRow.Item("CUSTOMER_ID"))},
                        {"SUPPLIER_ID", pf.CheckStringDBNull(xRow.Item("SUPPLIER_ID"))},
                        {"CREATE_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                    }
                    sSql = pf.GetInsertquery(table, col_value)
                End If
                Console.WriteLine(sSql)
                command.CommandText = sSql
                iAffectRows = command.ExecuteNonQuery()
                If iAffectRows = 0 Then
                    Return False
                End If
            Next

            Dim CHECK_DATE As New DateTime
            CHECK_DATE = DateTime.Now

            '即時時異動當日庫存數量SALE_QTY[覆核日]
            If xRow.Item("STOCK_TYPE").ToString = "公司" Then
                sSql = ""
                Dim New_Keyin_Qty As Double = 0
                '當日庫存異動數量
                sSql = "SELECT CONVERT(VARCHAR,[CHANGE_DATE],111) AS CDATE,[D_DEPOT_STOCKS_KEYIN_DIFF].[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                sSql &= ",SUM(ISNULL([KEYIN_DIFF_QTY],0)) as KEYIN_DIFF_QTY "
                sSql &= " FROM [D_DEPOT_STOCKS_KEYIN_DIFF] WITH (NOLOCK) "
                sSql &= " INNER JOIN [D_DEPOT_BASE] ON [D_DEPOT_BASE].[DEPOT_ID]=[D_DEPOT_STOCKS_KEYIN_DIFF].[DEPOT_ID]"
                sSql &= " AND [D_DEPOT_BASE].[DEPOT_TYPE]='公司'"
                sSql &= " WHERE [CHANGE_DATE] ='" & CHECK_DATE.ToString("yyyy/MM/dd") & "'"
                sSql &= " GROUP BY CONVERT(VARCHAR,[CHANGE_DATE],111),[D_DEPOT_STOCKS_KEYIN_DIFF].[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                Console.WriteLine(sSql)
                Dim qdt As New DataTable
                qdt = pf.GetDataTable(sSql)
                If qdt.Rows.Count > 0 Then
                    For j = 0 To qdt.Rows.Count - 1
                        Dim zRow As DataRow = qdt.Rows(j)
                        '取最近一次的庫存期末數量DEPOT_QTY
                        sSql = "SELECT TOP 1 [EFFECTIVE_DATE],[GOOD_QTY],[BAD_QTY],[DEPOT_QTY],[SALE_QTY] FROM [D_DEPOT_STOCKS_DAYS] WITH (NOLOCK) "
                        sSql &= " WHERE [DEPOT_ID]='" & zRow.Item("DEPOT_ID").ToString & "'"
                        sSql &= " AND [PRODUCT_ID]='" & zRow.Item("PRODUCT_ID").ToString & "'"
                        sSql &= " AND [BATCH_NUM]='" & zRow.Item("BATCH_NUM").ToString & "'"
                        sSql &= " ORDER BY [EFFECTIVE_DATE] DESC "
                        Console.WriteLine(sSql)
                        Dim qdt2 As New DataTable
                        qdt2 = pf.GetDataTable(sSql)
                        If qdt2.Rows.Count > 0 Then
                            '有查詢到前一日的期末庫存，庫存數量直接運算後更新
                            New_Keyin_Qty = Convert.ToDouble(pf.CheckNull(qdt2.Rows(0).Item("SALE_QTY"))) + Convert.ToDouble(pf.CheckNull(zRow.Item("KEYIN_DIFF_QTY")))
                        Else
                            '[D_DEPOT_STOCKS_DAYS]新增一筆,
                            Dim table_new As String = "D_DEPOT_STOCKS_DAYS"
                            Dim col_val_new(,) As String = {
                                {"DEPOT_ID", zRow.Item("DEPOT_ID")},
                                {"PRODUCT_ID", zRow.Item("PRODUCT_ID")},
                                {"BATCH_NUM", zRow.Item("BATCH_NUM")},
                                {"EFFECTIVE_DATE", CHECK_DATE.AddDays(-1).ToString("yyyy/MM/dd")},
                                {"INV_QTY", "0"},
                                {"DEPOT_QTY", "0"},
                                {"SALE_QTY", "0"}
                            }
                            sSql = pf.GetInsertquery(table_new, col_val_new)
                            Console.WriteLine(sSql)
                            pf.ExecSQL(sSql)

                            '若每日庫存沒有資料者，直接更新
                            New_Keyin_Qty = Convert.ToDouble(zRow.Item("KEYIN_DIFF_QTY"))
                        End If

                        Dim tableName As String = "D_DEPOT_STOCKS"
                        Dim pk() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM"}
                        Dim col_value(,) As String = {
                            {"DEPOT_ID", zRow.Item("DEPOT_ID").ToString},
                            {"PRODUCT_ID", zRow.Item("PRODUCT_ID").ToString},
                            {"BATCH_NUM", zRow.Item("BATCH_NUM").ToString},
                            {"SALE_QTY", New_Keyin_Qty},
                            {"ALTER_USER", "系統"},
                            {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                        }
                        '判斷D_DEPOT_STOCKS資料是否存在
                        Dim where_exist(,) As String = {
                            {"DEPOT_ID", zRow.Item("DEPOT_ID").ToString},
                            {"PRODUCT_ID", zRow.Item("PRODUCT_ID").ToString},
                            {"BATCH_NUM", zRow.Item("BATCH_NUM").ToString}
                        }
                        Dim check_exist As DataTable = pf.GetDB("D_DEPOT_STOCKS,NOLOCK", where_exist)
                        If check_exist.Rows.Count > 0 Then
                            sSql = pf.GetUpdatequery(tableName, pk, col_value)
                        Else
                            sSql = pf.GetInsertquery(tableName, col_value)
                        End If
                        Console.WriteLine(sSql)
                        command.CommandText = sSql
                        iAffectRows = command.ExecuteNonQuery()
                        If iAffectRows = 0 Then
                            Return False
                        End If
                    Next
                End If

            ElseIf xRow.Item("STOCK_TYPE").ToString = "寄售" Then
                '異動客戶寄售庫存[D_CUS_CON_QTY_DAYS]&[D_CUS_CON_QTY]
                sSql = "SELECT [CUSTOMER_ID],CONVERT(VARCHAR,[CHANGE_DATE],111) AS CDATE,[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                sSql &= ",SUM(ISNULL([CUS_DIFF_QTY],0)) as CUS_DIFF_QTY "
                sSql &= " FROM [D_DEPOT_STOCKS_KEYIN_DIFF] WITH (NOLOCK) "
                sSql &= " WHERE [CHANGE_DATE] ='" & CHECK_DATE.ToString("yyyy/MM/dd") & "'"
                sSql &= " AND [CUSTOMER_ID] ='" & xRow.Item("CUSTOMER_ID").ToString & "'"
                sSql &= " GROUP BY [CUSTOMER_ID],CONVERT(VARCHAR,[CHANGE_DATE],111),[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                Console.WriteLine(sSql)
                Dim qdt As New DataTable
                qdt = pf.GetDataTable(sSql)
                If qdt.Rows.Count > 0 Then
                    Dim CUS_New_Qty As Double = 0
                    For j = 0 To qdt.Rows.Count - 1
                        Dim zRow As DataRow = qdt.Rows(j)
                        '取D_CUS_CON_QTY_DAYS最近一次的庫存期末數量
                        sSql = "SELECT TOP 1 [CUS_DEPOT_QTY],[CUS_SALE_QTY] FROM [D_CUS_CON_QTY_DAYS] WITH (NOLOCK)"
                        sSql &= " WHERE [DEPOT_ID]='" & zRow.Item("DEPOT_ID").ToString & "'"
                        sSql &= " AND [PRODUCT_ID]='" & zRow.Item("PRODUCT_ID").ToString & "'"
                        sSql &= " AND [BATCH_NUM]='" & zRow.Item("BATCH_NUM").ToString & "'"
                        sSql &= " AND [CUSTOMER_ID]='" & zRow.Item("CUSTOMER_ID").ToString & "'"
                        sSql &= " ORDER BY [EFFECTIVE_DATE] DESC "
                        Console.WriteLine(sSql)
                        Dim qdt2 As New DataTable
                        qdt2 = pf.GetDataTable(sSql)
                        If qdt2.Rows.Count > 0 Then
                            CUS_New_Qty = Convert.ToDouble(pf.CheckNull(qdt2.Rows(0).Item("CUS_DEPOT_QTY"))) + Convert.ToDouble(pf.CheckNull(zRow.Item("CUS_DIFF_QTY")))
                        Else
                            '[D_CUS_CON_QTY_DAYS]新增一筆,
                            Dim table_new_cust As String = "D_CUS_CON_QTY_DAYS"
                            Dim col_val_new_cust(,) As String = {
                                {"DEPOT_ID", zRow.Item("DEPOT_ID")},
                                {"PRODUCT_ID", zRow.Item("PRODUCT_ID")},
                                {"BATCH_NUM", zRow.Item("BATCH_NUM")},
                                {"CUSTOMER_ID", zRow.Item("CUSTOMER_ID")},
                                {"EFFECTIVE_DATE", CHECK_DATE.AddDays(-1).ToString("yyyy/MM/dd")},
                                {"CUS_INV_QTY", "0"},
                                {"CUS_DEPOT_QTY", "0"},
                                {"CUS_SALE_QTY", "0"}
                            }
                            sSql = pf.GetInsertquery(table_new_cust, col_val_new_cust)
                            Console.WriteLine(sSql)
                            pf.ExecSQL(sSql)
                            '直接更新
                            CUS_New_Qty = Convert.ToDouble(zRow.Item("CUS_DIFF_QTY"))
                        End If
                        '判斷更新方式
                        Dim tableName As String = "D_CUS_CON_QTY"
                        sSql = "SELECT * FROM [" & tableName & "] WITH (NOLOCK) "
                        sSql &= " WHERE [DEPOT_ID]='" & zRow.Item("DEPOT_ID").ToString & "'"
                        sSql &= " AND [PRODUCT_ID]='" & zRow.Item("PRODUCT_ID").ToString & "'"
                        sSql &= " AND [BATCH_NUM]='" & zRow.Item("BATCH_NUM").ToString & "'"
                        sSql &= " AND [CUSTOMER_ID]='" & zRow.Item("CUSTOMER_ID").ToString & "'"
                        Dim chk_dt As New DataTable
                        chk_dt = pf.GetDataTable(sSql)
                        If chk_dt.Rows.Count > 0 Then
                            'UPDATE
                            Dim pk() As String = {"CUSTOMER_ID", "DEPOT_ID", "PRODUCT_ID", "BATCH_NUM"}
                            Dim col_value(,) As String = {
                                {"CUSTOMER_ID", zRow.Item("CUSTOMER_ID").ToString},
                                {"DEPOT_ID", zRow.Item("DEPOT_ID").ToString},
                                {"PRODUCT_ID", zRow.Item("PRODUCT_ID").ToString},
                                {"BATCH_NUM", zRow.Item("BATCH_NUM").ToString},
                                {"CUS_SALE_QTY", CUS_New_Qty},
                                {"ALTER_USER", "系統"},
                                {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                            }
                            sSql = pf.GetUpdatequery(tableName, pk, col_value)
                        Else
                            'INSERT
                            Dim col_value(,) As String = {
                                {"CUSTOMER_ID", zRow.Item("CUSTOMER_ID").ToString},
                                {"DEPOT_ID", zRow.Item("DEPOT_ID").ToString},
                                {"PRODUCT_ID", zRow.Item("PRODUCT_ID").ToString},
                                {"BATCH_NUM", zRow.Item("BATCH_NUM").ToString},
                                {"CUS_SALE_QTY", CUS_New_Qty},
                                {"CREATE_USER", "系統"},
                                {"CREATE_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                            }
                            sSql = pf.GetInsertquery(tableName, col_value)
                        End If
                        Console.WriteLine(sSql)
                        command.CommandText = sSql
                        iAffectRows = command.ExecuteNonQuery()
                        If iAffectRows = 0 Then
                            Return False
                        End If
                    Next
                End If

            ElseIf xRow.Item("STOCK_TYPE").ToString = "經銷" Then
                '異動經銷商庫存[D_DEALER_CON_QTY]&[D_DEALER_CON_QTY_DAYS]
                sSql = "SELECT [SUPPLIER_ID],CONVERT(VARCHAR,[CHANGE_DATE],111) AS CDATE,[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                sSql &= ",SUM(ISNULL([SUPPLIER_DIFF_QTY],0)) as SUPPLIER_DIFF_QTY "
                sSql &= " FROM [D_DEPOT_STOCKS_KEYIN_DIFF] WITH (NOLOCK) "
                sSql &= " WHERE [CHANGE_DATE] ='" & CHECK_DATE.ToString("yyyy/MM/dd") & "'"
                sSql &= " AND [SUPPLIER_ID] ='" & xRow.Item("SUPPLIER_ID").ToString & "'"
                sSql &= " GROUP BY [SUPPLIER_ID],CONVERT(VARCHAR,[CHANGE_DATE],111),[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                Console.WriteLine(sSql)
                Dim qdt As New DataTable
                qdt = pf.GetDataTable(sSql)
                If qdt.Rows.Count > 0 Then

                    Dim Supplier_New_Qty As Double = 0
                    For j = 0 To qdt.Rows.Count - 1
                        Dim zRow As DataRow = qdt.Rows(j)
                        '取D_DEALER_CON_QTY_DAYS最近一次的庫存期末數量
                        sSql = "SELECT TOP 1 [CON_DEPOT_QTY],[CON_SALE_QTY] FROM [D_DEALER_CON_QTY_DAYS] WITH (NOLOCK)"
                        sSql &= " WHERE [DEPOT_ID]='" & zRow.Item("DEPOT_ID").ToString & "'"
                        sSql &= " AND [PRODUCT_ID]='" & zRow.Item("PRODUCT_ID").ToString & "'"
                        sSql &= " AND [BATCH_NUM]='" & zRow.Item("BATCH_NUM").ToString & "'"
                        sSql &= " AND [SUPPLIER_ID]='" & zRow.Item("SUPPLIER_ID").ToString & "'"
                        sSql &= " ORDER BY [EFFECTIVE_DATE] DESC "
                        Console.WriteLine(sSql)
                        Dim qdt2 As New DataTable
                        qdt2 = pf.GetDataTable(sSql)
                        If qdt2.Rows.Count > 0 Then
                            Supplier_New_Qty = Convert.ToDouble(pf.CheckNull(qdt2.Rows(0).Item("CON_SALE_QTY"))) + Convert.ToDouble(pf.CheckNull(zRow.Item("SUPPLIER_DIFF_QTY")))
                        Else
                            '[D_DEALER_CON_QTY_DAYS]新增一筆,
                            Dim table_new_dealer As String = "D_DEALER_CON_QTY_DAYS"
                            Dim col_val_new_dealer(,) As String = {
                                {"DEPOT_ID", zRow.Item("DEPOT_ID")},
                                {"PRODUCT_ID", zRow.Item("PRODUCT_ID")},
                                {"BATCH_NUM", zRow.Item("BATCH_NUM")},
                                {"SUPPLIER_ID", zRow.Item("SUPPLIER_ID")},
                                {"EFFECTIVE_DATE", CHECK_DATE.AddDays(-1).ToString("yyyy/MM/dd")},
                                {"CON_INV_QTY", "0"},
                                {"CON_DEPOT_QTY", "0"},
                                {"CON_SALE_QTY", "0"}
                            }
                            sSql = pf.GetInsertquery(table_new_dealer, col_val_new_dealer)
                            Console.WriteLine(sSql)
                            pf.ExecSQL(sSql)
                            '直接更新
                            Supplier_New_Qty = Convert.ToDouble(zRow.Item("SUPPLIER_DIFF_QTY"))
                        End If
                        '判斷更新方式
                        Dim tableName As String = "D_DEALER_CON_QTY"
                        sSql = "SELECT * FROM [" & tableName & "] WITH (NOLOCK) "
                        sSql &= " WHERE [DEPOT_ID]='" & zRow.Item("DEPOT_ID").ToString & "'"
                        sSql &= " AND [PRODUCT_ID]='" & zRow.Item("PRODUCT_ID").ToString & "'"
                        sSql &= " AND [BATCH_NUM]='" & zRow.Item("BATCH_NUM").ToString & "'"
                        sSql &= " AND [SUPPLIER_ID]='" & zRow.Item("SUPPLIER_ID").ToString & "'"
                        Dim chk_dt As New DataTable
                        chk_dt = pf.GetDataTable(sSql)
                        If chk_dt.Rows.Count > 0 Then
                            'UPDATE
                            Dim pk() As String = {"SUPPLIER_ID", "DEPOT_ID", "PRODUCT_ID", "BATCH_NUM"}
                            Dim col_value(,) As String = {
                                {"SUPPLIER_ID", zRow.Item("SUPPLIER_ID").ToString},
                                {"DEPOT_ID", zRow.Item("DEPOT_ID").ToString},
                                {"PRODUCT_ID", zRow.Item("PRODUCT_ID").ToString},
                                {"BATCH_NUM", zRow.Item("BATCH_NUM").ToString},
                                {"CON_SALE_QTY", Supplier_New_Qty},
                                {"ALTER_USER", "系統"},
                                {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                            }
                            sSql = pf.GetUpdatequery(tableName, pk, col_value)
                        Else
                            'INSERT
                            Dim col_value(,) As String = {
                                {"SUPPLIER_ID", zRow.Item("SUPPLIER_ID").ToString},
                                {"DEPOT_ID", zRow.Item("DEPOT_ID").ToString},
                                {"PRODUCT_ID", zRow.Item("PRODUCT_ID").ToString},
                                {"BATCH_NUM", zRow.Item("BATCH_NUM").ToString},
                                {"CON_SALE_QTY", Supplier_New_Qty},
                                {"CREATE_USER", "系統"},
                                {"CREATE_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                            }
                            sSql = pf.GetInsertquery(tableName, col_value)
                        End If
                        Console.WriteLine(sSql)
                        command.CommandText = sSql
                        iAffectRows = command.ExecuteNonQuery()
                        If iAffectRows = 0 Then
                            Return False
                        End If

                        '更新D_DEPOT_STOCKS
                        Dim qdt3 As DataTable = pf.GetDB("D_DEPOT_BASE,NOLOCK", {{"DEPOT_ID", zRow.Item("DEPOT_ID").ToString}})
                        If qdt3.Rows.Count > 0 Then
                            If qdt3.Rows(0).Item("DEPOT_TYPE") = "經銷商" AndAlso qdt3.Rows(0).Item("DEPOT_ID").ToString = zRow.Item("DEPOT_ID").ToString Then
                                '取產品的VALID_PERIOD
                                Dim pWhere(,) As String = {
                                    {"PRODUCT_ID", zRow.Item("PRODUCT_ID")},
                                    {"BATCH_NUM", zRow.Item("BATCH_NUM")},
                                    {"ORDERBY_ASC", "DEPOT_ID"}
                                }
                                Dim pdt As DataTable = pf.GetDB("D_DEPOT_STOCKS,NOLOCK", pWhere)

                                '取最近一次的庫存期末數量DEPOT_QTY
                                sSql = "SELECT TOP 1 [EFFECTIVE_DATE],[GOOD_QTY],[BAD_QTY],[DEPOT_QTY],[SALE_QTY] FROM [D_DEPOT_STOCKS_DAYS] WITH (NOLOCK) "
                                sSql &= " WHERE [DEPOT_ID]='" & zRow.Item("DEPOT_ID").ToString & "'"
                                sSql &= " AND [PRODUCT_ID]='" & zRow.Item("PRODUCT_ID").ToString & "'"
                                sSql &= " AND [BATCH_NUM]='" & zRow.Item("BATCH_NUM").ToString & "'"
                                sSql &= " ORDER BY [EFFECTIVE_DATE] DESC "
                                Console.WriteLine(sSql)
                                'Dim qdt2 As New DataTable
                                Dim New_Keyin_Qty As Double = 0
                                qdt2 = pf.GetDataTable(sSql)
                                If qdt2.Rows.Count > 0 Then
                                    '有查詢到前一日的期末庫存，庫存數量直接運算後更新
                                    New_Keyin_Qty = Convert.ToDouble(pf.CheckNull(qdt2.Rows(0).Item("SALE_QTY"))) + Convert.ToDouble(pf.CheckNull(zRow.Item("SUPPLIER_DIFF_QTY")))
                                    New_Keyin_Qty = Supplier_New_Qty
                                Else
                                    '[D_DEPOT_STOCKS_DAYS]新增一筆,
                                    Dim table_new As String = "D_DEPOT_STOCKS_DAYS"
                                    Dim col_val_new(,) As String = {
                                        {"DEPOT_ID", zRow.Item("DEPOT_ID")},
                                        {"PRODUCT_ID", zRow.Item("PRODUCT_ID")},
                                        {"BATCH_NUM", zRow.Item("BATCH_NUM")},
                                        {"EFFECTIVE_DATE", CHECK_DATE.AddDays(-1).ToString("yyyy/MM/dd")},
                                        {"INV_QTY", "0"},
                                        {"DEPOT_QTY", "0"},
                                        {"SALE_QTY", "0"},
                                        {"VALID_PERIOD", CType(pdt.Rows(0).Item("VALID_PERIOD"), DateTime).ToString("yyyy/MM/dd")},
                                        {"CREATE_USER", "系統"},
                                        {"CREATE_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                                    }
                                    sSql = pf.GetInsertquery(table_new, col_val_new)
                                    Console.WriteLine(sSql)
                                    pf.ExecSQL(sSql)

                                    '若每日庫存沒有資料者，直接更新
                                    New_Keyin_Qty = Supplier_New_Qty
                                End If

                                Dim tableName2 As String = "D_DEPOT_STOCKS"
                                Dim pk2() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM"}
                                Dim col_value2(,) As String = {
                                    {"DEPOT_ID", zRow.Item("DEPOT_ID").ToString},
                                    {"PRODUCT_ID", zRow.Item("PRODUCT_ID").ToString},
                                    {"BATCH_NUM", zRow.Item("BATCH_NUM").ToString},
                                    {"SALE_QTY", New_Keyin_Qty},
                                    {"VALID_PERIOD", CType(pdt.Rows(0).Item("VALID_PERIOD"), DateTime).ToString("yyyy/MM/dd")},
                                    {"ALTER_USER", "系統"},
                                    {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                                }
                                '判斷D_DEPOT_STOCKS資料是否存在
                                Dim where_exist2(,) As String = {
                                    {"DEPOT_ID", zRow.Item("DEPOT_ID").ToString},
                                    {"PRODUCT_ID", zRow.Item("PRODUCT_ID").ToString},
                                    {"BATCH_NUM", zRow.Item("BATCH_NUM").ToString}
                                }
                                Dim check_exist As DataTable = pf.GetDB("D_DEPOT_STOCKS,NOLOCK", where_exist2)
                                If check_exist.Rows.Count > 0 Then
                                    col_value2 = {
                                        {"DEPOT_ID", zRow.Item("DEPOT_ID").ToString},
                                        {"PRODUCT_ID", zRow.Item("PRODUCT_ID").ToString},
                                        {"BATCH_NUM", zRow.Item("BATCH_NUM").ToString},
                                        {"SALE_QTY", New_Keyin_Qty},
                                        {"VALID_PERIOD", CType(pdt.Rows(0).Item("VALID_PERIOD"), DateTime).ToString("yyyy/MM/dd")},
                                        {"ALTER_USER", "系統"},
                                        {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                                    }
                                    sSql = pf.GetUpdatequery(tableName2, pk2, col_value2)
                                Else
                                    col_value2 = {
                                        {"DEPOT_ID", zRow.Item("DEPOT_ID").ToString},
                                        {"PRODUCT_ID", zRow.Item("PRODUCT_ID").ToString},
                                        {"BATCH_NUM", zRow.Item("BATCH_NUM").ToString},
                                        {"SALE_QTY", New_Keyin_Qty},
                                        {"COST_PRICE", pf.CheckNull(pdt.Rows(0).Item("COST_PRICE"))},
                                        {"UNIT_PRICE", pf.CheckNull(pdt.Rows(0).Item("UNIT_PRICE"))},
                                        {"VALID_PERIOD", pf.CheckDateTimeNull(pdt.Rows(0).Item("VALID_PERIOD"))},
                                        {"RED_SHOW", pf.CheckNull(pdt.Rows(0).Item("RED_SHOW"))},
                                        {"CREATE_USER", "系統"},
                                        {"CREATE_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                                    }
                                    sSql = pf.GetInsertquery(tableName2, col_value2)
                                End If
                                Console.WriteLine(sSql)
                                command.CommandText = sSql
                                iAffectRows = command.ExecuteNonQuery()
                                If iAffectRows = 0 Then
                                    Return False
                                End If

                            End If
                        End If

                    Next
                End If
            End If
        Catch ex As Exception
            pf.MyError(ex.ToString)
            Return False
        End Try
        Return True
    End Function

    '2019-05-03
    '計算目前倉庫的可異動數量
    '現有庫存數量-單據狀態為生效的進退單、上游換出單、借倉出庫單、調撥單(調出)、銷貨單、銷貨換出單
    '現有庫存數量-單據狀態為生效的進退單、上游換出單、調撥單(調出)、銷貨單、銷貨換出單
    Function Get_Depot_Out_Qty(DepotId As String, ProductId As String, BatchNum As String) As Double
        Dim Qty As Double = 0
        Dim Depot_Qty As Double = 0
        '銷貨單數量
        Dim SaleQty As Double = 0
        '銷貨換出單數量
        Dim GoodsOutQty As Double = 0
        '進退單數量
        Dim ReturnGoodsQty As Double = 0
        '上游換出單數量
        Dim SupplierGoodsOutQty As Double = 0
        '借倉出庫單數量
        Dim OtherGoodsOutQty As Double = 0
        '調撥單(調出)數量
        Dim TransferQty As Double = 0

        Dim sSql As String = ""
        Dim MasterTable As String = ""
        Dim DetailsTable As String = ""
        Dim join(,) As String
        Dim Select_Column(,) As String
        Dim where(,) As String

        Dim qdt As New DataTable
        Dim sum As DataTable
        Try
            '現有庫存的數量
            sSql = "SELECt ISNULL([GOOD_QTY],0) AS DepotQty FROM [D_DEPOT_STOCKS] WHERE [DEPOT_ID]='" & DepotId & "'"
            sSql &= " AND [PRODUCT_ID]='" & ProductId & "' AND [BATCH_NUM]='" & BatchNum & "'"
            qdt = pf.GetDataTable(sSql)
            If qdt.Rows.Count > 0 Then Depot_Qty = Convert.ToDouble(pf.CheckNull(qdt.Rows(0).Item("DepotQty")))

            '單據狀態=生效的銷貨單數量
            'sSql = "SELECT ISNULL(SUM([EXPORT_QTY]+[PRESENT_QTY]),0) AS SaleQty FROM [S_SALE_LIST_DETAIL] as detail"
            'sSql &= " JOIN [S_SALE_LIST_MASTER] as master on master.[SALE_ID]=detail.[SALE_ID]"
            'sSql &= " AND master.VOUCHER_STATUS='生效' AND master.DELETE_FLAG='N'"
            'sSql &= " WHERE detail.[DEPOT_ID]='" & DepotId & "'"
            'sSql &= " AND detail.[PRODUCT_ID]='" & ProductId & "'"
            'sSql &= " And detail.[BATCH_NUM]='" & BatchNum & "'"
            'qdt = pf.GetDataTable(sSql)
            'SaleQty = Convert.ToDouble(qdt.Rows(0).Item("SaleQty"))
            MasterTable = "S_SALE_LIST_MASTER"
            DetailsTable = "S_SALE_LIST_DETAIL"
            join = {{MasterTable, "SALE_ID", DetailsTable, "SALE_ID"}}
            Select_Column = {{"CUSTOM_SQL", "ISNULL(SUM([EXPORT_QTY]+[PRESENT_QTY]),0) As [SaleQty]"}, {DetailsTable, ""}}
            where = {{MasterTable, "DELETE_FLAG", "=", "N"},
                    {DetailsTable, "DEPOT_ID", "=", DepotId},
                    {DetailsTable, "PRODUCT_ID", "=", ProductId},
                    {DetailsTable, "BATCH_NUM", "=", BatchNum},
                    {MasterTable, "VOUCHER_STATUS", "=", "生效"}}
            qdt = pf.GetDB_Join(DetailsTable, join, Select_Column, where)
            If qdt.Rows.Count > 0 Then SaleQty = Convert.ToDouble(qdt.Rows(0).Item("SaleQty"))

            '單據狀態=生效的銷貨換出單數量
            'sSql = "SELECT ISNULL(SUM([QTY]),0) AS GoodsOutQty FROM [S_SALE_GOODSOUT_DETAIL] as detail"
            'sSql &= " JOIN [S_SALE_GOODSOUT_MASTER] as master on master.[S_GOODSOUT_ID]=detail.[S_GOODSOUT_ID]"
            'sSql &= " AND master.VOUCHER_STATUS='生效' AND master.DELETE_FLAG='N' AND master.IS_CLOSE='N'"
            'sSql &= " WHERE detail.[DEPOT_ID]='" & DepotId & "'"
            'sSql &= " AND detail.[PRODUCT_ID]='" & ProductId & "'"
            'sSql &= " And detail.[BATCH_NUM]='" & BatchNum & "'"
            'qdt = pf.GetDataTable(sSql)
            'GoodsOutQty = Convert.ToDouble(qdt.Rows(0).Item("GoodsOutQty"))
            MasterTable = "S_SALE_GOODSOUT_MASTER"
            DetailsTable = "S_SALE_GOODSOUT_DETAIL"
            join = {{MasterTable, "S_GOODSOUT_ID", DetailsTable, "S_GOODSOUT_ID"}}
            Select_Column = {{"CUSTOM_SQL", "ISNULL(SUM([QTY]),0) As [GoodsOutQty]"}, {DetailsTable, ""}}
            where = {{MasterTable, "DELETE_FLAG", "=", "N"},
                    {MasterTable, "IS_CLOSE", "=", "N"},
                    {DetailsTable, "DEPOT_ID", "=", DepotId},
                    {DetailsTable, "PRODUCT_ID", "=", ProductId},
                    {DetailsTable, "BATCH_NUM", "=", BatchNum},
                    {MasterTable, "VOUCHER_STATUS", "=", "生效"}}
            qdt = pf.GetDB_Join(DetailsTable, join, Select_Column, where)
            If qdt.Rows.Count > 0 Then GoodsOutQty = Convert.ToDouble(qdt.Rows(0).Item("GoodsOutQty"))

            '單據狀態=生效的進退單數量
            MasterTable = "P_RETURN_GOODS_MASTER"
            DetailsTable = "P_RETURN_GOODS_DETAIL"
            join = {{MasterTable, "RETURN_GOODS_ID", DetailsTable, "RETURN_GOODS_ID"}}
            Select_Column = {{"CUSTOM_SQL", "ISNULL(SUM([QTY]+[PRESENT_QTY]),0) As [ReturnGoodsQty]"}, {DetailsTable, ""}}
            where = {{DetailsTable, "DEPOT_ID", "=", DepotId},
                    {DetailsTable, "PRODUCT_ID", "=", ProductId},
                    {DetailsTable, "BATCH_NUM", "=", BatchNum},
                    {MasterTable, "VOUCHER_STATUS", "=", "生效"}}
            sum = pf.GetDB_Join(DetailsTable, join, Select_Column, where)
            If sum.Rows.Count > 0 Then ReturnGoodsQty = Convert.ToDouble(sum.Rows(0).Item("ReturnGoodsQty"))

            '單據狀態=生效的上游換出單數量
            MasterTable = "P_SUPPLIER_GOODSOUT_MASTER"
            DetailsTable = "P_SUPPLIER_GOODSOUT_DETAIL"
            join = {{MasterTable, "GOODS_OUT_ID", DetailsTable, "GOODS_OUT_ID"}}
            Select_Column = {{"CUSTOM_SQL", "ISNULL(SUM([QTY]+[PRESENT_QTY]),0) As [SupplierGoodsOutQty]"}, {DetailsTable, ""}}
            where = {{DetailsTable, "DEPOT_ID", "=", DepotId},
                    {DetailsTable, "PRODUCT_ID", "=", ProductId},
                    {DetailsTable, "BATCH_NUM", "=", BatchNum},
                    {MasterTable, "VOUCHER_STATUS", "=", "生效"}}
            sum = pf.GetDB_Join(DetailsTable, join, Select_Column, where)
            If sum.Rows.Count > 0 Then SupplierGoodsOutQty = Convert.ToDouble(sum.Rows(0).Item("SupplierGoodsOutQty"))

            '單據狀態=生效的借倉出庫單數量
            'MasterTable = "P_OTHER_GOODS_OUT_MASTER"
            'DetailsTable = "P_OTHER_GOODS_OUT_DETAIL"
            'join = {{MasterTable, "OTHER_GOODS_OUT_ID", DetailsTable, "OTHER_GOODS_OUT_ID"}}
            'Select_Column = {{"CUSTOM_SQL", "ISNULL(SUM([QTY]+[PRESENT_QTY]),0) As [OtherGoodsOutQty]"}, {DetailsTable, ""}}
            'where = {{DetailsTable, "DEPOT_ID", "=", DepotId},
            '         {DetailsTable, "PRODUCT_ID", "=", ProductId},
            '         {DetailsTable, "BATCH_NUM", "=", BatchNum},
            '         {MasterTable, "VOUCHER_STATUS", "=", "生效"}}
            'sum = pf.GetDB_Join(DetailsTable, join, Select_Column, where)
            'If sum.Rows.Count > 0 Then OtherGoodsOutQty = Convert.ToDouble(sum.Rows(0).Item("OtherGoodsOutQty"))
            OtherGoodsOutQty = 0

            '單據狀態=生效的調撥單(調出)
            MasterTable = "D_TRANSFER_MASTER"
            DetailsTable = "D_TRANSFER_DETAIL"
            join = {{MasterTable, "TRANSFER_ID", DetailsTable, "TRANSFER_ID"}}
            Select_Column = {{"CUSTOM_SQL", "ISNULL(SUM([TRANSFER_OUT_QTY]),0) As [TransferQty]"}, {DetailsTable, ""}}
            where = {{DetailsTable, "TRANSFER_OUT_DEPOT_ID", "=", DepotId},
                    {DetailsTable, "PRODUCT_ID", "=", ProductId},
                    {DetailsTable, "TRANSFER_OUT_BATCH_NUM", "=", BatchNum},
                    {MasterTable, "VOUCHER_STATUS", "=", "生效"}}
            sum = pf.GetDB_Join(DetailsTable, join, Select_Column, where)
            If sum.Rows.Count > 0 Then TransferQty = Convert.ToDouble(sum.Rows(0).Item("TransferQty"))

            Qty = Depot_Qty - SaleQty - GoodsOutQty - ReturnGoodsQty - SupplierGoodsOutQty - OtherGoodsOutQty - TransferQty

        Catch ex As Exception
            pf.MyError(ex.ToString)
        End Try
        Return Qty
    End Function

    '更新當日異動庫存數
    Public Sub UPDATE_D_DEPOT_STOCKS()
        Try
            Dim table As String = "D_DEPOT_STOCKS"
            Dim Check_Date As Date = DateTime.Now.ToString("yyyy/MM/dd")
            Dim sSql As String = ""
            sSql = "SELECT * FROM [D_DEPOT_STOCKS] WITH (NOLOCK)"
            Dim depot_stocks_dt As New DataTable
            depot_stocks_dt = pf.GetDataTable(sSql)
            For i As Integer = 0 To depot_stocks_dt.Rows.Count - 1
                '取前一日每日庫存檔
                sSql = "SELECT ISNULL([DEPOT_QTY],0) AS QTY FROM [D_DEPOT_STOCKS_DAYS] WITH (NOLOCK)"
                sSql &= " WHERE [DEPOT_ID]='" & depot_stocks_dt.Rows(i).Item("DEPOT_ID").ToString & "'"
                sSql &= " AND [PRODUCT_ID]='" & depot_stocks_dt.Rows(i).Item("PRODUCT_ID").ToString & "'"
                sSql &= " AND [BATCH_NUM]='" & depot_stocks_dt.Rows(i).Item("BATCH_NUM").ToString & "'"
                sSql &= " AND CONVERT(varchar,[EFFECTIVE_DATE],111)='" & DateTime.Now.AddDays(-1).ToString("yyyy/MM/dd") & "'"
                Dim dt1 As New DataTable
                dt1 = pf.GetDataTable(sSql)
                Dim qty1 As Double = 0
                If dt1.Rows.Count > 0 Then
                    qty1 = pf.CheckNull(dt1.Rows(0).Item("DEPOT_QTY"))
                End If
                '取今日異動檔
                sSql = "SELECT ISNULL(SUM([G_DIFF_QTY]),0) AS G_DIFF_QTY FROM [D_DEPOT_STOCKS_DIFF] WITH (NOLOCK)"
                sSql &= " WHERE [DEPOT_ID]='" & depot_stocks_dt.Rows(i).Item("DEPOT_ID").ToString & "'"
                sSql &= " AND [PRODUCT_ID]='" & depot_stocks_dt.Rows(i).Item("PRODUCT_ID").ToString & "'"
                sSql &= " AND [BATCH_NUM]='" & depot_stocks_dt.Rows(i).Item("BATCH_NUM").ToString & "'"
                sSql &= " AND CONVERT(varchar,[CHANGE_DATE],111)='" & DateTime.Now.AddDays(-1).ToString("yyyy/MM/dd") & "'"
                sSql &= " AND [VOUCHER_STATUS]='確認'"
                Dim dt2 As New DataTable
                dt2 = pf.GetDataTable(sSql)
                Dim qty2 As Double = 0
                If dt2.Rows.Count > 0 Then
                    qty2 = pf.CheckNull(dt2.Rows(0).Item("G_DIFF_QTY"))
                End If
                Dim qty3 As Double = 0
                qty3 = qty1 + qty2
                Dim pk() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM"}
                Dim col_value(,) As String = {
                {"DEPOT_ID", depot_stocks_dt.Rows(i).Item("DEPOT_ID").ToString},
                {"PRODUCT_ID", depot_stocks_dt.Rows(i).Item("PRODUCT_ID").ToString},
                {"BATCH_NUM", depot_stocks_dt.Rows(i).Item("BATCH_NUM").ToString},
                {"GOOD_QTY", qty3},
                {"ALTER_USER", "系統"},
                {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
            }
                sSql = pf.GetUpdatequery(table, pk, col_value)
                Console.WriteLine(sSql)
                pf.ExecSQL(sSql)
            Next
        Catch ex As Exception
            pf.MyError(ex.ToString)
        End Try
    End Sub

    '滾動每日庫存-公司庫存盤整單使用
    '異動出庫單的庫存數量[當日庫存][看覆核日]
    '計算公式：庫存數量=前一日的期末庫存數量[DEPOT_QTY]+覆核日=當日的庫存異動數量
    '更新到[D_DEPOT_STOCKS]
    'STOCK_OUT_ID:出庫單編號
    Function SCROLL_D_DEPOT_STOCKS_DAYS(STOCK_OUT_ID As String, STOCK_IN_ID As String, SCROLL_DATE As DateTime, command As SqlCommand) As Boolean
        Dim sSql As String = ""
        Dim iAffectRows As Integer = 0
        Dim OUT_mdt As New DataTable
        Dim OUT_ddt As New DataTable
        Dim IN_mdt As New DataTable
        Dim IN_ddt As New DataTable
        Try
            If STOCK_OUT_ID <> "" Then
                '出庫單主檔資料
                OUT_mdt = pf.GetDB("D_STOCK_OUT_MASTER,NOLOCK", {{"STOCK_OUT_ID", STOCK_OUT_ID}})
                Dim OUT_xRow As DataRow
                OUT_xRow = OUT_mdt.Rows(0)
                '生效日
                Dim OUT_EFFECTIVE_DATE As Date = Convert.ToDateTime(OUT_xRow.Item("EFFECTIVE_DATE")).ToString("yyyy/MM/dd")
                '覆核日
                Dim OUT_CHANGE_DATE As Date = Nothing
                If OUT_xRow.Item("VOUCHER_STATUS").ToString = "確認" Then
                    '覆核日
                    OUT_CHANGE_DATE = Convert.ToDateTime(OUT_xRow.Item("VOUCHER_TIME")).ToString("yyyy/MM/dd")
                ElseIf OUT_xRow.Item("VOUCHER_STATUS").ToString = "生效" Or OUT_xRow.Item("VOUCHER_STATUS").ToString = "作廢" Then
                    '異動日期
                    If OUT_xRow.Item("VOUCHER_STATUS").ToString = "生效" Then
                        If DBNull.Value.Equals(OUT_xRow.Item("ALTER_TIME")) Then
                            OUT_CHANGE_DATE = Convert.ToDateTime(OUT_xRow.Item("CREATE_TIME")).ToString("yyyy/MM/dd")
                        Else
                            OUT_CHANGE_DATE = Convert.ToDateTime(OUT_xRow.Item("ALTER_TIME")).ToString("yyyy/MM/dd")
                        End If
                    ElseIf OUT_xRow.Item("VOUCHER_STATUS").ToString = "作廢" Then
                        OUT_CHANGE_DATE = Convert.ToDateTime(OUT_xRow.Item("ALTER_TIME")).ToString("yyyy/MM/dd")
                    End If
                End If
                '盤整單、寄售盤整單 異動日與生效日期相同，
                If OUT_xRow.Item("PAPER_FROM") = "盤整單" Then
                    If OUT_xRow.Item("VOUCHER_STATUS").ToString = "生效" Or OUT_xRow.Item("VOUCHER_STATUS").ToString = "確認" Then
                        OUT_CHANGE_DATE = Convert.ToDateTime(OUT_xRow.Item("EFFECTIVE_DATE")).ToString("yyyy/MM/dd")
                    ElseIf OUT_xRow.Item("VOUCHER_STATUS").ToString = "作廢" Then
                        OUT_CHANGE_DATE = Convert.ToDateTime(OUT_xRow.Item("ALTER_TIME")).ToString("yyyy/MM/dd")
                    End If
                End If
                '將明細寫入[D_DEPOT_STOCKS_DIFF]，然後再統一計算庫存異動數量
                OUT_ddt = pf.GetDB("D_STOCK_OUT_DETAIL,NOLOCK", {{"STOCK_OUT_ID", STOCK_OUT_ID}})
                Dim OUT_table As String = "D_DEPOT_STOCKS_DIFF"
                For j As Integer = 0 To OUT_ddt.Rows.Count - 1
                    Dim OUT_yRow As DataRow
                    OUT_yRow = OUT_ddt.Rows(j)
                    Dim total_qty As Double = 0
                    Dim OUT_GOOd_QTY As Double = 0
                    Dim OUT_BAD_QTY As Double = 0
                    If pf.CheckIfDBNull(OUT_yRow.Item("STOCK_CHECK")) = 1 Then
                        '出庫確認
                        OUT_GOOd_QTY = (Convert.ToDouble(pf.CheckNull(OUT_yRow.Item("QTY"))) + Convert.ToDouble(pf.CheckNull(OUT_yRow.Item("PRESENT_QTY")))) * (-1)
                        OUT_BAD_QTY = Convert.ToDouble(pf.CheckNull(OUT_yRow.Item("BAD_QTY")) * (-1))
                    ElseIf pf.CheckIfDBNull(OUT_yRow.Item("STOCK_CHECK")) = 0 Then
                        '取消出庫確認
                        OUT_GOOd_QTY = (Convert.ToDouble(pf.CheckNull(OUT_yRow.Item("QTY"))) + Convert.ToDouble(pf.CheckNull(OUT_yRow.Item("PRESENT_QTY"))))
                        OUT_BAD_QTY = Convert.ToDouble(pf.CheckNull(OUT_yRow.Item("BAD_QTY")))
                    End If
                    If OUT_xRow.Item("STOCK_TYPE").ToString = "公司" Then
                        Dim col_value(,) As String = {
                        {"PAPER_ID", pf.CheckStringDBNull(OUT_yRow.Item("STOCK_OUT_ID"))},
                        {"EFFECTIVE_DATE", OUT_xRow.Item("EFFECTIVE_DATE")},
                        {"CHANGE_DATE", OUT_CHANGE_DATE},
                        {"DEPOT_ID", pf.CheckStringDBNull(OUT_yRow.Item("DEPOT_ID"))},
                        {"PRODUCT_ID", pf.CheckStringDBNull(OUT_yRow.Item("PRODUCT_ID"))},
                        {"BATCH_NUM", pf.CheckStringDBNull(OUT_yRow.Item("BATCH_NUM"))},
                        {"G_DIFF_QTY", OUT_GOOd_QTY},
                        {"B_DIFF_QTY", OUT_BAD_QTY},
                        {"C_DIFF_QTY", 0},
                        {"CUS_DIFF_QTY", 0},
                        {"SUPPLIER_DIFF_QTY", 0},
                        {"VOUCHER_STATUS", pf.CheckStringDBNull(OUT_xRow.Item("VOUCHER_STATUS"))},
                        {"CUSTOMER_ID", pf.CheckStringDBNull(OUT_xRow.Item("CUSTOMER_ID"))},
                        {"SUPPLIER_ID", pf.CheckStringDBNull(OUT_xRow.Item("SUPPLIER_ID"))},
                        {"CREATE_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                    }
                        sSql = pf.GetInsertquery(OUT_table, col_value)
                    End If
                    Console.WriteLine(sSql)
                    command.CommandText = sSql
                    iAffectRows = command.ExecuteNonQuery()
                    If iAffectRows = 0 Then
                        Return False
                    End If
                Next
            End If
            '入庫單
            If STOCK_IN_ID <> "" Then
                IN_mdt = pf.GetDB("D_STOCK_IN_MASTER,NOLOCK", {{"STOCK_IN_ID", STOCK_IN_ID}})
                Dim IN_xRow As DataRow
                IN_xRow = IN_mdt.Rows(0)
                '生效日
                Dim IN_EFFECTIVE_DATE As Date = Convert.ToDateTime(IN_xRow.Item("EFFECTIVE_DATE")).ToString("yyyy/MM/dd")
                '覆核日
                Dim IN_CHANGE_DATE As Date = Nothing
                If IN_xRow.Item("VOUCHER_STATUS").ToString = "確認" Then
                    '覆核日
                    IN_CHANGE_DATE = Convert.ToDateTime(IN_xRow.Item("VOUCHER_TIME")).ToString("yyyy/MM/dd")
                ElseIf IN_xRow.Item("VOUCHER_STATUS").ToString = "生效" Or IN_xRow.Item("VOUCHER_STATUS").ToString = "作廢" Then
                    '異動日期
                    If IN_xRow.Item("VOUCHER_STATUS").ToString = "生效" Then
                        If DBNull.Value.Equals(IN_xRow.Item("ALTER_TIME")) Then
                            IN_CHANGE_DATE = Convert.ToDateTime(IN_xRow.Item("CREATE_TIME")).ToString("yyyy/MM/dd")
                        Else
                            IN_CHANGE_DATE = Convert.ToDateTime(IN_xRow.Item("ALTER_TIME")).ToString("yyyy/MM/dd")
                        End If
                    ElseIf IN_xRow.Item("VOUCHER_STATUS").ToString = "作廢" Then
                        IN_CHANGE_DATE = Convert.ToDateTime(IN_xRow.Item("ALTER_TIME")).ToString("yyyy/MM/dd")
                    End If
                End If

                '將明細寫入[D_DEPOT_STOCKS_DIFF]，然後再統一計算庫存異動數量
                IN_ddt = pf.GetDB("D_STOCK_IN_DETAIL,NOLOCK", {{"STOCK_IN_ID", STOCK_IN_ID}})
                Dim IN_table As String = "D_DEPOT_STOCKS_DIFF"
                For j As Integer = 0 To IN_ddt.Rows.Count - 1
                    Dim IN_yRow As DataRow
                    IN_yRow = IN_ddt.Rows(j)
                    Dim total_qty As Double = 0
                    '良品
                    Dim IN_GOOD_QTY As Double = 0
                    '破損品
                    Dim IN_BAD_QTY As Double = 0
                    If pf.CheckIfDBNull(IN_yRow.Item("STOCK_CHECK")) = 1 Then
                        '入庫確認
                        total_qty = (Convert.ToDouble(pf.CheckNull(IN_yRow.Item("QTY"))) + Convert.ToDouble(pf.CheckNull(IN_yRow.Item("PRESENT_QTY"))))
                        IN_GOOD_QTY = (Convert.ToDouble(pf.CheckNull(IN_yRow.Item("QTY"))) + Convert.ToDouble(pf.CheckNull(IN_yRow.Item("PRESENT_QTY"))))
                        IN_BAD_QTY = Convert.ToDouble(pf.CheckNull(IN_yRow.Item("BAD_QTY")))
                    ElseIf pf.CheckIfDBNull(IN_yRow.Item("STOCK_CHECK")) = 0 Then
                        '取消入庫確認
                        total_qty = (Convert.ToDouble(pf.CheckNull(IN_yRow.Item("QTY"))) + Convert.ToDouble(pf.CheckNull(IN_yRow.Item("PRESENT_QTY")))) * (-1)
                        IN_GOOD_QTY = (Convert.ToDouble(pf.CheckNull(IN_yRow.Item("QTY"))) + Convert.ToDouble(pf.CheckNull(IN_yRow.Item("PRESENT_QTY")))) * (-1)
                        IN_BAD_QTY = Convert.ToDouble(pf.CheckNull(IN_yRow.Item("BAD_QTY"))) * (-1)
                    End If
                    If IN_xRow.Item("STOCK_TYPE").ToString = "公司" Then
                        Dim col_value(,) As String = {
                        {"PAPER_ID", pf.CheckStringDBNull(IN_yRow.Item("STOCK_IN_ID"))},
                        {"EFFECTIVE_DATE", IN_xRow.Item("EFFECTIVE_DATE")},
                        {"CHANGE_DATE", IN_CHANGE_DATE},
                        {"DEPOT_ID", pf.CheckStringDBNull(IN_yRow.Item("DEPOT_ID"))},
                        {"PRODUCT_ID", pf.CheckStringDBNull(IN_yRow.Item("PRODUCT_ID"))},
                        {"BATCH_NUM", pf.CheckStringDBNull(IN_yRow.Item("BATCH_NUM"))},
                        {"G_DIFF_QTY", IN_GOOD_QTY},
                        {"B_DIFF_QTY", IN_BAD_QTY},
                        {"C_DIFF_QTY", 0},
                        {"CUS_DIFF_QTY", 0},
                        {"SUPPLIER_DIFF_QTY", 0},
                        {"VOUCHER_STATUS", pf.CheckStringDBNull(IN_xRow.Item("VOUCHER_STATUS"))},
                        {"CUSTOMER_ID", pf.CheckStringDBNull(IN_xRow.Item("CUSTOMER_ID"))},
                        {"SUPPLIER_ID", pf.CheckStringDBNull(IN_xRow.Item("SUPPLIER_ID"))},
                        {"CREATE_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                    }
                        sSql = pf.GetInsertquery(IN_table, col_value)
                    End If
                    Console.WriteLine(sSql)
                    command.CommandText = sSql
                    iAffectRows = command.ExecuteNonQuery()
                    If iAffectRows = 0 Then
                        Return False
                    End If
                Next
            End If

            '開始滾動計算每日庫存檔
            Dim CHECK_DATE As DateTime = SCROLL_DATE
            Dim END_DATE As DateTime = DateTime.Now
            Dim check_days As Integer = 0
            Do While CHECK_DATE.ToString("yyyy/MM/dd") < END_DATE.ToString("yyyy/MM/dd")
                If UPDATE_D_DEPOT_STOCKS_DAYS2(STOCK_OUT_ID, STOCK_IN_ID, CHECK_DATE, command) = False Then
                    Return False
                End If
                check_days = check_days + 1
                If check_days > 0 Then
                    CHECK_DATE = SCROLL_DATE.AddDays(check_days)
                End If
            Loop
        Catch ex As Exception
            pf.MyError(ex.ToString)
            Return False
        End Try
        Return True
    End Function

    '滾動客戶寄售每日庫存-寄售盤整單使用
    '異動出庫單的庫存數量[當日庫存][看覆核日]
    '計算公式：庫存數量=前一日的期末庫存數量+覆核日=當日的庫存異動數量
    '更新到[D_DEPOT_STOCKS]
    'STOCK_OUT_ID:出庫單編號 STOCK_IN_ID:入庫單編號 SCROLL_DATE:滾動開始日期
    Function SCROLL_D_CUS_CON_QTY_DAYS(STOCK_OUT_ID As String, STOCK_IN_ID As String, SCROLL_DATE As DateTime, command As SqlCommand) As Boolean
        Dim sSql As String = ""
        Dim iAffectRows As Integer = 0
        Dim OUT_mdt As New DataTable
        Dim OUT_ddt As New DataTable
        Dim IN_mdt As New DataTable
        Dim IN_ddt As New DataTable
        Try
            If STOCK_OUT_ID <> "" Then
                '出庫單主檔資料
                OUT_mdt = pf.GetDB("D_STOCK_OUT_MASTER,NOLOCK", {{"STOCK_OUT_ID", STOCK_OUT_ID}})
                Dim OUT_xRow As DataRow
                OUT_xRow = OUT_mdt.Rows(0)
                '生效日
                Dim OUT_EFFECTIVE_DATE As Date = Convert.ToDateTime(OUT_xRow.Item("EFFECTIVE_DATE")).ToString("yyyy/MM/dd")
                '覆核日
                Dim OUT_CHANGE_DATE As Date = Nothing
                If OUT_xRow.Item("VOUCHER_STATUS").ToString = "確認" Then
                    '覆核日
                    OUT_CHANGE_DATE = Convert.ToDateTime(OUT_xRow.Item("VOUCHER_TIME")).ToString("yyyy/MM/dd")
                ElseIf OUT_xRow.Item("VOUCHER_STATUS").ToString = "生效" Or OUT_xRow.Item("VOUCHER_STATUS").ToString = "作廢" Then
                    '異動日期
                    OUT_CHANGE_DATE = Convert.ToDateTime(OUT_xRow.Item("ALTER_TIME")).ToString("yyyy/MM/dd")
                End If
                '將明細寫入[D_DEPOT_STOCKS_DIFF]，然後再統一計算庫存異動數量
                OUT_ddt = pf.GetDB("D_STOCK_OUT_DETAIL,NOLOCK", {{"STOCK_OUT_ID", STOCK_OUT_ID}})
                Dim OUT_table As String = "D_DEPOT_STOCKS_DIFF"
                For j As Integer = 0 To OUT_ddt.Rows.Count - 1
                    Dim OUT_yRow As DataRow
                    OUT_yRow = OUT_ddt.Rows(j)
                    Dim OUT_TOTAL_QTY As Double = 0
                    Dim OUT_GOOd_QTY As Double = 0
                    Dim OUT_BAD_QTY As Double = 0
                    If pf.CheckIfDBNull(OUT_yRow.Item("STOCK_CHECK")) = 1 Then
                        '出庫確認
                        OUT_TOTAL_QTY = (Convert.ToDouble(pf.CheckNull(OUT_yRow.Item("QTY"))) + Convert.ToDouble(pf.CheckNull(OUT_yRow.Item("PRESENT_QTY")))) * (-1)

                    ElseIf pf.CheckIfDBNull(OUT_yRow.Item("STOCK_CHECK")) = 0 Then
                        '取消出庫確認
                        OUT_TOTAL_QTY = (Convert.ToDouble(pf.CheckNull(OUT_yRow.Item("QTY"))) + Convert.ToDouble(pf.CheckNull(OUT_yRow.Item("PRESENT_QTY"))))
                    End If
                    If OUT_xRow.Item("STOCK_TYPE").ToString = "寄售" Then
                        Dim col_value(,) As String = {
                        {"PAPER_ID", pf.CheckStringDBNull(OUT_yRow.Item("STOCK_OUT_ID"))},
                        {"EFFECTIVE_DATE", OUT_xRow.Item("EFFECTIVE_DATE")},
                        {"CHANGE_DATE", OUT_CHANGE_DATE},
                        {"DEPOT_ID", pf.CheckStringDBNull(OUT_yRow.Item("DEPOT_ID"))},
                        {"PRODUCT_ID", pf.CheckStringDBNull(OUT_yRow.Item("PRODUCT_ID"))},
                        {"BATCH_NUM", pf.CheckStringDBNull(OUT_yRow.Item("BATCH_NUM"))},
                        {"G_DIFF_QTY", 0},
                        {"B_DIFF_QTY", 0},
                        {"C_DIFF_QTY", 0},
                        {"CUS_DIFF_QTY", OUT_TOTAL_QTY},
                        {"SUPPLIER_DIFF_QTY", 0},
                        {"VOUCHER_STATUS", pf.CheckStringDBNull(OUT_xRow.Item("VOUCHER_STATUS"))},
                        {"CUSTOMER_ID", pf.CheckStringDBNull(OUT_xRow.Item("CUSTOMER_ID"))},
                        {"SUPPLIER_ID", pf.CheckStringDBNull(OUT_xRow.Item("SUPPLIER_ID"))},
                        {"CREATE_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                    }
                        sSql = pf.GetInsertquery(OUT_table, col_value)
                    End If
                    Console.WriteLine(sSql)
                    command.CommandText = sSql
                    iAffectRows = command.ExecuteNonQuery()
                    If iAffectRows = 0 Then
                        Return False
                    End If
                Next
            End If
            '入庫單
            If STOCK_IN_ID <> "" Then
                IN_mdt = pf.GetDB("D_STOCK_IN_MASTER,NOLOCK", {{"STOCK_IN_ID", STOCK_IN_ID}})
                Dim IN_xRow As DataRow
                IN_xRow = IN_mdt.Rows(0)
                '生效日
                Dim IN_EFFECTIVE_DATE As Date = Convert.ToDateTime(IN_xRow.Item("EFFECTIVE_DATE")).ToString("yyyy/MM/dd")
                '覆核日
                Dim IN_CHANGE_DATE As Date = Nothing
                If IN_xRow.Item("VOUCHER_STATUS").ToString = "確認" Then
                    '覆核日
                    IN_CHANGE_DATE = Convert.ToDateTime(IN_xRow.Item("VOUCHER_TIME")).ToString("yyyy/MM/dd")
                ElseIf IN_xRow.Item("VOUCHER_STATUS").ToString = "生效" Or IN_xRow.Item("VOUCHER_STATUS").ToString = "作廢" Then
                    '異動日期
                    IN_CHANGE_DATE = Convert.ToDateTime(IN_xRow.Item("ALTER_TIME")).ToString("yyyy/MM/dd")
                End If
                '將明細寫入[D_DEPOT_STOCKS_DIFF]，然後再統一計算庫存異動數量
                IN_ddt = pf.GetDB("D_STOCK_IN_DETAIL,NOLOCK", {{"STOCK_IN_ID", STOCK_IN_ID}})
                Dim IN_table As String = "D_DEPOT_STOCKS_DIFF"
                For j As Integer = 0 To IN_ddt.Rows.Count - 1
                    Dim IN_yRow As DataRow
                    IN_yRow = IN_ddt.Rows(j)
                    Dim IN_TOTAL_QTY As Double = 0
                    If pf.CheckIfDBNull(IN_yRow.Item("STOCK_CHECK")) = 1 Then
                        '入庫確認
                        IN_TOTAL_QTY = (Convert.ToDouble(pf.CheckNull(IN_yRow.Item("QTY"))) + Convert.ToDouble(pf.CheckNull(IN_yRow.Item("PRESENT_QTY"))))
                    ElseIf pf.CheckIfDBNull(IN_yRow.Item("STOCK_CHECK")) = 0 Then
                        '取消入庫確認
                        IN_TOTAL_QTY = (Convert.ToDouble(pf.CheckNull(IN_yRow.Item("QTY"))) + Convert.ToDouble(pf.CheckNull(IN_yRow.Item("PRESENT_QTY")))) * (-1)
                    End If
                    If IN_xRow.Item("STOCK_TYPE").ToString = "寄售" Then
                        Dim col_value(,) As String = {
                        {"PAPER_ID", pf.CheckStringDBNull(IN_yRow.Item("STOCK_IN_ID"))},
                        {"EFFECTIVE_DATE", IN_xRow.Item("EFFECTIVE_DATE")},
                        {"CHANGE_DATE", IN_CHANGE_DATE},
                        {"DEPOT_ID", pf.CheckStringDBNull(IN_yRow.Item("DEPOT_ID"))},
                        {"PRODUCT_ID", pf.CheckStringDBNull(IN_yRow.Item("PRODUCT_ID"))},
                        {"BATCH_NUM", pf.CheckStringDBNull(IN_yRow.Item("BATCH_NUM"))},
                        {"G_DIFF_QTY", 0},
                        {"B_DIFF_QTY", 0},
                        {"C_DIFF_QTY", 0},
                        {"CUS_DIFF_QTY", IN_TOTAL_QTY},
                        {"SUPPLIER_DIFF_QTY", 0},
                        {"VOUCHER_STATUS", pf.CheckStringDBNull(IN_xRow.Item("VOUCHER_STATUS"))},
                        {"CUSTOMER_ID", pf.CheckStringDBNull(IN_xRow.Item("CUSTOMER_ID"))},
                        {"SUPPLIER_ID", pf.CheckStringDBNull(IN_xRow.Item("SUPPLIER_ID"))},
                        {"CREATE_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                    }
                        sSql = pf.GetInsertquery(IN_table, col_value)
                    End If
                    Console.WriteLine(sSql)
                    command.CommandText = sSql
                    iAffectRows = command.ExecuteNonQuery()
                    If iAffectRows = 0 Then
                        Return False
                    End If
                Next
            End If
            '開始滾動計算每日庫存檔
            Dim CHECK_DATE As DateTime = SCROLL_DATE
            Dim END_DATE As DateTime = DateTime.Now
            Dim check_days As Integer = 0
            Do While CHECK_DATE.ToString("yyyy/MM/dd") < END_DATE.ToString("yyyy/MM/dd")
                If UPDATE_D_CUS_CON_QTY_DAYS(CHECK_DATE, command) = False Then
                    Return False
                End If
                check_days = check_days + 1
                If check_days > 0 Then
                    CHECK_DATE = SCROLL_DATE.AddDays(check_days)
                End If
            Loop
        Catch ex As Exception
            pf.MyError(ex.ToString)
            Return False
        End Try

        Return True

    End Function

    '2020.11.20 加入倉庫代號、產品代號、批號
    '計算D_DEPOT_STOCKS_DAYS的數量
    'CHECK_DATE：每日庫存的日期
    Public Function UPDATE_D_DEPOT_STOCKS_DAYS_BY_DPB(CHECK_DATE As Date, DEPOT_ID As String, PRODUCT_ID As String, BATCH_NUM As String, sqlCommand As SqlCommand) As Boolean
        Dim i As Integer = 0
        Dim iRowAffected As Integer = 0
        Dim table As String = "D_DEPOT_STOCKS_DAYS"
        Dim xRow As DataRow
        Dim SaveOK As Boolean = True
        Dim sSql As String = ""
        Dim sSql2 As String = ""
        Dim sSql3 As String = ""
        Dim sSql4 As String = ""
        Try
            '產生庫存每日檔資料
            Dim qdt2 As New DataTable
            'sSql = ""
            'sSql = "SELECT * FROM [D_DEPOT_STOCKS_DAYS] WITH (NOLOCK)"
            'sSql &= " WHERE Convert(varchar, [EFFECTIVE_DATE], 111) ='" & CHECK_DATE.AddDays(-1).ToString("yyyy/MM/dd") & "'"
            'sSql &= " ORDER BY [DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
            'Console.WriteLine(sSql)
            'qdt2 = pf.GetDataTable(sSql)
            Dim where1(,) As String = {
                {"CUSTOM_SQL", " Convert(varchar, [EFFECTIVE_DATE], 111) ='" & CHECK_DATE.AddDays(-1).ToString("yyyy/MM/dd") & "'"},
                {"DEPOT_ID", DEPOT_ID},
                {"PRODUCT_ID", PRODUCT_ID},
                {"BATCH_NUM", BATCH_NUM},
                {"ORDERBY_ASC", "DEPOT_ID"},
                {"ORDERBY_ASC", "PRODUCT_ID"},
                {"ORDERBY_ASC", "BATCH_NUM"}
                }
            qdt2 = pf.GetDB("D_DEPOT_STOCKS_DAYS,NOLOCK", where1)

            If qdt2.Rows.Count = 0 Then
                pf.MyError("每日庫存檔無[" & CHECK_DATE.AddDays(-1).ToString("yyyy/MM/dd") & "]的資料，請洽系統管理員!")
                Return False
            Else
                For i = 0 To qdt2.Rows.Count - 1
                    xRow = qdt2.Rows(i)
                    '判斷資料是否存在
                    Dim chk_data As New DataTable
                    'sSql2 = ""
                    'sSql2 = "SELECT * FROM [D_DEPOT_STOCKS_DAYS] WITH (NOLOCK)"
                    'sSql2 &= " WHERE CONVERT(varchar, [EFFECTIVE_DATE], 111) ='" & CHECK_DATE.ToString("yyyy/MM/dd") & "'"
                    'sSql2 &= " AND [DEPOT_ID]='" & xRow.Item("DEPOT_ID").ToString & "'"
                    'sSql2 &= " AND [PRODUCT_ID]='" & xRow.Item("PRODUCT_ID").ToString & "'"
                    'sSql2 &= " AND [BATCH_NUM]='" & xRow.Item("BATCH_NUM").ToString & "'"
                    'Console.WriteLine(sSql2)
                    'chk_data = pf.GetDataTable(sSql2)
                    Dim where2(,) As String = {
                        {"CUSTOM_SQL", " Convert(varchar, [EFFECTIVE_DATE], 111) ='" & CHECK_DATE.ToString("yyyy/MM/dd") & "'"},
                        {"DEPOT_ID", xRow.Item("DEPOT_ID").ToString},
                        {"PRODUCT_ID", xRow.Item("PRODUCT_ID").ToString},
                        {"BATCH_NUM", xRow.Item("BATCH_NUM").ToString}
                    }
                    chk_data = pf.GetDB("D_DEPOT_STOCKS_DAYS,NOLOCK", where2)
                    If chk_data.Rows.Count = 0 Then
                        Dim col_value(,) As String = {
                            {"DEPOT_ID", pf.CheckStringDBNull(xRow.Item("DEPOT_ID"))},
                            {"PRODUCT_ID", pf.CheckStringDBNull(xRow.Item("PRODUCT_ID"))},
                            {"BATCH_NUM", pf.CheckStringDBNull(xRow.Item("BATCH_NUM"))},
                            {"EFFECTIVE_DATE", CHECK_DATE.ToString("yyyy/MM/dd")},
                            {"GOOD_QTY", pf.CheckNull(xRow.Item("GOOD_QTY"))},
                            {"BAD_QTY", pf.CheckNull(xRow.Item("BAD_QTY"))},
                            {"CUS_CON_QTY", pf.CheckNull(xRow.Item("CUS_CON_QTY"))},
                            {"COST_PRICE", pf.CheckNull(xRow.Item("COST_PRICE"))},
                            {"UNIT_PRICE", pf.CheckNull(xRow.Item("UNIT_PRICE"))},
                            {"VALID_PERIOD", pf.CheckDateTimeNull(xRow.Item("VALID_PERIOD"))},
                            {"RED_SHOW", pf.CheckNull(xRow.Item("RED_SHOW"))},
                            {"SALE_QTY", pf.CheckNull(xRow.Item("SALE_QTY"))},
                            {"DEPOT_QTY", pf.CheckNull(xRow.Item("DEPOT_QTY"))},
                            {"INV_QTY", pf.CheckNull(xRow.Item("INV_QTY"))},
                            {"CREATE_USER", "系統"},
                            {"CREATE_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                        }
                        sSql2 = pf.GetInsertquery(table, col_value)
                        Console.WriteLine(sSql2)
                        sqlCommand.CommandText = sSql2
                        iRowAffected = sqlCommand.ExecuteNonQuery()
                        If iRowAffected = 0 Then
                            pf.MyError("新增每日庫存檔日期[" & CHECK_DATE.ToString("yyyy/MM/dd") & "]的資料發生錯誤!")
                            Return False
                        End If
                    End If

                    '開始計算庫存數
                    '計算異動每日結算庫存數INV_QTY-依照生效日EFFECTIVE_DATE
                    sSql3 = ""
                    Dim New_Inv_Qty As Double = 0
                    Dim New_Inv_Bad_Qty As Double = 0
                    sSql3 = "SELECT [DEPOT_ID],[PRODUCT_ID],[BATCH_NUM],SUM(ISNULL([G_DIFF_QTY],0)) AS G_DIFF_QTY "
                    sSql3 &= ",SUM(ISNULL([B_DIFF_QTY],0)) AS B_DIFF_QTY"
                    sSql3 &= " FROM [D_DEPOT_STOCKS_DIFF] With (NOLOCK)"
                    sSql3 &= " WHERE Convert(varchar, [EFFECTIVE_DATE], 111) ='" & CHECK_DATE.ToString("yyyy/MM/dd") & "'"
                    sSql3 &= " AND [DEPOT_ID]='" & xRow.Item("DEPOT_ID").ToString & "'"
                    sSql3 &= " AND [PRODUCT_ID]='" & xRow.Item("PRODUCT_ID").ToString & "'"
                    sSql3 &= " AND [BATCH_NUM]='" & xRow.Item("BATCH_NUM").ToString & "'"
                    sSql3 &= " GROUP BY [DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                    Console.WriteLine(sSql3)
                    Dim qdt3 As New DataTable
                    qdt3 = pf.GetDataTable(sSql3)
                    If qdt3.Rows.Count > 0 Then
                        New_Inv_Qty = Convert.ToDouble(pf.CheckNull(xRow.Item("INV_QTY"))) + Convert.ToDouble(pf.CheckNull(qdt3.Rows(0).Item("G_DIFF_QTY")))
                        New_Inv_Bad_Qty = Convert.ToDouble(pf.CheckNull(xRow.Item("INV_BAD_QTY"))) + Convert.ToDouble(pf.CheckNull(qdt3.Rows(0).Item("B_DIFF_QTY")))
                        Dim PK2() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM", "EFFECTIVE_DATE"}
                        Dim col_value2(,) As String = {
                            {"DEPOT_ID", xRow.Item("DEPOT_ID")},
                            {"PRODUCT_ID", xRow.Item("PRODUCT_ID")},
                            {"BATCH_NUM", xRow.Item("BATCH_NUM")},
                            {"EFFECTIVE_DATE", CHECK_DATE},
                            {"INV_QTY", New_Inv_Qty},
                            {"INV_BAD_QTY", New_Inv_Bad_Qty},
                            {"ALTER_USER", "系統"},
                            {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                        }
                        sSql3 = pf.GetUpdatequery(table, PK2, col_value2)
                        Console.WriteLine(sSql3)
                        sqlCommand.CommandText = sSql3
                        iRowAffected = sqlCommand.ExecuteNonQuery()
                        If iRowAffected = 0 Then
                            pf.MyError("1-異動每日庫存檔日期[" & CHECK_DATE.ToString("yyyy/MM/dd") & "]的資料發生錯誤!")
                            Return False
                        End If
                    Else
                        New_Inv_Qty = Convert.ToDouble(pf.CheckNull(xRow.Item("INV_QTY")))
                        New_Inv_Bad_Qty = Convert.ToDouble(pf.CheckNull(xRow.Item("INV_BAD_QTY")))
                        Dim PK2() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM", "EFFECTIVE_DATE"}
                        Dim col_value2(,) As String = {
                            {"DEPOT_ID", xRow.Item("DEPOT_ID")},
                            {"PRODUCT_ID", xRow.Item("PRODUCT_ID")},
                            {"BATCH_NUM", xRow.Item("BATCH_NUM")},
                            {"EFFECTIVE_DATE", CHECK_DATE},
                            {"INV_QTY", New_Inv_Qty},
                            {"INV_BAD_QTY", New_Inv_Bad_Qty},
                            {"ALTER_USER", "系統"},
                            {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                        }

                        sSql3 = pf.GetUpdatequery(table, PK2, col_value2)
                        Console.WriteLine(sSql3)
                        sqlCommand.CommandText = sSql3
                        iRowAffected = sqlCommand.ExecuteNonQuery()
                        If iRowAffected = 0 Then
                            pf.MyError("1-異動每日庫存檔-每日結算庫存數量-日期[" & CHECK_DATE.ToString("yyyy/MM/dd") & "]的資料發生錯誤!")
                            Return False
                        End If
                    End If

                    '計算2-依照覆核日
                    ''計算異動倉庫現有庫存數Good_QTY,DEPOT_QTY-依照覆核日CHANGE_DATE
                    sSql4 = ""
                    Dim New_Good_Qty As Double = 0
                    Dim New_Bad_Qty As Double = 0
                    Dim New_Depot_Qty As Double = 0
                    sSql4 = "SELECT [DEPOT_ID],[PRODUCT_ID],[BATCH_NUM],SUM(ISNULL([G_DIFF_QTY],0)) AS G_DIFF_QTY "
                    sSql4 &= " ,SUM(ISNULL([B_DIFF_QTY],0)) AS B_DIFF_QTY "
                    sSql4 &= " FROM [D_DEPOT_STOCKS_DIFF] WITH (NOLOCK)"
                    sSql4 &= " WHERE Convert(varchar, [CHANGE_DATE], 111) ='" & CHECK_DATE.ToString("yyyy/MM/dd") & "'"
                    sSql4 &= " AND [DEPOT_ID]='" & xRow.Item("DEPOT_ID").ToString & "'"
                    sSql4 &= " AND [PRODUCT_ID]='" & xRow.Item("PRODUCT_ID").ToString & "'"
                    sSql4 &= " AND [BATCH_NUM]='" & xRow.Item("BATCH_NUM").ToString & "'"
                    sSql4 &= " GROUP BY [DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                    Console.WriteLine(sSql4)
                    Dim qdt4 As New DataTable
                    qdt4 = pf.GetDataTable(sSql4)
                    If qdt4.Rows.Count > 0 Then
                        New_Good_Qty = Convert.ToDouble(pf.CheckNull(xRow.Item("GOOD_QTY"))) + Convert.ToDouble(pf.CheckNull(qdt4.Rows(0).Item("G_DIFF_QTY")))
                        New_Bad_Qty = Convert.ToDouble(pf.CheckNull(xRow.Item("BAD_QTY"))) + Convert.ToDouble(pf.CheckNull(qdt4.Rows(0).Item("B_DIFF_QTY")))
                        New_Depot_Qty = Convert.ToDouble(pf.CheckNull(xRow.Item("DEPOT_QTY"))) + Convert.ToDouble(pf.CheckNull(qdt4.Rows(0).Item("G_DIFF_QTY")))
                        Dim PK2() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM", "EFFECTIVE_DATE"}
                        Dim col_value2(,) As String = {
                            {"DEPOT_ID", xRow.Item("DEPOT_ID")},
                            {"PRODUCT_ID", xRow.Item("PRODUCT_ID")},
                            {"BATCH_NUM", xRow.Item("BATCH_NUM")},
                            {"EFFECTIVE_DATE", CHECK_DATE},
                            {"GOOD_QTY", New_Good_Qty},
                            {"BAD_QTY", New_Bad_Qty},
                            {"DEPOT_QTY", New_Depot_Qty},
                            {"ALTER_USER", "系統"},
                            {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                        }
                        sSql4 = pf.GetUpdatequery(table, PK2, col_value2)
                        Console.WriteLine(sSql4)
                        sqlCommand.CommandText = sSql4
                        iRowAffected = sqlCommand.ExecuteNonQuery()
                        If iRowAffected = 0 Then
                            pf.MyError("2-異動每日庫存檔-每日結算庫存數量-日期[" & CHECK_DATE.ToString("yyyy/MM/dd") & "]的資料發生錯誤!")
                            Return False
                        End If
                    Else
                        New_Good_Qty = Convert.ToDouble(pf.CheckNull(xRow.Item("GOOD_QTY")))
                        New_Bad_Qty = Convert.ToDouble(pf.CheckNull(xRow.Item("BAD_QTY")))
                        New_Depot_Qty = Convert.ToDouble(pf.CheckNull(xRow.Item("DEPOT_QTY")))
                        Dim PK2() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM", "EFFECTIVE_DATE"}
                        Dim col_value2(,) As String = {
                            {"DEPOT_ID", xRow.Item("DEPOT_ID")},
                            {"PRODUCT_ID", xRow.Item("PRODUCT_ID")},
                            {"BATCH_NUM", xRow.Item("BATCH_NUM")},
                            {"EFFECTIVE_DATE", CHECK_DATE},
                            {"GOOD_QTY", New_Good_Qty},
                            {"BAD_QTY", New_Bad_Qty},
                            {"DEPOT_QTY", New_Depot_Qty},
                            {"ALTER_USER", "系統"},
                            {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                        }
                        sSql4 = pf.GetUpdatequery(table, PK2, col_value2)
                        Console.WriteLine(sSql4)
                        sqlCommand.CommandText = sSql4
                        iRowAffected = sqlCommand.ExecuteNonQuery()
                        If iRowAffected = 0 Then
                            pf.MyError("2-異動每日庫存檔-倉庫現有庫存數量-日期[" & CHECK_DATE.ToString("yyyy/MM/dd") & "]的資料發生錯誤!")
                            Return False
                        End If
                    End If

                    '--------------------------------------------------------
                    '計算3-依照覆核日
                    ''計算即時可動支庫存數,覆核日CHANGE_DATE
                    Dim sSql5 As String = ""
                    Dim New_Sale_Qty As Double = 0
                    sSql5 = "SELECT [DEPOT_ID],[PRODUCT_ID],[BATCH_NUM],SUM(ISNULL([KEYIN_DIFF_QTY],0)) AS KEYIN_DIFF_QTY "
                    sSql5 &= " FROM [D_DEPOT_STOCKS_KEYIN_DIFF] WITH (NOLOCK)"
                    sSql5 &= " WHERE Convert(varchar, [CHANGE_DATE], 111) ='" & CHECK_DATE.ToString("yyyy/MM/dd") & "'"
                    sSql5 &= " AND [DEPOT_ID]='" & xRow.Item("DEPOT_ID").ToString & "'"
                    sSql5 &= " AND [PRODUCT_ID]='" & xRow.Item("PRODUCT_ID").ToString & "'"
                    sSql5 &= " AND [BATCH_NUM]='" & xRow.Item("BATCH_NUM").ToString & "'"
                    sSql5 &= " GROUP BY [DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                    Console.WriteLine(sSql5)
                    Dim qdt5 As New DataTable
                    qdt5 = pf.GetDataTable(sSql5)
                    If qdt5.Rows.Count > 0 Then
                        'New_Good_Qty = Convert.ToDouble(pf.CheckNull(xRow.Item("DEPOT_QTY"))) + Convert.ToDouble(pf.CheckNull(qdt4.Rows(0).Item("G_DIFF_QTY")))
                        'New_Bad_Qty = Convert.ToDouble(pf.CheckNull(xRow.Item("BAD_QTY"))) + Convert.ToDouble(pf.CheckNull(qdt4.Rows(0).Item("B_DIFF_QTY")))
                        New_Sale_Qty = Convert.ToDouble(pf.CheckNull(xRow.Item("SALE_QTY"))) + Convert.ToDouble(pf.CheckNull(qdt5.Rows(0).Item("KEYIN_DIFF_QTY")))
                        Dim PK2() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM", "EFFECTIVE_DATE"}
                        Dim col_value2(,) As String = {
                            {"DEPOT_ID", xRow.Item("DEPOT_ID")},
                            {"PRODUCT_ID", xRow.Item("PRODUCT_ID")},
                            {"BATCH_NUM", xRow.Item("BATCH_NUM")},
                            {"EFFECTIVE_DATE", CHECK_DATE},
                            {"SALE_QTY", New_Sale_Qty},
                            {"ALTER_USER", "系統"},
                            {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                        }
                        sSql5 = pf.GetUpdatequery(table, PK2, col_value2)
                        Console.WriteLine(sSql5)
                        sqlCommand.CommandText = sSql5
                        iRowAffected = sqlCommand.ExecuteNonQuery()
                        If iRowAffected = 0 Then
                            pf.MyError("2-異動每日庫存檔-每日結算庫存數量-日期[" & CHECK_DATE.ToString("yyyy/MM/dd") & "]的資料發生錯誤!")
                            Return False
                        End If
                    Else

                        New_Sale_Qty = Convert.ToDouble(pf.CheckNull(xRow.Item("SALE_QTY")))
                        Dim PK2() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM", "EFFECTIVE_DATE"}
                        Dim col_value2(,) As String = {
                            {"DEPOT_ID", xRow.Item("DEPOT_ID")},
                            {"PRODUCT_ID", xRow.Item("PRODUCT_ID")},
                            {"BATCH_NUM", xRow.Item("BATCH_NUM")},
                            {"EFFECTIVE_DATE", CHECK_DATE},
                            {"SALE_QTY", New_Sale_Qty},
                            {"ALTER_USER", "系統"},
                            {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                        }
                        sSql5 = pf.GetUpdatequery(table, PK2, col_value2)
                        Console.WriteLine(sSql5)
                        sqlCommand.CommandText = sSql5
                        iRowAffected = sqlCommand.ExecuteNonQuery()
                        If iRowAffected = 0 Then
                            pf.MyError("2-異動每日庫存檔-倉庫現有庫存數量-日期[" & CHECK_DATE.ToString("yyyy/MM/dd") & "]的資料發生錯誤!")
                            Return False
                        End If
                    End If
                    '--------------------------------------------------------

                Next


            End If

        Catch ex As Exception
            pf.MyError(ex.ToString)
            Return False
        End Try

        Return True

    End Function

    '2020.11.20 加入客戶代號、倉庫代號、產品代號、批號
    '計算客戶寄售每日庫存檔的庫存數量
    Public Function UPDATE_D_CUS_CON_QTY_DAYS_BY_CDPB(CHECK_DATE As Date, CUSTOMER_ID As String, DEPOT_ID As String, PRODUCT_ID As String, BATCH_NUM As String, sqlCommand As SqlCommand) As Boolean
        Dim sSql As String = ""
        Dim table As String = "D_CUS_CON_QTY_DAYS"
        Dim yRow As DataRow
        Dim iRowAffected As Integer = 0
        Try
            Dim qdt2 As New DataTable
            'sSql = "SELECT * FROM [D_CUS_CON_QTY_DAYS]  WITH (NOLOCK)"
            'sSql &= " WHERE CONVERT(varchar,[EFFECTIVE_DATE],111)='" & CHECK_DATE.AddDays(-1).ToString("yyyy/MM/dd") & "'"
            'sSql &= " ORDER BY [CUSTOMER_ID],[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
            'qdt2 = pf.GetDataTable(sSql)
            Dim where1(,) As String = {
                {"CUSTOM_SQL", " CONVERT(varchar,[EFFECTIVE_DATE],111)='" & CHECK_DATE.AddDays(-1).ToString("yyyy/MM/dd") & "'"},
                {"CUSTOMER_ID", CUSTOMER_ID},
                {"DEPOT_ID", DEPOT_ID},
                {"PRODUCT_ID", PRODUCT_ID},
                {"BATCH_NUM", BATCH_NUM},
                {"ORDERBY_ASC", "CUSTOMER_ID"},
                {"ORDERBY_ASC", "DEPOT_ID"},
                {"ORDERBY_ASC", "PRODUCT_ID"},
                {"ORDERBY_ASC", "BATCH_NUM"}
            }
            qdt2 = pf.GetDB("D_CUS_CON_QTY_DAYS,NOLOCK", where1)
            If qdt2.Rows.Count = 0 Then
                Dim col_value(,) As String = {
                        {"DEPOT_ID", DEPOT_ID},
                        {"PRODUCT_ID", PRODUCT_ID},
                        {"BATCH_NUM", BATCH_NUM},
                        {"EFFECTIVE_DATE", CHECK_DATE.AddDays(-1).ToString("yyyy/MM/dd")},
                        {"CUSTOMER_ID", CUSTOMER_ID},
                        {"CUS_INV_QTY", "0"},
                        {"CUS_DEPOT_QTY", "0"},
                        {"CUS_SALE_QTY", "0"},
                        {"CREATE_USER", "系統"},
                        {"CREATE_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                    }
                sSql = pf.GetInsertquery(table, col_value)
                Console.WriteLine(sSql)
                sqlCommand.CommandText = sSql
                iRowAffected = sqlCommand.ExecuteNonQuery()
                If iRowAffected = 0 Then
                    Return False
                End If
                qdt2 = pf.GetDB("D_CUS_CON_QTY_DAYS,NOLOCK", where1)
            End If
            For j = 0 To qdt2.Rows.Count - 1
                yRow = qdt2.Rows(j)
                '判斷資料是否存在
                Dim chk_data As New DataTable
                'sSql = "SELECT * FROM [D_CUS_CON_QTY_DAYS] WITH (NOLOCK)"
                'sSql &= " WHERE Convert(varchar, [EFFECTIVE_DATE], 111) ='" & CHECK_DATE.ToString("yyyy/MM/dd") & "'"
                'sSql &= " AND [DEPOT_ID]='" & yRow.Item("DEPOT_ID").ToString & "'"
                'sSql &= " AND [PRODUCT_ID]='" & yRow.Item("PRODUCT_ID").ToString & "'"
                'sSql &= " AND [BATCH_NUM]='" & yRow.Item("BATCH_NUM").ToString & "'"
                'sSql &= " AND [CUSTOMER_ID]='" & yRow.Item("CUSTOMER_ID").ToString & "'"
                'chk_data = pf.GetDataTable(sSql)
                Dim where2(,) As String = {
                    {"CUSTOM_SQL", " CONVERT(varchar,[EFFECTIVE_DATE],111)='" & CHECK_DATE.ToString("yyyy/MM/dd") & "'"},
                    {"CUSTOMER_ID", yRow.Item("CUSTOMER_ID").ToString},
                    {"DEPOT_ID", yRow.Item("DEPOT_ID").ToString},
                    {"PRODUCT_ID", yRow.Item("PRODUCT_ID").ToString},
                    {"BATCH_NUM", yRow.Item("BATCH_NUM").ToString}
                }
                chk_data = pf.GetDB("D_CUS_CON_QTY_DAYS,NOLOCK", where2)

                If chk_data.Rows.Count = 0 Then
                    Dim col_value(,) As String = {
                        {"DEPOT_ID", yRow.Item("DEPOT_ID")},
                        {"PRODUCT_ID", yRow.Item("PRODUCT_ID")},
                        {"BATCH_NUM", yRow.Item("BATCH_NUM")},
                        {"EFFECTIVE_DATE", CHECK_DATE.ToString("yyyy/MM/dd")},
                        {"CUSTOMER_ID", yRow.Item("CUSTOMER_ID")},
                        {"CUS_INV_QTY", pf.CheckNull(yRow.Item("CUS_INV_QTY"))},
                        {"CUS_DEPOT_QTY", pf.CheckNull(yRow.Item("CUS_DEPOT_QTY"))},
                        {"CUS_SALE_QTY", pf.CheckNull(yRow.Item("CUS_SALE_QTY"))},
                        {"CREATE_USER", "系統"},
                        {"CREATE_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                    }
                    sSql = pf.GetInsertquery(table, col_value)
                    Console.WriteLine(sSql)
                    sqlCommand.CommandText = sSql
                    iRowAffected = sqlCommand.ExecuteNonQuery()
                    If iRowAffected = 0 Then
                        Return False
                    End If
                End If

                '計算每日結算庫存數
                '依照生效日-CUS_INV_QTY
                sSql = ""
                Dim New_Qty As Double = 0
                sSql = "SELECT [CUSTOMER_ID],[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM],SUM(ISNULL([CUS_DIFF_QTY],0)) as CUS_DIFF_QTY "
                sSql &= " FROM [D_DEPOT_STOCKS_DIFF] WITH (NOLOCK)  WHERE [EFFECTIVE_DATE]='" & CHECK_DATE.ToString("yyyy/MM/dd") & "'"
                sSql &= " AND [CUSTOMER_ID]='" & yRow.Item("CUSTOMER_ID").ToString & "'"
                sSql &= " AND [DEPOT_ID]='" & yRow.Item("DEPOT_ID").ToString & "'"
                sSql &= " AND [PRODUCT_ID]='" & yRow.Item("PRODUCT_ID").ToString & "'"
                sSql &= " AND [BATCH_NUM]='" & yRow.Item("BATCH_NUM").ToString & "'"
                sSql &= " AND ISNULL([CUS_DIFF_QTY],0)<>'0'"
                sSql &= " GROUP BY [CUSTOMER_ID],[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                Console.WriteLine(sSql)
                Dim qdt3 As New DataTable
                qdt3 = pf.GetDataTable(sSql)
                If qdt3.Rows.Count > 0 Then
                    New_Qty = Convert.ToDouble(pf.CheckNull(yRow.Item("CUS_INV_QTY"))) + Convert.ToDouble(pf.CheckNull(qdt3.Rows(0).Item("CUS_DIFF_QTY")))
                    Dim cus_table As String = "D_CUS_CON_QTY_DAYS"
                    Dim cus_pk2() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM", "EFFECTIVE_DATE", "CUSTOMER_ID"}
                    Dim cus_col_value2(,) As String = {
                        {"DEPOT_ID", yRow.Item("DEPOT_ID")},
                        {"PRODUCT_ID", yRow.Item("PRODUCT_ID")},
                        {"BATCH_NUM", yRow.Item("BATCH_NUM")},
                        {"EFFECTIVE_DATE", CHECK_DATE.ToString("yyyy/MM/dd")},
                        {"CUSTOMER_ID", yRow.Item("CUSTOMER_ID")},
                        {"CUS_INV_QTY", New_Qty},
                        {"ALTER_USER", "系統"},
                        {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                    }
                    sSql = pf.GetUpdatequery(cus_table, cus_pk2, cus_col_value2)
                    Console.WriteLine(sSql)
                    sqlCommand.CommandText = sSql
                    iRowAffected = sqlCommand.ExecuteNonQuery()
                    If iRowAffected = 0 Then
                        Return False
                    End If
                Else
                    New_Qty = Convert.ToDouble(pf.CheckNull(yRow.Item("CUS_INV_QTY")))
                    Dim cus_table As String = "D_CUS_CON_QTY_DAYS"
                    Dim cus_pk2() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM", "EFFECTIVE_DATE", "CUSTOMER_ID"}
                    Dim cus_col_value2(,) As String = {
                        {"DEPOT_ID", yRow.Item("DEPOT_ID")},
                        {"PRODUCT_ID", yRow.Item("PRODUCT_ID")},
                        {"BATCH_NUM", yRow.Item("BATCH_NUM")},
                        {"EFFECTIVE_DATE", CHECK_DATE.ToString("yyyy/MM/dd")},
                        {"CUSTOMER_ID", yRow.Item("CUSTOMER_ID")},
                        {"CUS_INV_QTY", New_Qty},
                        {"ALTER_USER", "系統"},
                        {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                    }
                    sSql = pf.GetUpdatequery(cus_table, cus_pk2, cus_col_value2)
                    Console.WriteLine(sSql)
                    sqlCommand.CommandText = sSql
                    iRowAffected = sqlCommand.ExecuteNonQuery()
                    If iRowAffected = 0 Then
                        Return False
                    End If
                End If

                '計算倉庫現有庫存數
                '依照覆核日CUS_DEPOT_QTY
                sSql = ""
                Dim New_Depot_Qty As Double = 0
                sSql = "SELECT [CUSTOMER_ID],[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM],SUM(ISNULL([CUS_DIFF_QTY],0)) as CUS_DIFF_QTY "
                sSql &= " FROM [D_DEPOT_STOCKS_DIFF] WITH (NOLOCK)  WHERE [CHANGE_DATE]='" & CHECK_DATE.ToString("yyyy/MM/dd") & "'"
                sSql &= " AND [CUSTOMER_ID]='" & yRow.Item("CUSTOMER_ID").ToString & "'"
                sSql &= " AND [DEPOT_ID]='" & yRow.Item("DEPOT_ID").ToString & "'"
                sSql &= " AND [PRODUCT_ID]='" & yRow.Item("PRODUCT_ID").ToString & "'"
                sSql &= " AND [BATCH_NUM]='" & yRow.Item("BATCH_NUM").ToString & "'"
                sSql &= " AND ISNULL([CUS_DIFF_QTY],0)<>'0'"
                sSql &= " GROUP BY [CUSTOMER_ID],[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                Console.WriteLine(sSql)
                Dim qdt4 As New DataTable
                qdt4 = pf.GetDataTable(sSql)
                If qdt4.Rows.Count > 0 Then
                    New_Depot_Qty = Convert.ToDouble(pf.CheckNull(yRow.Item("CUS_DEPOT_QTY"))) + Convert.ToDouble(pf.CheckNull(qdt4.Rows(0).Item("CUS_DIFF_QTY")))
                    Dim cus_table As String = "D_CUS_CON_QTY_DAYS"
                    Dim cus_pk2() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM", "EFFECTIVE_DATE", "CUSTOMER_ID"}
                    Dim cus_col_value2(,) As String = {
                        {"DEPOT_ID", yRow.Item("DEPOT_ID")},
                        {"PRODUCT_ID", yRow.Item("PRODUCT_ID")},
                        {"BATCH_NUM", yRow.Item("BATCH_NUM")},
                        {"EFFECTIVE_DATE", CHECK_DATE.ToString("yyyy/MM/dd")},
                        {"CUSTOMER_ID", yRow.Item("CUSTOMER_ID")},
                        {"CUS_DEPOT_QTY", New_Depot_Qty},
                        {"ALTER_USER", "系統"},
                        {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                    }
                    sSql = pf.GetUpdatequery(cus_table, cus_pk2, cus_col_value2)
                    Console.WriteLine(sSql)
                    sqlCommand.CommandText = sSql
                    iRowAffected = sqlCommand.ExecuteNonQuery()
                    If iRowAffected = 0 Then
                        Return False
                    End If
                Else
                    New_Depot_Qty = Convert.ToDouble(pf.CheckNull(yRow.Item("CUS_DEPOT_QTY")))
                    Dim cus_table As String = "D_CUS_CON_QTY_DAYS"
                    Dim cus_pk2() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM", "EFFECTIVE_DATE", "CUSTOMER_ID"}
                    Dim cus_col_value2(,) As String = {
                        {"DEPOT_ID", yRow.Item("DEPOT_ID")},
                        {"PRODUCT_ID", yRow.Item("PRODUCT_ID")},
                        {"BATCH_NUM", yRow.Item("BATCH_NUM")},
                        {"EFFECTIVE_DATE", CHECK_DATE.ToString("yyyy/MM/dd")},
                        {"CUSTOMER_ID", yRow.Item("CUSTOMER_ID")},
                        {"CUS_DEPOT_QTY", New_Depot_Qty},
                        {"ALTER_USER", "系統"},
                        {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                    }
                    sSql = pf.GetUpdatequery(cus_table, cus_pk2, cus_col_value2)
                    Console.WriteLine(sSql)
                    sqlCommand.CommandText = sSql
                    iRowAffected = sqlCommand.ExecuteNonQuery()
                    If iRowAffected = 0 Then
                        Return False
                    End If
                End If

                '------------------------------------------------------
                '結算即時可動支每日庫存
                '資料表：D_DEPOT_STOCKS_KEYIN_DIFF
                '寄售倉：CUS_DIFF_QTY
                '計算倉庫現有庫存數
                '依照覆核日CUS_DEPOT_QTY
                sSql = ""
                Dim New_Sale_Qty As Double = 0
                sSql = "SELECT [CUSTOMER_ID],[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM],SUM(ISNULL([CUS_DIFF_QTY],0)) as CUS_DIFF_QTY "
                sSql &= " FROM [D_DEPOT_STOCKS_KEYIN_DIFF] WITH (NOLOCK)  WHERE [CHANGE_DATE]='" & CHECK_DATE.ToString("yyyy/MM/dd") & "'"
                sSql &= " AND [CUSTOMER_ID]='" & yRow.Item("CUSTOMER_ID").ToString & "'"
                sSql &= " AND [DEPOT_ID]='" & yRow.Item("DEPOT_ID").ToString & "'"
                sSql &= " AND [PRODUCT_ID]='" & yRow.Item("PRODUCT_ID").ToString & "'"
                sSql &= " AND [BATCH_NUM]='" & yRow.Item("BATCH_NUM").ToString & "'"
                'sSql &= " AND ISNULL([CUS_DIFF_QTY],0)<>'0'"
                sSql &= " GROUP BY [CUSTOMER_ID],[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                Console.WriteLine(sSql)
                Dim qdt5 As New DataTable
                qdt5 = pf.GetDataTable(sSql)
                If qdt5.Rows.Count > 0 Then
                    New_Sale_Qty = Convert.ToDouble(pf.CheckNull(yRow.Item("CUS_SALE_QTY"))) + Convert.ToDouble(pf.CheckNull(qdt5.Rows(0).Item("CUS_DIFF_QTY")))
                    Dim cus_table As String = "D_CUS_CON_QTY_DAYS"
                    Dim cus_pk2() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM", "EFFECTIVE_DATE", "CUSTOMER_ID"}
                    Dim cus_col_value2(,) As String = {
                        {"DEPOT_ID", yRow.Item("DEPOT_ID")},
                        {"PRODUCT_ID", yRow.Item("PRODUCT_ID")},
                        {"BATCH_NUM", yRow.Item("BATCH_NUM")},
                        {"EFFECTIVE_DATE", CHECK_DATE.ToString("yyyy/MM/dd")},
                        {"CUSTOMER_ID", yRow.Item("CUSTOMER_ID")},
                        {"CUS_SALE_QTY", New_Sale_Qty},
                        {"ALTER_USER", "系統"},
                        {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                    }
                    sSql = pf.GetUpdatequery(cus_table, cus_pk2, cus_col_value2)
                    Console.WriteLine(sSql)
                    sqlCommand.CommandText = sSql
                    iRowAffected = sqlCommand.ExecuteNonQuery()
                    If iRowAffected = 0 Then
                        Return False
                    End If
                Else
                    New_Sale_Qty = Convert.ToDouble(pf.CheckNull(yRow.Item("CUS_SALE_QTY")))
                    Dim cus_table As String = "D_CUS_CON_QTY_DAYS"
                    Dim cus_pk2() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM", "EFFECTIVE_DATE", "CUSTOMER_ID"}
                    Dim cus_col_value2(,) As String = {
                        {"DEPOT_ID", yRow.Item("DEPOT_ID")},
                        {"PRODUCT_ID", yRow.Item("PRODUCT_ID")},
                        {"BATCH_NUM", yRow.Item("BATCH_NUM")},
                        {"EFFECTIVE_DATE", CHECK_DATE.ToString("yyyy/MM/dd")},
                        {"CUSTOMER_ID", yRow.Item("CUSTOMER_ID")},
                        {"CUS_SALE_QTY", New_Sale_Qty},
                        {"ALTER_USER", "系統"},
                        {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                    }
                    sSql = pf.GetUpdatequery(cus_table, cus_pk2, cus_col_value2)
                    Console.WriteLine(sSql)
                    sqlCommand.CommandText = sSql
                    iRowAffected = sqlCommand.ExecuteNonQuery()
                    If iRowAffected = 0 Then
                        Return False
                    End If
                End If
                '------------------------------------------------------
            Next
        Catch ex As Exception
            pf.MyError(ex.ToString)
            Return False
        End Try
        Return True
    End Function

    '2020.11.24加入經銷商代號、倉庫代號、產品代號、批號
    '計算經銷商每日庫存檔的庫存數量
    Public Function UPDATE_D_DEALER_CON_QTY_DAYS_BY_SDPB(CHECK_DATE As Date, SUPPLIER_ID As String, DEPOT_ID As String, PRODUCT_ID As String, BATCH_NUM As String, sqlCommand As SqlCommand) As Boolean
        Dim sSql As String = ""
        Dim sSql2 As String = ""
        Dim table As String = "D_DEALER_CON_QTY_DAYS"
        Dim yRow As DataRow
        Dim iRowAffected As Integer = 0
        Try
            sSql2 = "SELECT * FROM [D_DEALER_CON_QTY_DAYS] WITH (NOLOCK) WHERE CONVERT(varchar,[EFFECTIVE_DATE],111)='" & CHECK_DATE.AddDays(-1).ToString("yyyy/MM/dd") & "'"
            sSql2 &= " AND [DEPOT_ID]='" & DEPOT_ID & "'"
            sSql2 &= " AND [PRODUCT_ID]='" & PRODUCT_ID & "'"
            sSql2 &= " AND [BATCH_NUM]='" & BATCH_NUM & "'"
            sSql2 &= " AND [SUPPLIER_ID]='" & SUPPLIER_ID & "'"
            sSql2 &= " ORDER BY [SUPPLIER_ID],[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
            Console.WriteLine(sSql2)
            Dim qdt2 As New DataTable
            qdt2 = pf.GetDataTable(sSql2)
            If qdt2.Rows.Count = 0 Then
                Dim col_value(,) As String = {
                        {"DEPOT_ID", DEPOT_ID},
                        {"PRODUCT_ID", PRODUCT_ID},
                        {"BATCH_NUM", BATCH_NUM},
                        {"EFFECTIVE_DATE", CHECK_DATE.AddDays(1).ToString("yyyy/MM/dd")},
                        {"SUPPLIER_ID", SUPPLIER_ID},
                        {"CON_INV_QTY", "0"},
                        {"CON_DEPOT_QTY", "0"},
                        {"CON_SALE_QTY", "0"},
                        {"CREATE_USER", "系統"},
                        {"CREATE_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                    }
                sSql = pf.GetInsertquery(table, col_value)
                Console.WriteLine(sSql)
                sqlCommand.CommandText = sSql
                iRowAffected = sqlCommand.ExecuteNonQuery()
                If iRowAffected = 0 Then
                    Return False
                End If
                qdt2 = pf.GetDataTable(sSql2)
            End If

            For j = 0 To qdt2.Rows.Count - 1
                yRow = qdt2.Rows(j)
                '判斷資料是否存在
                sSql = "SELECT * FROM [D_DEALER_CON_QTY_DAYS] WITH (NOLOCK)"
                sSql &= " WHERE Convert(varchar, [EFFECTIVE_DATE], 111) ='" & CHECK_DATE.ToString("yyyy/MM/dd") & "'"
                sSql &= " AND [DEPOT_ID]='" & yRow.Item("DEPOT_ID").ToString & "'"
                sSql &= " AND [PRODUCT_ID]='" & yRow.Item("PRODUCT_ID").ToString & "'"
                sSql &= " AND [BATCH_NUM]='" & yRow.Item("BATCH_NUM").ToString & "'"
                sSql &= " AND [SUPPLIER_ID]='" & yRow.Item("SUPPLIER_ID").ToString & "'"
                Dim chk_data As New DataTable
                chk_data = pf.GetDataTable(sSql)
                If chk_data.Rows.Count = 0 Then
                    Dim col_value(,) As String = {
                        {"DEPOT_ID", yRow.Item("DEPOT_ID")},
                        {"PRODUCT_ID", yRow.Item("PRODUCT_ID")},
                        {"BATCH_NUM", yRow.Item("BATCH_NUM")},
                        {"EFFECTIVE_DATE", CHECK_DATE.ToString("yyyy/MM/dd")},
                        {"SUPPLIER_ID", yRow.Item("SUPPLIER_ID")},
                        {"CON_INV_QTY", pf.CheckNull(yRow.Item("CON_INV_QTY"))},
                        {"CON_DEPOT_QTY", pf.CheckNull(yRow.Item("CON_DEPOT_QTY"))},
                        {"CON_SALE_QTY", pf.CheckNull(yRow.Item("CON_SALE_QTY"))},
                        {"CREATE_USER", "系統"},
                        {"CREATE_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                    }
                    sSql = pf.GetInsertquery(table, col_value)
                    Console.WriteLine(sSql)
                    sqlCommand.CommandText = sSql
                    iRowAffected = sqlCommand.ExecuteNonQuery()
                    If iRowAffected = 0 Then
                        Return False
                    End If
                End If

                '計算異動每日結算庫存數
                '依照生效日-CON_INV_QTY
                sSql = ""
                Dim New_Inv_Qty As Double = 0
                sSql = "SELECT [SUPPLIER_ID],[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM],SUM(ISNULL([SUPPLIER_DIFF_QTY],0)) as SUPPLIER_DIFF_QTY "
                sSql &= " FROM [D_DEPOT_STOCKS_DIFF] WITH (NOLOCK)  WHERE [EFFECTIVE_DATE]='" & CHECK_DATE.ToString("yyyy/MM/dd") & "'"
                sSql &= " AND [SUPPLIER_ID]='" & yRow.Item("SUPPLIER_ID").ToString & "'"
                sSql &= " AND [DEPOT_ID]='" & yRow.Item("DEPOT_ID").ToString & "'"
                sSql &= " AND [PRODUCT_ID]='" & yRow.Item("PRODUCT_ID").ToString & "'"
                sSql &= " AND [BATCH_NUM]='" & yRow.Item("BATCH_NUM").ToString & "'"
                'sSql &= " AND ISNULL([SUPPLIER_DIFF_QTY],0)<>'0'"
                sSql &= " GROUP BY [SUPPLIER_ID],[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                Console.WriteLine(sSql)
                Dim qdt3 As New DataTable
                qdt3 = pf.GetDataTable(sSql)
                If qdt3.Rows.Count > 0 Then
                    New_Inv_Qty = Convert.ToDouble(pf.CheckNull(yRow.Item("CON_INV_QTY"))) + Convert.ToDouble(pf.CheckNull(qdt3.Rows(0).Item("SUPPLIER_DIFF_QTY")))
                    Dim dealer_pk2() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM", "EFFECTIVE_DATE", "SUPPLIER_ID"}
                    Dim dealer_col_value2(,) As String = {
                        {"DEPOT_ID", yRow.Item("DEPOT_ID")},
                        {"PRODUCT_ID", yRow.Item("PRODUCT_ID")},
                        {"BATCH_NUM", yRow.Item("BATCH_NUM")},
                        {"EFFECTIVE_DATE", CHECK_DATE.ToString("yyyy/MM/dd")},
                        {"SUPPLIER_ID", yRow.Item("SUPPLIER_ID")},
                        {"CON_INV_QTY", New_Inv_Qty},
                        {"ALTER_USER", "系統"},
                        {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                    }
                    sSql = pf.GetUpdatequery(table, dealer_pk2, dealer_col_value2)
                    Console.WriteLine(sSql)
                    sqlCommand.CommandText = sSql
                    iRowAffected = sqlCommand.ExecuteNonQuery()
                    If iRowAffected = 0 Then
                        Return False
                    End If
                Else
                    New_Inv_Qty = Convert.ToDouble(pf.CheckNull(yRow.Item("CON_INV_QTY")))
                    Dim dealer_pk2() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM", "EFFECTIVE_DATE", "SUPPLIER_ID"}
                    Dim dealer_col_value2(,) As String = {
                        {"DEPOT_ID", yRow.Item("DEPOT_ID")},
                        {"PRODUCT_ID", yRow.Item("PRODUCT_ID")},
                        {"BATCH_NUM", yRow.Item("BATCH_NUM")},
                        {"EFFECTIVE_DATE", CHECK_DATE.ToString("yyyy/MM/dd")},
                        {"SUPPLIER_ID", yRow.Item("SUPPLIER_ID")},
                        {"CON_INV_QTY", New_Inv_Qty},
                        {"ALTER_USER", "系統"},
                        {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                    }
                    sSql = pf.GetUpdatequery(table, dealer_pk2, dealer_col_value2)
                    Console.WriteLine(sSql)
                    sqlCommand.CommandText = sSql
                    iRowAffected = sqlCommand.ExecuteNonQuery()
                    If iRowAffected = 0 Then
                        Return False
                    End If
                End If

                '更新經銷商倉庫
                Dim dt As DataTable = pf.GetDB("D_DEPOT_BASE,NOLOCK", {{"DEPOT_ID", yRow.Item("DEPOT_ID").ToString}})
                If dt.Rows.Count > 0 Then
                    If dt.Rows(0).Item("DEPOT_TYPE").ToString = "經銷商" AndAlso dt.Rows(0).Item("SUPPLIER_ID").ToString = yRow.Item("SUPPLIER_ID").ToString Then
                        '取產品的VALID_PERIOD
                        Dim pWhere(,) As String = {
                                    {"PRODUCT_ID", yRow.Item("PRODUCT_ID")},
                                    {"BATCH_NUM", yRow.Item("BATCH_NUM")},
                                    {"ORDERBY_ASC", "DEPOT_ID"}
                                }
                        Dim pdt As DataTable = pf.GetDB("D_DEPOT_STOCKS,NOLOCK", pWhere)

                        Dim tableName2 As String = "D_DEPOT_STOCKS_DAYS"
                        Dim pk2() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM", "EFFECTIVE_DATE"}
                        Dim col_value2(,) As String = {
                                    {"DEPOT_ID", yRow.Item("DEPOT_ID").ToString},
                                    {"PRODUCT_ID", yRow.Item("PRODUCT_ID").ToString},
                                    {"BATCH_NUM", yRow.Item("BATCH_NUM").ToString},
                                    {"EFFECTIVE_DATE", CHECK_DATE.ToString("yyyy/MM/dd")},
                                    {"INV_QTY", New_Inv_Qty},
                                    {"VALID_PERIOD", CType(pdt.Rows(0).Item("VALID_PERIOD"), DateTime).ToString("yyyy/MM/dd")},
                                    {"ALTER_USER", "系統"},
                                    {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff")}
                                }
                        'sSql = pf.GetUpdatequery(tableName2, pk2, col_value2)

                        '判斷更新方式(insert/update)
                        Dim zWhere(,) As String = {
                             {"DEPOT_ID", yRow.Item("DEPOT_ID").ToString},
                             {"PRODUCT_ID", yRow.Item("PRODUCT_ID").ToString},
                             {"BATCH_NUM", yRow.Item("BATCH_NUM").ToString},
                             {"EFFECTIVE_DATE", CHECK_DATE.ToString("yyyy/MM/dd")}
                        }
                        Dim zdt As DataTable = pf.GetDB("D_DEPOT_STOCKS_DAYS,NOLOCK", zWhere)
                        If zdt.Rows.Count > 0 Then
                            sSql = pf.GetUpdatequery(tableName2, pk2, col_value2)
                        Else
                            col_value2 = {
                                {"DEPOT_ID", yRow.Item("DEPOT_ID").ToString},
                                {"PRODUCT_ID", yRow.Item("PRODUCT_ID").ToString},
                                {"BATCH_NUM", yRow.Item("BATCH_NUM").ToString},
                                {"EFFECTIVE_DATE", CHECK_DATE.ToString("yyyy/MM/dd")},
                                {"INV_QTY", New_Inv_Qty},
                                {"VALID_PERIOD", CType(pdt.Rows(0).Item("VALID_PERIOD"), DateTime).ToString("yyyy/MM/dd")},
                                {"CREATE_USER", "系統"},
                                {"CREATE_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff")}
                            }
                            sSql = pf.GetInsertquery(tableName2, col_value2)
                        End If
                        Console.WriteLine(sSql)
                        sqlCommand.CommandText = sSql
                        iRowAffected = sqlCommand.ExecuteNonQuery()
                        If iRowAffected = 0 Then
                            Return False
                        End If
                    End If
                End If

                '計算異動倉庫現有庫存數
                '依照覆核日
                sSql = ""
                Dim New_Depot_Qty As Double = 0
                sSql = "SELECT [SUPPLIER_ID],[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM],SUM(ISNULL([SUPPLIER_DIFF_QTY],0)) as SUPPLIER_DIFF_QTY "
                sSql &= " FROM [D_DEPOT_STOCKS_DIFF] WITH (NOLOCK)  WHERE [CHANGE_DATE]='" & CHECK_DATE.ToString("yyyy/MM/dd") & "'"
                sSql &= " AND [SUPPLIER_ID]='" & yRow.Item("SUPPLIER_ID").ToString & "'"
                sSql &= " AND [DEPOT_ID]='" & yRow.Item("DEPOT_ID").ToString & "'"
                sSql &= " AND [PRODUCT_ID]='" & yRow.Item("PRODUCT_ID").ToString & "'"
                sSql &= " AND [BATCH_NUM]='" & yRow.Item("BATCH_NUM").ToString & "'"
                'sSql &= " AND ISNULL([SUPPLIER_DIFF_QTY],0)<>'0'"
                sSql &= " GROUP BY [SUPPLIER_ID],[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                Console.WriteLine(sSql)
                Dim qdt4 As New DataTable
                qdt4 = pf.GetDataTable(sSql)
                If qdt4.Rows.Count > 0 Then
                    New_Depot_Qty = Convert.ToDouble(pf.CheckNull(yRow.Item("CON_DEPOT_QTY"))) + Convert.ToDouble(pf.CheckNull(qdt4.Rows(0).Item("SUPPLIER_DIFF_QTY")))
                    Dim dealer_pk2() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM", "EFFECTIVE_DATE", "SUPPLIER_ID"}
                    Dim dealer_col_value2(,) As String = {
                        {"DEPOT_ID", yRow.Item("DEPOT_ID")},
                        {"PRODUCT_ID", yRow.Item("PRODUCT_ID")},
                        {"BATCH_NUM", yRow.Item("BATCH_NUM")},
                        {"EFFECTIVE_DATE", CHECK_DATE.ToString("yyyy/MM/dd")},
                        {"SUPPLIER_ID", yRow.Item("SUPPLIER_ID")},
                        {"CON_DEPOT_QTY", New_Depot_Qty},
                        {"ALTER_USER", "系統"},
                        {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                    }
                    sSql = pf.GetUpdatequery(table, dealer_pk2, dealer_col_value2)
                    Console.WriteLine(sSql)
                    sqlCommand.CommandText = sSql
                    iRowAffected = sqlCommand.ExecuteNonQuery()
                    If iRowAffected = 0 Then
                        Return False
                    End If
                Else
                    New_Depot_Qty = Convert.ToDouble(pf.CheckNull(yRow.Item("CON_DEPOT_QTY")))
                    Dim dealer_pk2() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM", "EFFECTIVE_DATE", "SUPPLIER_ID"}
                    Dim dealer_col_value2(,) As String = {
                        {"DEPOT_ID", yRow.Item("DEPOT_ID")},
                        {"PRODUCT_ID", yRow.Item("PRODUCT_ID")},
                        {"BATCH_NUM", yRow.Item("BATCH_NUM")},
                        {"EFFECTIVE_DATE", CHECK_DATE.ToString("yyyy/MM/dd")},
                        {"SUPPLIER_ID", yRow.Item("SUPPLIER_ID")},
                        {"CON_DEPOT_QTY", New_Depot_Qty},
                        {"ALTER_USER", "系統"},
                        {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                    }
                    sSql = pf.GetUpdatequery(table, dealer_pk2, dealer_col_value2)
                    Console.WriteLine(sSql)
                    sqlCommand.CommandText = sSql
                    iRowAffected = sqlCommand.ExecuteNonQuery()
                    If iRowAffected = 0 Then
                        Return False
                    End If
                End If
                '更新經銷商倉庫
                dt = pf.GetDB("D_DEPOT_BASE,NOLOCK", {{"DEPOT_ID", yRow.Item("DEPOT_ID").ToString}})
                If dt.Rows.Count > 0 Then
                    If dt.Rows(0).Item("DEPOT_TYPE").ToString = "經銷商" AndAlso dt.Rows(0).Item("SUPPLIER_ID").ToString = yRow.Item("SUPPLIER_ID").ToString Then
                        '取產品的VALID_PERIOD
                        Dim pWhere(,) As String = {
                                    {"PRODUCT_ID", yRow.Item("PRODUCT_ID")},
                                    {"BATCH_NUM", yRow.Item("BATCH_NUM")},
                                    {"ORDERBY_ASC", "DEPOT_ID"}
                                }
                        Dim pdt As DataTable = pf.GetDB("D_DEPOT_STOCKS,NOLOCK", pWhere)

                        Dim tableName2 As String = "D_DEPOT_STOCKS_DAYS"
                        Dim pk2() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM", "EFFECTIVE_DATE"}
                        Dim col_value2(,) As String = {
                                    {"DEPOT_ID", yRow.Item("DEPOT_ID").ToString},
                                    {"PRODUCT_ID", yRow.Item("PRODUCT_ID").ToString},
                                    {"BATCH_NUM", yRow.Item("BATCH_NUM").ToString},
                                    {"EFFECTIVE_DATE", CHECK_DATE.ToString("yyyy/MM/dd")},
                                    {"GOOD_QTY", New_Depot_Qty},
                                    {"DEPOT_QTY", New_Depot_Qty},
                                    {"VALID_PERIOD", CType(pdt.Rows(0).Item("VALID_PERIOD"), DateTime).ToString("yyyy/MM/dd")},
                                    {"ALTER_USER", "系統"},
                                    {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff")}
                                }
                        'sSql = pf.GetUpdatequery(tableName2, pk2, col_value2)

                        '判斷更新方式(insert/update)
                        Dim zWhere(,) As String = {
                             {"DEPOT_ID", yRow.Item("DEPOT_ID").ToString},
                             {"PRODUCT_ID", yRow.Item("PRODUCT_ID").ToString},
                             {"BATCH_NUM", yRow.Item("BATCH_NUM").ToString},
                             {"EFFECTIVE_DATE", CHECK_DATE.ToString("yyyy/MM/dd")}
                        }
                        Dim zdt As DataTable = pf.GetDB("D_DEPOT_STOCKS_DAYS,NOLOCK", zWhere)
                        If zdt.Rows.Count > 0 Then
                            sSql = pf.GetUpdatequery(tableName2, pk2, col_value2)
                        Else
                            col_value2 = {
                                {"DEPOT_ID", yRow.Item("DEPOT_ID").ToString},
                                {"PRODUCT_ID", yRow.Item("PRODUCT_ID").ToString},
                                {"BATCH_NUM", yRow.Item("BATCH_NUM").ToString},
                                {"EFFECTIVE_DATE", CHECK_DATE.ToString("yyyy/MM/dd")},
                                {"GOOD_QTY", New_Depot_Qty},
                                {"DEPOT_QTY", New_Depot_Qty},
                                {"VALID_PERIOD", CType(pdt.Rows(0).Item("VALID_PERIOD"), DateTime).ToString("yyyy/MM/dd")},
                                {"CREATE_USER", "系統"},
                                {"CREATE_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff")}
                            }
                            sSql = pf.GetInsertquery(tableName2, col_value2)
                        End If
                        Console.WriteLine(sSql)
                        sqlCommand.CommandText = sSql
                        iRowAffected = sqlCommand.ExecuteNonQuery()
                        If iRowAffected = 0 Then
                            Return False
                        End If
                    End If
                End If

                '即時可動支庫存
                '計算異動倉庫現有庫存數
                '依照覆核日
                sSql = ""
                Dim New_Sale_Qty As Double = 0
                sSql = "SELECT [SUPPLIER_ID],[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM],SUM(ISNULL([SUPPLIER_DIFF_QTY],0)) as SUPPLIER_DIFF_QTY "
                sSql &= " FROM [D_DEPOT_STOCKS_KEYIN_DIFF] WITH (NOLOCK)  WHERE [CHANGE_DATE]='" & CHECK_DATE.ToString("yyyy/MM/dd") & "'"
                sSql &= " AND [SUPPLIER_ID]='" & yRow.Item("SUPPLIER_ID").ToString & "'"
                sSql &= " AND [DEPOT_ID]='" & yRow.Item("DEPOT_ID").ToString & "'"
                sSql &= " AND [PRODUCT_ID]='" & yRow.Item("PRODUCT_ID").ToString & "'"
                sSql &= " AND [BATCH_NUM]='" & yRow.Item("BATCH_NUM").ToString & "'"
                'sSql &= " AND ISNULL([SUPPLIER_DIFF_QTY],0)<>'0'"
                sSql &= " GROUP BY [SUPPLIER_ID],[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                Console.WriteLine(sSql)
                Dim qdt5 As New DataTable
                qdt5 = pf.GetDataTable(sSql)
                If qdt5.Rows.Count > 0 Then
                    New_Sale_Qty = Convert.ToDouble(pf.CheckNull(yRow.Item("CON_SALE_QTY"))) + Convert.ToDouble(pf.CheckNull(qdt5.Rows(0).Item("SUPPLIER_DIFF_QTY")))
                    Dim dealer_pk2() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM", "EFFECTIVE_DATE", "SUPPLIER_ID"}
                    Dim dealer_col_value2(,) As String = {
                        {"DEPOT_ID", yRow.Item("DEPOT_ID")},
                        {"PRODUCT_ID", yRow.Item("PRODUCT_ID")},
                        {"BATCH_NUM", yRow.Item("BATCH_NUM")},
                        {"EFFECTIVE_DATE", CHECK_DATE.ToString("yyyy/MM/dd")},
                        {"SUPPLIER_ID", yRow.Item("SUPPLIER_ID")},
                        {"CON_SALE_QTY", New_Sale_Qty},
                        {"ALTER_USER", "系統"},
                        {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                    }
                    sSql = pf.GetUpdatequery(table, dealer_pk2, dealer_col_value2)
                    Console.WriteLine(sSql)
                    sqlCommand.CommandText = sSql
                    iRowAffected = sqlCommand.ExecuteNonQuery()
                    If iRowAffected = 0 Then
                        Return False
                    End If
                Else
                    New_Sale_Qty = Convert.ToDouble(pf.CheckNull(yRow.Item("CON_SALE_QTY")))
                    Dim dealer_pk2() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM", "EFFECTIVE_DATE", "SUPPLIER_ID"}
                    Dim dealer_col_value2(,) As String = {
                        {"DEPOT_ID", yRow.Item("DEPOT_ID")},
                        {"PRODUCT_ID", yRow.Item("PRODUCT_ID")},
                        {"BATCH_NUM", yRow.Item("BATCH_NUM")},
                        {"EFFECTIVE_DATE", CHECK_DATE.ToString("yyyy/MM/dd")},
                        {"SUPPLIER_ID", yRow.Item("SUPPLIER_ID")},
                        {"CON_SALE_QTY", New_Sale_Qty},
                        {"ALTER_USER", "系統"},
                        {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                    }
                    sSql = pf.GetUpdatequery(table, dealer_pk2, dealer_col_value2)
                    Console.WriteLine(sSql)
                    sqlCommand.CommandText = sSql
                    iRowAffected = sqlCommand.ExecuteNonQuery()
                    If iRowAffected = 0 Then
                        Return False
                    End If
                End If
                '更新經銷商倉庫
                dt = pf.GetDB("D_DEPOT_BASE,NOLOCK", {{"DEPOT_ID", yRow.Item("DEPOT_ID").ToString}})
                If dt.Rows.Count > 0 Then
                    If dt.Rows(0).Item("DEPOT_TYPE").ToString = "經銷商" AndAlso dt.Rows(0).Item("SUPPLIER_ID").ToString = yRow.Item("SUPPLIER_ID").ToString Then
                        '取產品的VALID_PERIOD
                        Dim pWhere(,) As String = {
                                    {"PRODUCT_ID", yRow.Item("PRODUCT_ID")},
                                    {"BATCH_NUM", yRow.Item("BATCH_NUM")},
                                    {"ORDERBY_ASC", "DEPOT_ID"}
                                }
                        Dim pdt As DataTable = pf.GetDB("D_DEPOT_STOCKS,NOLOCK", pWhere)

                        Dim tableName2 As String = "D_DEPOT_STOCKS_DAYS"
                        Dim pk2() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM", "EFFECTIVE_DATE"}
                        Dim col_value2(,) As String = {
                                    {"DEPOT_ID", yRow.Item("DEPOT_ID").ToString},
                                    {"PRODUCT_ID", yRow.Item("PRODUCT_ID").ToString},
                                    {"BATCH_NUM", yRow.Item("BATCH_NUM").ToString},
                                    {"EFFECTIVE_DATE", CHECK_DATE.ToString("yyyy/MM/dd")},
                                    {"SALE_QTY", New_Sale_Qty},
                                    {"VALID_PERIOD", CType(pdt.Rows(0).Item("VALID_PERIOD"), DateTime).ToString("yyyy/MM/dd")},
                                    {"ALTER_USER", "系統"},
                                    {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff")}
                                }
                        'sSql = pf.GetUpdatequery(tableName2, pk2, col_value2)

                        '判斷更新方式(insert/update)
                        Dim zWhere(,) As String = {
                             {"DEPOT_ID", yRow.Item("DEPOT_ID").ToString},
                             {"PRODUCT_ID", yRow.Item("PRODUCT_ID").ToString},
                             {"BATCH_NUM", yRow.Item("BATCH_NUM").ToString},
                             {"EFFECTIVE_DATE", CHECK_DATE.ToString("yyyy/MM/dd")}
                        }
                        Dim zdt As DataTable = pf.GetDB("D_DEPOT_STOCKS_DAYS,NOLOCK", zWhere)
                        If zdt.Rows.Count > 0 Then
                            sSql = pf.GetUpdatequery(tableName2, pk2, col_value2)
                        Else
                            col_value2 = {
                                {"DEPOT_ID", yRow.Item("DEPOT_ID").ToString},
                                {"PRODUCT_ID", yRow.Item("PRODUCT_ID").ToString},
                                {"BATCH_NUM", yRow.Item("BATCH_NUM").ToString},
                                {"EFFECTIVE_DATE", CHECK_DATE.ToString("yyyy/MM/dd")},
                                {"SALE_QTY", New_Sale_Qty},
                                {"VALID_PERIOD", CType(pdt.Rows(0).Item("VALID_PERIOD"), DateTime).ToString("yyyy/MM/dd")},
                                {"CREATE_USER", "系統"},
                                {"CREATE_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff")}
                            }
                            sSql = pf.GetInsertquery(tableName2, col_value2)
                        End If
                        Console.WriteLine(sSql)
                        sqlCommand.CommandText = sSql
                        iRowAffected = sqlCommand.ExecuteNonQuery()
                        If iRowAffected = 0 Then
                            Return False
                        End If
                    End If
                End If
            Next
        Catch ex As Exception
            pf.MyError(ex.ToString)
            Return False
        End Try
        Return True
    End Function
End Class
