﻿Imports System.Data.SqlClient
Imports System.Text.RegularExpressions
Imports Microsoft.Office.Interop

Public Class DBFuntion

    Private strConnection As String

    Public Sub New()
        strConnection = My.Settings.HD_ERP_DBConnection
    End Sub

    '經由SQL指令將查詢資料存成DataTable
    Public Function GetDataTable(SQL As String) As DataTable
        '建立連接物件
        Dim cn As New SqlConnection(strConnection)
        '定義資料表物件
        Dim ExDataTable As DataTable
        Try
            '開啟連接物件
            cn.Open()
            '建立新的SqlCommand
            Dim cmd As New SqlCommand(SQL, cn)
            '調整等待時間
            cmd.CommandTimeout = 300
            '接收命令物件讀取到的資料
            Dim dr As SqlDataReader = cmd.ExecuteReader()
            '建立新的DataTable
            ExDataTable = New DataTable()
            '載入SqlDataReader的資料
            ExDataTable.Load(dr)
            '關閉SqlDataReader
            dr.Close()
            '釋放SqlCommand所占用的資源
            cmd.Dispose()
        Finally
            '關閉連接物件
            cn.Close()
        End Try
        '傳回DataTable
        Return ExDataTable
    End Function

    '執行SQL指令，並傳回影響筆數
    Public Function ExecSQL(SQL As String) As Integer
        '定義變數，用來接收更新資料後的影響筆數
        Dim iRowAffected As Integer = 0
        '建立連接物件
        Dim cn As New SqlConnection(strConnection)
        Try
            '開啟連接物件
            cn.Open()
            '建立新的SqlCommand
            Dim cmd As New SqlCommand(SQL, cn)
            '調整等待時間
            cmd.CommandTimeout = 300
            '執行SQL命令
            iRowAffected = cmd.ExecuteNonQuery()
            '釋放SqlCommand所占用的資源
            cmd.Dispose()
        Finally
            '關閉連接物件
            cn.Close()
        End Try
        '傳回影響筆數
        Return iRowAffected
    End Function

    '從資料表檢查單一欄位的值是否正確(欄位型態只限字串)
    Public Function CheckValue(TableName As String, KeyField As String,
                               QryField As String, KeyValue As String,
                               QryValue As String) As Boolean
        Dim IsCheck As Boolean = True
        Dim dt As DataTable
        dt = GetDataTable("SELECT " + QryField + " " +
                          "FROM " + TableName + " " +
                          "WHERE " + KeyField + " = '" + KeyValue + "' " +
                           "AND " + QryField + " = '" + QryValue + "' ")
        If dt.Rows.Count = 0 Then
            '找不到與指定欄位相符的值
            IsCheck = False
        End If
        Return IsCheck
    End Function

    '從資料表取得單一欄位的值
    Public Function GetValue(TableName As String, KeyField As String,
                    KeyValue As String, GetField As String) As Object
        Dim value As Object = Nothing
        Dim dt As DataTable
        dt = GetDataTable("SELECT " + GetField + " " +
                          "FROM " + TableName + " " +
                          "WHERE " + KeyField + " = '" + KeyValue + "' ")
        If dt.Rows.Count > 0 Then
            If Not IsDBNull(dt.Rows(0)(GetField)) Then
                value = dt.Rows(0)(GetField)
            End If
        End If
        Return value
    End Function

    '自訂錯誤訊息
    Public Sub MyError(Message As String)
        MessageBox.Show(Message, "錯誤", MessageBoxButtons.OK,
                        MessageBoxIcon.Error)
    End Sub

    '自訂確認訊息
    Public Function MyConfirmation(Message As String) As Boolean
        Dim IsConfirm As Boolean = True
        If MessageBox.Show(Message, "確認", MessageBoxButtons.YesNo,
           MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) =
                                    DialogResult.No Then
            IsConfirm = False
        End If
        Return IsConfirm
    End Function

    '自訂提示訊息
    Public Sub MyInformation(Message As String)
        MessageBox.Show(Message, "資訊", MessageBoxButtons.OK,
                        MessageBoxIcon.Information)
    End Sub

    '自訂警告訊息
    Public Sub MyWarning(Message As String)
        MessageBox.Show(Message, "警告", MessageBoxButtons.OK,
                        MessageBoxIcon.Warning)
    End Sub

    '回傳資料代號 用"_"區隔
    Public Function GetUID(ByVal MyString As String) As String
        Dim Result As String = ""
        Try
            If MyString = "" Then
                Return ""
            End If
            Dim charSeparators() As Char = {"_"c}
            Dim strResult() = MyString.Trim().Split(charSeparators, StringSplitOptions.RemoveEmptyEntries)
            If strResult.Length > 0 Then
                Result = strResult(0)
            Else
                Return MyString
            End If
            'Result = strResult(0)
        Catch ex As Exception
            MyError(ex.ToString)
        End Try
        Return Result
    End Function

    '檢查資料是否為DBNULL 是的話回傳空字串""-限定傳入字串
    Public Function CheckStringDBNull(chkValue) As String
        If DBNull.Value.Equals(chkValue) Then
            Return ""
        ElseIf String.IsNullOrEmpty(chkValue) Then
            Return ""
        ElseIf chkValue Is Nothing Then
            Return ""
        Else
            Return chkValue.ToString
        End If
    End Function

    '檢查資料是否為DBNULL 是的話回傳空字串""-限定傳入字串
    Public Function CheckIfDBNull(chkValue) As String
        If DBNull.Value.Equals(chkValue) Then
            Return ""
        ElseIf String.IsNullOrEmpty(chkValue) Then
            Return ""
        ElseIf chkValue Is Nothing Then
            Return ""
        Else
            Return chkValue.ToString
        End If
    End Function

    '格式轉換浮點數，到小數點第3位
    Public Sub MoneyFormat(ByVal formatting As String)
        If formatting <> "" Then
            Dim specifier As String = "N3"
            formatting = CType(formatting, Decimal).ToString(specifier)
        End If
    End Sub

    ''限定只能輸入為數字，小數點或backspace
    Public Sub MoneyFormat_Cells_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs)
        Try
            If Char.IsDigit(e.KeyChar) Or e.KeyChar = "." Or e.KeyChar = Chr(8) Then 'Backspace:
                '輸入的小數點為唯一
                If e.KeyChar = "." And InStr(CType(sender, System.Windows.Forms.TextBox).Text, ".") > 0 Then
                    e.Handled = True
                Else
                    '小數最多3位
                    If e.KeyChar <> Chr(8) And InStr(CType(sender, System.Windows.Forms.TextBox).Text, ".") > 0 Then
                        Dim sAry() As String = CType(sender, System.Windows.Forms.TextBox).Text.Split(".")
                        If sAry(1).Length >= 3 Then
                            e.Handled = True
                        Else
                            e.Handled = False
                        End If
                    Else
                        e.Handled = False
                    End If
                End If

                '輸入的負號是否在第一位
            ElseIf e.KeyChar = "-" And CType(sender, System.Windows.Forms.TextBox).Text = "" Then
                e.Handled = False
            Else
                e.Handled = True
            End If
        Catch ex As Exception
            MyError(ex.ToString)
        End Try
    End Sub


    '取得CREATE_USER及ALTER_USER之NAME
    Public Function GetName(ByVal id As String) As String
        Dim value As String = ""
        Select Case id
            Case "superadmin"
                value = "SuperAdmin"
            Case "hmanager"
                value = "Hmanager"
            Case "轉檔匯入"
                value = "轉檔匯入"
            Case Else
                value = GetValue("M_USER_INFO", "EMP_ID", id, "EMP_NAME")
        End Select
        Return value
    End Function

    '得到新增query
    Public Function GetInsertquery(ByVal TableName As String, ByVal col_val(,) As String) As String
        Dim query As String = ""
        Dim where As String = ""
        Dim count As Integer = 0

        query = "INSERT INTO [" & TableName & "] ("
        For index = 0 To col_val.GetUpperBound(0)
            If col_val(index, 0).Trim <> "" Then
                If count = 0 Then
                    query &= "[" & col_val(index, 0).Trim & "]"
                    count += 1
                Else
                    query &= ", [" & col_val(index, 0).Trim & "]"
                End If
            End If
        Next
        query &= ") VALUES ("
        count = 0
        For index = 0 To col_val.GetUpperBound(0)
            If col_val(index, 0).Trim <> "" Then
                If count = 0 Then
                    If col_val(index, 1) <> "Null" Then
                        If col_val(index, 1) <> "" Then
                            query &= "'" & col_val(index, 1).Replace("'", "''").Trim & "'"
                        Else
                            query &= "''"
                        End If
                    Else
                        query &= "Null"
                    End If
                    count += 1
                Else
                    If col_val(index, 1).Trim <> "Null" Then
                        If col_val(index, 1) <> "" Then
                            query &= ", '" & col_val(index, 1).Replace("'", "''").Trim & "'"
                        Else
                            query &= ",''"
                        End If
                    Else
                        query &= ", Null"
                    End If
                End If
            End If
        Next
        query &= ")"
        Console.WriteLine(query)
        Return query
    End Function

    '得到更新query
    Public Function GetUpdatequery(ByVal TableName As String, ByVal PK() As String, ByVal col_val(,) As String) As String

        Dim query As String = ""
        Dim where As String = ""
        Dim count As Integer = 0
        Dim Is_PK As Boolean = False

        For Each key As String In PK
            For index = 0 To col_val.GetUpperBound(0)
                If col_val(index, 0) = key Then
                    If count = 0 Then
                        where = " WHERE [" & key & "]='" & col_val(index, 1).Replace("'", "''").Trim & "'"
                        count += 1
                    Else
                        where &= " and [" & key & "]='" & col_val(index, 1).Replace("'", "''").Trim & "'"
                    End If
                End If
            Next
        Next

        query = "UPDATE [" & TableName & "]"
        count = 0
        For index = 0 To col_val.GetUpperBound(0)
            Is_PK = False
            If col_val(index, 1) <> "" Then
                col_val(index, 1) = col_val(index, 1).Replace("'", "''")
            End If
            For Each key As String In PK
                If col_val(index, 0).Trim = key Then
                    Is_PK = True
                End If
            Next

            If Is_PK = False Then
                If col_val(index, 0).Trim <> "" Then
                    If count = 0 Then
                        If col_val(index, 1).Trim <> "Null" Then
                            If col_val(index, 1) <> "" Then
                                query &= " SET " & "[" & col_val(index, 0).Trim & "]='" & col_val(index, 1).Trim & "'"
                            Else
                                query &= " SET " & "[" & col_val(index, 0).Trim & "]=''"
                            End If
                        Else
                            query &= " SET " & "[" & col_val(index, 0).Trim & "]=Null"
                        End If
                        count += 1
                    Else
                        If col_val(index, 1).Trim <> "Null" Then
                            If col_val(index, 1) <> "" Then
                                query &= ", [" & col_val(index, 0).Trim & "]='" & col_val(index, 1).Trim & "'"
                            Else
                                query &= ", [" & col_val(index, 0).Trim & "]=''"
                            End If
                        Else
                            query &= ", [" & col_val(index, 0).Trim & "]=Null"
                        End If
                    End If
                End If
            End If
            'Console.WriteLine(query)
        Next
        query &= where
        Return query
    End Function

    '得到刪除query
    Public Function GetDeletequery(ByVal TableName As String, ByVal Where(,) As String) As String
        Dim query As String = ""
        Dim count As Integer = 0
        query = "DELETE  FROM [" & TableName & "]"
        For index = 0 To Where.GetUpperBound(0)
            If count = 0 Then
                If Where(index, 0) = "CUSTOM_SQL" Then
                    query &= " WHERE " & Where(index, 1)
                Else
                    query &= " WHERE [" & Where(index, 0) & "]='" & Where(index, 1).Replace("'", "''").Trim & "'"
                End If
                count += 1
            Else
                If Where(index, 0) = "CUSTOM_SQL" Then
                    query &= " AND " & Where(index, 1)
                Else
                    query &= " AND [" & Where(index, 0) & "]='" & Where(index, 1).Replace("'", "''").Trim & "'"
                End If

            End If
        Next
        Return query
    End Function

    '查詢DB並JOIN
    'String      Table名稱 
    'String(,)  外部鍵(JOIN外部Table名稱  , JOIN ON 外部ColumnName, 來源Table名稱, 來源ColumnName)
    'String(,)  Select的欄位(Select TableName, Select ColumnName)
    'String(,)  Where的欄位(Where的TableName, Column, 等式符號 , Where的Value)
    '當Where的[等式符號]為[ORDERBY]時,可以成為排序條件,原[Value]欄位為設置降序(DESC)/升序(ASC)
    Public Function GetDB_Join(ByVal TableName As String, ByVal JOIN(,) As String, ByVal Select_Column(,) As String, ByVal Where(,) As String)
        Dim DB_Table As DataTable
        Dim str As String = ""
        Dim order As String = ""
        Dim group As String = ""
        Dim temp_str As String = ""

        Dim order_count As Integer = 0
        Dim group_count As Integer = 0
        Dim count As Integer = 0
        Dim If_Select_MainTable As Boolean = False

        '檢查Select_Column內是否有主要Table的欄位，是則只選擇特定欄位，否則選擇全部
        For index = 0 To Select_Column.GetUpperBound(0)
            If String.IsNullOrEmpty(Select_Column(index, 0)) Then
                Continue For
            End If
            If Select_Column(index, 0) = TableName.Split(",")(0) Then
                If_Select_MainTable = True
            End If
        Next
        If Not If_Select_MainTable Then
            str = "SELECT [" & TableName.Split(",")(0) & "].*"
            count += 1
        End If

        For index = 0 To Select_Column.GetUpperBound(0)
            If String.IsNullOrEmpty(Select_Column(index, 0)) Or String.IsNullOrEmpty(Select_Column(index, 1)) Then
                Continue For
            End If
            If count = 0 Then
                str &= "SELECT "
                count += 1
            Else
                str &= ", "
            End If
            If InStr(1, Select_Column(index, 1), "_AS_") > 0 Then
                If InStr(1, Split(Select_Column(index, 1), "_AS_")(0), "+") Then
                    temp_str = Split(Select_Column(index, 1), "_AS_")(0)
                    str &= "[" & Select_Column(index, 0) & "].[" & Split(temp_str, "+")(0) & "]+'_'+[" & Select_Column(index, 0) & "].[" & Split(temp_str, "+")(1) & "]"
                    str &= " AS [" & Split(Select_Column(index, 1), "_AS_")(1) & "]"
                Else
                    str &= "[" & Select_Column(index, 0) & "].[" & Split(Select_Column(index, 1), "_AS_")(0) & "] AS [" & Split(Select_Column(index, 1), "_AS_")(1) & "]"
                End If
            ElseIf Select_Column(index, 0) = "CUSTOM_SQL" Then
                str &= Select_Column(index, 1)
            Else
                str &= "[" & Select_Column(index, 0) & "].[" & Select_Column(index, 1) & "]"
            End If
        Next


        If InStr(1, TableName, ",") > 0 Then
            Select Case TableName.Split(",")(1)
                Case "NOLOCK"
                    str &= " FROM [" & TableName.Split(",")(0) & "] WITH (NOLOCK)"
            End Select
            Select Case TableName.Split(",")(0)
                Case "CUSTOM_SQL"
                    str &= " FROM " & TableName.Substring(11)
            End Select
        Else
            str &= " FROM [" & TableName.Split(",")(0) & "]"
        End If

        Dim join_list As New ArrayList
        For index = 0 To JOIN.GetUpperBound(0)
            If String.IsNullOrEmpty(JOIN(index, 0)) Then
                Continue For
            End If
            If InStr(1, JOIN(index, 0), "_AS_") > 0 Then
                str &= " LEFT JOIN [" & Split(JOIN(index, 0), "_AS_")(0) & "] AS [" & Split(JOIN(index, 0), "_AS_")(1) & "] "
                str &= "ON [" & JOIN(index, 2) & "].[" & JOIN(index, 3) & "]=[" & Split(JOIN(index, 0), "_AS_")(1) & "].[" & JOIN(index, 1) & "]"
            Else
                If Not join_list.Contains(JOIN(index, 0)) AndAlso JOIN(index, 0) <> "CUSTOM_SQL" Then
                    join_list.Add(JOIN(index, 0))
                    str &= " LEFT JOIN [" & JOIN(index, 0) & "]"
                    If InStr(1, TableName, ",") > 0 Then
                        Select Case TableName.Split(",")(1)
                            Case "NOLOCK"
                                str &= " WITH (NOLOCK) "
                        End Select
                    End If
                    str &= " ON [" & JOIN(index, 2) & "].[" & JOIN(index, 3) & "]=[" & JOIN(index, 0) & "].[" & JOIN(index, 1) & "]"
                ElseIf JOIN(index, 0) = "CUSTOM_SQL" Then
                    If JOIN(index, 1) = "CUSTOM_SQL" Then
                        Select Case JOIN(index, 2)
                            Case "INNER"
                                str &= " INNER JOIN " & JOIN(index, 3)
                            Case "LEFT"
                                str &= " LEFT JOIN " & JOIN(index, 3)
                            Case "RIGHT"
                                str &= " RIGHT JOIN " & JOIN(index, 3)
                        End Select
                    Else
                        str &= " LEFT JOIN " & JOIN(index, 3)
                    End If
                Else
                    str &= " And [" & JOIN(index, 2) & "].[" & JOIN(index, 3) & "]=[" & JOIN(index, 0) & "].[" & JOIN(index, 1) & "]"
                End If
            End If
        Next

        count = 0
        For index = 0 To Where.GetUpperBound(0)
            If Where(index, 3).ToString.Trim = "" Or Where(index, 3).ToString.Trim = "全部" Then
                Select Case Where(index, 2).ToString.Trim
                    Case "!=", "=="
                    Case Else
                        Continue For
                End Select
            End If
            If Where(index, 2) <> "ORDERBY" And Where(index, 2) <> "GROUPBY" Then
                If count = 0 Then
                    str &= " WHERE "
                    count += 1
                Else
                    str &= " And "
                End If
            End If
            Select Case Where(index, 2)
                Case "LIKE"
                    str &= "[" & Where(index, 0) & "].[" & Where(index, 1) & "] " & Where(index, 2) & " '%" & Where(index, 3).Replace("'", "''") & "%' "
                Case "=="
                    str &= "[" & Where(index, 0) & "].[" & Where(index, 1) & "]='" & Where(index, 3).Replace("'", "''") & "'"
                Case "ORDERBY"
                    If order_count = 0 Then
                        If Where(index, 0) = "CUSTOM_SQL" Then
                            order = " ORDER BY " & Where(index, 1) & Where(index, 3)
                        Else
                            order = " ORDER BY [" & Where(index, 0) & "].[" & Where(index, 1) & "]" & Where(index, 3)
                        End If
                        order_count += 1
                    Else
                        If Where(index, 0) = "CUSTOM_SQL" Then
                            order &= ", " & Where(index, 1) & Where(index, 3)
                        Else
                            order &= ", [" & Where(index, 0) & "].[" & Where(index, 1) & "] " & Where(index, 3)
                        End If
                    End If
                Case "GROUPBY"
                    If group_count = 0 Then
                        If Where(index, 0) = "CUSTOM_SQL" Then
                            group = " GROUP BY " & Where(index, 1)
                            group_count += 1
                        Else
                            group = " GROUP BY [" & Where(index, 0) & "].[" & Where(index, 1) & "]"
                            group_count += 1
                        End If
                    Else
                        If Where(index, 0) = "CUSTOM_SQL" Then
                            group &= ", " & Where(index, 1)
                        ElseIf Where(index, 0) = "HAVING" AndAlso Where(index, 1) <> "" Then
                            group &= " HAVING " & Where(index, 1)
                        ElseIf Where(index, 0) = "HAVING" Then
                            group &= ""
                        Else
                            group &= ", [" & Where(index, 0) & "].[" & Where(index, 1) & "]"
                        End If
                    End If
                Case "CUSTOM_SQL"
                    str &= Where(index, 3)
                Case Else
                    str &= "[" & Where(index, 0) & "].[" & Where(index, 1) & "]" & Where(index, 2) & "'" & Where(index, 3).Replace("'", "''") & "'"
            End Select
        Next
        str &= group
        str &= order
        Console.WriteLine(str)
        'Return str
        DB_Table = GetDataTable(str)
        'If DB_Table.Rows.Count > 0 Then
        '    For row_index As Integer = 0 To DB_Table.Rows.Count - 1
        '        For col_index As Integer = 0 To DB_Table.Rows(row_index).ItemArray.Count - 1
        '            DB_Table.Columns(col_index).ReadOnly = False
        '            If IsDBNull(DB_Table.Rows(row_index).Item(col_index)) Then
        '                Continue For
        '            End If
        '            DB_Table.Rows(row_index).Item(col_index) = DB_Table.Rows(row_index).Item(col_index).ToString.Trim
        '        Next
        '    Next
        'End If
        Return DB_Table
    End Function

    'Get_Joined_SQL_Qyery
    Public Function Get_DB_Join_Query(ByVal TableName As String, ByVal JOIN(,) As String, ByVal Select_Column(,) As String, ByVal Where(,) As String)
        Dim str As String = ""
        Dim order As String = ""
        Dim group As String = ""
        Dim temp_str As String = ""

        Dim order_count As Integer = 0
        Dim group_count As Integer = 0
        Dim count As Integer = 0
        Dim If_Select_MainTable As Boolean = False

        '檢查Select_Column內是否有主要Table的欄位，是則只選擇特定欄位，否則選擇全部
        For index = 0 To Select_Column.GetUpperBound(0)
            If String.IsNullOrEmpty(Select_Column(index, 0)) Then
                Continue For
            End If
            If Select_Column(index, 0) = TableName Then
                If_Select_MainTable = True
            End If
        Next
        If Not If_Select_MainTable Then
            str = "SELECT [" & TableName & "].*"
            count += 1
        End If

        For index = 0 To Select_Column.GetUpperBound(0)
            If String.IsNullOrEmpty(Select_Column(index, 0)) Or String.IsNullOrEmpty(Select_Column(index, 1)) Then
                Continue For
            End If
            If count = 0 Then
                str &= "SELECT "
                count += 1
            Else
                str &= ", "
            End If
            If InStr(1, Select_Column(index, 1), "_AS_") > 0 Then
                If InStr(1, Split(Select_Column(index, 1), "_AS_")(0), "+") Then
                    temp_str = Split(Select_Column(index, 1), "_AS_")(0)
                    str &= "[" & Select_Column(index, 0) & "].[" & Split(temp_str, "+")(0) & "]+'_'+[" & Select_Column(index, 0) & "].[" & Split(temp_str, "+")(1) & "]"
                    str &= " AS [" & Split(Select_Column(index, 1), "_AS_")(1) & "]"
                Else
                    str &= "[" & Select_Column(index, 0) & "].[" & Split(Select_Column(index, 1), "_AS_")(0) & "] AS [" & Split(Select_Column(index, 1), "_AS_")(1) & "]"
                End If
            ElseIf Select_Column(index, 0) = "CUSTOM_SQL" Then
                str &= Select_Column(index, 1)
            Else
                str &= "[" & Select_Column(index, 0) & "].[" & Select_Column(index, 1) & "]"
            End If
        Next

        str &= " FROM [" & TableName & "]"

        Dim join_list As New ArrayList
        For index = 0 To JOIN.GetUpperBound(0)
            If String.IsNullOrEmpty(JOIN(index, 0)) Then
                Continue For
            End If
            If InStr(1, JOIN(index, 0), "_AS_") > 0 Then
                str &= " LEFT JOIN [" & Split(JOIN(index, 0), "_AS_")(0) & "] AS [" & Split(JOIN(index, 0), "_AS_")(1) & "] "
                str &= " ON [" & JOIN(index, 2) & "].[" & JOIN(index, 3) & "]=[" & Split(JOIN(index, 0), "_AS_")(1) & "].[" & JOIN(index, 1) & "]"
            Else
                If Not join_list.Contains(JOIN(index, 0)) AndAlso JOIN(index, 0) <> "CUSTOM_SQL" Then
                    join_list.Add(JOIN(index, 0))
                    str &= " LEFT JOIN [" & JOIN(index, 0) & "]"
                    str &= " ON [" & JOIN(index, 2) & "].[" & JOIN(index, 3) & "]=[" & JOIN(index, 0) & "].[" & JOIN(index, 1) & "]"
                ElseIf JOIN(index, 0) = "CUSTOM_SQL" Then

                    If JOIN(index, 1) = "CUSTOM_SQL" Then
                        Select Case JOIN(index, 2)
                            Case "LEFT"
                                str &= " LEFT JOIN " & JOIN(index, 3)
                            Case "INNER"
                                str &= " INNER JOIN " & JOIN(index, 3)

                            Case "RIGHT"
                                str &= " RIGHT JOIN " & JOIN(index, 3)
                        End Select
                    Else
                        str &= " LEFT JOIN " & JOIN(index, 3)
                    End If
                Else
                    str &= " And [" & JOIN(index, 2) & "].[" & JOIN(index, 3) & "]=[" & JOIN(index, 0) & "].[" & JOIN(index, 1) & "]"
                End If
            End If
        Next

        count = 0
        For index = 0 To Where.GetUpperBound(0)
            If Where(index, 3).ToString.Trim = "" Or Where(index, 3).ToString.Trim = "全部" Then
                Select Case Where(index, 2).ToString.Trim
                    Case "!=", "=="
                    Case Else
                        Continue For
                End Select
            End If
            If Where(index, 2) <> "ORDERBY" And Where(index, 2) <> "GROUPBY" Then
                If count = 0 Then
                    str &= " WHERE "
                    count += 1
                Else
                    str &= " And "
                End If
            End If
            Select Case Where(index, 2)
                Case "Like"
                    str &= "[" & Where(index, 0) & "].[" & Where(index, 1) & "]" & Where(index, 2) & "'%" & Where(index, 3).Replace("'", "''") & "%'"
                Case "=="
                    str &= "[" & Where(index, 0) & "].[" & Where(index, 1) & "]='" & Where(index, 3).Replace("'", "''") & "'"
                Case "ORDERBY"
                    If order_count = 0 Then
                        If Where(index, 0) = "CUSTOM_SQL" Then
                            order = " ORDER BY " & Where(index, 1) & Where(index, 3)
                        Else
                            order = " ORDER BY [" & Where(index, 0) & "].[" & Where(index, 1) & "]" & Where(index, 3)
                        End If
                        order_count += 1
                    Else
                        If Where(index, 0) = "CUSTOM_SQL" Then
                            order &= ", " & Where(index, 1) & Where(index, 3)
                        Else
                            order &= ", [" & Where(index, 0) & "].[" & Where(index, 1) & "] " & Where(index, 3)
                        End If
                    End If
                Case "GROUPBY"
                    If group_count = 0 Then
                        If Where(index, 0) = "CUSTOM_SQL" Then
                            group = " GROUP BY " & Where(index, 1)
                            group_count += 1
                        Else
                            group = " GROUP BY [" & Where(index, 0) & "].[" & Where(index, 1) & "]"
                            group_count += 1
                        End If
                    Else
                        If Where(index, 0) = "CUSTOM_SQL" Then
                            group &= ", " & Where(index, 1)
                        ElseIf Where(index, 0) = "HAVING" AndAlso Where(index, 1) <> "" Then
                            group &= "HAVING " & Where(index, 1)
                        ElseIf Where(index, 0) = "HAVING" Then
                            group &= ""
                        Else
                            group &= ", [" & Where(index, 0) & "].[" & Where(index, 1) & "]"
                        End If
                    End If
                Case "CUSTOM_SQL"
                    str &= Where(index, 3)
                Case Else
                    str &= "[" & Where(index, 0) & "].[" & Where(index, 1) & "]" & Where(index, 2) & "'" & Where(index, 3).Replace("'", "''") & "'"
            End Select
        Next
        str &= group
        str &= order
        'Return str
        Console.WriteLine(str)
        Return str
    End Function

    'Get_INNER_Joined_SQL_Qyery
    Public Function Get_DB_INNER_Join_Query(ByVal TableName As String, ByVal JOIN(,) As String, ByVal Select_Column(,) As String, ByVal Where(,) As String)
        Dim str As String = ""
        Dim order As String = ""
        Dim group As String = ""
        Dim temp_str As String = ""

        Dim order_count As Integer = 0
        Dim group_count As Integer = 0
        Dim count As Integer = 0
        Dim If_Select_MainTable As Boolean = False

        '檢查Select_Column內是否有主要Table的欄位，是則只選擇特定欄位，否則選擇全部
        For index = 0 To Select_Column.GetUpperBound(0)
            If String.IsNullOrEmpty(Select_Column(index, 0)) Then
                Continue For
            End If
            If Select_Column(index, 0) = TableName Then
                If_Select_MainTable = True
            End If
        Next
        If Not If_Select_MainTable Then
            str = "SELECT [" & TableName & "].*"
            count += 1
        End If

        For index = 0 To Select_Column.GetUpperBound(0)
            If String.IsNullOrEmpty(Select_Column(index, 0)) Or String.IsNullOrEmpty(Select_Column(index, 1)) Then
                Continue For
            End If
            If count = 0 Then
                str &= "SELECT "
                count += 1
            Else
                str &= ", "
            End If
            If InStr(1, Select_Column(index, 1), "_AS_") > 0 Then
                If InStr(1, Split(Select_Column(index, 1), "_AS_")(0), "+") Then
                    temp_str = Split(Select_Column(index, 1), "_AS_")(0)
                    str &= "[" & Select_Column(index, 0) & "].[" & Split(temp_str, "+")(0) & "]+'_'+[" & Select_Column(index, 0) & "].[" & Split(temp_str, "+")(1) & "]"
                    str &= " AS [" & Split(Select_Column(index, 1), "_AS_")(1) & "]"
                Else
                    str &= "[" & Select_Column(index, 0) & "].[" & Split(Select_Column(index, 1), "_AS_")(0) & "] AS [" & Split(Select_Column(index, 1), "_AS_")(1) & "]"
                End If
            ElseIf Select_Column(index, 0) = "CUSTOM_SQL" Then
                str &= Select_Column(index, 1)
            Else
                str &= "[" & Select_Column(index, 0) & "].[" & Select_Column(index, 1) & "]"
            End If
        Next

        str &= " FROM [" & TableName & "]"

        Dim join_list As New ArrayList
        For index = 0 To JOIN.GetUpperBound(0)
            If String.IsNullOrEmpty(JOIN(index, 0)) Then
                Continue For
            End If
            If InStr(1, JOIN(index, 0), "_AS_") > 0 Then
                str &= " INNER JOIN [" & Split(JOIN(index, 0), "_AS_")(0) & "] AS [" & Split(JOIN(index, 0), "_AS_")(1) & "] "
                str &= "ON [" & JOIN(index, 2) & "].[" & JOIN(index, 3) & "]=[" & Split(JOIN(index, 0), "_AS_")(1) & "].[" & JOIN(index, 1) & "]"
            Else
                If Not join_list.Contains(JOIN(index, 0)) AndAlso JOIN(index, 0) <> "CUSTOM_SQL" Then
                    join_list.Add(JOIN(index, 0))
                    str &= " INNER JOIN [" & JOIN(index, 0) & "]"
                    str &= "ON [" & JOIN(index, 2) & "].[" & JOIN(index, 3) & "]=[" & JOIN(index, 0) & "].[" & JOIN(index, 1) & "]"
                ElseIf JOIN(index, 0) = "CUSTOM_SQL" Then
                    str &= " INNER JOIN " & JOIN(index, 3)
                Else
                    str &= " And [" & JOIN(index, 2) & "].[" & JOIN(index, 3) & "]=[" & JOIN(index, 0) & "].[" & JOIN(index, 1) & "]"
                End If
            End If
        Next

        count = 0
        For index = 0 To Where.GetUpperBound(0)
            If Where(index, 3).ToString.Trim = "" Or Where(index, 3).ToString.Trim = "全部" Then
                Select Case Where(index, 2).ToString.Trim
                    Case "!=", "=="
                    Case Else
                        Continue For
                End Select
            End If
            If Where(index, 2) <> "ORDERBY" And Where(index, 2) <> "GROUPBY" Then
                If count = 0 Then
                    str &= " WHERE "
                    count += 1
                Else
                    str &= " And "
                End If
            End If
            Select Case Where(index, 2)
                Case "Like"
                    str &= "[" & Where(index, 0) & "].[" & Where(index, 1) & "]" & Where(index, 2) & "'%" & Where(index, 3).Replace("'", "''") & "%'"
                Case "=="
                    str &= "[" & Where(index, 0) & "].[" & Where(index, 1) & "]='" & Where(index, 3).Replace("'", "''") & "'"
                Case "ORDERBY"
                    If order_count = 0 Then
                        order = " ORDER BY [" & Where(index, 0) & "].[" & Where(index, 1) & "] " & Where(index, 3)
                        order_count += 1
                    Else
                        order &= ", [" & Where(index, 0) & "].[" & Where(index, 1) & "] " & Where(index, 3)
                    End If
                Case "GROUPBY"
                    If group_count = 0 Then
                        If Where(index, 0) = "CUSTOM_SQL" Then
                            group = " GROUP BY " & Where(index, 1)
                            group_count += 1
                        Else
                            group = " GROUP BY [" & Where(index, 0) & "].[" & Where(index, 1) & "]"
                            group_count += 1
                        End If
                    Else
                        If Where(index, 0) = "CUSTOM_SQL" Then
                            group &= ", " & Where(index, 1)
                        ElseIf Where(index, 0) = "HAVING" AndAlso Where(index, 1) <> "" Then
                            group &= "HAVING " & Where(index, 1)
                        ElseIf Where(index, 0) = "HAVING" Then
                            group &= ""
                        Else
                            group &= ", [" & Where(index, 0) & "].[" & Where(index, 1) & "]"
                        End If
                    End If
                Case "CUSTOM_SQL"
                    str &= Where(index, 3)
                Case Else
                    str &= "[" & Where(index, 0) & "].[" & Where(index, 1) & "]" & Where(index, 2) & "'" & Where(index, 3).Replace("'", "''") & "'"
            End Select
        Next
        str &= group
        str &= order
        'Return str
        Console.WriteLine(str)
        Return str
    End Function

    'With_As多個Table並UNION後，SELECT指定欄位，回傳DB
    'WithAS_Array : {{"As名稱", "Select_Query"}}，Table別稱與子查詢QUERY
    'Union_Name : Union後的Table名稱
    'Union_Column: {{Column(n), Query(n+1)}}，單數位置為Union後要Select的Column名稱；雙數位置為其計算規則以SQL_QUERY方式填寫，要取原值則留空（長度為2n，n為WithAS_Array(0)的個數）
    ' Union_Column 範例: {"R_QTY", "", "R_QTY", "", "R_QTY", "*[CONTRAST_NUM]"}
    'Select_Column : {{"種類", "Column"}} 種類：COLUMN／GROUP_BY／ORDER_BY_ASC／ORDER_BY_DESC，內容為最後回傳之DataTable要選擇的欄位、條件
    'Select_Column : {{"CUSTOM_SQL", "Query"}}
    Public Function WithAS_UNION(ByVal WithAS_Array(,) As String, ByVal Union_Name As String, ByVal Union_Column(,) As String, ByVal Select_Column(,) As String)
        Dim DB_Table As DataTable
        Dim count = 0
        Dim where_count = 0
        Dim order_count = 0
        Dim group_count = 0
        Dim having_count = 0
        Dim str As String = ""
        Dim where As String = ""
        Dim group As String = ""
        Dim order As String = ""
        Dim having As String = ""
        Dim temp_str As String = ""
        Dim Union_Select(WithAS_Array.GetUpperBound(0)) As String
        Dim Union_Count(WithAS_Array.GetUpperBound(0)) As Integer

        For index = 0 To WithAS_Array.GetUpperBound(0)
            If count = 0 Then
                str &= "WITH [" & WithAS_Array(index, 0) & "] AS (" & WithAS_Array(index, 1) & ")"
                count += 1
            Else
                str &= ",  [" & WithAS_Array(index, 0) & "] AS (" & WithAS_Array(index, 1) & ")"
            End If

            For uc_index = 0 To Union_Column.GetUpperBound(0)
                If Union_Count(index) = 0 Then
                    Union_Count(index) += 1
                Else
                    Union_Select(index) &= ", "
                End If
                If Union_Column(uc_index, index * 2 + 1) = "" Then
                    Union_Select(index) &= "[" & Union_Column(uc_index, index * 2) & "]"
                Else
                    Union_Select(index) &= "[" & Union_Column(uc_index, index * 2) & "]" & Union_Column(uc_index, index * 2 + 1) & "AS [" & Union_Column(uc_index, index * 2) & "]"
                End If
                Dim a As String = Union_Select(index)
            Next
        Next

        count = 0
        If WithAS_Array.Length > 1 Then
            For index = 0 To WithAS_Array.GetUpperBound(0)
                If count = 0 Then
                    str &= ",  [" & Union_Name & "] AS (SELECT " & Union_Select(index) & " FROM [" & WithAS_Array(index, 0) & "]"
                    count += 1
                Else
                    str &= " UNION ALL SELECT " & Union_Select(index) & " FROM [" & WithAS_Array(index, 0) & "]"
                End If
            Next
            str &= ")"
        End If
        count = 0
        For index = 0 To Select_Column.GetUpperBound(0)
            Select Case Select_Column(index, 0)
                Case "WHERE", "GROUP_BY", "HAVING_SQL", "ORDER_BY_ASC", "ORDER_BY_DESC"
                Case Else
                    If count = 0 Then
                        str &= "SELECT "
                        count += 1
                    Else
                        str &= ",  "
                    End If
            End Select
            Select Case Select_Column(index, 0)
                Case "COLUMN"
                    If InStr(1, Select_Column(index, 1), "_AS_") > 0 Then
                        If InStr(1, Split(Select_Column(index, 1), "_AS_")(0), "+") Then
                            temp_str = Split(Select_Column(index, 1), "_AS_")(0)
                            str &= "[" & Split(temp_str, "+")(0) & "]+'_'+[" & Split(temp_str, "+")(1) & "]"
                            str &= " AS [" & Split(Select_Column(index, 1), "_AS_")(1) & "]"
                        Else
                            str &= "[" & Split(Select_Column(index, 1), "_AS_")(0) & "] AS [" & Split(Select_Column(index, 1), "_AS_")(1) & "]"
                        End If
                    Else
                        str &= "[" & Select_Column(index, 1) & "]"
                    End If
                Case "CUSTOM_SQL"
                    str &= Select_Column(index, 1)
                Case "HAVING_SQL"
                    If having_count = 0 Then
                        having = " HAVING " & Select_Column(index, 1) & ""
                        having_count += 1
                    Else
                        having &= " AND " & Select_Column(index, 1) & ""
                    End If
                Case "WHERE"
                    If where_count = 0 Then
                        where = " WHERE " & Select_Column(index, 1)
                        where_count += 1
                    Else
                        where &= " AND " & Select_Column(index, 1)
                    End If
                Case "GROUP_BY"
                    If group_count = 0 Then
                        group = " GROUP BY [" & Select_Column(index, 1) & "]"
                        group_count += 1
                    Else
                        group &= ", [" & Select_Column(index, 1) & "]"
                    End If
                Case "ORDER_BY_ASC"
                    If order_count = 0 Then
                        order = " ORDER BY [" & Select_Column(index, 1) & "] ASC"
                        order_count += 1
                    Else
                        order &= ", [" & Select_Column(index, 1) & "] ASC"
                    End If
                Case "ORDER_BY_DESC"
                    If order_count = 0 Then
                        order = " ORDER BY [" & Select_Column(index, 1) & "] DESC"
                        order_count += 1
                    Else
                        order &= ", [" & Select_Column(index, 1) & "] DESC"
                    End If
            End Select
        Next
        count = 0
        str &= " FROM [" & Union_Name & "]"
        str &= where & group & having & order
        Console.WriteLine(str)
        DB_Table = GetDataTable(str)
        If DB_Table.Rows.Count > 0 Then
            For row_index As Integer = 0 To DB_Table.Rows.Count - 1
                For col_index As Integer = 0 To DB_Table.Rows(row_index).ItemArray.Count - 1
                    DB_Table.Columns(col_index).ReadOnly = False
                    If IsDBNull(DB_Table.Rows(row_index).Item(col_index)) Then
                        Continue For
                    End If
                    DB_Table.Rows(row_index).Item(col_index) = DB_Table.Rows(row_index).Item(col_index).ToString.Trim
                Next
            Next
        End If
        Return DB_Table
    End Function

    '刪除DB
    'String      Table名稱 
    'String(,)  Where的欄位名稱與值
    Public Function DeleteDB(ByVal TableName As String, ByVal Where(,) As String)
        Dim str As String = ""
        Dim count As Integer = 0

        str = "DELETE FROM [" & TableName & "]"
        For index = 0 To Where.GetUpperBound(0)
            If (Where(index, 3).ToString.Trim = "" And Where(index, 2).ToString.Trim <> "!=") Or (Where(index, 3).ToString.Trim = "全部") Then
                Continue For
            End If
            If Where(index, 2) <> "ORDERBY" Then
                If count = 0 Then
                    str &= " WHERE "
                    count += 1
                Else
                    str &= " AND "
                End If
            End If
            Select Case Where(index, 2)
                Case "LIKE"
                    str &= "[" & Where(index, 0) & "].[" & Where(index, 1) & "]" & Where(index, 2) & "'%" & Where(index, 3).Replace("'", "''") & "%'"
                Case "ORDERBY"
                Case "CUSTOM_SQL"
                    str &= Where(index, 3)
                Case Else
                    str &= "[" & Where(index, 0) & "].[" & Where(index, 1) & "]" & Where(index, 2) & "'" & Where(index, 3).Replace("'", "''") & "'"
            End Select
        Next
        Console.WriteLine(str)
        Return ExecSQL(str)
    End Function

    '查詢DB
    'String      [Table名稱]
    'String(,)  Where的[欄位名稱]與[值]; 當[欄位名稱]為[ORDERBY_DESC]或是[ORDERBY_ASC]時,可以成為排序條件(降序/升序),原[值]欄位為需排序之[欄位名稱]
    Public Function GetDB(ByVal TableName As String, ByVal Where(,) As String)
        Dim DB_Table As DataTable
        Dim str As String = ""
        Dim count As Integer = 0
        Dim order_count As Integer = 0
        Dim order As String = ""

        str = "SELECT * From [" & TableName.Split(",")(0) & "]"
        If InStr(1, TableName, ",") > 0 Then
            Select Case TableName.Split(",")(1)
                Case "NOLOCK"
                    str &= " WITH (NOLOCK)"
            End Select
        End If
        For index = 0 To Where.GetUpperBound(0)
            If Where(index, 1).ToString.Trim = "" Then Continue For
            Select Case Where(index, 0)
                Case "ORDERBY_DESC"
                    If order_count = 0 Then
                        order &= " ORDER BY " & Where(index, 1) & " DESC"
                        order_count += 1
                    Else
                        order &= " ,  " & Where(index, 1) & " DESC"
                    End If
                Case "ORDERBY_ASC"
                    If order_count = 0 Then
                        order &= " ORDER BY " & Where(index, 1) & " ASC"
                        order_count += 1
                    Else
                        order &= " ,  " & Where(index, 1) & " ASC"
                    End If
                Case Else
                    If count = 0 Then
                        str &= " WHERE"
                        count += 1
                    Else
                        str &= " AND"
                    End If
                    If InStr(1, Where(index, 0), "<=_") > 0 Then
                        str &= " [" & Split(Where(index, 0), "=_")(1) & "]<='" & Where(index, 1).Replace("'", "''") & "'"
                    ElseIf InStr(1, Where(index, 0), ">=_") > 0 Then
                        str &= " [" & Split(Where(index, 0), "=_")(1) & "]>='" & Where(index, 1).Replace("'", "''") & "'"
                    ElseIf InStr(1, Where(index, 0), "LIKE_") > 0 Then
                        str &= " [" & Split(Where(index, 0), "LIKE_")(1) & "] LIKE '%" & Where(index, 1).Replace("'", "''") & "%'"
                    ElseIf Where(index, 0) = "CUSTOM_SQL" Then
                        str &= Where(index, 1)
                    Else
                        If Where(index, 1) = "Null" Then
                            str &= " [" & Where(index, 0) & "]=NULL"
                        Else
                            str &= " [" & Where(index, 0) & "]='" & Where(index, 1).Replace("'", "''") & "'"
                        End If
                    End If
            End Select
        Next
        str &= order
        Console.WriteLine(str)
        DB_Table = GetDataTable(str)

        Return DB_Table
    End Function

    'Textbox限制到小數點第幾位
    Public Sub TextBox_float(ByVal e As KeyPressEventArgs, ByVal textbox As Object, ByVal place As Integer)
        If My.Computer.Keyboard.CtrlKeyDown Then
            'MessageBox.Show(Asc(e.KeyChar))
            Select Case Asc(e.KeyChar)
                Case 1, 3, 24, 26 'C,A,X,Z
                    e.Handled = False
                Case 22 'V
                    Dim ori As Integer = System.Text.Encoding.Default.GetBytes(textbox.Text).Length
                    Dim paste As Integer = System.Text.Encoding.Default.GetBytes(Clipboard.GetText).Length
                    Dim paste_text As String = Clipboard.GetText
                    'MessageBox.Show(ori & " " & paste)
                    For Each txt As Char In paste_text
                        'MessageBox.Show(Asc(txt))
                        'If Not ((Asc(txt) >= 48 And Asc(txt) <= 57) Or
                        '    (Asc(txt) = 46)) Then '0-9
                        '    e.Handled = True
                        '    MyError("有不是數字及.的字元，不可貼上！")
                        '    Exit Sub
                        'End If
                    Next
                    If ori + paste <= 13 Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
            End Select
            Exit Sub
        End If
        If place = 0 Then '限定只能輸入整數
            If (Not Char.IsDigit(e.KeyChar) And Not e.KeyChar = Chr(Keys.Back)) Or
            (textbox.Text.Length > 8 And Not e.KeyChar = Chr(Keys.Back)) Then
                e.Handled = True
            End If
        ElseIf place > 0 Then '限定到小數第'place'位
            If Not Char.IsDigit(e.KeyChar) And Not e.KeyChar = "." And Not e.KeyChar = Chr(Keys.Back) Then
                e.Handled = True
            Else
                If (e.KeyChar = "." And textbox.Text.IndexOf(".") <> -1) Or
                   (e.KeyChar = "." And textbox.Text.Length = 0) Then
                    e.Handled = True
                ElseIf e.KeyChar = "." Then
                    e.Handled = False
                ElseIf Char.IsDigit(e.KeyChar) Then
                    If textbox.Text.IndexOf(".") <> -1 Then
                        If textbox.SelectionStart <= textbox.Text.IndexOf(".") Then
                            If textbox.Text.IndexOf(".") < 9 Then
                                e.Handled = False
                            Else
                                e.Handled = True
                            End If
                        ElseIf textbox.Text.Length > textbox.Text.IndexOf(".") + place Then
                            e.Handled = True
                        End If
                    Else
                        If textbox.Text.Length > 8 Then
                            e.Handled = True
                        Else
                            e.Handled = False
                        End If
                    End If
                End If
            End If
        End If
    End Sub

    'Textbox限制(place個)；[type="char+num"]-限定英文+數字；[type="char"]-限定英文；
    '[type="num"]-限定數字；[type=""]-無限制
    Public Sub TextBox_limit(ByVal e As KeyPressEventArgs, ByVal textbox As Object, ByVal place As Integer, ByVal type As String)
        If place > 0 Then
            If My.Computer.Keyboard.CtrlKeyDown Then
                'MessageBox.Show(Asc(e.KeyChar))
                Select Case Asc(e.KeyChar)
                    Case 1, 3, 24, 26 'C,A,X,Z
                        e.Handled = False
                    Case 22 'V
                        Dim ori As Integer = System.Text.Encoding.Default.GetBytes(textbox.Text).Length - System.Text.Encoding.Default.GetBytes(textbox.SelectedText).Length
                        Dim paste As Integer = System.Text.Encoding.Default.GetBytes(Clipboard.GetText).Length
                        Dim paste_text As String = Clipboard.GetText
                        'MessageBox.Show(ori & " " & paste)
                        'Console.WriteLine("ori= {0} ,paste = {1} , paste_text = {2}", ori, paste, paste_text)
                        Exit Sub
                        For Each txt As Char In paste_text
                            If type <> "" Then
                                Select Case type
                                    Case "char+num"
                                        'MessageBox.Show(Asc(txt))
                                        If Not ((Asc(txt) >= 97 And Asc(txt) <= 122) Or 'a-z
                                           (Asc(txt) >= 65 And Asc(txt) <= 90) Or 'A-Z
                                           (Asc(txt) >= 48 And Asc(txt) <= 57)) Then '0-9
                                            e.Handled = True
                                            MyError("有不是字母及數字的字元，不可貼上！")
                                            Exit Sub
                                        End If
                                    Case "char"
                                        If Not ((Asc(txt) >= 97 And Asc(txt) <= 122) Or 'a-z
                                           (Asc(txt) >= 65 And Asc(txt) <= 90)) Then 'A-Z
                                            e.Handled = True
                                            MyError("有不是字母的字元，不可貼上！")
                                            Exit Sub
                                        End If
                                    Case "num"
                                        If Not (Asc(txt) >= 48 And Asc(txt) <= 57) Then '0-9
                                            e.Handled = True
                                            MyError("有不是數字的字元，不可貼上！")
                                            Exit Sub
                                        End If
                                End Select
                            End If
                        Next
                        If ori + paste <= place Then
                            e.Handled = False
                        Else
                            e.Handled = True
                            MyError("超過規定最大位元：" & place & "，不可貼上！")
                        End If
                End Select
                Exit Sub
            End If
            If type <> "" Then
                Select Case type
                    Case "char+num"
                        If Not ((Asc(e.KeyChar) >= 97 And Asc(e.KeyChar) <= 122) Or 'a-z
                           (Asc(e.KeyChar) >= 65 And Asc(e.KeyChar) <= 90) Or 'A-Z
                           (Asc(e.KeyChar) >= 48 And Asc(e.KeyChar) <= 57)) Then '0-9
                            e.Handled = True
                        End If
                    Case "char"
                        If Not ((Asc(e.KeyChar) >= 97 And Asc(e.KeyChar) <= 122) Or 'a-z
                           (Asc(e.KeyChar) >= 65 And Asc(e.KeyChar) <= 90)) Then 'A-Z
                            e.Handled = True
                        End If
                    Case "num"
                        If Not (Asc(e.KeyChar) >= 48 And Asc(e.KeyChar) <= 57) Then '0-9
                            e.Handled = True
                        End If
                    Case "phonenum"
                        If Not (Asc(e.KeyChar) >= 48 And Asc(e.KeyChar) <= 57 Or Asc(e.KeyChar) = 45) Then '0-9
                            e.Handled = True
                        End If
                End Select
            End If
            If e.KeyChar = Chr(Keys.Back) Then
                e.Handled = False
            ElseIf textbox.SelectedText.Length > 0 Then
                If type <> "" Then
                    Select Case type
                        Case "char+num"
                            If Not ((Asc(e.KeyChar) >= 97 And Asc(e.KeyChar) <= 122) Or 'a-z
                               (Asc(e.KeyChar) >= 65 And Asc(e.KeyChar) <= 90) Or 'A-Z
                               (Asc(e.KeyChar) >= 48 And Asc(e.KeyChar) <= 57)) Then '0-9
                                e.Handled = True
                            End If
                        Case "char"
                            If Not ((Asc(e.KeyChar) >= 97 And Asc(e.KeyChar) <= 122) Or 'a-z
                               (Asc(e.KeyChar) >= 65 And Asc(e.KeyChar) <= 90)) Then 'A-Z
                                e.Handled = True
                            End If
                        Case "num"
                            If Not (Asc(e.KeyChar) >= 48 And Asc(e.KeyChar) <= 57) Then '0-9
                                e.Handled = True
                            End If
                    End Select
                End If
            ElseIf System.Text.Encoding.Default.GetBytes(textbox.Text).Length +
                System.Text.Encoding.Default.GetBytes(e.KeyChar).Length > place Then
                e.Handled = True
                'MyError("超過規定最大位元：" & place & "，不可再輸入！")
            End If
        End If
    End Sub

    'Datadridview_Textbox限制到小數點第幾位
    '整數部分限定輸入最大值預設為 9 位數
    Public Sub Datadridview_float(ByVal e As KeyPressEventArgs, ByVal place As Integer, dtg As DataGridView, ByVal colname As String, ByVal sender As Object)
        If dtg.CurrentCell.OwningColumn.Name = colname Then
            If My.Computer.Keyboard.CtrlKeyDown Then
                'MessageBox.Show(Asc(e.KeyChar))
                Select Case Asc(e.KeyChar)
                    Case 1, 24, 26 'A,C,X,Z
                        e.Handled = False
                    Case 22 'V
                        Dim ori As Integer = System.Text.Encoding.Default.GetBytes(sender.Text).Length - System.Text.Encoding.Default.GetBytes(sender.SelectedText).Length
                        Dim paste As Integer = System.Text.Encoding.Default.GetBytes(Clipboard.GetText).Length
                        Dim paste_text As String = Clipboard.GetText
                        If ori + paste <= 13 Then
                            e.Handled = False
                        Else
                            e.Handled = True
                        End If
                End Select
                Exit Sub
            End If
            If place = 0 Then '限定只能輸入整數
                If (Not Char.IsDigit(e.KeyChar) And Not e.KeyChar = Chr(Keys.Back)) Or
                (sender.Text.Length > 8 And Not e.KeyChar = Chr(Keys.Back)) Then
                    e.Handled = True
                End If
            ElseIf place > 0 Then '限定到小數第'place'位
                If Not Char.IsDigit(e.KeyChar) And Not e.KeyChar = "." And Not e.KeyChar = Chr(Keys.Back) Then
                    e.Handled = True
                Else
                    If (e.KeyChar = "." And sender.Text.IndexOf(".") <> -1) Or
                       (e.KeyChar = "." And sender.Text.Length = 0) Then
                        e.Handled = True
                    ElseIf e.KeyChar = "." Then
                        e.Handled = False
                    ElseIf Char.IsDigit(e.KeyChar) Then
                        If sender.Text.IndexOf(".") <> -1 Then
                            If sender.SelectionStart <= sender.Text.IndexOf(".") Then
                                If sender.Text.IndexOf(".") < 9 Then
                                    e.Handled = False
                                Else
                                    e.Handled = True
                                End If
                            ElseIf sender.Text.Length > sender.Text.IndexOf(".") + place Then
                                e.Handled = True
                            End If
                        Else
                            If sender.Text.Length > 8 Then
                                e.Handled = True
                            Else
                                e.Handled = False
                            End If
                        End If
                    End If
                End If
            End If
        End If
    End Sub

    '2020.05.28新增
    'Datadridview_Textbox限制到小數點第幾位
    '整數部分限定輸入最大值預設為 9 位數
    '允許輸入負數
    Public Sub Datadridview_float2(ByVal e As KeyPressEventArgs, ByVal place As Integer, dtg As DataGridView, ByVal colname As String, ByVal sender As Object)
        If dtg.CurrentCell.OwningColumn.Name = colname Then
            If My.Computer.Keyboard.CtrlKeyDown Then
                'MessageBox.Show(Asc(e.KeyChar))
                Select Case Asc(e.KeyChar)
                    Case 1, 24, 26 'A,C,X,Z
                        e.Handled = False
                    Case 22 'V
                        Dim ori As Integer = System.Text.Encoding.Default.GetBytes(sender.Text).Length - System.Text.Encoding.Default.GetBytes(sender.SelectedText).Length
                        Dim paste As Integer = System.Text.Encoding.Default.GetBytes(Clipboard.GetText).Length
                        Dim paste_text As String = Clipboard.GetText
                        If ori + paste <= 13 Then
                            e.Handled = False
                        Else
                            e.Handled = True
                        End If
                End Select
                Exit Sub
            End If
            If place = 0 Then '限定只能輸入整數
                If (Not Char.IsDigit(e.KeyChar) And Not e.KeyChar = Chr(Keys.Back)) Or
                (sender.Text.Length > 8 And Not e.KeyChar = Chr(Keys.Back) And Not e.KeyChar = "-") Then
                    e.Handled = True
                End If
            ElseIf place > 0 Then '限定到小數第'place'位
                If Not Char.IsDigit(e.KeyChar) And Not e.KeyChar = "." And Not e.KeyChar = Chr(Keys.Back) And Not e.KeyChar = "-" Then
                    e.Handled = True
                Else
                    If (e.KeyChar = "." And sender.Text.IndexOf(".") <> -1) Or
                       (e.KeyChar = "." And sender.Text.Length = 0) Then
                        e.Handled = True
                    ElseIf e.KeyChar = "." Then
                        e.Handled = False
                    ElseIf Char.IsDigit(e.KeyChar) Then
                        If sender.Text.IndexOf(".") <> -1 Then
                            If sender.SelectionStart <= sender.Text.IndexOf(".") Then
                                If sender.Text.IndexOf(".") < 9 Then
                                    e.Handled = False
                                Else
                                    e.Handled = True
                                End If
                            ElseIf sender.Text.Length > sender.Text.IndexOf(".") + place Then
                                e.Handled = True
                            End If
                        Else
                            If sender.Text.Length > 8 Then
                                e.Handled = True
                            Else
                                e.Handled = False
                            End If
                        End If
                    End If
                End If
            End If
        End If
    End Sub

    'Datadridview_Textbox限制(place個)；[type="char+num"]-限定英文+數字；[type="char"]-限定英文；
    '[type="num"]-限定數字；[type=""]-無限制
    Public Sub Datadridview_limit(ByVal e As KeyPressEventArgs, ByVal place As Integer, dtg As DataGridView, ByVal colname As String, ByVal sender As Object, ByVal type As String)
        If dtg.CurrentCell.OwningColumn.Name = colname Then
            If place > 0 Then
                If My.Computer.Keyboard.CtrlKeyDown Then
                    'MessageBox.Show(Asc(e.KeyChar))
                    Select Case Asc(e.KeyChar)
                        Case 1, 24, 26 'A,C,X,Z
                            e.Handled = False
                        Case 22 'V
                            Dim ori As Integer = System.Text.Encoding.Default.GetBytes(sender.Text).Length - System.Text.Encoding.Default.GetBytes(sender.SelectedText).Length
                            Dim paste As Integer = System.Text.Encoding.Default.GetBytes(Clipboard.GetText).Length
                            Dim paste_text As String = Clipboard.GetText
                            'MessageBox.Show(ori & " " & paste)
                            For Each txt As Char In paste_text
                                If type <> "" Then
                                    Select Case type
                                        Case "char+num"
                                            'MessageBox.Show(Asc(txt))
                                            If Not ((Asc(txt) >= 97 And Asc(txt) <= 122) Or 'a-z
                                           (Asc(txt) >= 65 And Asc(txt) <= 90) Or 'A-Z
                                           (Asc(txt) >= 48 And Asc(txt) <= 57)) Then '0-9
                                                e.Handled = True
                                                MyError("有不是字母及數字的字元，不可貼上！")
                                                Exit Sub
                                            End If
                                        Case "char"
                                            If Not ((Asc(txt) >= 97 And Asc(txt) <= 122) Or 'a-z
                                           (Asc(txt) >= 65 And Asc(txt) <= 90)) Then 'A-Z
                                                e.Handled = True
                                                MyError("有不是字母的字元，不可貼上！")
                                                Exit Sub
                                            End If
                                        Case "num"
                                            If Not (Asc(txt) >= 48 And Asc(txt) <= 57) Then '0-9
                                                e.Handled = True
                                                MyError("有不是數字的字元，不可貼上！")
                                                Exit Sub
                                            End If
                                    End Select
                                End If
                            Next
                            If ori + paste <= place Then
                                e.Handled = False
                            Else
                                e.Handled = True
                                MyError("超過規定最大位元：" & place & "，不可貼上！")
                            End If
                    End Select
                    Exit Sub
                End If

                If type <> "" Then
                    Select Case type
                        Case "char+num"
                            If Not ((Asc(e.KeyChar) >= 97 And Asc(e.KeyChar) <= 122) Or 'a-z
                           (Asc(e.KeyChar) >= 65 And Asc(e.KeyChar) <= 90) Or 'A-Z
                           (Asc(e.KeyChar) >= 48 And Asc(e.KeyChar) <= 57)) Then '0-9
                                e.Handled = True
                            End If
                        Case "char"
                            If Not ((Asc(e.KeyChar) >= 97 And Asc(e.KeyChar) <= 122) Or 'a-z
                           (Asc(e.KeyChar) >= 65 And Asc(e.KeyChar) <= 90)) Then 'A-Z
                                e.Handled = True
                            End If
                        Case "num"
                            If Not (Asc(e.KeyChar) >= 48 And Asc(e.KeyChar) <= 57) Then '0-9
                                e.Handled = True
                            End If
                        Case "num_neg"
                            If Not ((Asc(e.KeyChar) >= 48 And Asc(e.KeyChar) <= 57) Or Asc(e.KeyChar) = 45) Then '0-9 , -
                                e.Handled = True
                            End If
                            If (Not sender.SelectionStart = 0) AndAlso Asc(e.KeyChar) = 45 Then
                                e.Handled = True
                            End If
                    End Select
                End If
                If e.KeyChar = Chr(Keys.Back) Then
                    e.Handled = False
                ElseIf sender.SelectedText.Length > 0 Then
                    If type <> "" Then
                        Select Case type
                            Case "char+num"
                                If Not ((Asc(e.KeyChar) >= 97 And Asc(e.KeyChar) <= 122) Or 'a-z
                               (Asc(e.KeyChar) >= 65 And Asc(e.KeyChar) <= 90) Or 'A-Z
                               (Asc(e.KeyChar) >= 48 And Asc(e.KeyChar) <= 57)) Then '0-9
                                    e.Handled = True
                                End If
                            Case "char"
                                If Not ((Asc(e.KeyChar) >= 97 And Asc(e.KeyChar) <= 122) Or 'a-z
                               (Asc(e.KeyChar) >= 65 And Asc(e.KeyChar) <= 90)) Then 'A-Z
                                    e.Handled = True
                                End If
                            Case "num"
                                If Not (Asc(e.KeyChar) >= 48 And Asc(e.KeyChar) <= 57) Then '0-9
                                    e.Handled = True
                                End If
                            Case "num_neg"
                                If Not ((Asc(e.KeyChar) >= 48 And Asc(e.KeyChar) <= 57) Or Asc(e.KeyChar) = 45) Then '0-9 , -
                                    e.Handled = True
                                End If
                                If (Not sender.SelectionStart = 0) AndAlso Asc(e.KeyChar) = 45 Then
                                    e.Handled = True
                                End If
                        End Select
                    End If
                ElseIf System.Text.Encoding.Default.GetBytes(sender.Text).Length +
                    System.Text.Encoding.Default.GetBytes(e.KeyChar).Length > place Then
                    e.Handled = True
                    'MyError("超過規定最大位元：" & place & "，不可再輸入！")
                End If
            End If
        End If
    End Sub

    '檢查是否為空，如果為空轉成0(用於計算)
    Public Function CheckNull(Value) As Double
        If IsDBNull(Value) Then
            Return 0
        ElseIf String.IsNullOrEmpty(Value) Then
            Return 0
        ElseIf IsNothing(Value) Then
            Return 0
        Else
            Return CType(Value.ToString, Double)
        End If
    End Function

    '檢查日期是否NULL，如果為NULL傳空字串
    Public Function CheckDateTimeNull(Value) As String
        If IsDBNull(Value) Then
            Return ""
        ElseIf String.IsNullOrEmpty(Value) Then
            Return ""
        ElseIf IsNothing(Value) Then
            Return ""
        Else
            Return CDate(Value.ToString).ToString("yyyy/MM/dd HH:mm:ss")
        End If
    End Function

    '只有Form可以使用的倉庫，只挑選[DEPOT_ID]、[DEPOT_NAME]、[DEPOT_ID+DEPOT_NAME]為COMBINE_NAME這三格
    Public Function Available_DEPOT(ByVal Form As String, ByVal Company As String) As DataTable
        Dim DEPOT As New DataTable
        Dim table As String = "D_DP_PROFILE"
        Dim join(,) As String = {{"D_DEPOT_BASE", "DEPOT_ID", table, "DEPOT_ID"}}
        Dim Select_Column(,) As String = {{"D_DEPOT_BASE", "DEPOT_ID"},
                                          {"D_DEPOT_BASE", "DEPOT_NAME"},
                                          {"D_DEPOT_BASE", "DEPOT_NAME+DEPOT_ID_AS_COMBINE_NAME"},
                                          {table, ""}}

        Dim where(,) As String = {}

        If Form <> "" Then

            Select Case Company
                Case ""
                    where = {{table, "FROM_ID", "=", Form},
                         {table, "IS_USED", "=", "1"}}
                Case Else
                    where = {{table, "FROM_ID", "=", Form},
                         {table, "IS_USED", "=", "1"},
                         {"D_DEPOT_BASE", "COMPANY_ID", "=", Company}}
            End Select

            DEPOT = GetDB_Join(table, join, Select_Column, where)
            Return DEPOT

        Else

            table = "D_DEPOT_BASE"
            Select_Column = {{"D_DEPOT_BASE", "DEPOT_ID"},
                             {"D_DEPOT_BASE", "DEPOT_NAME"},
                             {"D_DEPOT_BASE", "DEPOT_ID+DEPOT_NAME_AS_COMBINE_NAME"}}

            Select Case Company
                Case ""
                    where = {}
                Case Else
                    where = {{table, "COMPANY_ID", "=", Company}}
            End Select

            DEPOT = GetDB_Join(table, {}, Select_Column, where)
            Return DEPOT

        End If
    End Function


    '只有Form可以使用的倉庫，只挑選[DEPOT_ID]、[DEPOT_NAME]、[DEPOT_ID+DEPOT_NAME]為COMBINE_NAME這三格
    Public Function Available_LendDEPOT(ByVal Form As String, ByVal Company As String) As DataTable
        Dim DEPOT As New DataTable
        Dim table As String = "D_DP_PROFILE"
        Dim join(,) As String = {{"D_DEPOT_BASE", "DEPOT_ID", table, "DEPOT_ID"}}
        Dim Select_Column(,) As String = {{"D_DEPOT_BASE", "DEPOT_ID"},
                                          {"D_DEPOT_BASE", "DEPOT_NAME"},
                                          {"D_DEPOT_BASE", "DEPOT_NAME+DEPOT_ID_AS_COMBINE_NAME"},
                                          {table, ""}}

        Dim where(,) As String = {}
        table = "D_LEND_DEPOT_BASE"
        Select_Column = {{"D_LEND_DEPOT_BASE", "DEPOT_ID"},
                            {"D_LEND_DEPOT_BASE", "DEPOT_NAME"},
                            {"D_LEND_DEPOT_BASE", "DEPOT_NAME+DEPOT_ID_AS_COMBINE_NAME"}}

        Select Case Company
            Case ""
                where = {}
            Case Else
                where = {{table, "COMPANY_ID", "=", Company}}
        End Select

        DEPOT = GetDB_Join(table, {}, Select_Column, where)
        Return DEPOT

    End Function

    '計算付款日
    Public Function PAYDATE(ByVal SUPPLIER_ID As String, INV_DATE As DateTimePicker, PAY_METHOD_ID As Double, PAY_CONDITION_ID As Double) As Date
        Dim PAY_DATE As Date = Today
        Dim CHECKOUT_DAY As Date = Nothing
        Dim query As DataTable = GetDB("P_SUPPLIER_BASE", {{"SUPPLIER_ID", SUPPLIER_ID}})
        Dim dtr As DataRow = query.Rows(0)
        '月結
        If PAY_CONDITION_ID = 1 Then

            Dim day As Integer = INV_DATE.Value.Day
            Dim month As Integer = INV_DATE.Value.Month
            '結帳日不為30，代表不是整個月為一個循環
            If dtr.Item("CHECKOUT_DAY") = -1 Then
                MyError("未設定結帳日，請至設定檔設定！")
                Return PAY_DATE
            End If
            If dtr.Item("PAYMENT_DAY") = -1 Then
                MyError("未設定付款條件天數，請至設定檔設定！")
                Return PAY_DATE
            End If
            If dtr.Item("PAYMENT_DATE") = -1 Then
                MyError("未設定付款日，請至設定檔設定！")
                Return PAY_DATE
            End If

            If dtr.Item("CHECKOUT_DAY") <> 30 Then
                CHECKOUT_DAY = INV_DATE.Value.ToString("yyyy/MM/" & dtr.Item("CHECKOUT_DAY"))
                If day > dtr.Item("CHECKOUT_DAY") Then
                    '如果開發票之日大於結帳日，為下個月一起結帳
                    CHECKOUT_DAY = CHECKOUT_DAY.AddMonths(1)
                End If
                '如果結帳日為30，代表一個月為循環
            ElseIf dtr.Item("CHECKOUT_DAY") = 30 Then
                '取得月底
                CHECKOUT_DAY = DateTime.Parse(INV_DATE.Value.ToString("yyyy/MM/01")).AddMonths(1).AddDays(-1)
            End If

            '付款條件天數除以30取商數，加上商數個月，取月底
            Dim PAYMENT_DAY As Integer = dtr.Item("PAYMENT_DAY") \ 30
            PAY_DATE = DateTime.Parse(CHECKOUT_DAY.ToString("yyyy/MM/01")).AddMonths(PAYMENT_DAY + 1).AddDays(-1)

            '付款條件天數除以30取餘數，加上餘數日
            Dim res_day As Integer = dtr.Item("PAYMENT_DAY") Mod 30

            '如果付款日等於0代表沒有設定付款日，付款日期為月底加上餘數日
            If dtr.Item("PAYMENT_DATE") = 0 Then
                PAY_DATE = PAY_DATE.AddDays(res_day)
                '如果付款日設定為30，代表月底為付款日
            ElseIf dtr.Item("PAYMENT_DATE") = 30 Then
                PAY_DATE = DateTime.Parse(PAY_DATE.AddDays(res_day).ToString("yyyy/MM/01")).AddMonths(1).AddDays(-1)
            Else
                '如果付款日不為0或30，餘數日大於付款日，次月付款；如果餘數日小於等於付款日，本月付款
                If res_day > dtr.Item("PAYMENT_DATE") Then
                    PAY_DATE = DateTime.Parse(PAY_DATE.AddDays(res_day).ToString("yyyy/MM/" & dtr.Item("PAYMENT_DATE"))).AddMonths(1)
                Else
                    PAY_DATE = DateTime.Parse(PAY_DATE.AddDays(res_day).ToString("yyyy/MM/" & dtr.Item("PAYMENT_DATE")))
                End If
            End If

            '貨到
        ElseIf PAY_CONDITION_ID = 2 Then

            If dtr.Item("PAYMENT_DAY") = -1 Then
                MyError("未設定付款條件天數，請至設定檔設定！")
                Return PAY_DATE
            End If

            '發票日期+付款條件天數為付款日-1天(起算日為發票當日)
            Dim PAYMENT_DAY As Integer = dtr.Item("PAYMENT_DAY")
            PAY_DATE = INV_DATE.Value.AddDays(PAYMENT_DAY - 1).ToShortDateString

        End If
        Return PAY_DATE
    End Function

    '
    '抓明細中SYSID最大的編號
    Public Function max_SYSID(ByVal SYSID As String, ByVal TABLE As String, ByVal ID As String, ByVal IDSAME As String) As Long
        Dim max As Long = Nothing
        Dim number As String
        number = "SELECT TOP 1 [" & SYSID & "] FROM [" & TABLE & " ] WITH (NOLOCK) "
        number &= "WHERE [" & ID & "] = '" & IDSAME & "'"
        number &= "ORDER BY [" & SYSID & "] DESC "
        If GetDataTable(number).Rows.Count > 0 Then
            Dim reg As New Regex("\d+")
            Dim m As Match = reg.Match(GetDataTable(number).Rows(0).Item(SYSID))
            If m.Success Then
                max = m.Value + 1
            End If
        Else
            max = 1
        End If
        Return max
    End Function

    '產生單據最新編號
    '[YOU_WANT]-想要的欄位，例：STOCK_IN_ID
    '[TABLE]-所在之資料表，例：D_STOCK_IN_MASTER
    '[CODE]-所在資料表所對應的PAPER_ID，例：UD_21700
    '[COMPANY_ID]-所選擇公司別之代號，自動撈出對應的COMPANY_CODE，例：HD會撈出對應H
    '如想撈出最新入庫單編號，可參考下例：
    'pf.CREATE_PAPER_ID("STOCK_IN_ID", "D_STOCK_IN_MASTER", "UD_21700", COMPANY_ID)
    Public Function CREATE_PAPER_ID(ByVal YOU_WANT As String, ByVal TABLE As String, ByVal CODE As String, ByVal COMPANY_ID As String, ByVal EFFECTIVE_DATE As Date) As String
        Dim ID As String = ""
        Dim number As String
        Dim IDStart As String = ""
        IDStart = GetValue("M_PAPER_NO", "PAPER_ID", CODE, "PAPER_CODE") &
                  GetValue("M_COMPANY_BASE", "COMPANY_ID", COMPANY_ID, "COMPANY_CODE") &
                  EFFECTIVE_DATE.ToString("yyMM")
        'number = "SELECT TOP 1 [" & YOU_WANT & "] FROM [" & TABLE & " ] WITH(NOLOCK) "
        'number &= "WHERE LEFT(CONVERT(varchar,[CREATE_TIME],112),6) = '" & Date.Now.ToString("yyyyMM") & "'"
        'number &= "AND [COMPANY_CODE] = '" & GetValue("M_COMPANY_BASE", "COMPANY_ID", COMPANY_ID, "COMPANY_CODE") & "'"
        number = "SELECT TOP 1 [" & YOU_WANT & "] FROM [" & TABLE & " ] WITH(NOLOCK) "
        number &= "WHERE [" & YOU_WANT & "] LIKE '" & IDStart & "%'"
        number &= "ORDER BY [" & YOU_WANT & "] DESC "
        If GetValue("M_COMPANY_BASE", "COMPANY_ID", COMPANY_ID, "COMPANY_CODE") <> "" Then
            If GetDataTable(number).Rows.Count > 0 Then
                Dim reg As New Regex("\d+")
                Dim m As Match = reg.Match(GetDataTable(number).Rows(0).Item(YOU_WANT))
                If m.Success Then
                    ID = GetValue("M_PAPER_NO", "PAPER_ID", CODE, "PAPER_CODE") &
                         GetValue("M_COMPANY_BASE", "COMPANY_ID", COMPANY_ID, "COMPANY_CODE") &
                         m.Value + 1
                End If
            Else
                ID = GetValue("M_PAPER_NO", "PAPER_ID", CODE, "PAPER_CODE") &
                     GetValue("M_COMPANY_BASE", "COMPANY_ID", COMPANY_ID, "COMPANY_CODE") &
                     EFFECTIVE_DATE.ToString("yyMM") & "00001"
            End If
        Else
            ID = ""
        End If
        Return ID
    End Function

    '產生單據最新編號，流水編號5碼(使用單據有銷貨單、銷退單、折讓單)
    '[YOU_WANT]-想要的欄位，例：STOCK_IN_ID
    '[TABLE]-所在之資料表，例：D_STOCK_IN_MASTER
    '[CODE]-所在資料表所對應的PAPER_ID，例：UD_21700
    '[COMPANY_ID]-所選擇公司別之代號，自動撈出對應的COMPANY_CODE，例：HD會撈出對應H
    '[EFFECTIVE_DATE]-單據的生效日期，例：銷貨單的生效日期2020/05/01
    '如想撈出最新入庫單編號，可參考下例：
    'pf.CREATE_PAPER_ID("STOCK_IN_ID", "D_STOCK_IN_MASTER", "UD_21700", COMPANY_ID)
    Public Function CREATE_PAPER_ID2(ByVal YOU_WANT As String, ByVal TABLE As String, ByVal CODE As String, ByVal COMPANY_ID As String, EFFECTIVE_DATE As Date) As String
        Dim ID As String = ""
        Dim IDStart As String = ""
        Dim number As String
        'IDStart = GetValue("M_PAPER_NO", "PAPER_ID", CODE, "PAPER_CODE") &
        '             GetValue("M_COMPANY_BASE", "COMPANY_ID", COMPANY_ID, "COMPANY_CODE") &
        '             DateTime.Now.ToString("yyMM")
        IDStart = GetValue("M_PAPER_NO", "PAPER_ID", CODE, "PAPER_CODE") &
                     GetValue("M_COMPANY_BASE", "COMPANY_ID", COMPANY_ID, "COMPANY_CODE") &
                     EFFECTIVE_DATE.ToString("yyMM")
        'number = "SELECT TOP 1 [" & YOU_WANT & "] FROM [" & TABLE & " ] WITH(NOLOCK) WHERE LEFT(CONVERT(varchar,[CREATE_TIME],112),6) = '" & Date.Now.ToString("yyyyMM") & "'"
        number = "SELECT TOP 1 [" & YOU_WANT & "] FROM [" & TABLE & " ] WITH(NOLOCK) WHERE [" & YOU_WANT & "] LIKE '" & IDStart & "%'"
        number &= " ORDER BY [" & YOU_WANT & "] DESC "
        If GetValue("M_COMPANY_BASE", "COMPANY_ID", COMPANY_ID, "COMPANY_CODE") <> "" Then
            If GetDataTable(number).Rows.Count > 0 Then
                Dim reg As New Regex("\d+")
                Dim m As Match = reg.Match(GetDataTable(number).Rows(0).Item(YOU_WANT))
                If m.Success Then
                    ID = GetValue("M_PAPER_NO", "PAPER_ID", CODE, "PAPER_CODE") &
                         GetValue("M_COMPANY_BASE", "COMPANY_ID", COMPANY_ID, "COMPANY_CODE") &
                         m.Value + 1
                End If
            Else
                'ID = GetValue("M_PAPER_NO", "PAPER_ID", CODE, "PAPER_CODE") &
                '     GetValue("M_COMPANY_BASE", "COMPANY_ID", COMPANY_ID, "COMPANY_CODE") &
                '     DateTime.Now.ToString("yyMM") & "00001"
                ID = GetValue("M_PAPER_NO", "PAPER_ID", CODE, "PAPER_CODE") &
                     GetValue("M_COMPANY_BASE", "COMPANY_ID", COMPANY_ID, "COMPANY_CODE") &
                     EFFECTIVE_DATE.ToString("yyMM") & "00001"
            End If
        Else
            ID = ""
        End If
        Return ID
    End Function

    '無公司別
    '產生單據最新編號，流水編號5碼(使用單據有借倉入庫單、借倉出庫單)
    '[YOU_WANT]-想要的欄位，例：STOCK_IN_ID
    '[TABLE]-所在之資料表，例：D_STOCK_IN_MASTER
    '[CODE]-所在資料表所對應的PAPER_ID，例：UD_21700
    '如想撈出最新入庫單編號，可參考下例：
    'pf.CREATE_PAPER_ID("STOCK_IN_ID", "D_STOCK_IN_MASTER", "UD_21700")
    Public Function CREATE_PAPER_ID3(ByVal YOU_WANT As String, ByVal TABLE As String, ByVal CODE As String, ByVal EFFECTIVE_DATE As Date) As String
        Dim ID As String = ""
        Dim IDStart As String = ""
        Dim number As String
        Try
            IDStart = GetValue("M_PAPER_NO", "PAPER_ID", CODE, "PAPER_CODE") &
                         EFFECTIVE_DATE.ToString("yyMM")
            'number = "SELECT TOP 1 [" & YOU_WANT & "] FROM [" & TABLE & " ] WITH(NOLOCK) WHERE LEFT(CONVERT(varchar,[CREATE_TIME],112),6) = '" & Date.Now.ToString("yyyyMM") & "'"
            number = "SELECT TOP 1 [" & YOU_WANT & "] FROM [" & TABLE & " ] WITH(NOLOCK) WHERE [" & YOU_WANT & "] LIKE '" & IDStart & "%'"
            number &= " ORDER BY [" & YOU_WANT & "] DESC "
            If GetDataTable(number).Rows.Count > 0 Then
                Dim reg As New Regex("\d+")
                Dim m As Match = reg.Match(GetDataTable(number).Rows(0).Item(YOU_WANT))
                If m.Success Then
                    ID = GetValue("M_PAPER_NO", "PAPER_ID", CODE, "PAPER_CODE") &
                             m.Value + 1
                End If
            Else
                ID = GetValue("M_PAPER_NO", "PAPER_ID", CODE, "PAPER_CODE") &
                         EFFECTIVE_DATE.ToString("yyMM") & "0001"
            End If
        Catch ex As Exception

        End Try
        Return ID
    End Function

    '日期流水號建立(五碼) (1.隨意欄位 2.資料表 3.需求日期)
    'EX. pf.CREATE_SYSID("PAPER_DATE", "S_PRODUCT_SHIPPING", Me.DateTimePicker5.Value.ToString("yyyy/MM/dd")) 則會取得yyyymmdd00012
    Public Function CREATE_SYSID(ByVal YOU_WANT As String, ByVal TABLE As String, ByVal EFFECTIVE_DATE As String) As String
        Dim ID As String = ""
        Dim number As String
        Try
            number = "SELECT COUNT([" & YOU_WANT & "]) AS C FROM [" & TABLE & " ] WHERE [" & YOU_WANT & "] = '" & EFFECTIVE_DATE & "'"

            ID = EFFECTIVE_DATE.Replace("/", "") & (Val(GetDataTable(number).Rows(0).Item("C")) + 1).ToString.PadLeft(5, "0")

        Catch ex As Exception

        End Try

        Return ID
    End Function

    '找小於等於今日最大關帳日
    Public Function Close_day() As Date
        Dim Closeday As Date
        Dim dt As DataTable
        dt = GetDB("A_CLOSE_DAY", {{"<=_CLOSE_DATE", Today}, {"ORDERBY_DESC", "CLOSE_DATE"}})
        If dt.Rows.Count > 0 Then
            Closeday = CType(dt.Rows(0).Item("CLOSE_DATE"), Date)
        Else
            Closeday = "2015/01/01"
        End If
        Return Closeday
    End Function

    '入庫單檢查庫存及虛擬庫存是否有產品資料，如無資料INSERT數量0的產品資料
    Public Sub Stock_exist(STOCK_IN_ID As String)
        Dim sql As String
        sql = "SELECT * FROM [D_STOCK_IN_DETAIL] WITH (NOLOCK) "
        sql &= "WHERE [STOCK_IN_ID] = '" & STOCK_IN_ID & "'"

        For Each row In GetDataTable(sql).Rows
            sql = "SELECT * FROM [D_DEPOT_STOCKS] WITH (NOLOCK) "
            sql &= " WHERE [DEPOT_ID]='" & row.Item("DEPOT_ID") & "'"
            sql &= " AND [PRODUCT_ID]='" & row.Item("PRODUCT_ID") & "'"
            sql &= " AND [BATCH_NUM]='" & row.Item("BATCH_NUM") & "'"
            If GetDataTable(sql).Rows.Count = 0 Then
                Dim table As String = "D_DEPOT_STOCKS"
                Dim pk() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM"}
                Dim col_val(,) As String = {{"DEPOT_ID", row.Item("DEPOT_ID")},
                    {"PRODUCT_ID", row.Item("PRODUCT_ID")},
                    {"BATCH_NUM", row.Item("BATCH_NUM")},
                    {"VALID_PERIOD", row.Item("VALID_PERIOD")},
                    {"GOOD_QTY", "0"},
                    {"RED_SHOW", "0"},
                    {"BAD_QTY", "0"},
                    {"CUS_CON_QTY", "0"},
                    {"CREATE_USER", "系統"},
                    {"CREATE_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}}
                ExecSQL(GetInsertquery(table, col_val))
                Console.WriteLine(GetInsertquery(table, col_val))
            End If

            sql = "SELECT * FROM [D_DEPOT_STOCKS_DAYS] WITH (NOLOCK) "
            sql &= " WHERE [DEPOT_ID]='" & row.Item("DEPOT_ID") & "'"
            sql &= " AND [PRODUCT_ID]='" & row.Item("PRODUCT_ID") & "'"
            sql &= " AND [BATCH_NUM]='" & row.Item("BATCH_NUM") & "'"
            If GetDataTable(sql).Rows.Count = 0 Then
                Dim table As String = "D_DEPOT_STOCKS_DAYS"
                Dim pk() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM"}
                Dim col_val(,) As String = {{"DEPOT_ID", row.Item("DEPOT_ID")},
                    {"PRODUCT_ID", row.Item("PRODUCT_ID")},
                    {"BATCH_NUM", row.Item("BATCH_NUM")},
                    {"VALID_PERIOD", row.Item("VALID_PERIOD")},
                    {"EFFECTIVE_DATE", Today.AddDays(-1).ToString("yyyy/MM/dd")},
                    {"GOOD_QTY", "0"},
                    {"RED_SHOW", "0"},
                    {"BAD_QTY", "0"},
                    {"CUS_CON_QTY", "0"},
                    {"CREATE_USER", "系統"},
                    {"CREATE_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}}
                ExecSQL(GetInsertquery(table, col_val))
                Console.WriteLine(GetInsertquery(table, col_val))
            End If
        Next
    End Sub

    '借倉入庫
    '入庫單檢查庫存及虛擬庫存是否有產品資料，如無資料INSERT數量0的產品資料
    Public Sub Lend_Stock_exist(STOCK_IN_ID As String)
        Dim sql As String
        sql = "SELECT * FROM [D_LEND_STOCK_IN_DETAIL] WITH (NOLOCK) "
        sql &= "WHERE [STOCK_IN_ID] = '" & STOCK_IN_ID & "'"

        For Each row In GetDataTable(sql).Rows
            sql = "SELECT * FROM [D_LEND_DEPOT_STOCKS] WITH (NOLOCK) "
            sql &= " WHERE [DEPOT_ID]='" & row.Item("DEPOT_ID") & "'"
            sql &= " AND [PRODUCT_ID]='" & row.Item("PRODUCT_ID") & "'"
            sql &= " AND [BATCH_NUM]='" & row.Item("BATCH_NUM") & "'"
            If GetDataTable(sql).Rows.Count = 0 Then
                Dim table As String = "D_LEND_DEPOT_STOCKS"
                Dim pk() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM"}
                Dim col_val(,) As String = {{"DEPOT_ID", row.Item("DEPOT_ID")},
                    {"PRODUCT_ID", row.Item("PRODUCT_ID")},
                    {"BATCH_NUM", row.Item("BATCH_NUM")},
                    {"VALID_PERIOD", row.Item("VALID_PERIOD")},
                    {"GOOD_QTY", "0"},
                    {"RED_SHOW", "0"},
                    {"BAD_QTY", "0"},
                    {"CUS_CON_QTY", "0"},
                    {"CREATE_USER", "系統"},
                    {"CREATE_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}}
                ExecSQL(GetInsertquery(table, col_val))
            End If

            sql = "SELECT * FROM [D_LEND_DEPOT_STOCKS_DAYS] WITH (NOLOCK) "
            sql &= " WHERE [DEPOT_ID]='" & row.Item("DEPOT_ID") & "'"
            sql &= " AND [PRODUCT_ID]='" & row.Item("PRODUCT_ID") & "'"
            sql &= " AND [BATCH_NUM]='" & row.Item("BATCH_NUM") & "'"
            If GetDataTable(sql).Rows.Count = 0 Then
                Dim table As String = "D_LEND_DEPOT_STOCKS_DAYS"
                Dim pk() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM"}
                Dim col_val(,) As String = {{"DEPOT_ID", row.Item("DEPOT_ID")},
                    {"PRODUCT_ID", row.Item("PRODUCT_ID")},
                    {"BATCH_NUM", row.Item("BATCH_NUM")},
                    {"VALID_PERIOD", row.Item("VALID_PERIOD")},
                    {"EFFECTIVE_DATE", Today.AddDays(-1).ToString("yyyy/MM/dd")},
                    {"GOOD_QTY", "0"},
                    {"RED_SHOW", "0"},
                    {"BAD_QTY", "0"},
                    {"CUS_CON_QTY", "0"},
                    {"CREATE_USER", "系統"},
                    {"CREATE_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}}
                ExecSQL(GetInsertquery(table, col_val))
                Console.WriteLine(GetInsertquery(table, col_val))
            End If
        Next
    End Sub

    '隱藏不需要顯示之欄位
    Public Sub Hide_Columns(DGV As Object, cols() As String)
        For Each col In cols
            DGV.Columns(col).Visible = False
        Next
    End Sub

    'textbox_tooltips；[ctl]-放入FORM名稱；例：pf.Textbox_tooltips(ME)
    '需在textbox中[Tag]屬性中輸入限定格式(參考case)；[MaxLength]屬性中輸入限制字元數
    Public Sub Textbox_tooltips(ByVal ctl As Control)
        Dim toolTip As New ToolTip
        toolTip.ShowAlways = True
        If TypeOf ctl Is TextBox Then
            Dim textbox As TextBox = CType(ctl, TextBox)
            If textbox.Tag <> "" Then
                Select Case textbox.Tag.ToString.Split(",")(0)
                    Case "nolimit"
                        toolTip.SetToolTip(textbox, "字元限制：" & textbox.MaxLength)
                    Case "char+num"
                        toolTip.SetToolTip(textbox, "限定英數，字元限制：" & textbox.MaxLength)
                    Case "char"
                        toolTip.SetToolTip(textbox, "限定英文，字元限制：" & textbox.MaxLength)
                    Case "num"
                        toolTip.SetToolTip(textbox, "限定數字，字元限制：" & textbox.MaxLength)
                    Case "float"
                        If InStr(1, textbox.Tag.ToString, ",") > 0 Then
                            toolTip.SetToolTip(textbox, "限定浮點數，小數點至多至第" & textbox.Tag.ToString.Split(",")(1) & "位，字元限制：" & textbox.MaxLength)
                        Else
                            toolTip.SetToolTip(textbox, "限定浮點數，字元限制：" & textbox.MaxLength)
                        End If
                    Case "custom"
                        If InStr(1, textbox.Tag.ToString, ",") > 0 Then
                            toolTip.SetToolTip(textbox, textbox.Tag.ToString.Split(",")(1))
                        End If
                End Select
            End If
        ElseIf TypeOf ctl Is ComboBox Then
            Dim combobox As ComboBox = CType(ctl, ComboBox)
            combobox.DropDownStyle = ComboBoxStyle.DropDown
            'combobox.AutoCompleteMode = AutoCompleteMode.Suggest
            'combobox.AutoCompleteSource = AutoCompleteSource.ListItems
            combobox.DropDownHeight = 180
        End If
        If ctl.HasChildren Then
            For Each c As Control In ctl.Controls
                Textbox_tooltips(c)
            Next
        End If
    End Sub

    'datagridview行名稱上的tooltips
    '[dtg]-設定之DataGridView；[Column]-設定之Column
    '[type="nolimit"]-無限制；[type="char+num"]-限定英文+數字；[type="char"]-限定英文；
    '[type="num"]-限定數字；[type="float,?"]-限定浮點數，","後為小數點至多至第"?"位，也可不設定"?"，設[type="float"]
    '[type="custom,?"]-直接顯示"?"中的文字
    '[Length]-限定"Length"位元
    Public Sub Datagridview_Header_tooltips(ByVal dtg As DataGridView, ByVal Column As String, ByVal type As String, ByVal Length As Integer)
        Select Case type.ToString.Split(",")(0)
            Case "nolimit"
                dtg.Columns(Column).ToolTipText = "字元限制：" & Length
            Case "char+num"
                dtg.Columns(Column).ToolTipText = "限定英數，字元限制：" & Length
            Case "char"
                dtg.Columns(Column).ToolTipText = "限定英文，字元限制：" & Length
            Case "num"
                dtg.Columns(Column).ToolTipText = "限定數字，字元限制：" & Length
            Case "float"
                If InStr(1, type.ToString, ",") > 0 Then
                    dtg.Columns(Column).ToolTipText = "限定浮點數，小數點至多至第" & type.ToString.Split(",")(1) & "位，字元限制：" & Length
                Else
                    dtg.Columns(Column).ToolTipText = "限定浮點數，字元限制：" & Length
                End If
            Case "custom"
                If InStr(1, type.ToString, ",") > 0 Then
                    dtg.Columns(Column).ToolTipText = type.ToString.Split(",")(1)
                End If
        End Select
    End Sub

    '建立人:張玉玲
    '產生單據最新系統編號
    '[SYSID_CODE]-自動編號開頭，例："PJ"
    '[YOU_WANT]-想要的欄位，例：STOCK_IN_ID
    '[TABLE]-所在之資料表，例：D_STOCK_IN_MASTER
    '[CODE]-所在資料表所對應的PAPER_ID，例：UD_21700
    '[COMPANY_ID]-所選擇公司別之代號，自動撈出對應的COMPANY_CODE，例：HD會撈出對應H
    '如想撈出最新入庫單編號，可參考下例：
    'pf.CREATE_PAPER_ID("STOCK_IN_ID", "D_STOCK_IN_MASTER", "UD_21700", COMPANY_ID)
    Public Function CREATE_PAPER_SYSID(ByVal SYSID_CODE As String, ByVal YOU_WANT As String, ByVal TABLE As String, ByVal COMPANY_ID As String) As String
        Dim ID As String = ""
        Dim number As String
        number = "SELECT TOP 1 [" & YOU_WANT & "] FROM [" & TABLE & " ] WITH(NOLOCK) WHERE LEFT(CONVERT(varchar,[CREATE_TIME],112),6) = '" & Date.Now.ToString("yyyyMM") & "'"
        number &= "ORDER BY [" & YOU_WANT & "] DESC "
        If GetValue("M_COMPANY_BASE", "COMPANY_ID", COMPANY_ID, "COMPANY_CODE") <> "" Then
            If GetDataTable(number).Rows.Count > 0 Then
                Dim reg As New Regex("\d+")
                Dim m As Match = reg.Match(GetDataTable(number).Rows(0).Item(YOU_WANT))
                If m.Success Then
                    ID = SYSID_CODE & GetValue("M_COMPANY_BASE", "COMPANY_ID", COMPANY_ID, "COMPANY_CODE") &
                         DateTime.Now.ToString("yyMM") & (m.Value + 1).ToString.PadLeft(4, "0")
                End If
            Else
                ID = SYSID_CODE & GetValue("M_COMPANY_BASE", "COMPANY_ID", COMPANY_ID, "COMPANY_CODE") &
                     DateTime.Now.ToString("yyMM") & "0001"
            End If
        Else
            ID = ""
        End If
        Return ID
    End Function

    'Button顏色(請放在[ Button.EnabledChanged ]事件中)
    Public Sub Button_color(sender As Object, e As EventArgs)
        If sender.Enabled Then
            sender.BackColor = ColorTranslator.FromHtml("#c7e7f7")
        Else
            sender.BackColor = ColorTranslator.FromHtml("#6c9fb8")
        End If
    End Sub


    '執行SQL指令，並傳回影響筆數 RS_R70400使用 批次寫入
    Public Function SqlBulkCopy_test(SQL, number) As Integer
        '建立連接物件
        Dim cn As New SqlConnection(strConnection)
        Dim SQLBC As New SqlBulkCopy(strConnection)
        Try
            '開啟連接物件
            cn.Open()

            '新增
            SQLBC.BatchSize = 1000
            SQLBC.BulkCopyTimeout = 60
            SQLBC.DestinationTableName = "TMP_RS_R70400"

            If number = 2 Then
                SQLBC.ColumnMappings.Add("WORK_ID", "WORK_ID")
                SQLBC.ColumnMappings.Add("EMP_ID", "EMP_ID")
                SQLBC.ColumnMappings.Add("EMP_NAME", "EMP_NAME")
                SQLBC.ColumnMappings.Add("CUSTOMER_ID", "CUSTOMER_ID")
                SQLBC.ColumnMappings.Add("CUSTOMER_SHORT_NAME", "CUSTOMER_SHORT_NAME")
                SQLBC.ColumnMappings.Add("INV_POSTAL_CODE", "INV_POSTAL_CODE")
                SQLBC.ColumnMappings.Add("HI_OFFICE_CLASS", "HI_OFFICE_CLASS")
                SQLBC.ColumnMappings.Add("OFFICE_GRADE", "OFFICE_GRADE")
                SQLBC.ColumnMappings.Add("IS_KA", "IS_KA")
                SQLBC.ColumnMappings.Add("PRODUCT_CLASS_ID", "PRODUCT_CLASS_ID")
                SQLBC.ColumnMappings.Add("PRODUCT_ID", "PRODUCT_ID")
                SQLBC.ColumnMappings.Add("PRODUCT_NAME", "PRODUCT_NAME")
                SQLBC.ColumnMappings.Add("AIM_QTY", "AIM_QTY")
                SQLBC.ColumnMappings.Add("QTY", "QTY")
                SQLBC.ColumnMappings.Add("CHECK_QTY", "CHECK_QTY")
                SQLBC.ColumnMappings.Add("AIM_AMOUNT", "AIM_AMOUNT")
                SQLBC.ColumnMappings.Add("REAL_AMOUNT_UNTAXED", "REAL_AMOUNT_UNTAXED")
                SQLBC.ColumnMappings.Add("ZERO", "ZERO")
                SQLBC.ColumnMappings.Add("SHOW_TYPE", "SHOW_TYPE")
                SQLBC.ColumnMappings.Add("number1", "number1")
            End If

            If number = 3 Then
                SQLBC.ColumnMappings.Add("WORK_ID", "WORK_ID")
                SQLBC.ColumnMappings.Add("EMP_ID", "EMP_ID")
                SQLBC.ColumnMappings.Add("EMP_NAME", "EMP_NAME")
                SQLBC.ColumnMappings.Add("CUSTOMER_ID", "CUSTOMER_ID")
                SQLBC.ColumnMappings.Add("CUSTOMER_SHORT_NAME", "CUSTOMER_SHORT_NAME")
                SQLBC.ColumnMappings.Add("INV_POSTAL_CODE", "INV_POSTAL_CODE")
                SQLBC.ColumnMappings.Add("HI_OFFICE_CLASS", "HI_OFFICE_CLASS")
                SQLBC.ColumnMappings.Add("OFFICE_GRADE", "OFFICE_GRADE")
                SQLBC.ColumnMappings.Add("IS_KA", "IS_KA")
                SQLBC.ColumnMappings.Add("PRODUCT_CLASS_ID", "PRODUCT_CLASS_ID")
                SQLBC.ColumnMappings.Add("PRODUCT_ID", "PRODUCT_ID")
                SQLBC.ColumnMappings.Add("PRODUCT_NAME", "PRODUCT_NAME")
                SQLBC.ColumnMappings.Add("AIM_QTY", "AIM_QTY")
                SQLBC.ColumnMappings.Add("QTY", "QTY")
                SQLBC.ColumnMappings.Add("CHECK_QTY", "CHECK_QTY")
                SQLBC.ColumnMappings.Add("AIM_AMOUNT", "AIM_AMOUNT")
                SQLBC.ColumnMappings.Add("REAL_AMOUNT_UNTAXED", "REAL_AMOUNT_UNTAXED")
                SQLBC.ColumnMappings.Add("ZERO", "ZERO")
                SQLBC.ColumnMappings.Add("SHOW_TYPE", "SHOW_TYPE")
                SQLBC.ColumnMappings.Add("number1", "number1")
            End If

            If number = 4 Then
                SQLBC.ColumnMappings.Add("WORK_ID", "WORK_ID")
                SQLBC.ColumnMappings.Add("EMP_ID", "EMP_ID")
                SQLBC.ColumnMappings.Add("EMP_NAME", "EMP_NAME")
                SQLBC.ColumnMappings.Add("CUSTOMER_ID", "CUSTOMER_ID")
                SQLBC.ColumnMappings.Add("CUSTOMER_SHORT_NAME", "CUSTOMER_SHORT_NAME")
                SQLBC.ColumnMappings.Add("INV_POSTAL_CODE", "INV_POSTAL_CODE")
                SQLBC.ColumnMappings.Add("HI_OFFICE_CLASS", "HI_OFFICE_CLASS")
                SQLBC.ColumnMappings.Add("OFFICE_GRADE", "OFFICE_GRADE")
                SQLBC.ColumnMappings.Add("IS_KA", "IS_KA")
                SQLBC.ColumnMappings.Add("PRODUCT_CLASS_ID", "PRODUCT_CLASS_ID")
                SQLBC.ColumnMappings.Add("PRODUCT_ID", "PRODUCT_ID")
                SQLBC.ColumnMappings.Add("PRODUCT_NAME", "PRODUCT_NAME")
                SQLBC.ColumnMappings.Add("CHECK_QTY", "CHECK_QTY")
                SQLBC.ColumnMappings.Add("AIM_AMOUNT", "AIM_AMOUNT")
                SQLBC.ColumnMappings.Add("REAL_AMOUNT_UNTAXED", "REAL_AMOUNT_UNTAXED")
                SQLBC.ColumnMappings.Add("RATE", "RATE")
                SQLBC.ColumnMappings.Add("ZERO", "ZERO")
                SQLBC.ColumnMappings.Add("SHOW_TYPE", "SHOW_TYPE")
                SQLBC.ColumnMappings.Add("number1", "number1")
            End If
            SQLBC.WriteToServer(SQL)

            '建立新的SqlCommand
            'Dim cmd As New SqlCommand(SQL, cn)
            '調整等待時間
            'cmd.CommandTimeout = 300
            '執行SQL命令
            'iRowAffected = cmd.ExecuteNonQuery()
            '釋放SqlCommand所占用的資源
            cn.Dispose()
        Finally
            '關閉連接物件
            cn.Close()
        End Try
        '傳回影響筆數
        'Return iRowAffected
    End Function


End Class
