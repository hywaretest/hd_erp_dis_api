﻿Imports System.ComponentModel
Imports System.Data.SqlClient

Public Class Form1
    Dim pf As New DBFuntion
    Dim hdstocks As New HDStocksFunction
    Dim hdlendstocks As New HDLendStocksFunction
    Dim RunItem As String = ""
    Dim ReportMsg As String = ""
    Dim ScheduleStartTime As String = "00:30"
    Dim CHECK_DATE_FROM As Date
    Dim CHECK_DATE_TO As Date
    Dim Process_String As String = ""
    Dim Process_Record_String As String = ""


    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Try
            '判斷是否已經正在執行每日庫存結算
            Dim check_plan_dt As DataTable = pf.GetDB("D_STOCKS_PARAMETER,NOLOCK", {})
            If check_plan_dt.Rows.Count > 0 Then
                If pf.CheckNull(check_plan_dt.Rows(0).Item("Execute_Process")) = "1" Then
                    'pf.MyInformation("系統正在執行")
                    Label11.Text = "系統正在執行每日結算作業...請稍後..."
                    If Button1.Enabled = False Then
                        Button1.Enabled = True
                    End If
                    Exit Sub
                End If
            End If
            '變換滑鼠圖案為等待
            Me.Cursor = Cursors.WaitCursor
            Dim sql1 As String = "update [D_STOCKS_PARAMETER] SET [Execute_Process]='1'"
            pf.ExecSQL(sql1)
            Button1.Text = "開始執行每日庫存結算作業..."
            Me.Button1.Enabled = False
            Me.ProgressBar1.Minimum = 0
            BackgroundWorker1.RunWorkerAsync()
            Me.Cursor = Cursors.Default
        Catch ex As Exception
            pf.MyError(ex.ToString)
        End Try
    End Sub

    Sub CheckSchduleData()
        Try
            Me.DateTimePicker1.MaxDate = CDate(DateTime.Now.AddDays(-1).ToString("yyyy/MM/dd"))
            Me.DateTimePicker2.MaxDate = CDate(DateTime.Now.ToString("yyyy/MM/dd"))

            '取得執行時間
            Dim checkDB As DataTable = pf.GetDB("D_STOCKS_PARAMETER", {})
            If checkDB.Rows.Count > 0 Then
                Label2.Text = CInt(checkDB.Rows(0).Item("COUNT_HOUR")).ToString("D2") & ":" & CInt(checkDB.Rows(0).Item("COUNT_MINITES")).ToString("D2")
            Else
                Label2.Text = ScheduleStartTime
            End If

        Catch ex As Exception
            pf.MyError(ex.ToString)
        End Try
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles Me.Load
        CheckSchduleData()
        Me.DateTimePicker1.MinDate = "2021/05/31"
        Me.DateTimePicker2.MinDate = "2021/05/31"
        Me.Timer1.Enabled = True
    End Sub

    Private Sub BackgroundWorker1_ProgressChanged(sender As Object, e As ProgressChangedEventArgs) Handles BackgroundWorker1.ProgressChanged
        Me.ProgressBar1.Value = e.ProgressPercentage
        Me.Label10.Text = "(" & Me.ProgressBar1.Value & "/100)"
        If ReportMsg <> "" Then Me.Label6.Text += ReportMsg & vbCrLf
        If Me.ProgressBar1.Value = 100 Then
            Me.Label9.Text = "每日結算作業完成!"
            Me.Label6.Text = ""
            Me.Label11.Text = ""
            Me.Label12.Text = ""
            Timer1.Enabled = True
        Else
            Me.Label9.Text = RunItem & "執行中....."
            Me.Label11.Text = Process_String
            Me.Label12.Text = Process_Record_String
        End If
    End Sub

    Private Sub BackgroundWorker1_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles BackgroundWorker1.RunWorkerCompleted
        Try
            If (e.Error IsNot Nothing) Then
                MessageBox.Show(e.Error.Message)
            ElseIf e.Cancelled Then
                MessageBox.Show("使用者取消執行!")
            Else
                Button1.Text = "執行每日庫存結算作業"
                Button1.Enabled = True
                Timer1.Enabled = True
                'Label1.Text += "資料匯入成功！" & vbCrLf
                'Me.Panel1.VerticalScroll.Value = Panel1.VerticalScroll.Maximum
            End If
            Dim sql2 As String = "update [D_STOCKS_PARAMETER] SET [Execute_Process]='0'"
            pf.ExecSQL(sql2)
        Catch ex As Exception
            pf.MyError(ex.ToString)
        End Try

    End Sub

    Private Sub BackgroundWorker1_DoWork(sender As Object, e As DoWorkEventArgs) Handles BackgroundWorker1.DoWork
        Try
            Dim worker As BackgroundWorker = CType(sender, BackgroundWorker)
            '更新公司每日庫存
            If CheckBox1.Checked = True Then CHK_D_DEPOT_STOCK_DAYS(worker, e)
            '更新寄售每日庫存
            If CheckBox2.Checked = True Then CHK_D_CUS_CON_QTY_DAYS(worker, e)
            '更新經銷商每日庫存
            If CheckBox3.Checked = True Then CHK_D_DEALER_CON_QTY_DAYS(worker, e)
            '更新借倉每日庫存數
            If CheckBox4.Checked = True Then CHK_D_LEND_DEPOT_STOCKS_DAYS(worker, e)

        Catch ex As Exception
            pf.MyError(ex.ToString)
        End Try

    End Sub

    Private Sub Update_work(ByVal worker As BackgroundWorker, ByVal e As DoWorkEventArgs)
        Try
            '更新公司每日庫存
            Me.Label6.Text += "更新公司每日庫存..." & vbCrLf
            hdstocks.CHK_DEPOT_STOCK_DAYS()

            '更新客戶寄售每日庫存
            Me. 'Label6.Text+= "更新客戶寄售每日庫存..." & vbCrLf
            hdstocks.CHK_D_CUS_CON_QTY_DAYS()

            '更新經銷商每日庫存
            Me.Label6.Text += "更新經銷商寄售每日庫存..." & vbCrLf
            hdstocks.CHK_D_DEALER_CON_QTY_DAYS()

            ''更新當日庫存
            'hdstocks.UPDATE_D_DEPOT_STOCKS()

            '更新借倉每日庫存檔
            Me.Label6.Text += "更新借倉寄售每日庫存..." & vbCrLf
            hdlendstocks.CHK_LEND_DEPOT_STOCK_DAYS()

        Catch ex As Exception
            pf.MyError(ex.ToString)
        End Try
    End Sub

    '更新公司每日庫存
    Public Sub CHK_D_DEPOT_STOCK_DAYS(ByVal worker As BackgroundWorker, ByVal e As DoWorkEventArgs)

        Dim i As Integer = 0
        Dim j As Integer = 0
        Dim x As Integer = 0
        Dim iRowAffected As Integer = 0
        Dim table As String = "D_DEPOT_STOCKS_DAYS"
        Dim xRow As DataRow
        Dim SaveOK As Boolean = True
        Dim sSql As String = ""
        Dim sSql2 As String = ""
        Dim sSql3 As String = ""
        Dim sSql4 As String = ""
        Dim percentComplete
        Dim len As Integer = 0
        Dim LastStocksDaTe As DateTime = DateTime.Now
        Dim CHECK_DATE As DateTime = DateTime.Now
        Dim SaveComplete As Boolean = True
        RunItem = "結算公司庫存每日檔"
        '開啟連線
        Dim conn_str As String
        conn_str = My.Settings.HD_ERP_DBConnection
        Dim conn As New SqlConnection(conn_str)

        '開啟連接物件
        conn.Open()

        '啟用交易紀錄
        Dim transaction = conn.BeginTransaction

        Try
            Dim Command = conn.CreateCommand
            Command.Connection = conn
            Command.Transaction = transaction

            '產生庫存每日檔資料
            sSql = ""
            Dim count_day As Integer = 0
            LastStocksDaTe = Me.DateTimePicker1.Value
            CHECK_DATE = Me.DateTimePicker2.Value
            Dim Depot_Id As String = ""
            Dim Product_Id As String = ""
            Dim Batch_Num As String = ""
            Dim where(,) As String = {}

            Dim count_date As DateTime = LastStocksDaTe
            Dim diff = DateDiff(DateInterval.Day, CDate(LastStocksDaTe.ToString("yyyy/MM/dd")), CDate(CHECK_DATE.AddDays(-1).ToString("yyyy/MM/dd")))
            Do While count_date.ToString("yyyy/MM/dd") <= CHECK_DATE.ToString("yyyy/MM/dd")
                sSql = "SELECT * FROM [D_DEPOT_STOCKS_DAYS] WITH (NOLOCK)"
                sSql &= " WHERE Convert(varchar, [EFFECTIVE_DATE], 111) ='" & count_date.AddDays(-1).ToString("yyyy/MM/dd") & "'"
                sSql &= " ORDER BY [DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                'sSql = "  SELECT [DEPOT_ID],[PRODUCT_ID],[BATCH_NUM],MAX([EFFECTIVE_DATE]) AS EFFECTIVE_DATE"
                'sSql &= " From [D_DEPOT_STOCKS_DAYS] WITH (NOLCOK) GROUP BY [DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                'sSql &= " HAVING MAX([EFFECTIVE_DATE])<'" & count_date.AddDays(-1).ToString("yyyy/MM/dd") & "'"
                Console.WriteLine(sSql)
                Dim company_stocks_dt As DataTable = pf.GetDataTable(sSql)
                'ReportMsg = sSql
                'len = dt.Rows.Count * diff
                len = company_stocks_dt.Rows.Count
                For x = 0 To company_stocks_dt.Rows.Count - 1
                    Console.WriteLine(i.ToString)
                    Process_String = "執行日期：" & count_date.ToString("yyyy/MM/dd") & " 每日庫存結算作業"
                    Process_Record_String = "(" & x & " / " & len & ")"
                    percentComplete = CInt((x / len) * 100)
                    worker.ReportProgress(percentComplete)
                    xRow = company_stocks_dt.Rows(x)
                    ReportMsg = x

                    Depot_Id = xRow.Item("DEPOT_ID").ToString
                    Product_Id = xRow.Item("PRODUCT_ID").ToString
                    Batch_Num = xRow.Item("BATCH_NUM").ToString

                    '判斷資料是否存在
                    sSql2 = ""
                    sSql2 = "SELECT * FROM [D_DEPOT_STOCKS_DAYS] WITH (NOLOCK)"
                    sSql2 &= " WHERE CONVERT(varchar, [EFFECTIVE_DATE], 111) ='" & count_date.ToString("yyyy/MM/dd") & "'"
                    sSql2 &= " AND [DEPOT_ID]='" & Depot_Id & "'"
                    sSql2 &= " AND [PRODUCT_ID]='" & Product_Id & "'"
                    sSql2 &= " AND [BATCH_NUM]='" & Batch_Num & "'"
                    Dim chk_data As New DataTable
                    Console.WriteLine(sSql2)
                    chk_data = pf.GetDataTable(sSql2)
                    where = {
                        {"CUSTOM_SQL", " CONVERT(varchar, [EFFECTIVE_DATE], 111) ='" & count_date.ToString("yyyy/MM/dd") & "'"},
                        {"DEPOT_ID", Depot_Id},
                        {"PRODUCT_ID", Product_Id},
                        {"BATCH_NUM", Batch_Num}
                    }
                    chk_data = pf.GetDB("D_DEPOT_STOCKS_DAYS,NOLOCK", where)
                    If chk_data.Rows.Count = 0 Then
                        Dim col_value(,) As String = {
                            {"DEPOT_ID", Depot_Id},
                            {"PRODUCT_ID", Product_Id},
                            {"BATCH_NUM", Batch_Num},
                            {"EFFECTIVE_DATE", count_date.ToString("yyyy/MM/dd")},
                            {"GOOD_QTY", xRow.Item("GOOD_QTY")},
                            {"BAD_QTY", xRow.Item("BAD_QTY")},
                            {"CUS_CON_QTY", xRow.Item("CUS_CON_QTY")},
                            {"COST_PRICE", xRow.Item("COST_PRICE")},
                            {"UNIT_PRICE", xRow.Item("UNIT_PRICE")},
                            {"VALID_PERIOD", xRow.Item("VALID_PERIOD")},
                            {"RED_SHOW", xRow.Item("RED_SHOW")},
                            {"INV_QTY", xRow.Item("INV_QTY")},
                            {"INV_BAD_QTY", xRow.Item("INV_BAD_QTY")},
                            {"DEPOT_QTY", xRow.Item("DEPOT_QTY")},
                            {"SALE_QTY", xRow.Item("SALE_QTY")},
                            {"CREATE_USER", "SYSTEM"},
                            {"CREATE_TIME", DateTime.Now.ToString("yyyy/MM/dd HH: mm:ss")}
                        }
                        sSql2 = pf.GetInsertquery(table, col_value)
                        Console.WriteLine(sSql2)
                        Command.CommandText = sSql2
                        iRowAffected = Command.ExecuteNonQuery()
                        If iRowAffected = 0 Then
                            transaction.Rollback()
                            SaveComplete = False
                            Exit Sub
                        End If
                        len = len + 1
                    End If

                    If SaveComplete = True AndAlso hdstocks.UPDATE_D_DEPOT_STOCKS_DAYS_BY_DPB(count_date, Depot_Id, Product_Id, Batch_Num, Command) = False Then
                        hdstocks.D_STOCKS_SETTLEMENT(count_date, "1", "2")
                        transaction.Rollback()
                        SaveComplete = False
                        Exit Sub
                    End If

                    '生效日小於計算日要從生效日重新計算庫存數直到計算日
                    Dim PASS_CHECK_DATE As DateTime
                    If SaveComplete = True Then
                        Dim k As Integer = 0
                        sSql = "SELECT * FROM [D_DEPOT_STOCKS_DIFF] WITH (NOLOCK)"
                        sSql &= " WHERE CONVERT(VARCHAR,[EFFECTIVE_DATE],111)<'" & count_date.ToString("yyyy/MM/dd") & "'"
                        sSql &= " AND CONVERT(VARCHAR,[CHANGE_DATE],111)='" & count_date.ToString("yyyy/MM/dd") & "'"
                        sSql &= " AND [DEPOT_ID]='" & Depot_Id & "'"
                        sSql &= " AND [PRODUCT_ID]='" & Product_Id & "'"
                        sSql &= " AND [BATCH_NUM]='" & Batch_Num & "'"
                        sSql &= " ORDER BY [EFFECTIVE_DATE]"
                        Console.WriteLine(sSql)
                        Dim other_dt As New DataTable
                        'other_dt = pf.GetDataTable(sSql)
                        where = {
                           {"CUSTOM_SQL", " CONVERT(varchar, [EFFECTIVE_DATE], 111) <'" & count_date.ToString("yyyy/MM/dd") & "'"},
                           {"CUSTOM_SQL", " CONVERT(varchar, [EFFECTIVE_DATE], 111) ='" & count_date.ToString("yyyy/MM/dd") & "'"},
                            {"DEPOT_ID", Depot_Id},
                            {"PRODUCT_ID", Product_Id},
                            {"BATCH_NUM", Batch_Num}
                        }
                        other_dt = pf.GetDB("D_DEPOT_STOCKS_DIFF,NOLOCK", where)
                        If other_dt.Rows.Count > 0 Then
                            For k = 0 To other_dt.Rows.Count - 1
                                PASS_CHECK_DATE = CDate(other_dt.Rows(k).Item("EFFECTIVE_DATE")).ToString("yyyy/MM/dd")
                                While PASS_CHECK_DATE.ToString("yyyy/MM/dd") < count_date.ToString("yyyy/MM/dd")
                                    '檢查D_DEPOT_STOCKS_DAYS是否有前一天的記錄，如果沒有自動新增一筆記錄
                                    sSql = "SELECT * ,(SELECT TOP 1 [VALID_PERIOD] from [P_SHIPPED_GOODS_DETAIL] WHERE [DEPOT_ID]=[D_DEPOT_STOCKS_DIFF].[DEPOT_ID] and [PRODUCT_ID]=[D_DEPOT_STOCKS_DIFF].[PRODUCT_ID] and [BATCH_NUM]=[D_DEPOT_STOCKS_DIFF].[BATCH_NUM]) AS [VALID_PERIOD]"
                                    sSql &= " FROM [D_DEPOT_STOCKS_DIFF] WITH (NOLOCK) "
                                    'sSql &= " INNER Join [D_DEPOT_BASE] On [D_DEPOT_BASE].[DEPOT_ID]=[D_DEPOT_STOCKS_DIFF].[DEPOT_ID] And [D_DEPOT_BASE].[DEPOT_TYPE]='公司'"
                                    sSql &= " WHERE CONVERT(VARCHAR,[EFFECTIVE_DATE],111)='" & PASS_CHECK_DATE.ToString("yyyy/MM/dd") & "'"
                                    sSql &= " AND CONVERT(VARCHAR,[CHANGE_DATE],111)='" & count_date.ToString("yyyy/MM/dd") & "'"
                                    Console.WriteLine(sSql)
                                    Dim check_dt1 As DataTable = pf.GetDataTable(sSql)
                                    For Each check_row In check_dt1.Rows
                                        Dim check_where(,) As String = {
                                            {"DEPOT_ID", check_row.Item("DEPOT_ID")},
                                            {"PRODUCT_ID", check_row.Item("PRODUCT_ID")},
                                            {"BATCH_NUM", check_row.Item("BATCH_NUM")},
                                            {"EFFECTIVE_DATE", PASS_CHECK_DATE.AddDays(-1).ToString("yyyy/MM/dd")}
                                        }
                                        Dim check_dt2 As DataTable = pf.GetDB("D_DEPOT_STOCKS_DAYS,NOLOCK", check_where)
                                        If check_dt2.Rows.Count = 0 Then
                                            Dim Valid_Date As Date
                                            If pf.CheckDateTimeNull(check_row.item("VALID_PERIOD")) = "" Then
                                                check_where = {
                                                    {"DEPOT_ID", check_row.Item("DEPOT_ID")},
                                                    {"PRODUCT_ID", check_row.Item("PRODUCT_ID")},
                                                    {"BATCH_NUM", check_row.Item("BATCH_NUM")},
                                                    {"CUSTOM_SQL", " ISNULL(VALID_PERIOD,'')<>''"}
                                                }
                                                Dim check_dt3 As DataTable = pf.GetDB("D_DEPOT_STOCKS_DAYS,NOLOCK", check_where)
                                                If check_dt3.Rows.Count > 0 Then
                                                    Valid_Date = CDate(check_dt3.Rows(0).Item("VALID_PERIOD")).ToString("yyyy/MM/dd")
                                                End If
                                            Else
                                                Valid_Date = CDate(check_row.item("VALID_PERIOD")).ToString("yyyy/MM/dd")
                                            End If
                                            Dim Insert_Data(,) As String = {
                                                {"DEPOT_ID", check_row.Item("DEPOT_ID")},
                                                {"PRODUCT_ID", check_row.Item("PRODUCT_ID")},
                                                {"BATCH_NUM", check_row.Item("BATCH_NUM")},
                                                {"EFFECTIVE_DATE", PASS_CHECK_DATE.AddDays(-1).ToString("yyyy/MM/dd")},
                                                {"VALID_PERIOD", Valid_Date},
                                                {"GOOD_QTY", "0"},
                                                {"BAD_QTY", "0"},
                                                {"INV_QTY", "0"},
                                                {"INV_BAD_QTY", "0"},
                                                {"DEPOT_QTY", "0"},
                                                {"SALE_QTY", "0"}
                                            }
                                            Dim sSql6 As String = pf.GetInsertquery("D_DEPOT_STOCKS_DAYS", Insert_Data)
                                            Console.WriteLine(sSql6)
                                            Command.CommandText = sSql6
                                            iRowAffected = Command.ExecuteNonQuery()
                                            If iRowAffected = 0 Then
                                                transaction.Rollback()
                                                SaveComplete = False
                                                Exit Sub
                                            End If
                                        End If
                                    Next
                                    '建立生效日的每日庫存的資料並且計算
                                    'If UPDATE_D_DEPOT_STOCKS_DAYS(other_dt.Rows(k).Item("EFFECTIVE_DATE"), command) = False Then
                                    If SaveComplete = True AndAlso hdstocks.UPDATE_D_DEPOT_STOCKS_DAYS_BY_DPB(PASS_CHECK_DATE, other_dt.Rows(k).Item("DEPOT_ID"), other_dt.Rows(k).Item("PRODUCT_ID"), other_dt.Rows(k).Item("BATCH_NUM"), Command) = False Then
                                        '2019/08/26寫入庫存結算紀錄檔
                                        hdstocks.D_STOCKS_SETTLEMENT(PASS_CHECK_DATE, "1", "2")
                                        transaction.Rollback()
                                        SaveComplete = False
                                        Exit Sub
                                    End If
                                    PASS_CHECK_DATE = PASS_CHECK_DATE.AddDays(1).ToString("yyyy/MM/dd")
                                    Console.WriteLine(PASS_CHECK_DATE.ToString("yyyy/MM/dd"))
                                End While
                            Next
                        End If
                    End If
                    '2019/08/26寫入庫存結算紀錄檔
                    If SaveComplete = True Then
                        hdstocks.D_STOCKS_SETTLEMENT(count_date, "1", "1")
                    End If
                Next
                count_day = count_day + 1
                count_date = LastStocksDaTe.AddDays(count_day)
            Loop

            'If SaveComplete = True Then

            'End If
            transaction.Commit()
            Command.Dispose()

            percentComplete = 100
            worker.ReportProgress(percentComplete)

        Catch ex As Exception
            pf.MyError(ex.ToString)
            transaction.Rollback()
            Exit Sub
        Finally
            conn.Close()
        End Try
    End Sub

    '客戶寄售庫存每日檔
    Public Sub CHK_D_CUS_CON_QTY_DAYS(ByVal worker As BackgroundWorker, ByVal e As DoWorkEventArgs)
        Dim i As Integer = 0
        Dim j As Integer = 0
        Dim x As Integer = 0
        Dim iRowAffected As Integer = 0
        Dim table As String = "D_CUS_CON_QTY_DAYS"
        Dim xRow As DataRow
        Dim SaveOK As Boolean = True
        Dim sSql As String = ""
        Dim sSql2 As String = ""
        Dim sSql3 As String = ""
        Dim sSql4 As String = ""
        Dim percentComplete
        Dim cus_len As Integer = 0
        Dim CHECK_DATE As DateTime = DateTime.Now
        Dim Customer_Id As String = ""
        Dim Depot_Id As String = ""
        Dim Product_Id As String = ""
        Dim Batch_Num As String = ""
        Dim where(,) As String = {}

        '開啟連線
        Dim conn_str As String
        conn_str = My.Settings.HD_ERP_DBConnection
        Dim conn As New SqlConnection(conn_str)

        '開啟連接物件
        conn.Open()

        '啟用交易紀錄
        Dim transaction = conn.BeginTransaction

        Try
            Dim Command = conn.CreateCommand
            Command.Connection = conn
            Command.Transaction = transaction
            RunItem = "客戶寄售庫存每日檔"
            '產生庫存每日檔資料
            sSql = ""
            Dim count_date As DateTime = Me.DateTimePicker1.Value
            CHECK_DATE = Me.DateTimePicker2.Value
            Dim diff = DateDiff(DateInterval.Day, CDate(count_date.ToString("yyyy/MM/dd")), CDate(CHECK_DATE.AddDays(-1).ToString("yyyy/MM/dd")))

            Do While count_date.ToString("yyyy/MM/dd") <= CHECK_DATE.ToString("yyyy/MM/dd")
                'ReportMsg = "D_CUS_CON_QTY_DAYS-" & count_date.ToString("yyyy/MM/dd") & "-" & CHECK_DATE.AddDays(-1).ToString("yyyy/MM/dd")

                'sSql = "SELECT * FROM [D_CUS_CON_QTY_DAYS] WITH (NOLOCK)"
                'sSql &= " WHERE Convert(varchar, [EFFECTIVE_DATE], 111) ='" & count_date.AddDays(-1).ToString("yyyy/MM/dd") & "'"
                'sSql &= " ORDER BY [CUSTOMER_ID],[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                'Dim dt As DataTable = pf.GetDataTable(sSql)
                where = {
                    {"CUSTOM_SQL", " Convert(varchar, [EFFECTIVE_DATE], 111) ='" & count_date.AddDays(-1).ToString("yyyy/MM/dd") & "'"},
                    {"ORDERBY_ASC", "CUSTOMER_ID"}, {"ORDERBY_ASC", "DEPOT_ID"},
                    {"ORDERBY_ASC", "PRODUCT_ID"}, {"ORDERBY_ASC", "BATCH_NUM"}
                }
                Dim cus_stocks_dt As DataTable = pf.GetDB("D_CUS_CON_QTY_DAYS,NOLOCK", where)

                cus_len = cus_stocks_dt.Rows.Count
                For x = 0 To cus_stocks_dt.Rows.Count - 1
                    Process_String = "執行日期：" & count_date.ToString("yyyy/MM/dd") & " 寄售每日庫存結算作業"
                    Process_Record_String = "(" & x & " / " & cus_len & ")"
                    percentComplete = CInt((x / cus_len) * 100)
                    worker.ReportProgress(percentComplete)
                    xRow = cus_stocks_dt.Rows(x)

                    Customer_Id = xRow.Item("CUSTOMER_ID").ToString
                    Depot_Id = xRow.Item("DEPOT_ID").ToString
                    Product_Id = xRow.Item("PRODUCT_ID").ToString
                    Batch_Num = xRow.Item("BATCH_NUM").ToString

                    If SaveOK = True AndAlso hdstocks.UPDATE_D_CUS_CON_QTY_DAYS_BY_CDPB(count_date.ToString("yyyy/MM/dd"), Customer_Id, Depot_Id, Product_Id, Batch_Num, Command) = False Then
                        transaction.Rollback()
                        SaveOK = False
                        Exit Sub
                    End If
                    If SaveOK = True Then
                        Dim k As Integer = 0
                        '生效日小於計算日要從生效日重新計算庫存數直到計算日
                        sSql = "SELECT [EFFECTIVE_DATE] FROM [D_DEPOT_STOCKS_DIFF] WITH (NOLOCK)"
                        sSql &= " WHERE CONVERT(VARCHAR,[EFFECTIVE_DATE],111)<'" & count_date.ToString("yyyy/MM/dd") & "'"
                        sSql &= " AND CONVERT(VARCHAR,[CHANGE_DATE],111)='" & count_date.ToString("yyyy/MM/dd") & "'"
                        sSql &= " AND ISNULL([CUS_DIFF_QTY],0)<>'0'"
                        sSql &= " AND [CUSTOMER_ID]='" & Customer_Id & "'"
                        sSql &= " AND [DEPOT_ID]='" & Depot_Id & "'"
                        sSql &= " AND [PRODUCT_ID]='" & Product_Id & "'"
                        sSql &= " AND [BATCH_NUM]='" & Batch_Num & "'"
                        sSql &= " GROUP BY [EFFECTIVE_DATE]"
                        sSql &= " ORDER BY [EFFECTIVE_DATE]"
                        Dim cus_other_dt As New DataTable
                        cus_other_dt = pf.GetDataTable(sSql)
                        Dim Check_Pass_Date As DateTime = DateTime.Now
                        If cus_other_dt.Rows.Count > 0 Then
                            For k = 0 To cus_other_dt.Rows.Count - 1
                                Check_Pass_Date = cus_other_dt.Rows(k).Item("EFFECTIVE_DATE")
                                While Check_Pass_Date <= count_date.ToString("yyyy/MM/dd")
                                    '建立生效日的每日庫存的資料並且計算
                                    If SaveOK = True AndAlso hdstocks.UPDATE_D_CUS_CON_QTY_DAYS_BY_CDPB(Check_Pass_Date, Customer_Id, Depot_Id, Product_Id, Batch_Num, Command) = False Then
                                        transaction.Rollback()
                                        SaveOK = False
                                        Exit Sub
                                    End If
                                    Check_Pass_Date = Check_Pass_Date.AddDays(1)
                                End While

                            Next
                        End If
                    End If
                Next
                count_date = count_date.AddDays(1)
            Loop

            transaction.Commit()
            Command.Dispose()

            percentComplete = 100
            worker.ReportProgress(percentComplete)

        Catch ex As Exception
            pf.MyError(ex.ToString)
            transaction.Rollback()
            Exit Sub
        Finally
            conn.Close()
        End Try
    End Sub

    '經銷商每日庫存檔
    Public Sub CHK_D_DEALER_CON_QTY_DAYS(ByVal worker As BackgroundWorker, ByVal e As DoWorkEventArgs)
        Dim i As Integer = 0
        Dim j As Integer = 0
        Dim x As Integer = 0
        Dim iRowAffected As Integer = 0
        Dim table As String = "D_DEALER_CON_QTY_DAYS"
        Dim xRow As DataRow
        Dim SaveOK As Boolean = True
        Dim sSql As String = ""
        Dim sSql2 As String = ""
        Dim sSql3 As String = ""
        Dim sSql4 As String = ""
        Dim percentComplete
        Dim supplier_len As Integer = 0
        Dim CHECK_DATE As DateTime = DateTime.Now
        Dim Supplier_Id As String = ""
        Dim Depot_Id As String = ""
        Dim Product_Id As String = ""
        Dim Batch_Num As String = ""
        Dim where(,) As String = {}

        '開啟連線
        Dim conn_str As String
        conn_str = My.Settings.HD_ERP_DBConnection
        Dim conn As New SqlConnection(conn_str)

        '開啟連接物件
        conn.Open()

        '啟用交易紀錄
        Dim transaction = conn.BeginTransaction

        Try
            Dim Command = conn.CreateCommand
            Command.Connection = conn
            Command.Transaction = transaction
            RunItem = "經銷商每日庫存檔"
            '產生庫存每日檔資料
            Dim LastStocksDaTe As DateTime = Me.DateTimePicker1.Value
            CHECK_DATE = Me.DateTimePicker2.Value
            Dim count_date As DateTime = LastStocksDaTe
            Dim diff = DateDiff(DateInterval.Day, CDate(LastStocksDaTe.ToString("yyyy/MM/dd")), CDate(CHECK_DATE.AddDays(-1).ToString("yyyy/MM/dd")))
            Do While count_date.ToString("yyyy/MM/dd") <= CHECK_DATE.ToString("yyyy/MM/dd")

                ReportMsg = "D_DEALER_CON_QTY_DAYS-" & count_date.ToString("yyyy/MM/dd") & "-" & CHECK_DATE.AddDays(-1).ToString("yyyy/MM/dd")

                where = {
                    {"CUSTOM_SQL", " Convert(varchar, [EFFECTIVE_DATE], 111) ='" & count_date.AddDays(-1).ToString("yyyy/MM/dd") & "'"},
                    {"ORDERBY_ASC", "SUPPLIER_ID"}, {"ORDERBY_ASC", "DEPOT_ID"}, {"ORDERBY_ASC", "PRODUCT_ID"}, {"ORDERBY_ASC", "BATCH_NUM"}
                }
                Dim supplier_stocks_dt As DataTable = pf.GetDB("D_DEALER_CON_QTY_DAYS,NOLOCK", where)
                'len = dt.Rows.Count * diff
                supplier_len = supplier_stocks_dt.Rows.Count
                For x = 0 To supplier_stocks_dt.Rows.Count - 1
                    Process_String = "執行日期：" & count_date.ToString("yyyy/MM/dd") & " 經銷商每日庫存結算作業"
                    Process_Record_String = "(" & x & " / " & supplier_len & ")"
                    percentComplete = CInt((x / supplier_len) * 100)
                    worker.ReportProgress(percentComplete)
                    xRow = supplier_stocks_dt.Rows(x)

                    Supplier_Id = xRow.Item("SUPPLIER_ID").ToString
                    Depot_Id = xRow.Item("DEPOT_ID").ToString
                    Product_Id = xRow.Item("PRODUCT_ID").ToString
                    Batch_Num = xRow.Item("BATCH_NUM").ToString

                    If SaveOK = True AndAlso hdstocks.UPDATE_D_DEALER_CON_QTY_DAYS_BY_SDPB(count_date, Supplier_Id, Depot_Id, Product_Id, Batch_Num, Command) = False Then
                        transaction.Rollback()
                        SaveOK = False
                        Exit Sub
                    End If
                    If SaveOK = True Then
                        Dim k As Integer = 0
                        '生效日小於計算日要從生效日重新計算庫存數直到計算日
                        sSql = "SELECT [SUPPLIER_ID],[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM],[EFFECTIVE_DATE] FROM [D_DEPOT_STOCKS_DIFF] WITH (NOLOCK)"
                        sSql &= " WHERE CONVERT(VARCHAR,[EFFECTIVE_DATE],111)<'" & count_date.ToString("yyyy/MM/dd") & "'"
                        sSql &= " AND CONVERT(VARCHAR,[CHANGE_DATE],111)='" & count_date.ToString("yyyy/MM/dd") & "'"
                        sSql &= " AND SUPPLIER_ID='" & Supplier_Id & "'"
                        sSql &= " AND DEPOT_ID='" & Depot_Id & "'"
                        sSql &= " AND PRODUCT_ID='" & Product_Id & "'"
                        sSql &= " AND BATCH_NUM='" & Batch_Num & "'"
                        'sSql &= " AND ISNULL([SUPPLIER_DIFF_QTY],0)<>'0'"
                        sSql &= " GROUP BY [SUPPLIER_ID],[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM],[EFFECTIVE_DATE]"
                        sSql &= " ORDER BY [EFFECTIVE_DATE]"
                        Dim other_dt As New DataTable
                        other_dt = pf.GetDataTable(sSql)
                        Dim Check_Pass_Date As DateTime = DateTime.Now
                        If other_dt.Rows.Count > 0 Then
                            For k = 0 To other_dt.Rows.Count - 1
                                Check_Pass_Date = other_dt.Rows(k).Item("EFFECTIVE_DATE")
                                While Check_Pass_Date.ToString("yyyy/MM/dd") <= count_date.ToString("yyyy/MM/dd")
                                    '建立生效日的每日庫存的資料並且計算
                                    If SaveOK = True AndAlso hdstocks.UPDATE_D_DEALER_CON_QTY_DAYS_BY_SDPB(other_dt.Rows(k).Item("EFFECTIVE_DATE"), Supplier_Id, Depot_Id, Product_Id, Batch_Num, Command) = False Then
                                        transaction.Rollback()
                                        SaveOK = False
                                        Exit Sub
                                    End If
                                    Check_Pass_Date = Check_Pass_Date.AddDays(1)
                                End While

                            Next
                        End If
                    End If
                Next
                count_date = count_date.AddDays(1)
            Loop

            transaction.Commit()
            Command.Dispose()

            percentComplete = 100
            worker.ReportProgress(percentComplete)

        Catch ex As Exception
            pf.MyError(ex.ToString)
            transaction.Rollback()
            Exit Sub
        Finally
            conn.Close()
        End Try

    End Sub

    '借倉庫存每日檔
    Public Sub CHK_D_LEND_DEPOT_STOCKS_DAYS(ByVal worker As BackgroundWorker, ByVal e As DoWorkEventArgs)

        Dim i As Integer = 0
        Dim j As Integer = 0
        Dim x As Integer = 0
        Dim iRowAffected As Integer = 0
        Dim table As String = "D_LEND_DEPOT_STOCKS_DAYS"
        Dim xRow As DataRow
        Dim SaveOK As Boolean = True
        Dim sSql As String = ""
        Dim sSql2 As String = ""
        Dim sSql3 As String = ""
        Dim sSql4 As String = ""
        Dim percentComplete
        Dim lend_stocks_dt_len As Integer = 0
        Dim CHECK_DATE As DateTime = DateTime.Now
        Dim Depot_Id As String = ""
        Dim Product_Id As String = ""
        Dim Batch_Num As String = ""
        Dim Valid_Period As DateTime = DateTime.Now
        Dim where(,) As String = {}
        RunItem = "結算借倉庫存每日檔"
        '開啟連線
        Dim conn_str As String
        conn_str = My.Settings.HD_ERP_DBConnection
        Dim conn As New SqlConnection(conn_str)

        '開啟連接物件
        conn.Open()

        '啟用交易紀錄
        Dim transaction = conn.BeginTransaction

        Try
            Dim Command = conn.CreateCommand
            Command.Connection = conn
            Command.Transaction = transaction

            '產生庫存每日檔資料
            sSql = ""
            Dim count_date As DateTime = Me.DateTimePicker1.Value
            CHECK_DATE = Me.DateTimePicker2.Value
            Dim diff = DateDiff(DateInterval.Day, CDate(count_date.ToString("yyyy/MM/dd")), CDate(CHECK_DATE.AddDays(-1).ToString("yyyy/MM/dd")))
            Do While count_date.ToString("yyyy/MM/dd") <= CHECK_DATE.ToString("yyyy/MM/dd")

                sSql = "SELECT * FROM [D_LEND_DEPOT_STOCKS_DAYS] WITH (NOLOCK)"
                sSql &= " WHERE Convert(varchar, [EFFECTIVE_DATE], 111) ='" & count_date.AddDays(-1).ToString("yyyy/MM/dd") & "'"
                sSql &= " ORDER BY [DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                Dim dt As DataTable = pf.GetDataTable(sSql)
                where = {
                    {"CUSTOM_SQL", " Convert(varchar, [EFFECTIVE_DATE], 111) ='" & count_date.AddDays(-1).ToString("yyyy/MM/dd") & "'"},
                    {"ORDERBY_ASC", "DEPOT_ID"}, {"ORDERBY_ASC", "PRODUCT_ID"}, {"ORDERBY_ASC", "PRODUCT_ID"}
                }
                Dim lend_stocks_dt As DataTable = pf.GetDataTable(sSql)
                lend_stocks_dt_len = lend_stocks_dt.Rows.Count

                For i = 0 To lend_stocks_dt.Rows.Count - 1

                    Process_String = "執行日期：" & count_date.ToString("yyyy/MM/dd") & " 借倉每日庫存結算作業"
                    Process_Record_String = "(" & i & " / " & lend_stocks_dt_len & ")"
                    percentComplete = CInt((i / lend_stocks_dt_len) * 100)
                    worker.ReportProgress(percentComplete)
                    xRow = lend_stocks_dt.Rows(i)

                    Depot_Id = xRow.Item("DEPOT_ID").ToString
                    Product_Id = xRow.Item("PRODUCT_ID").ToString
                    Batch_Num = xRow.Item("BATCH_NUM").ToString
                    Valid_Period = xRow.Item("VALID_PERIOD")

                    '建立生效日的每日庫存的資料並且計算
                    If SaveOK = True AndAlso hdlendstocks.UPDATE_D_LEND_DEPOT_STOCKS_DAYS_BY_DPB(count_date, Depot_Id, Product_Id, Batch_Num, Valid_Period, Command) = False Then
                        'D_STOCKS_SETTLEMENT(chk_date, "1", "2")
                        transaction.Rollback()
                        SaveOK = False
                        Exit Sub
                    End If
                    'If UPDATE_D_LEND_DEPOT_STOCKS_DAYS(chk_date, command) = False Then
                    '    'D_STOCKS_SETTLEMENT(chk_date, "1", "2")
                    '    transaction.Rollback()
                    '    SaveComplete = False
                    '    Return SaveComplete
                    'End If

                    If SaveOK = True Then
                        Dim k As Integer = 0
                        '生效日小於計算日要從生效日重新計算庫存數直到計算日
                        sSql = "SELECT [DEPOT_ID],[PRODUCT_ID],[BATCH_NUM],[EFFECTIVE_DATE] FROM [D_LEND_DEPOT_STOCKS_DIFF] WITH (NOLOCK)"
                        sSql &= " WHERE CONVERT(VARCHAR,[EFFECTIVE_DATE],111)<'" & count_date.ToString("yyyy/MM/dd") & "'"
                        sSql &= " AND CONVERT(VARCHAR,[CHANGE_DATE],111)='" & count_date.ToString("yyyy/MM/dd") & "'"
                        sSql &= " AND [DEPOT_ID]='" & Depot_Id & "'"
                        sSql &= " AND [PRODUCT_ID]='" & Product_Id & "'"
                        sSql &= " AND [BATCH_NUM]='" & Batch_Num & "'"
                        sSql &= " GROUP BY [DEPOT_ID],[PRODUCT_ID],[BATCH_NUM],[EFFECTIVE_DATE]"
                        sSql &= " ORDER BY [EFFECTIVE_DATE]"
                        Console.WriteLine(sSql)
                        Dim other_dt As New DataTable
                        other_dt = pf.GetDataTable(sSql)
                        If other_dt.Rows.Count > 0 Then
                            For k = 0 To other_dt.Rows.Count - 1
                                '取得VALID_PERIOD有效日期
                                Dim qdt2 As DataTable = pf.GetDB("D_LEND_DEPOT_STOCKS,NOLOCK", {{"DEPOT_ID", other_dt.Rows(k).Item("DEPOT_ID")}, {"PRODUCT_ID", other_dt.Rows(k).Item("PRODUCT_ID")}, {"BATCH_NUM", other_dt.Rows(k).Item("BATCH_NUM")}})
                                Dim q_valid_period As Date
                                If qdt2.Rows.Count > 0 Then
                                    q_valid_period = qdt2.Rows(0).Item("VALID_PERIOD")
                                End If
                                Dim chk_other_date As Date = other_dt.Rows(k).Item("EFFECTIVE_DATE")
                                While chk_other_date.ToString("yyyy/MM/dd") <= count_date.ToString("yyyy/MM/dd")
                                    '建立生效日的每日庫存的資料並且計算
                                    If SaveOK = True AndAlso hdlendstocks.UPDATE_D_LEND_DEPOT_STOCKS_DAYS_BY_DPB(chk_other_date, Depot_Id, Product_Id, Batch_Num, q_valid_period, Command) = False Then
                                        '2019/08/26寫入庫存結算紀錄檔
                                        'D_STOCKS_SETTLEMENT(chk_date, "1", "2")
                                        transaction.Rollback()
                                        SaveOK = False
                                        Exit Sub
                                    End If
                                    chk_other_date = chk_other_date.AddDays(1)
                                End While

                                'If UPDATE_D_LEND_DEPOT_STOCKS_DAYS(other_dt.Rows(k).Item("EFFECTIVE_DATE"), command) = False Then
                                '    '2019/08/26寫入庫存結算紀錄檔
                                '    'D_STOCKS_SETTLEMENT(chk_date, "1", "2")
                                '    transaction.Rollback()
                                '    SaveComplete = False
                                '    Return SaveComplete
                                'End If
                            Next
                        End If
                    End If
                Next
                count_date = count_date.AddDays(1)
            Loop


            transaction.Commit()
            Command.Dispose()

            percentComplete = 100
            worker.ReportProgress(percentComplete)

        Catch ex As Exception
            pf.MyError(ex.ToString)
            transaction.Rollback()
            Exit Sub
        Finally
            conn.Close()
        End Try
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        Try
            CheckSchduleData()
            '檢視目前時間與Lable2
            Dim Now_Time As String = DateTime.Now.ToString("HH:mm")
            Console.WriteLine(Now_Time)
            If Button1.Enabled = True AndAlso Now_Time = Label2.Text Then
                Me.DateTimePicker1.Enabled = False
                Button1.PerformClick()
                Button1.Enabled = False
                Timer1.Enabled = False
            Else
                Button1.Enabled = True
                Timer1.Enabled = True
            End If
        Catch ex As Exception
            pf.MyError(ex.ToString)
        End Try
    End Sub

    Private Sub Form1_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Try
            '判斷是否已經正在執行每日庫存結算
            Dim check_plan_dt As DataTable = pf.GetDB("D_STOCKS_PARAMETER,NOLOCK", {})
            If check_plan_dt.Rows.Count > 0 Then
                If pf.CheckNull(check_plan_dt.Rows(0).Item("Execute_Process")) = "1" Then
                    Dim sql2 As String = "update [D_STOCKS_PARAMETER] SET [Execute_Process]='0'"
                    pf.ExecSQL(sql2)
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub
End Class
