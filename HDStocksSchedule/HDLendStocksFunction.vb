﻿Imports System.Data.SqlClient
Public Class HDLendStocksFunction
    Private pf As New DBFuntion

    Public Sub New()

    End Sub

    '檢查公司借倉庫存每日檔
    '2019/04/16 新增SaleQty可銷售數量
    'Good_Qty依照生效日 SaleQty依照覆核日
    Public Function CHK_LEND_DEPOT_STOCK_DAYS() As Boolean
        Dim conn_str As String
        conn_str = My.Settings.HD_ERP_DBConnection
        Dim conn As New SqlConnection(conn_str)
        Dim iRowAffected As Integer = 0 '接收更新資料A後的影響筆數
        Dim SaveComplete As Boolean = True

        Dim sSql As String = ""
        Dim xRow As DataRow

        '開啟連接物件
        conn.Open()

        '啟用交易紀錄
        Dim transaction = conn.BeginTransaction
        Try
            Dim command = conn.CreateCommand
            command.Connection = conn
            command.Transaction = transaction
            '判斷公司庫存每日檔是否有資料，如果沒有每日庫存檔，自己做一個前一日每日庫存資料，資料來源D_LEND_DEPOT_STOCKS
            '若有資料者，取每日庫存檔的最新日期開始結算滾動到昨日的每日庫存資料
            sSql = "SELECT * FROM [D_LEND_DEPOT_STOCKS_DAYS] WHERE CONVERT(varchar,[EFFECTIVE_DATE],111)='" & DateTime.Now.AddDays(-1).ToString("yyyy/MM/dd") & "'"
            Dim dt As New DataTable
            dt = pf.GetDataTable(sSql)
            Console.WriteLine(sSql)
            'Dim Str As String = ""
            'Str = "  SELECT[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM],MAX([EFFECTIVE_DATE]) AS EFFECTIVE_DATE"
            'Str &= " From [D_LEND_DEPOT_STOCKS_DAYS] GROUP BY [DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
            'Str &= " HAVING MAX([EFFECTIVE_DATE])<'" & DateTime.Now.AddDays(-1).ToString("yyyy/MM/dd") & "'"
            'dt = pf.GetDataTable(Str)
            'If dt.rows.count > 0 Then
            'End If
            If dt.Rows.Count = 0 Then
                '取每日庫存最新日期
                'sSql = "SELECT TOP 1 CONVERT(varchar,[EFFECTIVE_DATE],111) FROM [D_LEND_DEPOT_STOCKS_DAYS] ORDER BY EFFECTIVE_DATE DESC"
                'Console.WriteLine(sSql)
                'Dim qdt As New DataTable
                'qdt = pf.GetDataTable(sSql)
                Dim Str As String = ""
                Str = "  SELECT [DEPOT_ID],[PRODUCT_ID],[BATCH_NUM],[VALID_PERIOD],MAX([EFFECTIVE_DATE]) AS EFFECTIVE_DATE"
                Str &= " From [D_LEND_DEPOT_STOCKS_DAYS] GROUP BY [DEPOT_ID],[PRODUCT_ID],[BATCH_NUM],[VALID_PERIOD]"
                Str &= " HAVING MAX([EFFECTIVE_DATE])<'" & DateTime.Now.AddDays(-1).ToString("yyyy/MM/dd") & "'"
                Dim qdt As DataTable = pf.GetDataTable(Str)
                If qdt.Rows.Count > 0 Then
                    Dim start_date As New DateTime
                    Dim end_date As DateTime
                    Dim days As Integer = 1
                    Dim chk_date As New DateTime

                    Dim a As Integer = 0

                    For a = 0 To qdt.Rows.Count - 1
                        '開始日期
                        start_date = Convert.ToDateTime(qdt.Rows(a).Item("EFFECTIVE_DATE"))
                        '結束日期
                        end_date = DateTime.Now
                        days = 1
                        chk_date = start_date.AddDays(days)
                        'start_date = Convert.ToDateTime(qdt.Rows(0).Item(0))
                        'end_date = DateTime.Now
                        'chk_date = start_date.AddDays(days)
                        Do While chk_date.ToString("yyyy/MM/dd") < end_date.ToString("yyyy/MM/dd")                         '
                            Console.WriteLine(chk_date.ToString("yyyy/MM/dd"))
                            '建立生效日的每日庫存的資料並且計算
                            If UPDATE_D_LEND_DEPOT_STOCKS_DAYS_BY_DPB(chk_date, qdt.Rows(a).Item("DEPOT_ID"), qdt.Rows(a).Item("PRODUCT_ID"), qdt.Rows(a).Item("BATCH_NUM"), qdt.Rows(a).Item("VALID_PERIOD"), command) = False Then
                                'D_STOCKS_SETTLEMENT(chk_date, "1", "2")
                                transaction.Rollback()
                                SaveComplete = False
                                Return SaveComplete
                            End If
                            'If UPDATE_D_LEND_DEPOT_STOCKS_DAYS(chk_date, command) = False Then
                            '    'D_STOCKS_SETTLEMENT(chk_date, "1", "2")
                            '    transaction.Rollback()
                            '    SaveComplete = False
                            '    Return SaveComplete
                            'End If

                            If SaveComplete = True Then
                                Dim k As Integer = 0
                                '生效日小於計算日要從生效日重新計算庫存數直到計算日
                                sSql = "SELECT [DEPOT_ID],[PRODUCT_ID],[BATCH_NUM],[EFFECTIVE_DATE] FROM [D_LEND_DEPOT_STOCKS_DIFF] WITH (NOLOCK)"
                                sSql &= " WHERE CONVERT(VARCHAR,[EFFECTIVE_DATE],111)<'" & chk_date.ToString("yyyy/MM/dd") & "'"
                                sSql &= " AND CONVERT(VARCHAR,[CHANGE_DATE],111)='" & chk_date.ToString("yyyy/MM/dd") & "'"
                                sSql &= " AND [DEPOT_ID]='" & qdt.Rows(a).Item("DEPOT_ID") & "'"
                                sSql &= " AND [PRODUCT_ID]='" & qdt.Rows(a).Item("PRODUCT_ID") & "'"
                                sSql &= " AND [BATCH_NUM]='" & qdt.Rows(a).Item("BATCH_NUM") & "'"
                                sSql &= " GROUP BY [DEPOT_ID],[PRODUCT_ID],[BATCH_NUM],[EFFECTIVE_DATE]"
                                sSql &= " ORDER BY [EFFECTIVE_DATE]"
                                Console.WriteLine(sSql)
                                Dim other_dt As New DataTable
                                other_dt = pf.GetDataTable(sSql)
                                If other_dt.Rows.Count > 0 Then
                                    For k = 0 To other_dt.Rows.Count - 1
                                        '取得VALID_PERIOD有效日期
                                        Dim qdt2 As DataTable = pf.GetDB("D_LEND_DEPOT_STOCKS,NOLOCK", {{"DEPOT_ID", other_dt.Rows(k).Item("DEPOT_ID")}, {"PRODUCT_ID", other_dt.Rows(k).Item("PRODUCT_ID")}, {"BATCH_NUM", other_dt.Rows(k).Item("BATCH_NUM")}})
                                        Dim q_valid_period As Date
                                        If qdt2.Rows.Count > 0 Then
                                            q_valid_period = qdt2.Rows(0).Item("VALID_PERIOD")
                                        End If
                                        Dim chk_other_date As Date = other_dt.Rows(k).Item("EFFECTIVE_DATE")
                                        While chk_other_date.ToString("yyyy/MM/dd") <= chk_date.ToString("yyyy/MM/dd")
                                            '建立生效日的每日庫存的資料並且計算
                                            If UPDATE_D_LEND_DEPOT_STOCKS_DAYS_BY_DPB(chk_other_date, other_dt.Rows(k).Item("DEPOT_ID"), other_dt.Rows(k).Item("PRODUCT_ID"), other_dt.Rows(k).Item("BATCH_NUM"), q_valid_period, command) = False Then
                                                '2019/08/26寫入庫存結算紀錄檔
                                                'D_STOCKS_SETTLEMENT(chk_date, "1", "2")
                                                transaction.Rollback()
                                                SaveComplete = False
                                                Return SaveComplete
                                            End If
                                            chk_other_date = chk_other_date.AddDays(1)
                                        End While
                                        '建立生效日的每日庫存的資料並且計算
                                        'If UPDATE_D_LEND_DEPOT_STOCKS_DAYS_BY_DPB(other_dt.Rows(k).Item("EFFECTIVE_DATE"), other_dt.Rows(k).Item("DEPOT_ID"), other_dt.Rows(k).Item("PRODUCT_ID"), other_dt.Rows(k).Item("BATCH_NUM"), command) = False Then
                                        '    '2019/08/26寫入庫存結算紀錄檔
                                        '    'D_STOCKS_SETTLEMENT(chk_date, "1", "2")
                                        '    transaction.Rollback()
                                        '    SaveComplete = False
                                        '    Return SaveComplete
                                        'End If
                                        'If UPDATE_D_LEND_DEPOT_STOCKS_DAYS(other_dt.Rows(k).Item("EFFECTIVE_DATE"), command) = False Then
                                        '    '2019/08/26寫入庫存結算紀錄檔
                                        '    'D_STOCKS_SETTLEMENT(chk_date, "1", "2")
                                        '    transaction.Rollback()
                                        '    SaveComplete = False
                                        '    Return SaveComplete
                                        'End If
                                    Next
                                End If
                            End If
                            '2019/08/26寫入庫存結算紀錄檔
                            'D_STOCKS_SETTLEMENT(chk_date, "1", "1")
                            days = days + 1
                            chk_date = start_date.AddDays(days)
                            'Console.WriteLine(chk_date)
                        Loop
                    Next

                Else

                    '預設將D_LEND_DEPOT_STOCKS現有的庫存自動做一個昨日的每日庫存檔
                    sSql = "SELECT * FROM [D_LEND_DEPOT_STOCKS] WITH (NOLOCK) ORDER BY DEPOT_ID,PRODUCT_ID,BATCH_NUM "
                    Dim qdt2 As New DataTable
                    qdt2 = pf.GetDataTable(sSql)
                    Dim table As String = "D_LEND_DEPOT_STOCKS_DAYS"
                    Dim j As Integer = 0
                    For j = 0 To qdt2.Rows.Count - 1
                        xRow = qdt2.Rows(j)
                        Dim col_value(,) As String = {
                            {"DEPOT_ID", pf.CheckStringDBNull(xRow.Item("DEPOT_ID"))},
                            {"PRODUCT_ID", pf.CheckStringDBNull(xRow.Item("PRODUCT_ID"))},
                            {"BATCH_NUM", pf.CheckStringDBNull(xRow.Item("BATCH_NUM"))},
                            {"EFFECTIVE_DATE", DateTime.Now.AddDays(-1).ToString("yyyy/MM/dd")},
                            {"GOOD_QTY", "0"},
                            {"BAD_QTY", "0"},
                            {"CUS_CON_QTY", "0"},
                            {"COST_PRICE", "0"},
                            {"UNIT_PRICE", "0"},
                            {"VALID_PERIOD", xRow.Item("VALID_PERIOD")},
                            {"RED_SHOW", xRow.Item("RED_SHOW")},
                            {"INV_QTY", "0"},
                            {"INV_BAD_QTY", "0"},
                            {"DEPOT_QTY", "0"},
                            {"DEPOT_BAD_QTY", "0"},
                            {"SALE_QTY", "0"},
                            {"CREATE_USER", "系統"},
                            {"CREATE_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                        }
                        sSql = pf.GetInsertquery(table, col_value)
                        Console.WriteLine(sSql)
                        command.CommandText = sSql
                        iRowAffected = command.ExecuteNonQuery()
                        If iRowAffected = 0 Then
                            SaveComplete = False
                            transaction.Rollback()
                        End If
                    Next
                End If
            End If

            command.Dispose()
            If SaveComplete = True Then
                transaction.Commit()
            End If
        Catch ex As Exception
            'pf.MyInformation(xRow.Item("DEPOT_ID") & "-" & xRow.Item("PRODUCT_ID") & "-" & xRow.Item("BATCH_NUM"))
            pf.MyError(ex.ToString)
            transaction.Rollback()
            Return False
        Finally
            conn.Close()
            conn.Dispose()
        End Try

        Return True

    End Function

    '計算D_LEND_DEPOT_STOCKS_DAYS的數量
    'LEND_CHECK_DATE：每日庫存的日期
    Public Function UPDATE_D_LEND_DEPOT_STOCKS_DAYS(LEND_CHECK_DATE As Date, sqlCommand As SqlCommand) As Boolean
        Dim i As Integer = 0
        Dim iRowAffected As Integer = 0
        Dim table As String = "D_LEND_DEPOT_STOCKS_DAYS"
        Dim xRow As DataRow
        Dim SaveOK As Boolean = True
        Dim sSql As String = ""
        Dim sSql2 As String = ""
        Dim sSql3 As String = ""
        Dim sSql4 As String = ""
        Try
            '產生庫存每日檔資料
            sSql = ""
            sSql = "SELECT * FROM [D_LEND_DEPOT_STOCKS_DAYS] WITH (NOLOCK)"
            sSql &= " WHERE Convert(varchar, [EFFECTIVE_DATE], 111) ='" & LEND_CHECK_DATE.AddDays(-1).ToString("yyyy/MM/dd") & "'"
            sSql &= " ORDER BY [DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
            Console.WriteLine(sSql)
            Dim qdt2 As New DataTable
            qdt2 = pf.GetDataTable(sSql)
            If qdt2.Rows.Count = 0 Then
                pf.MyError("每日庫存檔無[" & LEND_CHECK_DATE.AddDays(-1).ToString("yyyy/MM/dd") & "]的資料，請洽系統管理員!")
                Return False
            Else
                For i = 0 To qdt2.Rows.Count - 1
                    xRow = qdt2.Rows(i)
                    '判斷資料是否存在
                    sSql2 = ""
                    sSql2 = "SELECT * FROM [D_LEND_DEPOT_STOCKS_DAYS] WITH (NOLOCK)"
                    sSql2 &= " WHERE Convert(varchar, [EFFECTIVE_DATE], 111) ='" & LEND_CHECK_DATE.ToString("yyyy/MM/dd") & "'"
                    sSql2 &= " AND [DEPOT_ID]='" & xRow.Item("DEPOT_ID").ToString & "'"
                    sSql2 &= " AND [PRODUCT_ID]='" & xRow.Item("PRODUCT_ID").ToString & "'"
                    sSql2 &= " AND [BATCH_NUM]='" & xRow.Item("BATCH_NUM").ToString & "'"
                    Dim chk_data As New DataTable
                    Console.WriteLine(sSql2)
                    chk_data = pf.GetDataTable(sSql2)
                    If chk_data.Rows.Count = 0 Then
                        Dim col_value(,) As String = {
                           {"DEPOT_ID", xRow.Item("DEPOT_ID")},
                           {"PRODUCT_ID", xRow.Item("PRODUCT_ID")},
                           {"BATCH_NUM", xRow.Item("BATCH_NUM")},
                           {"EFFECTIVE_DATE", LEND_CHECK_DATE.ToString("yyyy/MM/dd")},
                           {"GOOD_QTY", xRow.Item("GOOD_QTY")},
                           {"BAD_QTY", xRow.Item("BAD_QTY")},
                           {"CUS_CON_QTY", xRow.Item("CUS_CON_QTY")},
                           {"COST_PRICE", xRow.Item("COST_PRICE")},
                           {"UNIT_PRICE", xRow.Item("UNIT_PRICE")},
                           {"VALID_PERIOD", xRow.Item("VALID_PERIOD")},
                           {"RED_SHOW", xRow.Item("RED_SHOW")},
                           {"INV_QTY", xRow.Item("INV_QTY")},
                           {"INV_BAD_QTY", xRow.Item("INV_BAD_QTY")},
                           {"DEPOT_QTY", xRow.Item("DEPOT_QTY")},
                           {"DEPOT_BAD_QTY", xRow.Item("DEPOT_BAD_QTY")},
                           {"SALE_QTY", xRow.Item("SALE_QTY")},
                           {"CREATE_USER", "系統"},
                           {"CREATE_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                        }
                        sSql2 = pf.GetInsertquery(table, col_value)
                        Console.WriteLine(sSql2)
                        sqlCommand.CommandText = sSql2
                        iRowAffected = sqlCommand.ExecuteNonQuery()
                        If iRowAffected = 0 Then
                            pf.MyError("新增每日庫存檔日期[" & LEND_CHECK_DATE.ToString("yyyy/MM/dd") & "]的資料發生錯誤!")
                            Return False
                        End If
                    End If

                    '開始計算庫存數
                    '計算異動庫存數Good_QTY-依照生效日
                    sSql3 = ""
                    Dim New_Good_Qty As Double = 0
                    Dim New_Inv_Qty As Double = 0
                    Dim New_Inv_Bad_Qty As Double = 0
                    sSql3 = "SELECT [DEPOT_ID],[PRODUCT_ID],[BATCH_NUM],SUM(ISNULL([G_DIFF_QTY],0)) as Qty,SUM(ISNULL([B_DIFF_QTY],0)) as Bad_Qty "
                    sSql3 &= " FROM [D_LEND_DEPOT_STOCKS_DIFF] WITH (NOLOCK) WHERE CONVERT(varchar,[EFFECTIVE_DATE],111)='" & LEND_CHECK_DATE.ToString("yyyy/MM/dd") & "'"
                    sSql3 &= " AND [DEPOT_ID]='" & xRow.Item("DEPOT_ID").ToString & "'"
                    sSql3 &= " AND [PRODUCT_ID]='" & xRow.Item("PRODUCT_ID").ToString & "'"
                    sSql3 &= " AND [BATCH_NUM]='" & xRow.Item("BATCH_NUM").ToString & "'"
                    sSql3 &= " GROUP BY [DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                    Console.WriteLine(sSql3)
                    Dim qdt3 As New DataTable
                    qdt3 = pf.GetDataTable(sSql3)
                    If qdt3.Rows.Count > 0 Then
                        New_Good_Qty = Convert.ToDouble(xRow.Item("GOOD_QTY")) + Convert.ToDouble(qdt3.Rows(0).Item("Qty"))
                        New_Inv_Qty = Convert.ToDouble(xRow.Item("INV_QTY")) + Convert.ToDouble(qdt3.Rows(0).Item("Qty"))
                        New_Inv_Bad_Qty = Convert.ToDouble(xRow.Item("INV_BAD_QTY")) + Convert.ToDouble(qdt3.Rows(0).Item("Bad_Qty"))
                        Dim PK2() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM", "EFFECTIVE_DATE"}
                        Dim col_value2(,) As String = {
                            {"DEPOT_ID", xRow.Item("DEPOT_ID")},
                            {"PRODUCT_ID", xRow.Item("PRODUCT_ID")},
                            {"BATCH_NUM", xRow.Item("BATCH_NUM")},
                            {"EFFECTIVE_DATE", LEND_CHECK_DATE},
                            {"GOOD_QTY", New_Good_Qty},
                            {"INV_QTY", New_Inv_Qty},
                            {"INV_BAD_QTY", New_Inv_Bad_Qty},
                            {"ALTER_USER", "系統"},
                            {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                        }
                        sSql3 = pf.GetUpdatequery(table, PK2, col_value2)
                        Console.WriteLine(sSql3)
                        sqlCommand.CommandText = sSql3
                        iRowAffected = sqlCommand.ExecuteNonQuery()
                        If iRowAffected = 0 Then
                            pf.MyError("1-異動每日庫存檔日期[" & LEND_CHECK_DATE.ToString("yyyy/MM/dd") & "]的資料發生錯誤!")
                            Return False
                        End If
                    Else
                        New_Good_Qty = Convert.ToDouble(xRow.Item("GOOD_QTY"))
                        New_Inv_Qty = Convert.ToDouble(xRow.Item("INV_QTY"))
                        New_Inv_Bad_Qty = Convert.ToDouble(xRow.Item("INV_BAD_QTY"))
                        Dim PK2() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM", "EFFECTIVE_DATE"}
                        Dim col_value2(,) As String = {
                            {"DEPOT_ID", xRow.Item("DEPOT_ID")},
                            {"PRODUCT_ID", xRow.Item("PRODUCT_ID")},
                            {"BATCH_NUM", xRow.Item("BATCH_NUM")},
                            {"EFFECTIVE_DATE", LEND_CHECK_DATE},
                            {"GOOD_QTY", New_Good_Qty},
                            {"INV_QTY", New_Inv_Qty},
                            {"INV_BAD_QTY", New_Inv_Bad_Qty},
                            {"ALTER_USER", "系統"},
                            {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                        }
                        sSql3 = pf.GetUpdatequery(table, PK2, col_value2)
                        Console.WriteLine(sSql3)
                        sqlCommand.CommandText = sSql3
                        iRowAffected = sqlCommand.ExecuteNonQuery()
                        If iRowAffected = 0 Then
                            pf.MyError("1-異動每日庫存檔日期[" & LEND_CHECK_DATE.ToString("yyyy/MM/dd") & "]的資料發生錯誤!")
                            Return False
                        End If
                    End If

                    '計算2-依照覆核日
                    ''計算異動庫存數SALE_QTY-依照覆核日
                    sSql4 = ""
                    Dim New_Sale_Qty As Double = 0
                    Dim New_Depot_Qty As Double = 0
                    Dim New_Depot_Bad_Qty As Double = 0
                    sSql4 = "SELECT [DEPOT_ID],[PRODUCT_ID],[BATCH_NUM],SUM(ISNULL([G_DIFF_QTY],0)) as Qty,SUM(ISNULL([B_DIFF_QTY],0)) as Bad_Qty "
                    sSql4 &= " FROM [D_LEND_DEPOT_STOCKS_DIFF] WITH (NOLOCK) WHERE CONVERT(varchar,[CHANGE_DATE],111)='" & LEND_CHECK_DATE.ToString("yyyy/MM/dd") & "'"
                    sSql4 &= " AND [DEPOT_ID]='" & xRow.Item("DEPOT_ID").ToString & "'"
                    sSql4 &= " AND [PRODUCT_ID]='" & xRow.Item("PRODUCT_ID").ToString & "'"
                    sSql4 &= " AND [BATCH_NUM]='" & xRow.Item("BATCH_NUM").ToString & "'"
                    sSql4 &= " GROUP BY [DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                    Console.WriteLine(sSql4)
                    Dim qdt4 As New DataTable
                    qdt4 = pf.GetDataTable(sSql4)
                    If qdt4.Rows.Count > 0 AndAlso qdt4.Rows(0).Item("Qty") <> 0 Then
                        New_Sale_Qty = Convert.ToDouble(xRow.Item("Sale_Qty")) + Convert.ToDouble(qdt4.Rows(0).Item("Qty"))
                        New_Depot_Qty = Convert.ToDouble(xRow.Item("DEPOT_QTY")) + Convert.ToDouble(qdt4.Rows(0).Item("Qty"))
                        New_Depot_Bad_Qty = Convert.ToDouble(xRow.Item("DEPOT_BAD_QTY")) + Convert.ToDouble(qdt4.Rows(0).Item("Bad_Qty"))
                        Dim PK2() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM", "EFFECTIVE_DATE"}
                        Dim col_value2(,) As String = {
                            {"DEPOT_ID", xRow.Item("DEPOT_ID")},
                            {"PRODUCT_ID", xRow.Item("PRODUCT_ID")},
                            {"BATCH_NUM", xRow.Item("BATCH_NUM")},
                            {"EFFECTIVE_DATE", LEND_CHECK_DATE},
                            {"DEPOT_QTY", New_Depot_Qty},
                            {"DEPOT_BAD_QTY", New_Depot_Bad_Qty},
                            {"SALE_QTY", New_Sale_Qty},
                            {"ALTER_USER", "系統"},
                            {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                        }
                        sSql4 = pf.GetUpdatequery(table, PK2, col_value2)
                        Console.WriteLine(sSql4)
                        sqlCommand.CommandText = sSql4
                        iRowAffected = sqlCommand.ExecuteNonQuery()
                        If iRowAffected = 0 Then
                            pf.MyError("2-異動每日庫存檔日期[" & LEND_CHECK_DATE.ToString("yyyy/MM/dd") & "]的資料發生錯誤!")
                            Return False
                        End If
                    Else
                        New_Depot_Qty = Convert.ToDouble(xRow.Item("DEPOT_QTY"))
                        New_Depot_Bad_Qty = Convert.ToDouble(xRow.Item("DEPOT_BAD_QTY"))
                        New_Sale_Qty = Convert.ToDouble(xRow.Item("Sale_Qty"))
                        Dim PK2() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM", "EFFECTIVE_DATE"}
                        Dim col_value2(,) As String = {
                            {"DEPOT_ID", xRow.Item("DEPOT_ID")},
                            {"PRODUCT_ID", xRow.Item("PRODUCT_ID")},
                            {"BATCH_NUM", xRow.Item("BATCH_NUM")},
                            {"EFFECTIVE_DATE", LEND_CHECK_DATE},
                            {"DEPOT_QTY", New_Depot_Qty},
                            {"DEPOT_BAD_QTY", New_Depot_Bad_Qty},
                            {"SALE_QTY", New_Sale_Qty},
                            {"ALTER_USER", "系統"},
                            {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                        }
                        sSql4 = pf.GetUpdatequery(table, PK2, col_value2)
                        Console.WriteLine(sSql4)
                        sqlCommand.CommandText = sSql4
                        iRowAffected = sqlCommand.ExecuteNonQuery()
                        If iRowAffected = 0 Then
                            pf.MyError("2-異動每日庫存檔日期[" & LEND_CHECK_DATE.ToString("yyyy/MM/dd") & "]的資料發生錯誤!")
                            Return False
                        End If
                    End If
                    '更新D_LEND_DEPOT_STOCKS
                    Dim sSql5 As String = ""
                    sSql5 = "SELECT [DEPOT_ID],[PRODUCT_ID],[BATCH_NUM],SUM(ISNULL([G_DIFF_QTY],0)) as Qty "
                    sSql5 &= " FROM [D_LEND_DEPOT_STOCKS] WITH (NOLOCK) WHERE CONVERT(varchar,[CHANGE_DATE],111)='" & LEND_CHECK_DATE.ToString("yyyy/MM/dd") & "'"
                    sSql5 &= " AND [DEPOT_ID]='" & xRow.Item("DEPOT_ID").ToString & "'"
                    sSql5 &= " AND [PRODUCT_ID]='" & xRow.Item("PRODUCT_ID").ToString & "'"
                    sSql5 &= " AND [BATCH_NUM]='" & xRow.Item("BATCH_NUM").ToString & "'"
                    sSql5 &= " GROUP BY [DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                Next
            End If
        Catch ex As Exception
            pf.MyError(ex.ToString)
            Return False
        End Try
        Return True
    End Function

    '借倉入庫
    '入庫單檢查庫存及虛擬庫存是否有產品資料，如無資料INSERT數量0的產品資料
    Public Sub Lend_Stock_exist(STOCK_IN_ID As String)
        Dim sql As String
        sql = "SELECT * FROM [D_LEND_STOCK_IN_DETAIL] WITH (NOLOCK) "
        sql &= "WHERE [STOCK_IN_ID] = '" & STOCK_IN_ID & "'"

        For Each row In pf.GetDataTable(sql).Rows
            sql = "SELECT * FROM [D_LEND_DEPOT_STOCKS] WITH (NOLOCK) "
            sql &= " WHERE [DEPOT_ID]='" & row.Item("DEPOT_ID") & "'"
            sql &= " AND [PRODUCT_ID]='" & row.Item("PRODUCT_ID") & "'"
            sql &= " AND [BATCH_NUM]='" & row.Item("BATCH_NUM") & "'"
            If pf.GetDataTable(sql).Rows.Count = 0 Then
                Dim table As String = "D_LEND_DEPOT_STOCKS"
                Dim pk() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM"}
                Dim col_val(,) As String = {{"DEPOT_ID", row.Item("DEPOT_ID")},
                    {"PRODUCT_ID", row.Item("PRODUCT_ID")},
                    {"BATCH_NUM", row.Item("BATCH_NUM")},
                    {"VALID_PERIOD", pf.CheckDateTimeNull(row.Item("VALID_PERIOD"))},
                    {"GOOD_QTY", "0"},
                    {"RED_SHOW", "0"},
                    {"BAD_QTY", "0"},
                    {"CUS_CON_QTY", "0"},
                    {"INV_QTY", "0"},
                    {"INV_BAD_QTY", "0"},
                    {"DEPOT_QTY", "0"},
                    {"DEPOT_BAD_QTY", "0"},
                    {"SALE_QTY", "0"},
                    {"CREATE_USER", "系統"},
                    {"CREATE_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}}
                pf.ExecSQL(pf.GetInsertquery(table, col_val))
                '2020.03.16借倉每日檔新增一筆預設前一日為0的資料
                Dim table2 As String = "D_LEND_DEPOT_STOCKS_DAYS"
                Dim pk2() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM", "EFFECTIVE_DATE"}
                Dim col_val2(,) As String = {{"DEPOT_ID", row.Item("DEPOT_ID")},
                    {"PRODUCT_ID", row.Item("PRODUCT_ID")},
                    {"BATCH_NUM", row.Item("BATCH_NUM")},
                    {"EFFECTIVE_DATE", DateTime.Now.AddDays(-1).ToString("yyyy/MM/dd")},
                    {"VALID_PERIOD", pf.CheckDateTimeNull(row.Item("VALID_PERIOD"))},
                    {"GOOD_QTY", "0"},
                    {"RED_SHOW", "0"},
                    {"BAD_QTY", "0"},
                    {"CUS_CON_QTY", "0"},
                    {"INV_QTY", "0"},
                    {"INV_BAD_QTY", "0"},
                    {"DEPOT_QTY", "0"},
                    {"DEPOT_BAD_QTY", "0"},
                    {"SALE_QTY", "0"},
                    {"CREATE_USER", "系統"},
                    {"CREATE_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}}
                pf.ExecSQL(pf.GetInsertquery(table2, col_val2))
            End If
        Next
    End Sub

    '異動入庫單的當日庫存庫存數量
    '計算公式：庫存數量=前一日的期末庫存數量+[覆核日]=當日庫存異動數量
    '更新到[D_DEPOT_STOCKS]
    'STOCK_IN_ID:出庫單編號
    Function UPDATE_D_LEND_DEPOT_STOCKS_IN(STOCK_IN_ID As String, command As SqlCommand) As Boolean
        Dim sSql As String = ""
        Dim iAffectRows As Integer = 0
        Try
            Dim IN_mdt As New DataTable
            Dim IN_ddt As New DataTable
            '出庫單主檔資料
            'sSql = "SELECT * FROM [D_LEND_STOCK_IN_MASTER] WITH (NOLOCK) WHERE [STOCK_IN_ID]='" & STOCK_IN_ID & "'"
            'mdt = pf.GetDataTable(sSql)
            IN_mdt = pf.GetDB("D_LEND_STOCK_IN_MASTER,NOLOCK", {{"STOCK_IN_ID", STOCK_IN_ID}})
            Dim IN_xRow As DataRow
            IN_xRow = IN_mdt.Rows(0)
            '生效日
            Dim IN_EFFECTIVE_DATE As Date = Convert.ToDateTime(IN_xRow.Item("EFFECTIVE_DATE")).ToString("yyyy/MM/dd")
            '覆核日
            Dim IN_CHANGE_DATE As Date = Nothing
            If IN_xRow.Item("VOUCHER_STATUS").ToString = "確認" Then
                '覆核日
                IN_CHANGE_DATE = Convert.ToDateTime(IN_xRow.Item("VOUCHER_TIME")).ToString("yyyy/MM/dd")
            ElseIf IN_xRow.Item("VOUCHER_STATUS").ToString = "生效" Or IN_xRow.Item("VOUCHER_STATUS").ToString = "作廢" Then
                '異動日期
                IN_CHANGE_DATE = Convert.ToDateTime(IN_xRow.Item("ALTER_TIME")).ToString("yyyy/MM/dd")
            End If

            '將明細寫入[D_DEPOT_STOCKS_DIFF]，然後再統一計算庫存異動數量
            'sSql = "SELECT * FROM [D_LEND_STOCK_IN_DETAIL] WITH (NOLOCK) WHERE [STOCK_IN_ID]='" & STOCK_IN_ID & "'"
            'ddt = pf.GetDataTable(sSql)
            IN_ddt = pf.GetDB("D_LEND_STOCK_IN_DETAIL,NOLOCK", {{"STOCK_IN_ID", STOCK_IN_ID}})
            Dim IN_table As String = "D_LEND_DEPOT_STOCKS_DIFF"
            For j As Integer = 0 To IN_ddt.Rows.Count - 1
                Dim IN_yRow As DataRow
                IN_yRow = IN_ddt.Rows(j)
                Dim IN_total_qty As Double = 0
                '良品
                Dim IN_good_qty As Double = 0
                '破損品
                Dim IN_bad_qty As Double = 0
                If pf.CheckIfDBNull(IN_yRow.Item("STOCK_CHECK")) = 1 Then
                    '入庫確認
                    IN_total_qty = (Convert.ToDouble(IN_yRow.Item("QTY")) + Convert.ToDouble(IN_yRow.Item("PRESENT_QTY")))
                    IN_good_qty = (Convert.ToDouble(IN_yRow.Item("QTY")) + Convert.ToDouble(IN_yRow.Item("PRESENT_QTY")))
                    IN_bad_qty = Convert.ToDouble(IN_yRow.Item("BAD_QTY"))
                ElseIf pf.CheckIfDBNull(IN_yRow.Item("STOCK_CHECK")) = 0 Then
                    '取消入庫確認
                    IN_total_qty = (Convert.ToDouble(IN_yRow.Item("QTY")) + Convert.ToDouble(IN_yRow.Item("PRESENT_QTY"))) * (-1)
                    IN_good_qty = (Convert.ToDouble(IN_yRow.Item("QTY")) + Convert.ToDouble(IN_yRow.Item("PRESENT_QTY"))) * (-1)
                    IN_bad_qty = Convert.ToDouble(IN_yRow.Item("BAD_QTY")) * (-1)
                End If
                If IN_xRow.Item("STOCK_TYPE").ToString = "公司" Then
                    Dim col_value(,) As String = {
                        {"PAPER_ID", pf.CheckStringDBNull(IN_yRow.Item("STOCK_IN_ID"))},
                        {"EFFECTIVE_DATE", IN_xRow.Item("EFFECTIVE_DATE")},
                        {"CHANGE_DATE", IN_CHANGE_DATE},
                        {"DEPOT_ID", pf.CheckStringDBNull(IN_yRow.Item("DEPOT_ID"))},
                        {"PRODUCT_ID", pf.CheckStringDBNull(IN_yRow.Item("PRODUCT_ID"))},
                        {"BATCH_NUM", pf.CheckStringDBNull(IN_yRow.Item("BATCH_NUM"))},
                        {"G_DIFF_QTY", IN_good_qty},
                        {"B_DIFF_QTY", IN_bad_qty},
                        {"C_DIFF_QTY", 0},
                        {"CUS_DIFF_QTY", 0},
                        {"SUPPLIER_DIFF_QTY", 0},
                        {"VOUCHER_STATUS", pf.CheckStringDBNull(IN_xRow.Item("VOUCHER_STATUS"))},
                        {"CUSTOMER_ID", pf.CheckStringDBNull(IN_xRow.Item("CUSTOMER_ID"))},
                        {"SUPPLIER_ID", pf.CheckStringDBNull(IN_xRow.Item("SUPPLIER_ID"))},
                        {"CREATE_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                    }
                    sSql = pf.GetInsertquery(IN_table, col_value)
                End If
                Console.WriteLine(sSql)
                command.CommandText = sSql
                iAffectRows = command.ExecuteNonQuery()
                If iAffectRows = 0 Then
                    Return False
                End If
            Next

            Dim LEND_CHECK_DATE As New DateTime
            LEND_CHECK_DATE = DateTime.Now

            '及時異動當日庫存數量[覆核日]
            If IN_xRow.Item("STOCK_TYPE").ToString = "公司" Then
                sSql = ""
                Dim IN_New_Qty As Double = 0
                Dim IN_Good_New_Qty As Double = 0
                Dim IN_Bad_New_Qty As Double = 0
                '當日庫存異動數量
                sSql = "SELECT CONVERT(VARCHAR,[CHANGE_DATE],111) AS CDATE,[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                sSql &= ",SUM(ISNULL([G_DIFF_QTY],0)) as Qty,SUM(ISNULL([B_DIFF_QTY],0)) as Bad_Qty "
                sSql &= " FROM [D_LEND_DEPOT_STOCKS_DIFF] WITH (NOLOCK) "
                sSql &= " WHERE [CHANGE_DATE] ='" & LEND_CHECK_DATE.ToString("yyyy/MM/dd") & "'"
                sSql &= " GROUP BY CONVERT(VARCHAR,[CHANGE_DATE],111),[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                Console.WriteLine(sSql)
                Dim qdt As New DataTable
                qdt = pf.GetDataTable(sSql)
                If qdt.Rows.Count > 0 Then
                    For j = 0 To qdt.Rows.Count - 1
                        Dim zRow As DataRow = qdt.Rows(j)
                        '取最近一次的庫存期末數量
                        sSql = "SELECT TOP 1 [EFFECTIVE_DATE],[GOOD_QTY],[BAD_QTY] FROM [D_LEND_DEPOT_STOCKS_DAYS] WITH (NOLOCK) "
                        sSql &= " WHERE [DEPOT_ID]='" & zRow.Item("DEPOT_ID").ToString & "'"
                        sSql &= " AND [PRODUCT_ID]='" & zRow.Item("PRODUCT_ID").ToString & "'"
                        sSql &= " AND [BATCH_NUM]='" & zRow.Item("BATCH_NUM").ToString & "'"
                        sSql &= " AND [EFFECTIVE_DATE]='" & LEND_CHECK_DATE.AddDays(-1).ToString("yyyy/MM/dd") & "'"
                        sSql &= " ORDER BY [EFFECTIVE_DATE] DESC "
                        Console.WriteLine(sSql)
                        Dim qdt2 As New DataTable
                        qdt2 = pf.GetDataTable(sSql)
                        If qdt2.Rows.Count > 0 Then
                            '有查詢到前一日的期末庫存，庫存數量直接運算後更新
                            IN_New_Qty = Convert.ToDouble(qdt2.Rows(0).Item("GOOD_QTY")) + Convert.ToDouble(zRow.Item("Qty"))
                            IN_Good_New_Qty = Convert.ToDouble(qdt2.Rows(0).Item("GOOD_QTY")) + Convert.ToDouble(zRow.Item("Qty"))
                            IN_Bad_New_Qty = Convert.ToDouble(qdt2.Rows(0).Item("BAD_QTY")) + Convert.ToDouble(zRow.Item("Bad_Qty"))

                        Else
                            '若每日庫存沒有資料者，直接更新
                            IN_New_Qty = Convert.ToDouble(zRow.Item("Qty"))
                            IN_Good_New_Qty = Convert.ToDouble(zRow.Item("Qty"))
                            IN_Bad_New_Qty = Convert.ToDouble(zRow.Item("Bad_Qty"))

                        End If

                        Dim tableName As String = "D_LEND_DEPOT_STOCKS"
                        Dim pk() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM"}
                        Dim col_value(,) As String = {
                            {"DEPOT_ID", zRow.Item("DEPOT_ID").ToString},
                            {"PRODUCT_ID", zRow.Item("PRODUCT_ID").ToString},
                            {"BATCH_NUM", zRow.Item("BATCH_NUM").ToString},
                            {"GOOD_QTY", IN_Good_New_Qty},
                            {"BAD_QTY", IN_Bad_New_Qty},
                            {"DEPOT_QTY", IN_Good_New_Qty},
                            {"DEPOT_BAD_QTY", IN_Bad_New_Qty},
                            {"SALE_QTY", IN_Good_New_Qty},
                            {"ALTER_USER", "系統"},
                            {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                        }
                        sSql = pf.GetUpdatequery(tableName, pk, col_value)
                        Console.WriteLine(sSql)
                        command.CommandText = sSql
                        iAffectRows = command.ExecuteNonQuery()
                        If iAffectRows = 0 Then
                            Return False
                        End If
                    Next
                End If
            End If
        Catch ex As Exception
            pf.MyError(ex.ToString)
            Return False
        End Try
        Return True
    End Function

    '2019-05-03
    '計算目前倉庫的可異動數量
    '現有庫存數量-單據狀態為生效的借倉出庫單
    Function Get_Lend_Depot_Out_Qty(DepotId As String, ProductId As String, BatchNum As String) As Double
        Dim Qty As Double = 0
        Dim Depot_Qty As Double = 0
        '銷貨單數量
        Dim SupplierGoodsOutQty As Double = 0
        '借倉出庫單數量
        Dim OtherGoodsOutQty As Double = 0
        '調撥單(調出)數量
        Dim TransferQty As Double = 0

        Dim sSql As String = ""
        Dim MasterTable As String = ""
        Dim DetailsTable As String = ""
        Dim join(,) As String
        Dim Select_Column(,) As String
        Dim where(,) As String

        Dim qdt As New DataTable
        Dim sum As DataTable
        Try
            '現有庫存的數量
            sSql = "SELECt ISNULL([GOOD_QTY],0) AS DepotQty FROM [D_LEND_DEPOT_STOCKS] WHERE [DEPOT_ID]='" & DepotId & "'"
            sSql &= " AND [PRODUCT_ID]='" & ProductId & "' AND [BATCH_NUM]='" & BatchNum & "'"
            qdt = pf.GetDataTable(sSql)
            If qdt.Rows.Count > 0 Then Depot_Qty = Convert.ToDouble(qdt.Rows(0).Item("DepotQty"))

            '單據狀態=生效的借倉出庫單數量
            MasterTable = "P_OTHER_GOODS_OUT_MASTER"
            DetailsTable = "P_OTHER_GOODS_OUT_DETAIL"
            join = {{MasterTable, "OTHER_GOODS_OUT_ID", DetailsTable, "OTHER_GOODS_OUT_ID"}}
            Select_Column = {{"CUSTOM_SQL", "ISNULL(SUM([QTY]+[PRESENT_QTY]),0) As [OtherGoodsOutQty]"}, {DetailsTable, ""}}
            where = {{DetailsTable, "DEPOT_ID", "=", DepotId},
                     {DetailsTable, "PRODUCT_ID", "=", ProductId},
                     {DetailsTable, "BATCH_NUM", "=", BatchNum},
                     {MasterTable, "VOUCHER_STATUS", "=", "生效"}}
            sum = pf.GetDB_Join(DetailsTable, join, Select_Column, where)
            If sum.Rows.Count > 0 Then OtherGoodsOutQty = Convert.ToDouble(sum.Rows(0).Item("OtherGoodsOutQty"))
            OtherGoodsOutQty = 0

            Qty = Depot_Qty - OtherGoodsOutQty

        Catch ex As Exception
            pf.MyError(ex.ToString)
        End Try
        Return Qty
    End Function

    '借倉出庫
    '異動出庫單的庫存數量[當日庫存][看覆核日]
    '計算公式：庫存數量=前一日的期末庫存數量+覆核日=當日的庫存異動數量
    '更新到[D_DEPOT_STOCKS]
    'STOCK_OUT_ID:出庫單編號
    Function UPDATE_D_LEND_DEPOT_STOCKS_OUT(STOCK_OUT_ID As String, command As SqlCommand) As Boolean
        Dim sSql As String = ""
        Dim iAffectRows As Integer = 0
        Try
            Dim OUT_mdt As New DataTable
            Dim OUT_ddt As New DataTable
            '出庫單主檔資料
            'sSql = "SELECT * FROM [D_LEND_STOCK_OUT_MASTER] WITH (NOLOCK) WHERE [STOCK_OUT_ID]='" & STOCK_OUT_ID & "'"
            'mdt = pf.GetDataTable(sSql)
            OUT_mdt = pf.GetDB("D_LEND_STOCK_OUT_MASTER,NOLOCK", {{"STOCK_OUT_ID", STOCK_OUT_ID}})
            Dim OUT_xRow As DataRow
            OUT_xRow = OUT_mdt.Rows(0)
            '生效日
            Dim OUT_EFFECTIVE_DATE As Date = Convert.ToDateTime(OUT_xRow.Item("EFFECTIVE_DATE")).ToString("yyyy/MM/dd")
            '覆核日
            Dim OUT_CHANGE_DATE As Date = Nothing
            If OUT_xRow.Item("VOUCHER_STATUS").ToString = "確認" Then
                '覆核日
                OUT_CHANGE_DATE = Convert.ToDateTime(OUT_xRow.Item("VOUCHER_TIME")).ToString("yyyy/MM/dd")
            ElseIf OUT_xRow.Item("VOUCHER_STATUS").ToString = "生效" Or OUT_xRow.Item("VOUCHER_STATUS").ToString = "作廢" Then
                '異動日期
                OUT_CHANGE_DATE = Convert.ToDateTime(OUT_xRow.Item("ALTER_TIME")).ToString("yyyy/MM/dd")
            End If

            '將明細寫入[D_DEPOT_STOCKS_DIFF]，然後再統一計算庫存異動數量
            'sSql = "SELECT * FROM [D_LEND_STOCK_OUT_DETAIL] WITH (NOLOCK) WHERE [STOCK_OUT_ID]='" & STOCK_OUT_ID & "'"
            'ddt = pf.GetDataTable(sSql)
            OUT_ddt = pf.GetDB("D_LEND_STOCK_OUT_DETAIL,NOLOCK", {{"STOCK_OUT_ID", STOCK_OUT_ID}})
            Dim OUT_table As String = "D_LEND_DEPOT_STOCKS_DIFF"
            For j As Integer = 0 To OUT_ddt.Rows.Count - 1
                Dim OUT_yRow As DataRow
                OUT_yRow = OUT_ddt.Rows(j)
                Dim OUT_total_qty As Double = 0
                Dim OUT_good_qty As Double = 0
                Dim OUT_bad_qty As Double = 0
                If pf.CheckIfDBNull(OUT_yRow.Item("STOCK_CHECK")) = 1 Then
                    '出庫確認
                    OUT_good_qty = (Convert.ToDouble(OUT_yRow.Item("QTY")) + Convert.ToDouble(OUT_yRow.Item("PRESENT_QTY"))) * (-1)
                    OUT_bad_qty = Convert.ToDouble(OUT_yRow.Item("BAD_QTY") * (-1))

                ElseIf pf.CheckIfDBNull(OUT_yRow.Item("STOCK_CHECK")) = 0 Then
                    '取消出庫確認
                    OUT_good_qty = (Convert.ToDouble(OUT_yRow.Item("QTY")) + Convert.ToDouble(OUT_yRow.Item("PRESENT_QTY")))
                    OUT_bad_qty = Convert.ToDouble(OUT_yRow.Item("BAD_QTY"))
                End If
                If OUT_xRow.Item("STOCK_TYPE").ToString = "公司" Then
                    Dim col_value(,) As String = {
                        {"PAPER_ID", pf.CheckStringDBNull(OUT_yRow.Item("STOCK_OUT_ID"))},
                        {"EFFECTIVE_DATE", OUT_xRow.Item("EFFECTIVE_DATE")},
                        {"CHANGE_DATE", OUT_CHANGE_DATE},
                        {"DEPOT_ID", pf.CheckStringDBNull(OUT_yRow.Item("DEPOT_ID"))},
                        {"PRODUCT_ID", pf.CheckStringDBNull(OUT_yRow.Item("PRODUCT_ID"))},
                        {"BATCH_NUM", pf.CheckStringDBNull(OUT_yRow.Item("BATCH_NUM"))},
                        {"G_DIFF_QTY", OUT_good_qty},
                        {"B_DIFF_QTY", OUT_bad_qty},
                        {"C_DIFF_QTY", 0},
                        {"CUS_DIFF_QTY", 0},
                        {"SUPPLIER_DIFF_QTY", 0},
                        {"VOUCHER_STATUS", pf.CheckStringDBNull(OUT_xRow.Item("VOUCHER_STATUS"))},
                        {"CUSTOMER_ID", pf.CheckStringDBNull(OUT_xRow.Item("CUSTOMER_ID"))},
                        {"SUPPLIER_ID", pf.CheckStringDBNull(OUT_xRow.Item("SUPPLIER_ID"))},
                        {"CREATE_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                    }
                    sSql = pf.GetInsertquery(OUT_table, col_value)
                End If
                Console.WriteLine(sSql)
                command.CommandText = sSql
                iAffectRows = command.ExecuteNonQuery()
                If iAffectRows = 0 Then
                    Return False
                End If
            Next

            Dim OUT_CHECK_DATE As New DateTime
            OUT_CHECK_DATE = DateTime.Now
            '異動當日庫存的數量-以覆核日
            If OUT_xRow.Item("STOCK_TYPE").ToString = "公司" Then
                sSql = ""
                Dim OUT_New_Qty As Double = 0
                Dim OUT_Good_New_Qty As Double = 0
                Dim OUT_Bad_New_Qty As Double = 0
                '當日庫存異動數量
                sSql = "SELECT CONVERT(VARCHAR,[CHANGE_DATE],111) AS CDATE,[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                sSql &= ",SUM(ISNULL([G_DIFF_QTY],0)) as Qty ,SUM(ISNULL([B_DIFF_QTY],0)) as Bad_Qty "
                sSql &= " FROM [D_LEND_DEPOT_STOCKS_DIFF] WITH (NOLOCK) "
                sSql &= " WHERE [CHANGE_DATE] ='" & OUT_CHECK_DATE.ToString("yyyy/MM/dd") & "'"
                sSql &= " GROUP BY CONVERT(VARCHAR,[CHANGE_DATE],111),[DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                Console.WriteLine(sSql)
                Dim qdt As New DataTable
                qdt = pf.GetDataTable(sSql)
                If qdt.Rows.Count > 0 Then
                    For j = 0 To qdt.Rows.Count - 1
                        Dim zRow As DataRow = qdt.Rows(j)
                        '取最近一次的庫存期末數量
                        'sSql = "SELECT TOP 1 [EFFECTIVE_DATE],[GOOD_QTY] FROM [D_DEPOT_STOCKS_DAYS] WITH (NOLOCK) "
                        sSql = "SELECT TOP 1 [EFFECTIVE_DATE],[SALE_QTY],[BAD_QTY] FROM [D_LEND_DEPOT_STOCKS_DAYS] WITH (NOLOCK) "
                        sSql &= " WHERE [DEPOT_ID]='" & zRow.Item("DEPOT_ID").ToString & "'"
                        sSql &= " AND [PRODUCT_ID]='" & zRow.Item("PRODUCT_ID").ToString & "'"
                        sSql &= " AND [BATCH_NUM]='" & zRow.Item("BATCH_NUM").ToString & "'"
                        sSql &= " ORDER BY [EFFECTIVE_DATE] DESC "
                        Console.WriteLine(sSql)
                        Dim qdt2 As New DataTable
                        qdt2 = pf.GetDataTable(sSql)
                        If qdt2.Rows.Count > 0 Then
                            '有查詢到前一日的期末庫存，庫存數量直接運算後更新
                            OUT_New_Qty = Convert.ToDouble(qdt2.Rows(0).Item("SALE_QTY")) + Convert.ToDouble(zRow.Item("Qty"))
                            OUT_Good_New_Qty = Convert.ToDouble(qdt2.Rows(0).Item("SALE_QTY")) + Convert.ToDouble(zRow.Item("Qty"))
                            OUT_Bad_New_Qty = Convert.ToDouble(qdt2.Rows(0).Item("BAD_QTY")) + Convert.ToDouble(zRow.Item("Bad_Qty"))
                        Else
                            '若每日庫存沒有資料者，直接更新
                            '2019-04-01討論後需要再驗證
                            OUT_New_Qty = Convert.ToDouble(zRow.Item("Qty"))
                            OUT_Good_New_Qty = Convert.ToDouble(zRow.Item("Qty"))
                            OUT_Bad_New_Qty = Convert.ToDouble(zRow.Item("Bad_Qty"))
                        End If

                        Dim tableName As String = "D_LEND_DEPOT_STOCKS"
                        Dim pk() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM"}
                        Dim col_value(,) As String = {
                            {"DEPOT_ID", zRow.Item("DEPOT_ID").ToString},
                            {"PRODUCT_ID", zRow.Item("PRODUCT_ID").ToString},
                            {"BATCH_NUM", zRow.Item("BATCH_NUM").ToString},
                            {"GOOD_QTY", OUT_Good_New_Qty},
                            {"BAD_QTY", OUT_Bad_New_Qty},
                            {"DEPOT_QTY", OUT_Good_New_Qty},
                            {"DEPOT_BAD_QTY", OUT_Bad_New_Qty},
                            {"ALTER_USER", "系統"},
                            {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                        }
                        sSql = pf.GetUpdatequery(tableName, pk, col_value)
                        Console.WriteLine(sSql)
                        command.CommandText = sSql
                        iAffectRows = command.ExecuteNonQuery()
                        If iAffectRows = 0 Then
                            Return False
                        End If
                    Next

                End If
            End If
        Catch ex As Exception
            pf.MyError(ex.ToString)
            Return False
        End Try

        Return True

    End Function


    '滾動每日庫存-公司借倉庫存盤整單使用
    '異動出庫單的庫存數量[當日庫存][看覆核日]
    '計算公式：庫存數量=前一日的期末庫存數量+覆核日=當日的庫存異動數量
    '更新到[D_DEPOT_STOCKS]
    'STOCK_OUT_ID:出庫單編號
    Function SCROLL_D_LEND_DEPOT_STOCKS_DAYS(STOCK_OUT_ID As String, STOCK_IN_ID As String, SCROLL_DATE As DateTime, command As SqlCommand) As Boolean
        Dim sSql As String = ""
        Dim iAffectRows As Integer = 0
        Dim OUT_mdt As New DataTable
        Dim OUT_ddt As New DataTable
        Dim IN_mdt As New DataTable
        Dim IN_ddt As New DataTable

        Try
            If STOCK_OUT_ID <> "" Then
                '出庫單主檔資料
                'sSql = "SELECT * FROM [D_STOCK_OUT_MASTER] WITH (NOLOCK) WHERE [STOCK_OUT_ID]='" & STOCK_OUT_ID & "'"
                'mdt = pf.GetDataTable(sSql)
                OUT_mdt = pf.GetDB("D_LEND_STOCK_OUT_MASTER,NOLOCK", {{"STOCK_OUT_ID", STOCK_OUT_ID}})
                Dim OUT_xRow As DataRow
                OUT_xRow = OUT_mdt.Rows(0)
                '生效日
                Dim OUT_EFFECTIVE_DATE As Date = Convert.ToDateTime(OUT_xRow.Item("EFFECTIVE_DATE")).ToString("yyyy/MM/dd")
                '覆核日
                Dim OUT_CHANGE_DATE As Date = Nothing
                If OUT_xRow.Item("VOUCHER_STATUS").ToString = "確認" Then
                    '覆核日
                    OUT_CHANGE_DATE = Convert.ToDateTime(OUT_xRow.Item("VOUCHER_TIME")).ToString("yyyy/MM/dd")
                ElseIf OUT_xRow.Item("VOUCHER_STATUS").ToString = "生效" Or OUT_xRow.Item("VOUCHER_STATUS").ToString = "作廢" Then
                    '異動日期
                    OUT_CHANGE_DATE = Convert.ToDateTime(OUT_xRow.Item("ALTER_TIME")).ToString("yyyy/MM/dd")
                End If

                '將明細寫入[D_LEND_DEPOT_STOCKS_DIFF]，然後再統一計算庫存異動數量
                'sSql = "SELECT * FROM [D_LEND_STOCK_OUT_DETAIL] WITH (NOLOCK) WHERE [STOCK_OUT_ID]='" & STOCK_OUT_ID & "'"
                'ddt = pf.GetDataTable(sSql)
                OUT_ddt = pf.GetDB("D_LEND_STOCK_OUT_DETAIL,NOLOCK", {{"STOCK_OUT_ID", STOCK_OUT_ID}})
                Dim OUT_table As String = "D_LEND_DEPOT_STOCKS_DIFF"
                For j As Integer = 0 To OUT_ddt.Rows.Count - 1
                    Dim OUT_yRow As DataRow
                    OUT_yRow = OUT_ddt.Rows(j)
                    Dim total_qty As Double = 0
                    Dim OUT_GOOd_QTY As Double = 0
                    Dim OUT_BAD_QTY As Double = 0
                    If pf.CheckIfDBNull(OUT_yRow.Item("STOCK_CHECK")) = 1 Then
                        '出庫確認
                        OUT_GOOd_QTY = (Convert.ToDouble(OUT_yRow.Item("QTY")) + Convert.ToDouble(OUT_yRow.Item("PRESENT_QTY"))) * (-1)
                        OUT_BAD_QTY = Convert.ToDouble(OUT_yRow.Item("BAD_QTY") * (-1))

                    ElseIf pf.CheckIfDBNull(OUT_yRow.Item("STOCK_CHECK")) = 0 Then
                        '取消出庫確認
                        OUT_GOOd_QTY = (Convert.ToDouble(OUT_yRow.Item("QTY")) + Convert.ToDouble(OUT_yRow.Item("PRESENT_QTY")))
                        OUT_BAD_QTY = Convert.ToDouble(OUT_yRow.Item("BAD_QTY"))
                    End If
                    If OUT_xRow.Item("STOCK_TYPE").ToString = "公司" Then
                        Dim col_value(,) As String = {
                            {"PAPER_ID", pf.CheckStringDBNull(OUT_yRow.Item("STOCK_OUT_ID"))},
                            {"EFFECTIVE_DATE", OUT_xRow.Item("EFFECTIVE_DATE")},
                            {"CHANGE_DATE", OUT_CHANGE_DATE},
                            {"DEPOT_ID", pf.CheckStringDBNull(OUT_yRow.Item("DEPOT_ID"))},
                            {"PRODUCT_ID", pf.CheckStringDBNull(OUT_yRow.Item("PRODUCT_ID"))},
                            {"BATCH_NUM", pf.CheckStringDBNull(OUT_yRow.Item("BATCH_NUM"))},
                            {"G_DIFF_QTY", OUT_GOOd_QTY},
                            {"B_DIFF_QTY", OUT_BAD_QTY},
                            {"C_DIFF_QTY", 0},
                            {"CUS_DIFF_QTY", 0},
                            {"SUPPLIER_DIFF_QTY", 0},
                            {"VOUCHER_STATUS", pf.CheckStringDBNull(OUT_xRow.Item("VOUCHER_STATUS"))},
                            {"CUSTOMER_ID", pf.CheckStringDBNull(OUT_xRow.Item("CUSTOMER_ID"))},
                            {"SUPPLIER_ID", pf.CheckStringDBNull(OUT_xRow.Item("SUPPLIER_ID"))},
                            {"CREATE_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                        }
                        sSql = pf.GetInsertquery(OUT_table, col_value)
                    End If
                    Console.WriteLine(sSql)
                    command.CommandText = sSql
                    iAffectRows = command.ExecuteNonQuery()
                    If iAffectRows = 0 Then
                        Return False
                    End If
                Next
            End If


            '入庫單
            If STOCK_IN_ID <> "" Then
                IN_mdt = pf.GetDB("D_LEND_STOCK_IN_MASTER,NOLOCK", {{"STOCK_IN_ID", STOCK_IN_ID}})
                Dim IN_xRow As DataRow
                IN_xRow = IN_mdt.Rows(0)
                '生效日
                Dim IN_EFFECTIVE_DATE As Date = Convert.ToDateTime(IN_xRow.Item("EFFECTIVE_DATE")).ToString("yyyy/MM/dd")
                '覆核日
                Dim IN_CHANGE_DATE As Date = Nothing
                If IN_xRow.Item("VOUCHER_STATUS").ToString = "確認" Then
                    '覆核日
                    IN_CHANGE_DATE = Convert.ToDateTime(IN_xRow.Item("VOUCHER_TIME")).ToString("yyyy/MM/dd")
                ElseIf IN_xRow.Item("VOUCHER_STATUS").ToString = "生效" Or IN_xRow.Item("VOUCHER_STATUS").ToString = "作廢" Then
                    '異動日期
                    IN_CHANGE_DATE = Convert.ToDateTime(IN_xRow.Item("ALTER_TIME")).ToString("yyyy/MM/dd")
                End If

                '將明細寫入[D_DEPOT_STOCKS_DIFF]，然後再統一計算庫存異動數量
                'sSql = "SELECT * FROM [D_LEND_STOCK_IN_DETAIL] WITH (NOLOCK) WHERE [STOCK_IN_ID]='" & STOCK_IN_ID & "'"
                'ddt = pf.GetDataTable(sSql)
                IN_ddt = pf.GetDB("D_LEND_STOCK_IN_DETAIL,NOLOCK", {{"STOCK_IN_ID", STOCK_IN_ID}})
                Dim IN_table As String = "D_LEND_DEPOT_STOCKS_DIFF"
                For j As Integer = 0 To IN_ddt.Rows.Count - 1
                    Dim IN_yRow As DataRow
                    IN_yRow = IN_ddt.Rows(j)
                    Dim total_qty As Double = 0
                    '良品
                    Dim IN_GOOD_QTY As Double = 0
                    '破損品
                    Dim IN_BAD_QTY As Double = 0
                    If pf.CheckIfDBNull(IN_yRow.Item("STOCK_CHECK")) = 1 Then
                        '入庫確認
                        total_qty = (Convert.ToDouble(IN_yRow.Item("QTY")) + Convert.ToDouble(IN_yRow.Item("PRESENT_QTY")))
                        IN_GOOD_QTY = (Convert.ToDouble(IN_yRow.Item("QTY")) + Convert.ToDouble(IN_yRow.Item("PRESENT_QTY")))
                        IN_BAD_QTY = Convert.ToDouble(IN_yRow.Item("BAD_QTY"))
                    ElseIf pf.CheckIfDBNull(IN_yRow.Item("STOCK_CHECK")) = 0 Then
                        '取消入庫確認
                        total_qty = (Convert.ToDouble(IN_yRow.Item("QTY")) + Convert.ToDouble(IN_yRow.Item("PRESENT_QTY"))) * (-1)
                        IN_GOOD_QTY = (Convert.ToDouble(IN_yRow.Item("QTY")) + Convert.ToDouble(IN_yRow.Item("PRESENT_QTY"))) * (-1)
                        IN_BAD_QTY = Convert.ToDouble(IN_yRow.Item("BAD_QTY")) * (-1)
                    End If
                    If IN_xRow.Item("STOCK_TYPE").ToString = "公司" Then
                        Dim col_value(,) As String = {
                            {"PAPER_ID", pf.CheckStringDBNull(IN_yRow.Item("STOCK_IN_ID"))},
                            {"EFFECTIVE_DATE", IN_xRow.Item("EFFECTIVE_DATE")},
                            {"CHANGE_DATE", IN_CHANGE_DATE},
                            {"DEPOT_ID", pf.CheckStringDBNull(IN_yRow.Item("DEPOT_ID"))},
                            {"PRODUCT_ID", pf.CheckStringDBNull(IN_yRow.Item("PRODUCT_ID"))},
                            {"BATCH_NUM", pf.CheckStringDBNull(IN_yRow.Item("BATCH_NUM"))},
                            {"G_DIFF_QTY", IN_GOOD_QTY},
                            {"B_DIFF_QTY", IN_BAD_QTY},
                            {"C_DIFF_QTY", 0},
                            {"CUS_DIFF_QTY", 0},
                            {"SUPPLIER_DIFF_QTY", 0},
                            {"VOUCHER_STATUS", pf.CheckStringDBNull(IN_xRow.Item("VOUCHER_STATUS"))},
                            {"CUSTOMER_ID", pf.CheckStringDBNull(IN_xRow.Item("CUSTOMER_ID"))},
                            {"SUPPLIER_ID", pf.CheckStringDBNull(IN_xRow.Item("SUPPLIER_ID"))},
                            {"CREATE_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                        }
                        sSql = pf.GetInsertquery(IN_table, col_value)
                    End If
                    Console.WriteLine(sSql)
                    command.CommandText = sSql
                    iAffectRows = command.ExecuteNonQuery()
                    If iAffectRows = 0 Then
                        Return False
                    End If
                Next
            End If

            '開始滾動計算每日庫存檔
            Dim CHECK_DATE As DateTime = SCROLL_DATE
            Dim END_DATE As DateTime = DateTime.Now
            Dim check_days As Integer = 0
            Do While CHECK_DATE.ToString("yyyy/MM/dd") < END_DATE.ToString("yyyy/MM/dd")
                If UPDATE_D_LEND_DEPOT_STOCKS_DAYS(CHECK_DATE, command) = False Then
                    Return False
                End If
                check_days = check_days + 1
                If check_days > 0 Then
                    CHECK_DATE = SCROLL_DATE.AddDays(check_days)
                End If
            Loop
        Catch ex As Exception
            pf.MyError(ex.ToString)
            Return False
        End Try

        Return True

    End Function

    '2020.11.24加入倉庫代號、產品代號、批號
    '計算D_LEND_DEPOT_STOCKS_DAYS的數量
    'LEND_CHECK_DATE：每日庫存的日期
    Public Function UPDATE_D_LEND_DEPOT_STOCKS_DAYS_BY_DPB(LEND_CHECK_DATE As Date, DEPOT_ID As String, PRODUCT_ID As String, BATCH_NUM As String, VALID_PERIOD As Date, sqlCommand As SqlCommand) As Boolean
        Dim i As Integer = 0
        Dim iRowAffected As Integer = 0
        Dim table As String = "D_LEND_DEPOT_STOCKS_DAYS"
        Dim xRow As DataRow
        Dim SaveOK As Boolean = True
        Dim sSql As String = ""
        Dim sSql2 As String = ""
        Dim sSql3 As String = ""
        Dim sSql4 As String = ""
        Try
            '產生庫存每日檔資料
            sSql = ""
            sSql = "SELECT * FROM [D_LEND_DEPOT_STOCKS_DAYS] WITH (NOLOCK)"
            sSql &= " WHERE Convert(varchar, [EFFECTIVE_DATE], 111) ='" & LEND_CHECK_DATE.AddDays(-1).ToString("yyyy/MM/dd") & "'"
            sSql &= " AND [DEPOT_ID]='" & DEPOT_ID & "'"
            sSql &= " AND [PRODUCT_ID]='" & PRODUCT_ID & "'"
            sSql &= " AND [BATCH_NUM]='" & BATCH_NUM & "'"
            sSql &= " ORDER BY [DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
            Console.WriteLine(sSql)
            Dim qdt2 As New DataTable
            qdt2 = pf.GetDataTable(sSql)
            If qdt2.Rows.Count = 0 Then
                '產生每日庫存記錄
                Dim col_value_insert(,) As String = {
                    {"DEPOT_ID", DEPOT_ID},
                    {"PRODUCT_ID", PRODUCT_ID},
                    {"BATCH_NUM", BATCH_NUM},
                    {"EFFECTIVE_DATE", LEND_CHECK_DATE.AddDays(-1).ToString("yyyy/MM/dd")},
                    {"VALID_PERIOD", VALID_PERIOD.ToString("yyyy/MM/dd")},
                    {"GOOD_QTY", 0},
                    {"BAD_QTY", 0},
                    {"CUS_CON_QTY", 0},
                    {"COST_PRICE", 0},
                    {"UNIT_PRICE", 0},
                    {"INV_QTY", 0},
                    {"INV_BAD_QTY", 0},
                    {"DEPOT_QTY", 0},
                    {"DEPOT_BAD_QTY", 0},
                    {"SALE_QTY", 0}
                }
                sSql2 = pf.GetInsertquery("D_LEND_DEPOT_STOCKS_DAYS", col_value_insert)
                Console.WriteLine(sSql2)
                sqlCommand.CommandText = sSql2
                iRowAffected = sqlCommand.ExecuteNonQuery()
                If iRowAffected = 0 Then
                    pf.MyError("新增每日庫存檔日期[" & LEND_CHECK_DATE.AddDays(-1).ToString("yyyy/MM/dd") & "]的資料發生錯誤!")
                    Return False
                End If
                '再次重新查詢
                qdt2 = pf.GetDataTable(sSql)
            End If
            If qdt2.Rows.Count = 0 Then
                'pf.MyError("每日庫存檔無[" & LEND_CHECK_DATE.AddDays(-1).ToString("yyyy/MM/dd") & "]的資料，請洽系統管理員!")
                'Return False
                '取得有效日期

                '產生每日庫存記錄
                Dim col_value_insert(,) As String = {
                    {"DEPOT_ID", DEPOT_ID},
                    {"PRODUCT_ID", PRODUCT_ID},
                    {"BATCH_NUM", BATCH_NUM},
                    {"EFFECTIVE_DATE", LEND_CHECK_DATE.AddDays(-1).ToString("yyyy/MM/dd")},
                    {"VALID_PERIOD", VALID_PERIOD.ToString("yyyy/MM/dd")},
                    {"GOOD_QTY", 0},
                    {"BAD_QTY", 0},
                    {"CUS_CON_QTY", 0},
                    {"COST_PRICE", 0},
                    {"UNIT_PRICE", 0},
                    {"INV_QTY", 0},
                    {"INV_BAD_QTY", 0},
                    {"DEPOT_QTY", 0},
                    {"DEPOT_BAD_QTY", 0},
                    {"SALE_QTY", 0}
                }
                sSql2 = pf.GetInsertquery("D_LEND_DEPOT_STOCKS_DAYS", col_value_insert)
                Console.WriteLine(sSql2)
                sqlCommand.CommandText = sSql2
                iRowAffected = sqlCommand.ExecuteNonQuery()
                If iRowAffected = 0 Then
                    pf.MyError("新增每日庫存檔日期[" & LEND_CHECK_DATE.AddDays(-1).ToString("yyyy/MM/dd") & "]的資料發生錯誤!")
                    Return False
                End If
            Else
                For i = 0 To qdt2.Rows.Count - 1
                    xRow = qdt2.Rows(i)
                    '判斷資料是否存在
                    sSql2 = ""
                    sSql2 = "SELECT * FROM [D_LEND_DEPOT_STOCKS_DAYS] WITH (NOLOCK)"
                    sSql2 &= " WHERE Convert(varchar, [EFFECTIVE_DATE], 111) ='" & LEND_CHECK_DATE.ToString("yyyy/MM/dd") & "'"
                    sSql2 &= " AND [DEPOT_ID]='" & xRow.Item("DEPOT_ID").ToString & "'"
                    sSql2 &= " AND [PRODUCT_ID]='" & xRow.Item("PRODUCT_ID").ToString & "'"
                    sSql2 &= " AND [BATCH_NUM]='" & xRow.Item("BATCH_NUM").ToString & "'"
                    Dim chk_data As New DataTable
                    Console.WriteLine(sSql2)
                    chk_data = pf.GetDataTable(sSql2)
                    If chk_data.Rows.Count = 0 Then
                        Dim col_value(,) As String = {
                           {"DEPOT_ID", pf.CheckStringDBNull(xRow.Item("DEPOT_ID"))},
                           {"PRODUCT_ID", pf.CheckStringDBNull(xRow.Item("PRODUCT_ID"))},
                           {"BATCH_NUM", pf.CheckStringDBNull(xRow.Item("BATCH_NUM"))},
                           {"EFFECTIVE_DATE", pf.CheckDateTimeNull(LEND_CHECK_DATE.ToString("yyyy/MM/dd"))},
                           {"GOOD_QTY", pf.CheckNull(xRow.Item("GOOD_QTY"))},
                           {"BAD_QTY", pf.CheckNull(xRow.Item("BAD_QTY"))},
                           {"CUS_CON_QTY", pf.CheckNull(xRow.Item("CUS_CON_QTY"))},
                           {"COST_PRICE", pf.CheckNull(xRow.Item("COST_PRICE"))},
                           {"UNIT_PRICE", pf.CheckNull(xRow.Item("UNIT_PRICE"))},
                           {"VALID_PERIOD", pf.CheckDateTimeNull(xRow.Item("VALID_PERIOD"))},
                           {"RED_SHOW", xRow.Item("RED_SHOW")},
                           {"INV_QTY", pf.CheckNull(xRow.Item("INV_QTY"))},
                           {"INV_BAD_QTY", pf.CheckNull(xRow.Item("INV_BAD_QTY"))},
                           {"DEPOT_QTY", pf.CheckNull(xRow.Item("DEPOT_QTY"))},
                           {"DEPOT_BAD_QTY", pf.CheckNull(xRow.Item("DEPOT_BAD_QTY"))},
                           {"SALE_QTY", pf.CheckNull(xRow.Item("SALE_QTY"))},
                           {"CREATE_USER", "系統"},
                           {"CREATE_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                        }
                        sSql2 = pf.GetInsertquery(table, col_value)
                        Console.WriteLine(sSql2)
                        sqlCommand.CommandText = sSql2
                        iRowAffected = sqlCommand.ExecuteNonQuery()
                        If iRowAffected = 0 Then
                            pf.MyError("新增每日庫存檔日期[" & LEND_CHECK_DATE.ToString("yyyy/MM/dd") & "]的資料發生錯誤!")
                            Return False
                        End If
                    End If

                    '開始計算庫存數
                    '計算異動庫存數Good_QTY-依照生效日
                    sSql3 = ""
                    Dim New_Good_Qty As Double = 0
                    Dim New_Inv_Qty As Double = 0
                    Dim New_Inv_Bad_Qty As Double = 0
                    sSql3 = "SELECT [DEPOT_ID],[PRODUCT_ID],[BATCH_NUM],SUM(ISNULL([G_DIFF_QTY],0)) as Qty,SUM(ISNULL([B_DIFF_QTY],0)) as Bad_Qty "
                    sSql3 &= " FROM [D_LEND_DEPOT_STOCKS_DIFF] WITH (NOLOCK) WHERE CONVERT(varchar,[EFFECTIVE_DATE],111)='" & LEND_CHECK_DATE.ToString("yyyy/MM/dd") & "'"
                    sSql3 &= " AND [DEPOT_ID]='" & xRow.Item("DEPOT_ID").ToString & "'"
                    sSql3 &= " AND [PRODUCT_ID]='" & xRow.Item("PRODUCT_ID").ToString & "'"
                    sSql3 &= " AND [BATCH_NUM]='" & xRow.Item("BATCH_NUM").ToString & "'"
                    sSql3 &= " GROUP BY [DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                    Console.WriteLine(sSql3)
                    Dim qdt3 As New DataTable
                    qdt3 = pf.GetDataTable(sSql3)
                    If qdt3.Rows.Count > 0 Then
                        New_Good_Qty = Convert.ToDouble(xRow.Item("GOOD_QTY")) + Convert.ToDouble(qdt3.Rows(0).Item("Qty"))
                        New_Inv_Qty = Convert.ToDouble(xRow.Item("INV_QTY")) + Convert.ToDouble(qdt3.Rows(0).Item("Qty"))
                        New_Inv_Bad_Qty = Convert.ToDouble(xRow.Item("INV_BAD_QTY")) + Convert.ToDouble(qdt3.Rows(0).Item("Bad_Qty"))
                        Dim PK2() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM", "EFFECTIVE_DATE"}
                        Dim col_value2(,) As String = {
                            {"DEPOT_ID", xRow.Item("DEPOT_ID")},
                            {"PRODUCT_ID", xRow.Item("PRODUCT_ID")},
                            {"BATCH_NUM", xRow.Item("BATCH_NUM")},
                            {"EFFECTIVE_DATE", LEND_CHECK_DATE},
                            {"GOOD_QTY", New_Good_Qty},
                            {"INV_QTY", New_Inv_Qty},
                            {"INV_BAD_QTY", New_Inv_Bad_Qty},
                            {"ALTER_USER", "系統"},
                            {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                        }
                        sSql3 = pf.GetUpdatequery(table, PK2, col_value2)
                        Console.WriteLine(sSql3)
                        sqlCommand.CommandText = sSql3
                        iRowAffected = sqlCommand.ExecuteNonQuery()
                        If iRowAffected = 0 Then
                            pf.MyError("1-異動每日庫存檔日期[" & LEND_CHECK_DATE.ToString("yyyy/MM/dd") & "]的資料發生錯誤!")
                            Return False
                        End If
                    Else
                        New_Good_Qty = Convert.ToDouble(xRow.Item("GOOD_QTY"))
                        New_Inv_Qty = Convert.ToDouble(xRow.Item("INV_QTY"))
                        New_Inv_Bad_Qty = Convert.ToDouble(xRow.Item("INV_BAD_QTY"))
                        Dim PK2() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM", "EFFECTIVE_DATE"}
                        Dim col_value2(,) As String = {
                            {"DEPOT_ID", xRow.Item("DEPOT_ID")},
                            {"PRODUCT_ID", xRow.Item("PRODUCT_ID")},
                            {"BATCH_NUM", xRow.Item("BATCH_NUM")},
                            {"EFFECTIVE_DATE", LEND_CHECK_DATE},
                            {"GOOD_QTY", New_Good_Qty},
                            {"INV_QTY", New_Inv_Qty},
                            {"INV_BAD_QTY", New_Inv_Bad_Qty},
                            {"ALTER_USER", "系統"},
                            {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                        }
                        sSql3 = pf.GetUpdatequery(table, PK2, col_value2)
                        Console.WriteLine(sSql3)
                        sqlCommand.CommandText = sSql3
                        iRowAffected = sqlCommand.ExecuteNonQuery()
                        If iRowAffected = 0 Then
                            pf.MyError("1-異動每日庫存檔日期[" & LEND_CHECK_DATE.ToString("yyyy/MM/dd") & "]的資料發生錯誤!")
                            Return False
                        End If
                    End If

                    '計算2-依照覆核日
                    ''計算異動庫存數SALE_QTY-依照覆核日
                    sSql4 = ""
                    Dim New_depot_Qty As Double = 0
                    Dim New_depot_bad_Qty As Double = 0
                    Dim New_Sale_Qty As Double = 0
                    sSql4 = "SELECT [DEPOT_ID],[PRODUCT_ID],[BATCH_NUM],SUM(ISNULL([G_DIFF_QTY],0)) as Qty,SUM(ISNULL([B_DIFF_QTY],0)) as Bad_Qty "
                    sSql4 &= " FROM [D_LEND_DEPOT_STOCKS_DIFF] WITH (NOLOCK) WHERE CONVERT(varchar,[CHANGE_DATE],111)='" & LEND_CHECK_DATE.ToString("yyyy/MM/dd") & "'"
                    sSql4 &= " AND [DEPOT_ID]='" & xRow.Item("DEPOT_ID").ToString & "'"
                    sSql4 &= " AND [PRODUCT_ID]='" & xRow.Item("PRODUCT_ID").ToString & "'"
                    sSql4 &= " AND [BATCH_NUM]='" & xRow.Item("BATCH_NUM").ToString & "'"
                    sSql4 &= " GROUP BY [DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                    Console.WriteLine(sSql4)
                    Dim qdt4 As New DataTable
                    qdt4 = pf.GetDataTable(sSql4)
                    If qdt4.Rows.Count > 0 AndAlso qdt4.Rows(0).Item("Qty") <> 0 Then
                        New_depot_Qty = Convert.ToDouble(xRow.Item("DEPOT_QTY")) + Convert.ToDouble(qdt4.Rows(0).Item("Qty"))
                        New_depot_bad_Qty = Convert.ToDouble(xRow.Item("DEPOT_BAD_QTY")) + Convert.ToDouble(qdt4.Rows(0).Item("Bad_Qty"))
                        New_Sale_Qty = Convert.ToDouble(xRow.Item("Sale_Qty")) + Convert.ToDouble(qdt4.Rows(0).Item("Qty"))
                        Dim PK2() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM", "EFFECTIVE_DATE"}
                        Dim col_value2(,) As String = {
                            {"DEPOT_ID", xRow.Item("DEPOT_ID")},
                            {"PRODUCT_ID", xRow.Item("PRODUCT_ID")},
                            {"BATCH_NUM", xRow.Item("BATCH_NUM")},
                            {"EFFECTIVE_DATE", LEND_CHECK_DATE},
                            {"DEPOT_QTY", New_depot_Qty},
                            {"DEPOT_BAD_QTY", New_depot_bad_Qty},
                            {"SALE_QTY", New_Sale_Qty},
                            {"ALTER_USER", "系統"},
                            {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                        }
                        sSql4 = pf.GetUpdatequery(table, PK2, col_value2)
                        Console.WriteLine(sSql4)
                        sqlCommand.CommandText = sSql4
                        iRowAffected = sqlCommand.ExecuteNonQuery()
                        If iRowAffected = 0 Then
                            pf.MyError("2-異動每日庫存檔日期[" & LEND_CHECK_DATE.ToString("yyyy/MM/dd") & "]的資料發生錯誤!")
                            Return False
                        End If
                    Else
                        New_depot_Qty = Convert.ToDouble(xRow.Item("DEPOT_QTY"))
                        New_depot_bad_Qty = Convert.ToDouble(xRow.Item("DEPOT_BAD_QTY"))
                        New_Sale_Qty = Convert.ToDouble(xRow.Item("Sale_Qty"))
                        Dim PK2() As String = {"DEPOT_ID", "PRODUCT_ID", "BATCH_NUM", "EFFECTIVE_DATE"}
                        Dim col_value2(,) As String = {
                            {"DEPOT_ID", xRow.Item("DEPOT_ID")},
                            {"PRODUCT_ID", xRow.Item("PRODUCT_ID")},
                            {"BATCH_NUM", xRow.Item("BATCH_NUM")},
                            {"EFFECTIVE_DATE", LEND_CHECK_DATE},
                            {"DEPOT_QTY", New_depot_Qty},
                            {"DEPOT_BAD_QTY", New_depot_bad_Qty},
                            {"SALE_QTY", New_Sale_Qty},
                            {"ALTER_USER", "系統"},
                            {"ALTER_TIME", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}
                        }
                        sSql4 = pf.GetUpdatequery(table, PK2, col_value2)
                        Console.WriteLine(sSql4)
                        sqlCommand.CommandText = sSql4
                        iRowAffected = sqlCommand.ExecuteNonQuery()
                        If iRowAffected = 0 Then
                            pf.MyError("2-異動每日庫存檔日期[" & LEND_CHECK_DATE.ToString("yyyy/MM/dd") & "]的資料發生錯誤!")
                            Return False
                        End If
                    End If
                    '更新D_LEND_DEPOT_STOCKS
                    Dim sSql5 As String = ""
                    sSql5 = "SELECT [DEPOT_ID],[PRODUCT_ID],[BATCH_NUM],SUM(ISNULL([G_DIFF_QTY],0)) as Qty "
                    sSql5 &= " FROM [D_LEND_DEPOT_STOCKS] WITH (NOLOCK) WHERE CONVERT(varchar,[CHANGE_DATE],111)='" & LEND_CHECK_DATE.ToString("yyyy/MM/dd") & "'"
                    sSql5 &= " AND [DEPOT_ID]='" & xRow.Item("DEPOT_ID").ToString & "'"
                    sSql5 &= " AND [PRODUCT_ID]='" & xRow.Item("PRODUCT_ID").ToString & "'"
                    sSql5 &= " AND [BATCH_NUM]='" & xRow.Item("BATCH_NUM").ToString & "'"
                    sSql5 &= " GROUP BY [DEPOT_ID],[PRODUCT_ID],[BATCH_NUM]"
                Next
            End If
        Catch ex As Exception
            pf.MyError(ex.ToString)
            Return False
        End Try
        Return True
    End Function


End Class
